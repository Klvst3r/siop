<?php


//Code that will allow us to make the validations of the fields from the form by the post method
/**
 * Function used to validate and clean a field
 * @param input $campo It has to be type field POST
 * @return string
 */

function validate_field($field){

	$field = trim($field);

	$field = stripcslashes($field);

	$field = htmlspecialchars($field);


	return $field;

}


function format_date($field){

	$date = explode('/', $field);
                      	$year = $date[2];
                      	$month = $date[1];
                      	if (strlen($month) == 1){
                      		$month = '0' . $month;
                      	}
                      	$day = $date[0];
                      	if(strlen($day) == 1){
                      		$day = '0' . $day;
                      	}



	$field = $year . '-' . $month . '-' . $day;

	return $field;

}//format_field

function inverse_date($field){
  $date = explode('-',$field);
      $day = $date[2];
      $month = $date[1];
      $year = $date[0];

  $field = $day . '/' . $month . '/' . $year;

  return $field;      

}//inverse_date

function reinverse_date($field){
  $date = explode('/',$field);
      $day = $date[0];
      $month = $date[1];
      $year = $date[2];

  $field = $year . '-' . $month . '-' . $day;

  return $field;      

}//inverse_date
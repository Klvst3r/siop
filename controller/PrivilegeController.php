<?php 
//After generating the entity we create the controller
//we import UserDao to use it in the defined class
include '../data/PrivilegeDAO.php';

class PrivilegeController {

	public function getPrivilege($privilege){

		$obj_priv = new Privilege();
		
		$obj_priv->setDesc_priv($privilege);

		//We send the object from PrivilegeDAO
		return PrivilegeDAO::getPrivilege($obj_priv);
	}


	public function regPrivilege($desc_priv, $create_priv, $select_priv, $insert_priv, $update_priv, $delete_priv, $backup_priv, $report_priv){

		$obj_priv = new Privilege();
		
		$obj_priv->setDesc_priv($desc_priv);
		$obj_priv->setCreate_priv($create_priv);
		$obj_priv->setSelect_priv($select_priv);
		$obj_priv->setInsert_priv($insert_priv);
		$obj_priv->setUpdate_priv($update_priv);
		$obj_priv->setDelete_priv($delete_priv);
		$obj_priv->setBackup_priv($backup_priv);
		$obj_priv->setReport_priv($report_priv);
		
		return PrivilegeDAO::regPrivilege($obj_priv);
	}

	public function updatePrivilege($id_priv, $desc_priv, $create_priv, $select_priv, $insert_priv, $update_priv, $delete_priv, $backup_priv, $report_priv){

		$obj_priv = new Privilege();
		
		$obj_priv->setId_priv($id_priv);
		$obj_priv->setDesc_priv($desc_priv);
		$obj_priv->setCreate_priv($create_priv);
		$obj_priv->setSelect_priv($select_priv);
		$obj_priv->setInsert_priv($insert_priv);
		$obj_priv->setUpdate_priv($update_priv);
		$obj_priv->setDelete_priv($delete_priv);
		$obj_priv->setBackup_priv($backup_priv);
		$obj_priv->setReport_priv($report_priv);
		
		return PrivilegeDAO::updatePrivilege($obj_priv);
	} //method updatePrivilege

	public function delPrivilege($id_priv){

		$obj_priv = new Privilege();
		
		$obj_priv->setId_priv($id_priv);
		
		return PrivilegeDAO::delPrivilege($obj_priv);
	}//delPrivilege




} //Class PrivilegeController	

?>
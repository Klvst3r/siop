<?php
include '../data/OrgDAO.php';

class OrgController {

	public function getOrg(){

		$obj_org = new Organization();
		
		$obj_org->setId_org($id);

		return OrgDAO::getOrg();

	}//getConfig

	public function updateOrg($id_org, $org_nick, $org_name, $org_dir, $org_pob, $org_cp, $org_tel, $org_ext, $org_email){

		$obj_org = new Organization();

		$obj_org->setId_org($id_org);
		$obj_org->setOrg_nick($org_nick);
		$obj_org->setOrg_name($org_name);
		$obj_org->setOrg_dir($org_dir);
		$obj_org->setOrg_pob($org_pob);
		$obj_org->setOrg_cp($org_cp);
		$obj_org->setOrg_tel($org_tel);
		$obj_org->setOrg_ext($org_ext);
		$obj_org->setOrg_email($org_email);

		return ConfigDAO::updateOrg($obj_org);

	}//updateOrg

}//Class ConfigController

?>
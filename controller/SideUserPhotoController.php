<?php
//After generating the entity we create the controller
//we import UserDao to use it in the defined class
include '../data/SideUserDAO.php';

class SideUserPhotoController {

		public function getSidePhoto($id_user){

		$obj_user = new SideUser();

		$obj_user->setId_user($id_user);

		return SideUserDAO::getSidePhoto($obj_user);

	}//method getProfile


} //Class SideUserPhotoController
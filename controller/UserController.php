<?php
//After generating the entity we create the controller
//we import UserDao to use it in the defined class
include '../data/UserDAO.php';
class UserController {
	//defining the fncion with two parameters, packages the user and password in an object called entity user
	// and will send it to UserID to validate it
	public static function login($user_name, $password) {
		//It is instantiated and not imported because it is already added from the entity
		$obj_user = new User();
		//We send the object to the user from the application
		$obj_user->setUser_name($user_name);
		$obj_user->setUser_pass($password);
		
		//The class we send by the method the parameters of the object (user and password) as parameters of the method
		//returning the result of the operation
		return UserDAO::login($obj_user);
	} //Method Login

	//To init the Session of User need obtain data of user
	// We get the data from the database
	public function getUser($user_name, $user_password){

		$obj_user = new User();

		$obj_user->setUser_name($user_name);
		$obj_user->setUser_pass($user_password);
		//We send the object from UserDAO
		return UserDAO::getUser($obj_user);

	}//method getUser


	public function getProfile($id_user){

		$obj_user = new User();

		$obj_user->setId_user($id_user);

		return UserDAO::getProfile($obj_user);

	}//method getProfile

	public function getInSystem($date_begin, $date_end){
		$obj_user = new User();

		$obj_user->setDate_begin($date_begin);
		$obj_user->setDate_end($date_end);

		return UserDAO::getInSystem($obj_user);
	}

	public function getSelectProfile($id_user){
		$obj_user = new User();

		$obj_user->setId_user($id_user);

		return UserDAO::getSelectProfile($obj_user);

	}//method getSelectProfile

	public function updateProfile($id_user, $privilege, $status, $direction, $departament, $position){
		$obj_user = new User();

		$obj_user->setId_user($id_user);
		$obj_user->setId_priv($privilege);
		$obj_user->setId_user_status($status);
		$obj_user->setId_dir($direction);
		$obj_user->setId_dep($departament);
		$obj_user->setUser_position($position);

		return UserDAO::updateProfile($obj_user);
	}// updateProfile

	public function updateGenerals($id_user, $name, $user_name, $user_resume, $user_movil, $user_email, $user_tel, $user_ext){
		$obj_user = new User();

		$obj_user->setId_user($id_user);
		$obj_user->setName($name);
		$obj_user->setUser_name($user_name);
		$obj_user->setUser_resume($user_resume);
		$obj_user->setUser_movil($user_movil);
		$obj_user->setUser_email($user_email);
		$obj_user->setUser_tel($user_tel);
		$obj_user->setUser_ext($user_ext);

		return UserDAO::updateGenerals($obj_user);

	} //UpdateGeneral

	public function updateAbout($id_user, $about){
		$obj_user = new User();

		$obj_user->setId_user($id_user);
		$obj_user->setAbout($about);

		return UserDAO::updateAbout($obj_user);

	}//updateAbout

	public function changePass($id_user, $last_pass, $new_pass, $rewrite_pass){
		$obj_user = new User();

		$obj_user->setId_user($id_user);
		$obj_user->setUser_pass($last_pass);
		$obj_user->setNew_pass($new_pass);
		$obj_user->setRewrite_pass($rewrite_pass);

		return UserDAO::changePass($obj_user);

	}//changePass

	public function updatePass($id_user, $new_pass){
		$obj_user = new User();

		$obj_user->setId_user($id_user);
		$obj_user->setNew_pass($new_pass);

		return UserDAO::changePass($obj_user);

	}//changePass


	public function regUser($id_priv, $id_status, $id_dir, $id_dep, $position){
		$obj_user = new User();

		$obj_user->setId_priv($id_priv);
		$obj_user->setId_user_status($id_status);
		$obj_user->setId_dir($id_dir);
		$obj_user->setId_dep($id_dep);
		$obj_user->setUser_position($position);

		return UserDAO::regUser($obj_user);
	} //regUser


	public function getLastUser(){

		return UserDAO::getLastUser();

	}//lastUserProfile

	public function getSelects($id_last_user){
		$obj_user = new User();

		$obj_user->setLastUser($id_last_user);

		return UserDAO::getSelects($obj_user);

	}//getSelects

	public function getGenerals($id_user){
		$obj_user = new User();

		$obj_user->setId_user($id_user);

		return UserDAO::getGenerals($obj_user);

	}//getGenerals

	public function getAbout($id_user){
		$obj_user = new User();

		$obj_user->setId_user($id_user);

		return UserDAO::getAbout($obj_user);

	}//getAbout

	public function getPhoto($id_user){
		$obj_user = new User();

		$obj_user->setId_user($id_user);

		return UserDAO::getPhoto($obj_user);

	}//getPhoto

	public function regPass($id_user, $new_pass){
		$obj_user =  new User();

		$obj_user->setId_user($id_user);
		$obj_user->setNew_pass($new_pass);

		return UserDAO::regPass($obj_user);
	}//regPass

	public function delUser($id_user){
		$obj_user = new User();

		$obj_user->setId_user($id_user);

		return UserDAO::delUser($obj_user);
	}//delUser

	public function getUserActive($active){
		$obj_user = new User();

		$obj_user->setId_active($active);

		return UserDAO::getUserActive($obj_user);

	}//getAdminInfo

	public function getUserDeleted($deleted){
		$obj_user = new User();

		$obj_user->setId_deleted($deleted);

		return UserDAO::getUserDeleted($obj_user);

	}//getUserDeleted




	


}//Class UserController
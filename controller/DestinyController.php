<?php

include '../data/DestinyDAO.php';

class DestinyController extends Connect {
	
	public static function getDestiny($privilege){

		$obj_dest = new Destiny();

		$obj_dest->setId_destiny($privilege);

		return DestinyDAO::getDestiny($obj_dest);

	}//getDestiny

	public static function cleanDiv($value){


		//echo "Valor: " . $value;
		$obj_dest = new Destiny();

		$obj_dest->setId_destiny($value);
		

		return DestinyDAO::cleanDiv($obj_dest);

	}//cleanDiv

}//Class
?>
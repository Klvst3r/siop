<?php 
//After generating the entity we create the controller
//we import UserDao to use it in the defined class
include '../data/DepDAO.php';

class DepController {

	public function getDep($dep){

		$obj_dep = new Departament();
		
		$obj_dep->setDepartament($dep);

		//We send the object from DirDAO
		return DepDAO::getDep($obj_dir);
	} //method getDir


	public function regDep($departament){

		$obj_dep = new Departament();
		
		$obj_dep->setDepartament($departament);
		
		return DepDAO::regDep($obj_dep);
	}

	public function updateDep($id_dep, $departament){

		$obj_dep = new Departament();
		
		$obj_dep->setId_dep($id_dep);
		$obj_dep->setDepartament($departament);

		
		return DepDAO::updateDep($obj_dep);
	} //method updateDep

	
	public function delDep($id_dep){

		$obj_dep = new Departament();
		
		$obj_dep->setId_dep($id_dep);
		
		return DepDAO::delDep($obj_dep);


	}//delDep




} //Class PrivilegeController	

?>
<?php

include '../data/DocumentDAO.php';

class DocumentController {
	public function regDocument($id_destiny, $id_muni, $id_type_doc, $id_status_doc, $id_classif, $subject_doc, $desc_send_position, $folio_doc, $send_doc, $origin_doc, $dir_sender, $movil_doc, $tel_doc, $ext_doc, $email_doc, $date_doc, $date_recep, $reference){
	
		$obj_doc = new Document();

		$obj_doc->setId_destiny($id_destiny);
		$obj_doc->setId_muni($id_muni);
		$obj_doc->setId_type_doc($id_type_doc);
		$obj_doc->setId_status_doc($id_status_doc);
		$obj_doc->setId_classif($id_classif);
		$obj_doc->setSubject_doc($subject_doc);
		$obj_doc->setDesc_send_position($desc_send_position);
		$obj_doc->setFolio_doc($folio_doc);
		$obj_doc->setSend_doc($send_doc);
		$obj_doc->setOrigin_doc($origin_doc);
		$obj_doc->setDir_sender($dir_sender);
		$obj_doc->setMovil_doc($movil_doc);
		$obj_doc->setTel_doc($tel_doc);
		$obj_doc->setExt_doc($ext_doc);
		$obj_doc->setEmail_doc($email_doc);
		$obj_doc->setDate_doc($date_doc);
		$obj_doc->setDate_recep($date_recep);
		$obj_doc->setReference($reference);

		return DocumentDAO::regDocument($obj_doc);
	} //function regDocument

	public function detailsDoc($id_doc){

		$obj_doc = new Document();

		$obj_doc->setId_doc($id_doc);

		return DocumentDAO::detailsDoc($obj_doc);

	}//function detailsDoc

	public function getDoc($id_doc){

		$obj_doc = new Document();

		$obj_doc->setId_doc($id_doc);

		return DocumentDAO::getDoc($obj_doc);

	}///getDoc method


	public function updateDoc($id_doc, $id_destiny, $id_muni, $id_type_doc, $id_status_doc, $subject_doc, $desc_send_position, $folio_doc, $send_doc, $origin_doc, $dir_sender, $movil_doc, $tel_doc, $ext_doc, $email_doc, $date_doc, $date_recep, $reference){

		$obj_doc = new Document();

		$obj_doc->setId_doc($id_doc);
		$obj_doc->setId_destiny($id_destiny);
		$obj_doc->setId_muni($id_muni);
		$obj_doc->setId_type_doc($id_type_doc);
		$obj_doc->setId_status_doc($id_status_doc);
		$obj_doc->setSubject_doc($subject_doc);
		$obj_doc->setDesc_send_position($desc_send_position);
		$obj_doc->setFolio_doc($folio_doc);
        $obj_doc->setSend_doc($send_doc);
        $obj_doc->setOrigin_doc($origin_doc);
        $obj_doc->setDir_sender($dir_sender);
        $obj_doc->setMovil_doc($movil_doc);
        $obj_doc->setTel_doc($tel_doc);
        $obj_doc->setExt_doc($ext_doc);
        $obj_doc->setEmail_doc($email_doc);
        $obj_doc->setDate_doc($date_doc);
        $obj_doc->setDate_recep($date_recep);
        $obj_doc->setReference($reference);

        return DocumentDAO::updateDoc($obj_doc);

     }//updateDoc method

     public function delbyFolio($id_doc){
     	$obj_doc = new Document();

		$obj_doc->setId_doc($id_doc);

		return DocumentDAO::delbyFolio($obj_doc);

     }//delbyFolio Method

     public function updateTrack($id_doc, $value){
     	$obj_doc = new Document();

     	$obj_doc->setId_doc($id_doc);
     	$obj_doc->setStatus($value);

     	return DocumentDAO::updateTrack($obj_doc);
     	
     }//updateTrack method


    public function getSelect($id_doc){
    	$obj_doc = new Document();

     	$obj_doc->setId_doc($id_doc);

     	return DocumentDAO::getSelect($obj_doc);

    }//getSelect method


   

}//Class DocumentController
?>
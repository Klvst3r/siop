<?php 
//After generating the entity we create the controller
//we import UserDao to use it in the defined class
include '../data/DirDAO.php';

class DirController {

	public function getDir($dir){

		$obj_dir = new Dir();
		
		$obj_dir->setDir_name($dir);

		//We send the object from DirDAO
		return DirDAO::getDir($obj_dir);
	} //method getDir


	public function regDir($dir_name){

		$obj_dir = new Dir();
		
		$obj_dir->setDir_name($dir_name);
		
		return DirDAO::regDir($obj_dir);
	}

	public function updateDir($id_dir, $dir_name){

		$obj_dir = new Dir();
		
		$obj_dir->setId_dir($id_dir);
		$obj_dir->setDir_name($dir_name);

		
		return DirDAO::updateDir($obj_dir);
	} //method updateDir

	
	public function delDir($id_dir){

		$obj_dir = new Dir();
		
		$obj_dir->setId_dir($id_dir);
		
		return DirDAO::delDir($obj_dir);


	}//delDir




} //Class PrivilegeController	

?>
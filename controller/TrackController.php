<?php

include '../data/TrackDAO.php';

class TrackController {
	 public function regTrack($id_doc, $dir, $status_doc, $departament, $charTramit, $classif, $descrip_doc, $folio_doc, $turned_original, $turned_copy, $date_doc, $instructions){
	
		$obj_track = new Track();

		$obj_track->setId_doc($id_doc);
		$obj_track->setId_dir($dir);
		$obj_track->setId_status_doc($status_doc);
		$obj_track->setId_dep($departament);
		$obj_track->setId_char($charTramit);
		$obj_track->setId_classif($classif);
		$obj_track->setId_desc($descrip_doc);
		$obj_track->setFolio_turned($folio_doc);
		$obj_track->setDoc_origin($turned_original);
		$obj_track->setDoc_cpp($turned_copy);
		$obj_track->setDate_turned($date_doc);
		$obj_track->setInstructions($instructions);
		
		return TrackDAO::regTrack($obj_track);
	} //function regTrack

	public function getTrack($id_doc){

		$obj_track = new Track();

		$obj_track->setId_doc($id_doc);

		return TrackDAO::getTrack($obj_track);

	}///getDoc method



	public function updateTrack($id_turn, $answer, $observation){

		$obj_track = new Track();

		$obj_track -> setId_turn($id_turn);
		$obj_track -> setAnswer($answer);
		$obj_track -> setObservation($observation);

		return TrackDAO::updateTrack($obj_track);

	} //updateTrack method


} //Class TrackController

?>	
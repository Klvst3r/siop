<?php
//After generating the entity we create the controller
//we import UserDao to use it in the defined class
include '../data/HeadUserDAO.php';

class HeadUserPhotoController {

		public function getHeadPhoto($id_user){

		$obj_user = new HeadUser();

		$obj_user->setId_user($id_user);

		return HeadUserDAO::getHeadPhoto($obj_user);

	}//method getProfile


} //Class HeadUserPhotoController
<?php

include 'Connect.php';
include '../model/Organization.php';

class OrgDAO extends Connect {

	protected static $cnx;

	private static function getConection(){

		self::$cnx = Connect::connection();

	}//function getConection

	private static function disconnect(){

		self::$cnx = null;
	}

	public static function getOrg(){

		$query = "SELECT id_org, org_nick, org_name, org_dir, org_pob, org_cp, org_tel, org_tel, org_ext, org_email, id_user FROM organizations";

		self::getConection();

		$result = self::$cnx->prepare($query);

		/*$id_org = $id->getId_org();
		$result->bindParam(":id_org", $id_org);*/

		$result->execute();

		$data = $result->fetch();

			$org = new Organization();			

			$org->setId_org($data["id_org"]);
			$org->setOrg_nick($data["org_nick"]);
			$org->setOrg_name($data["org_name"]);
			$org->setOrg_dir($data["org_dir"]);
			$org->setOrg_pob($data["org_pob"]);
			$org->setOrg_cp($data["org_cp"]);
			$org->setOrg_tel($data["org_tel"]);
			$org->setOrg_ext($data["org_ext"]);
			$org->setOrg_email($data["org_email"]);
			$org->setId_user($data["id_user"]);

			self::disconnect();

	
		return $org;

	}//getNumAccess

	public static function updateOrg($org){
		$query = "UPDATE `organizations` SET `org_nick` = :org_nick, `org_name` = :org_name, `org_dir` = :org_dir, `org_pob` = :org_pob, `org_cp` = :org_cp, `org_tel` = :org_tel, `org_ext` = :org_ext, `org_email` = :org_email WHERE `organizations`.`id_org` = :id_org";

	 	self::getConection();
	
		$result = self::$cnx->prepare($query);

		$id_org = $org->getId_org();
		$result->bindParam(":id_org", $id_org);

		$org_nick = $org->getOrg_nick();
		$result->bindParam(":org_nick", $org_nick);

		$org_name = $org->getOrg_name();
		$result->bindParam(":org_name", $org_name);		

		$org_dir = $org->getOrg_dir();
		$result->bindParam(":org_dir", $org_dir);

		$org_pob = $org->getOrg_pob();
		$result->bindParam(":org_pob", $org_pob);		

		$org_cp = $org->getOrg_cp();
		$result->bindParam(":org_cp", $org_cp);		

		$org_tel = $org->getOrg_tel();
		$result->bindParam(":org_tel", $org_tel);

		$org_ext = $org->getOrg_ext();
		$result->bindParam(":org_ext", $org_ext);	

		$org_email = $org->getOrg_email();
		$result->bindParam(":org_email", $org_email);

		if($result->execute()){
			
			self::disconnect();
			return true;
		}

		return false;

	}//updateOrg



}//Class ConfigDAO

?>
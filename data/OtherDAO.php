<?php 
//Import conection
include 'Reconnect.php';
include '../model/User.php';

class OtherDAO extends Reconnect {
	protected static $cnx;

	
	private static function getConection(){
		
		self::$cnx = Connect::connection();
		
	}


	private static function disconnect(){
		
		self::$cnx = null;
	}

	public static function verifyUser($priv){

		$query = "SELECT id_user, user_name FROM users WHERE id_priv = :id_priv";

		self::getConection();
	
		$result = self::$cnx->prepare($query);

		$id_priv =	$priv->getId_Priv();
		$result->bindParam(":id_priv", $id_priv);

		$result->execute();

		$rows = $result->rowCount();

		if($rows > 0){
			//return true;
			//echo "Busqueda Localizada<br/>";
			echo '<p class="text-danger"><b>Acción cancelada!! </b></p>';

			//$rows = $result->rowCount();

			if($rows == 1){
				echo 'Existe ' . $rows . ' usuario registrado con el privilegio que intenta eliminar.';
				echo '<br/><br/><div class="bg-success "> <center>Redirigiendo...</center></div>';
			}elseif($rows > 1){
				echo 'Existen ' . $rows . ' usuarios registrados con el privilegio que intenta eliminar';
				echo '<br/><br/><div class="bg-success "> <center>Redirigiendo...</center></div>';
			}
			//last 13 --> 14
			echo"<meta HTTP-EQUIV='Refresh' CONTENT='3; URL=action.php?a=22'<head/>";

			//echo "Filas: " . $rows;


			//$found = "true";
			


			return true;
			//return $found;
			//$user = new User();

			
			self::disconnect();

			//echo $user->getId_Ext();
			


			//return $user->setId_Ext($found);



		}else{
			//Controller to delete privilege
            PrivilegeController::delPrivilege($id_priv);                   	   
            //last 13 -> 14
            header("location:action.php?a=22");
		}

		return false;


	}//verifyUser

	public static function verifyUserDir($priv){

		$query = "SELECT id_user, user_name FROM users WHERE id_dir = :id_dir";

		self::getConection();
	
		$result = self::$cnx->prepare($query);

		$id_dir =	$priv->getId_dir();
		$result->bindParam(":id_dir", $id_dir);

		$result->execute();

		$rows = $result->rowCount();

		if($rows > 0){
			//return true;
			//echo "Busqueda Localizada<br/>";
			echo '<p class="text-danger"><b>Acción cancelada!! </b></p>';

			//$rows = $result->rowCount();

			if($rows == 1){
				echo 'Existe ' . $rows . ' usuario registrado perteneciente a la dirección que intenta eliminar.';
				echo '<br/><br/><div class="bg-success "> <center>Redirigiendo...</center></div>';
			}elseif($rows > 1){
				echo 'Existen ' . $rows . ' usuarios registrados pertenecientes a la dirección que intenta eliminar';
				echo '<br/><br/><div class="bg-success "> <center>Redirigiendo...</center></div>';
			}
			//last 13 --> 14
			echo"<meta HTTP-EQUIV='Refresh' CONTENT='3; URL=action.php?a=36'<head/>";

			//echo "Filas: " . $rows;


			//$found = "true";
			


			return true;
			//return $found;
			//$user = new User();

			
			self::disconnect();

			//echo $user->getId_Ext();
			


			//return $user->setId_Ext($found);



		}else{
			//Controller to delete dir
            DirController::delDir($id_dir);                   	   
            header("location:action.php?a=36");
		}

		return false;


	}//verifyUserDir

	public static function verifyUserDep($departament){

		$query = "SELECT id_user, user_name FROM users WHERE id_dep = :id_dep";

		self::getConection();
	
		$result = self::$cnx->prepare($query);

		$id_dep =	$departament->getId_dep();
		$result->bindParam(":id_dep", $id_dep);

		$result->execute();

		$rows = $result->rowCount();

		if($rows > 0){
			//return true;
			//echo "Busqueda Localizada<br/>";
			echo '<p class="text-danger"><b>Acción cancelada!! </b></p>';

			//$rows = $result->rowCount();

			if($rows == 1){
				echo 'Existe ' . $rows . ' usuario registrado perteneciente al departamento que intenta eliminar.';
				echo '<br/><br/><div class="bg-success "> <center>Redirigiendo...</center></div>';
			}elseif($rows > 1){
				echo 'Existen ' . $rows . ' usuarios registrados pertenecientes al departamento que intenta eliminar';
				echo '<br/><br/><div class="bg-success "> <center>Redirigiendo...</center></div>';
			}
			//last 13 --> 14
			echo"<meta HTTP-EQUIV='Refresh' CONTENT='3; URL=action.php?a=41'<head/>";

			//echo "Filas: " . $rows;


			//$found = "true";
			


			return true;
			//return $found;
			//$user = new User();

			
			self::disconnect();

			//echo $user->getId_Ext();
			


			//return $user->setId_Ext($found);



		}else{
			//Controller to delete departament
            DepController::delDep($id_dep);                   	   
            header("location:action.php?a=41");
		}

		return false;


	}//verifyUserDir







}//Class OthoeDAO
?>
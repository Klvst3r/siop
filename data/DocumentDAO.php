<?php

include 'OtherConnect.php';
include '../model/Document.php';

class DocumentDAO extends OtherConnect {
	protected static $cnx;


	private static function getConection(){

		self::$cnx = Connect::connection();

	}


	private static function disconnect(){

		self::$cnx = null;
	}

	public static function regDocument($doc){

		
		$query = "INSERT INTO `documents` (`id_doc`, `id_destiny`, `id_muni`, `id_type_doc`, `id_status_doc`, `id_classif`, `subject_doc`,  
			`desc_send_position`, `folio_doc`, `send_doc`, `origin_doc`, `dir_sender`, `movil_doc`, `tel_doc`, `ext_doc`, 
			`email_doc`, `date_doc`, `date_recep`, `reference`, `status`) VALUES 
			(NULL, :id_destiny, :id_muni, :id_type_doc, :id_status_doc, :id_classif, :subject_doc, 
			:desc_send_position, :folio_doc, :send_doc, :origin_doc, :dir_sender, :movil_doc, :tel_doc, :ext_doc, 
			:email_doc, :date_doc, :date_recep, :reference, '1')
		";

		echo $query;
		
		self::getConection();
	
		$result = self::$cnx->prepare($query);


		$id_destiny = $doc->getId_destiny();
		$result->bindParam(":id_destiny", $id_destiny);

		$id_muni = $doc->getId_muni();
		$result->bindParam(":id_muni", $id_muni);
		
		$id_type_doc = $doc->getId_type_doc();
		$result->bindParam(":id_type_doc", $id_type_doc);

		$id_status_doc = $doc->getId_status_doc();
		$result->bindParam(":id_status_doc", $id_status_doc);

		$id_classif = $doc->getId_classif();
		$result->bindParam(":id_classif", $id_classif);			

		$subject_doc = $doc->getSubject_doc();
		$result->bindParam(":subject_doc", $subject_doc);

		$desc_send_position = $doc->getDesc_send_position();
		$result->bindParam(":desc_send_position", $desc_send_position);

		$folio_doc = $doc->getFolio_doc();
		$result->bindParam(":folio_doc", $folio_doc);
		
		$send_doc = $doc->getSend_doc();
		$result->bindParam(":send_doc", $send_doc);

		$origin_doc = $doc->getOrigin_doc();
		$result->bindParam(":origin_doc", $origin_doc);

		$dir_sender = $doc->getDir_sender();
		$result->bindParam(":dir_sender", $dir_sender);		
		
		$movil_doc = $doc->getMovil_doc();
		$result->bindParam(":movil_doc", $movil_doc);

		$tel_doc = $doc->getTel_doc();
		$result->bindParam(":tel_doc", $tel_doc);		
		
		$ext_doc = $doc->getExt_doc();
		$result->bindParam(":ext_doc", $ext_doc);
		
		$email_doc = $doc->getEmail_doc();
		$result->bindParam(":email_doc", $email_doc);
		
		$date_doc = $doc->getDate_doc();
		$result->bindParam(":date_doc", $date_doc);

		$date_recep = $doc->getDate_recep();
		$result->bindParam(":date_recep", $date_recep);		
		 
		$reference = $doc->getReference();
		$result->bindParam(":reference", $reference);


		if($result->execute()){
			
			self::disconnect();

			return true;
		}

		return false;	

		
	}// regDocument method


	public static function detailsDoc($doc){

		$id_doc = $doc->getId_doc();		
		$val = $id_doc;


		//echo "Val: " . $id_doc;

		//echo "Other Val: " . $val;		
		
		
		
		

		$query = "SELECT id_doc, id_destiny, id_muni, id_type_doc, id_status_doc, subject_doc, desc_send_position, folio_doc, send_doc, origin_doc, 
			dir_sender, movil_doc, tel_doc, ext_doc, email_doc, date_doc, date_recep, reference, status FROM documents WHERE id_doc = $val
		";

		


	} //detailsDoc method

	public static function getDoc($doc){

		//$id_doc = $doc->getId_doc();	

		$query = "SELECT A.id_doc, A.id_destiny, B.desc_destiny, B.destiny_position, A.id_muni, C.desc_muni, 
			A.id_type_doc, D.desc_type_doc, A.id_status_doc, E.desc_status_docs, A.subject_doc, A.desc_send_position, 
			A.folio_doc, A.send_doc, A.origin_doc, A.dir_sender, A.movil_doc, A.tel_doc, A.ext_doc, A.email_doc, 
			A.date_doc, A.date_recep, A.reference, A.status 
			FROM documents A, destiny_docs B, municipalities C, type_docs D, status_docs E 
			WHERE A.id_destiny = B.id_destiny and A.id_muni = C.id_muni and A.id_type_doc = D.id_type_doc 
			and A.id_status_doc = E.id_status_doc and A.id_doc = :id_doc
		";	

		self::getConection();
	
		$result = self::$cnx->prepare($query);

		$id_doc = $doc->getId_doc();
		$result->bindParam(":id_doc", $id_doc);		

		$result->execute();

		$data = $result->fetch();

		$doc = new Document();

		$doc->setId_doc($data["id_doc"]);
		$doc->setId_destiny($data["id_destiny"]);
		$doc->setDesc_destiny($data["desc_destiny"]);
		$doc->setDestiny_position($data["destiny_position"]);
		$doc->setId_muni($data["id_muni"]);
		$doc->setDesc_muni($data["desc_muni"]);
		$doc->setId_type_doc($data["id_type_doc"]);
		$doc->setDesc_type_doc($data["desc_type_doc"]);
		$doc->setId_status_doc($data["id_status_doc"]);
		$doc->setDesc_status_doc($data["desc_status_docs"]);
		$doc->setSubject_doc($data["subject_doc"]);
		$doc->setDesc_send_position($data["desc_send_position"]);
		$doc->setFolio_doc($data["folio_doc"]);
		$doc->setSend_doc($data["send_doc"]);
		$doc->setOrigin_doc($data["origin_doc"]);
		$doc->setDir_sender($data["dir_sender"]);
		$doc->setMovil_doc($data["movil_doc"]);
		$doc->setTel_doc($data["tel_doc"]);
		$doc->setExt_doc($data["ext_doc"]);
		$doc->setEmail_doc($data["email_doc"]);
		$doc->setDate_doc($data["date_doc"]);
		$doc->setDate_recep($data["date_recep"]);
		$doc->setReference($data["reference"]);

		self::disconnect();

		//Return the entity
		return $doc;


	} //getDoc method

	public static function updateDoc($doc){


	

		$query = " UPDATE `documents` SET `id_destiny` = :id_destiny, `id_muni` = :id_muni, `id_type_doc` = :id_type_doc, 
		`id_status_doc` = :id_status_doc, `subject_doc` = :subject_doc, `desc_send_position` = :desc_send_position, 
		`origin_doc` = :origin_doc, `dir_sender` = :dir_sender, `movil_doc` = :movil_doc, `tel_doc` = :tel_doc, `ext_doc` = :ext_doc,  
		`email_doc` = :email_doc, `date_doc` = :date_doc, `date_recep` = :date_recep, `reference` = :reference, 
		`folio_doc` = :folio_doc, `send_doc` = :send_doc WHERE `documents`.`id_doc` = :id_doc;
		";

		//echo $query;


		self::getConection();

		$result = self::$cnx->prepare($query);

		$id_destiny = $doc->getId_destiny();
		$result->bindParam(":id_destiny", $id_destiny);

		$id_muni = $doc->getId_muni();
		$result->bindParam(":id_muni", $id_muni);

		$id_type_doc = $doc->getId_type_doc();
		$result->bindParam(":id_type_doc", $id_type_doc);

		$id_status_doc = $doc->getId_status_doc();
		$result->bindParam(":id_status_doc", $id_status_doc);		

		$subject_doc = $doc->getSubject_doc();
		$result->bindParam(":subject_doc", $subject_doc);

		//echo $subject_doc;

		$desc_send_position = $doc->getDesc_send_position();
		$result->bindParam(":desc_send_position", $desc_send_position);

		$folio_doc = $doc->getFolio_doc();
		$result->bindParam(":folio_doc", $folio_doc);

		$send_doc = $doc->getSend_doc();
		$result->bindParam(":send_doc", $send_doc);

		$origin_doc = $doc->getOrigin_doc();
		$result->bindParam(":origin_doc", $origin_doc);

		$dir_sender = $doc->getDir_sender();
		$result->bindParam(":dir_sender", $dir_sender);		
		
		$movil_doc = $doc->getMovil_doc();
		$result->bindParam(":movil_doc", $movil_doc);

		$tel_doc = $doc->getTel_doc();
		$result->bindParam(":tel_doc", $tel_doc);		
		
		$ext_doc = $doc->getExt_doc();
		$result->bindParam(":ext_doc", $ext_doc);

		$email_doc = $doc->getEmail_doc();
		$result->bindParam(":email_doc", $email_doc);
		
		$date_doc = $doc->getDate_doc();
		$result->bindParam(":date_doc", $date_doc);

		$date_recep = $doc->getDate_recep();
		$result->bindParam(":date_recep", $date_recep);		
		 
		$reference = $doc->getReference();
		$result->bindParam(":reference", $reference);

		$id_doc = $doc->getId_doc();
		$result->bindParam(":id_doc", $id_doc);



		if($result->execute()){
			
			self::disconnect();

			return true;
		}

		return false;


	}//updateDoc Method


	public static function delbyFolio($doc){

		$query = " UPDATE `documents` SET `status` = :status WHERE `documents`.`id_doc` = :id_doc ";

		self::getConection();

		$result = self::$cnx->prepare($query);

		$id_doc = $doc->getId_doc();
		$result->bindParam(":id_doc", $id_doc);

		$status = '0';
		$result->bindParam(":status", $status);

		if($result->execute()){
			
			self::disconnect();

			return true;
		}

		return false;

	}// delbyFolio method

	
	public static function updateTrack($doc){

		$query = "UPDATE `documents` SET `status` = :status WHERE `documents`.`id_doc` = :id_doc";

		self::getConection();

		$result = self::$cnx->prepare($query);

		$id_doc = $doc->getId_doc();
		$result->bindParam(":id_doc", $id_doc);

		$status = $doc->getStatus();
		$result->bindParam(":status", $status);

		if($result->execute()){
			
			self::disconnect();

			return true;
		}

		return false;

	}//updateTrack method

	public static function getSelect($doc){

		$query = "SELECT A.id_status_doc, B.desc_status_docs, A.id_classif, C.desc_classif 
				  FROM documents A, status_docs B, classifications C 
				  WHERE id_doc = :id_doc and A.id_status_doc = B.id_status_doc and A.id_classif = C.id_classif ";

		self::getConection();
	
		$result = self::$cnx->prepare($query);

		$id_doc = $doc->getId_doc();
		$result->bindParam(":id_doc", $id_doc);		

		$result->execute();

		$data = $result->fetch();

		$doc = new Document();


		$doc->setId_status_doc($data["id_status_doc"]);
		$doc->setDesc_status_doc($data["desc_status_docs"]);
		$doc->setId_classif($data["id_classif"]);
		$doc->setDesc_classif($data["desc_classif"]);

		self::disconnect();

		//Return the entity
		return $doc;

	}//getSelect method

	public static function getDocTracked($doc){

		$query = "SELECT A.id_doc as ID, A.folio_doc as Folio, A.subject_doc as Asunto, A.origin_doc as Origen, B.answer as Respuesta, B.observation as 'Observación', C.desc_destiny as Destinatario, C.destiny_position as Cargo, A.send_doc as Remitente, A.origin_doc as Origen, 
			A.date_doc as Elaborado, A.date_recep as Recibido, A.id_destiny as Destinatario, A.id_muni as Muni, D.desc_muni as Municipio, 
			A.desc_send_position as Send_position, A.dir_sender as Domicilio, A.tel_doc as Telefono, A.ext_doc as Extension, 
			A.movil_doc as Movil, A.email_doc as Email, A.reference as Referencia, E.desc_type_doc as Tipo  
    		FROM documents A, documents_turned B, destiny_docs C, municipalities D, type_docs E, documents_turned F     
    		WHERE A.status = :id_doc AND A.id_doc = B.id_doc AND A.folio_doc = '$folio_doc' AND A.id_destiny = C.id_destiny AND A.id_muni = D.id_muni 
    		AND A.id_type_doc = E.id_type_doc AND A.id_doc = F.id_doc 
    		ORDER BY A.folio_doc";

    	self::getConection();
	
		$result = self::$cnx->prepare($query);

		$id_doc = $doc->getId_doc();
		$result->bindParam(":id_doc", $id_doc);		

		$result->execute();

		$data = $result->fetch();

		$doc = new Document();

		$doc->setId_doc($data["id_doc"]);
		$doc->setFolio_doc($data["folio_doc"]);
		$doc->setSubject_doc($data["subject_doc"]);
		$doc->setOrigin_doc($data["origin_doc"]);
		$doc->setAnswer($data["answer"]);   
		$doc->setObservation($data["observation"]); 
		$doc->setDesc_destiny($data["desc_destiny"]);
		$doc->setDestiny_position($data["destiny_position"]);
		$doc->setSend_doc($data["send_doc"]);
		$doc->setOrigin_doc($data["origin_doc"]);
		$doc->setDate_doc($data["date_doc"]);
		$doc->setDate_recep($data["date_recep"]);
		$doc->setId_destiny($data["id_destiny"]);
		$doc->setId_muni($data["id_muni"]);
		$doc->setDesc_muni($data["desc_muni"]);
		$doc->setDesc_send_position($data["desc_send_position"]);
		$doc->setDir_sender($data["dir_sender"]); 
		$doc->setTel_doc($data["tel_doc"]);
		$doc->setExt_doc($data["ext_doc"]);
		$doc->setMovil_doc($data["movil_doc"]);
		$doc->setEmail_doc($data["email_doc"]);
		$doc->setReference($data["reference"]);
		$doc->setDesc_type_doc($data["desc_type_doc"]);

		self::disconnect();

		//Return the entity
		return $doc;



	}//getDocTracked method




} //Class DocumentDAO
<?php

include 'OtherConnect.php';
include '../model/History.php';

class OtherHistoryDAO extends OtherConnect {

	protected static $cnx;

	private static function getConection(){
			self::$cnx = Connect::connection();
		
	}

	private static function disconnect(){
		self::$cnx = null;
	}

	public static function getNumAccess(){

		$query = "SELECT count(*) as access from history_access";

		self::getConection();

		$result = self::$cnx->prepare($query);

		if($result->execute()){

			$data = $result->fetch();

			$history = new History();			

			$history->setRecords($data["access"]);

			self::disconnect();

		}

		return $history;

	}//getNumAccess


}	

?>
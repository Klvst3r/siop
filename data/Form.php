<?php


class form {

  function form($name, $method, $action, $role, $enctype, $class){
  //function form($name, $method, $action, $role, $enctype, $target){
  	echo "<form name='$name' method='$method' action='$action' role='$role' $enctype class='$class'>";

  }//form function

  function formNew($name, $method, $action, $role, $enctype, $target){
    echo "<form name='$name' method='$method' action='$action' role='$role' $enctype class='$class' target='$target'>";

  }//form function


  function verifyParams($array, $limit, $type){
           if (count($array) <> $limit){
                echo 'faltan parámetros para agregar un campo tipo <b>' . $type . '</b>... solo pasó <b>' . count($array) . '</b> de <b>' . $limit . '</b>.';
                return 0;
           }
           else{
                return 1;  //Return1=true
           }
  }//function verifyParams

  function addField($type, $p){
  	switch ($type){
  		case 1: //text
        	if ($check = $this -> verifyParams($p, 15, "TEXTO")){

		  

         echo '<div class="form-group">
                <label class="' . $p["class_label"] .'" for="'. $p["field_name"] . '"><b> ' . $p["label_field"] . '</b></label> 
                <div class="' . $p["div_field"] . '">
                  <input type="text" name="' . $p["field_name"] . '" class="' . $p["input_class"] . '" id="' . $p["field_name"] . '" ' .
                  $p["readonly"] . ' ' . $p["disabled"] . ' value="' . $p["value"] . '" maxlength="' . $p["maxlength"] . '" size="' .
                   $p["size"] . '" style="'. $p["style"] . '" ' . $p["js"] . '    placeholder="' . $p["placeholder"] .
                  '" '. $p["required"]. ' ' . $p["autofocus"] . ' autocomplete="off" />
                </div>
              </div>';
          echo '<div class="space">&nbsp;</div>';



            }
        break;
               case 2: //password
                   if ($check = $this -> verifyParams($p, 15, "PASSWORD")){
                       /*echo "<input type='password' name='$p[nombre]' maxlength='$p[maxlength]' size='$p[size]' style='$p[style]' $p[js]>";   */
                    
                echo '<div class="form-group">
                        <label class="' . $p["class_label"] .'" for="'. $p["field_name"] . '"><b> ' . $p["label_field"] . '</b></label> 
                        <div class="' . $p["div_field"] . '">
                          <input type="password" name="' . $p["field_name"] . '" class="' . $p["input_class"] . '" id=" ' . $p["field_name"] . '" ' .
                          $p["readonly"] . ' ' . $p["disabled"] . ' value="' . $p["value"] . '" maxlength="' . $p["maxlength"] .
                          '" size="' . $p["size"] . '" style="'. $p["style"] . '" ' . $p["js"] . ' placeholder="' . $p["placeholder"] .
                          '" '. $p["required"]. ' ' . $p["autofocus"] . '/>
                        </div>
                      </div>';

                   }
               break;
        case 3: //submit-button
  			if ($check = $this -> verifyParams($p, 6, "SUBMIT")){
            	
            	echo '<button type="submit" name="' . $p["name"] . '" class="' . $p["type_button"] . '" ' . $p["disabled"] . ' data-toggle="tooltip" data-placement="top" title="'. $p["tooltip"] . '"><i class="' . $p["icon"] . '"></i> &nbsp;&nbsp;' . $p["legend"] . '</button>';
            }
        break;
        case 4: //hidden
            if ($check = $this -> verifyParams($p, 2, "HIDDEN")){
                /*echo "<input name='$p[nombre]' type='hidden' value='$p[value]'/>";*/
                /*<input type="hidden" value="<?php echo $usuario->id ?>" name="usuario_id" />*/
                echo '<input type="hidden" value="' . $p["value"] . '" name="' . $p["field_name"] . '" />';
            }
        break;
        case 5:
        	if ($check = $this -> verifyParams($p, 5, "EMAIL")){
        		

        		echo '<div class="form-group">
                        <label for="' . $p["field_name"] . '">' . $p["label_field"] . '</label>
                          <input type="email" value="' .$p["value"] . '" name="' . $p["field_name"] . '" class="form-control" id="' .
                           $p["field_name"] . '" placeholder="' . $p["placeholder"] . '" ' . $p["required"]. '/>
                      </div>';
        	}
        break;
        case 6:
          if ($check = $this -> verifyParams($p, 11, "EMAIL")){
            

            echo '<div class="form-group">
                        <label for="' . $p["field_name"] . '">' . $p["label_field"] . '</label>
                          <textarea  value="' .$p["value"] . '" name="' . $p["field_name"] . '" class="form-control" id="' .
                           $p["field_name"] . '" placeholder="' . $p["placeholder"] . '" ' . $p["required"]. ' rows=' . $p["rows"] . '>' . $p["value"] . '</textarea>
                  </div>';
          }
        break;
          case 7: //TEL
          if ($check = $this -> verifyParams($p, 15, "TEL")){

        

                 echo '<div class="form-group">
                        <label class="' . $p["class_label"] .'" for=" '. $p["field_name"] . ' "><b> ' . $p["label_field"] . '</b></label>
                      <div class="' . $p["div_field"] . '">  
                          <input type="tel" name="' . $p["field_name"] . '" class="' . $p["input_class"] . '" id=" ' . $p["field_name"] . '" ' .
                          $p["readonly"] . ' ' . $p["disabled"] . ' value="' . $p["value"] . '" maxlength=' . $p["maxlength"] .
                          'size=' . $p["size"] . 'style='. $p["style"] . ' ' . $p["js"] . '    placeholder="' . $p["placeholder"] .
                          '" '. $p["required"]. ' ' . $p["autofocus"] . '/>
                        </div>
                      </div>';
                echo '<div class="space">&nbsp;</div>';



            }
        break;
        case 8: //EMAIL
          if ($check = $this -> verifyParams($p, 15, "Email")){



                 echo '<div class="form-group">
                        <label class="' . $p["class_label"] .'" for=" '. $p["field_name"] . ' "><b> ' . $p["label_field"] . '</b></label>
                       <div class="' . $p["div_field"] . '">    
                          <input type="email" name="' . $p["field_name"] . '" class="' . $p["input_class"] . '" id=" ' . $p["field_name"] . '" ' .
                          $p["readonly"] . ' ' . $p["disabled"] . ' value="' . $p["value"] . '" maxlength=' . $p["maxlength"] .
                          'size=' . $p["size"] . 'style='. $p["style"] . ' ' . $p["js"] . '    placeholder="' . $p["placeholder"] .
                          '" '. $p["required"]. ' ' . $p["autofocus"] . '/>
                        </div>
                      </div>';



            }
        break;

        case 9: //textarea
          if ($checa = $this -> verifyParams($p, 12, "TEXTAREA")){



           

              echo '<div class="form-group">
                      <label for="'. $p["field_name"] . '"> ' . $p["label_field"] . '</label>
                      <textarea value="' . $p["value"] . '" name="' . $p["field_name"] . '" ' . $p["readonly"] . ' ' . $p["disabled"] . ' rows="' . $p["rows"]
                      . '" cols="' . $p["cols"]  . '" class="form-control" style="' . $p["style"] . '" required="' . $p["required"] . '" ' . $p["js"]
                      . ' placeholder="'. $p["placeholder"] . '">' . $p["content"] . '</textarea>
                    </div>
              ';


          }
          break;

        case 10: //textarea
          if ($checa = $this -> verifyParams($p, 11, "RADIO")){



           
            


              echo '
              <table border="0" >            
              <div class="form-group" id="wrapper">
                <tr>
                <td width="350">
    
                  <label for="' . $p["field_name"] . '"> ' . $p["label_field"] . '</label> 
                  </td>';

                  if ($p["selected"] == false){

                  echo '<td width="50" align="center">
                    <input type="radio" name="' . $p["name_option"] . '" value="' . $p["value_no"] . '" checked>No</input> 
                  </td>
                  <td width="50" align="center">
                     <input type="radio" name="' . $p["name_option"] . '" value="' . $p["value_yes"] . '">Si</input>
                  </td>';
                  }else{
                    echo '<td width="50" align="center">
                    <input type="radio" name="' . $p["name_option"] . '" value="' . $p["value_no"] . '" >No</input> 
                  </td>
                  <td width="50" align="center">
                    <input type="radio" name="' . $p["name_option"] . '" value="' . $p["value_yes"] . '" checked>Si</input>
                  </td>';

                  }


                  echo '</label>
                </tr>
              </div>
              </table>
            ';


          }
          break;

          case 11: //Checkboxes
           if ($checa = $this -> verifyParams($p, 6, "CHECKBOX")){
            
            


              echo '
              <table border="0" >            
              <div class="form-group" id="wrapper">
                <tr>
                <td width="">
    
                  <label for="' . $p["field_name"] . '"> ' . $p["label_field"] . '</label> 
                  
                    <input type="checkbox" name="' . $p["field_name"] . '" value="' . $p["value"] . '">' . $p["legend"] . '</input> 
                  </td>
                  ';
                  

                  echo '</label>
                </tr>
              </div>
              </table>
            ';


          }
        break;



        default: //
                echo 'Error al agregar el campo, opción no valida <b>' . ($type). '</b>...el programa abortó.';
        exit;

  	}//switch

  }//function addFlield






  function closeForm(){
    echo "</form>";
        return 1;
    }//closeForm


}//Class Form
?>

<?php 

//Import conection
include 'SideConnect.php';
include '../model/SideUser.php';

class SideUserDAO extends SideConnect {
	protected static $cnx;


	private static function getConection(){

		self::$cnx = SideConnect::connection();
		
	}


	private static function disconnect(){
	
		self::$cnx = null;
	}

	public static function getSidePhoto($user){

	 	$query = "SELECT id_user, name, user_name, user_photo FROM users WHERE id_user = :id_user";

	 	self::getConection();

	 	$result = self::$cnx->prepare($query);

	 	$user_bd = $user->getId_user();
		$result->bindParam(":id_user", $user_bd);

		$result->execute();

		$data = $result->fetch();

		$user = new SideUser();

		$user->setId_user($data["id_user"]);

		$user->setUser_photo($data["user_photo"]);

		$user->setName($data["name"]);
		
		$user->setUser_name($data["user_name"]);


		return $user;


	 }//function getSidePhoto


} //class SideUserDAO
<?php


 class SideConnect {


	public static function connection(){
		try {

			
			$cn = new PDO(
			  "mysql:host=localhost;dbname=siop", 
			  "root", 
			  "desarrollo", 
			  array(
			    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
			  )
			);


			/*echo "Connection Successful <br/>";*/
			return $cn;

		} catch(PDOException $ex) {
			die($ex->getMessage());
		  }
	}//Connection

	
}

/*Test of Connection*/
/*SideConnect::connection();*/

<?php
	//include 'Connect.php';
	include '../model/Destiny.php';
	class DestinyDAO extends Connect {
		protected static $cnx;

		private static function getConection(){

			self::$cnx = Connect::connection();

		}//function getConection

		private static function disconnect(){

			self::$cnx = null;
		}

		public static function getDestiny($destiny){

			$query = "SELECT desc_destiny, destiny_position from destiny_docs where id_destiny = :id_destiny";

			self::getConection();

			$result = self::$cnx->prepare($query);

			$id_destiny = $destiny->getId_destiny();

			$result->bindParam(":id_destiny", $id_destiny);

			$result->execute();

			$data = $result->fetch();

			$destiny->setDesc_destiny($data["desc_destiny"]);
			$destiny->setDestiny_position($data["destiny_position"]);

			return $destiny;

		}//getDestiny

		public static function cleanDiv($destiny){

			$query = "SELECT destiny_position from destiny_docs where id_destiny = :id_destiny";

			self::getConection();


			$result = self::$cnx->prepare($query);

			$id_destiny = $destiny->getId_destiny();
			$result->bindParam(":id_destiny", $id_destiny);

			$result->execute();

			$data = $result->fetch();

			//echo "Position" . $data["destiny_position"];

			$destiny->setDestiny_position($data["destiny_position"]);

			return $destiny;

			

			
			



		}//cleanDiv



	}//Class DestinyDAO

?>
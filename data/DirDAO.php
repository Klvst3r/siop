<?php
//Import conection
include 'Connect.php';
include '../model/Dir.php';

class DirDAO extends Connect {
	protected static $cnx;

	private static function getConection(){

		self::$cnx = Connect::connection();

	}//function getConection

	private static function disconnect(){

		self::$cnx = null;
	}

	public static function regDir($dir){

		$query = "INSERT INTO directions (id_dir, dir_name, status) VALUES (NULL, :dir_name, :status)";

		self::getConection();

		$result = self::$cnx->prepare($query);

		//Obtain data from Model
		$dir_name = $dir->getDir_name();
		$result->bindParam(":dir_name", $dir_name);

		$status = '1';
		$result->bindParam(":status", $status);


		//Execute Query
		if($result->execute()){
			//return true;
			//echo "Insercion Exitosa";
			return true;
		}

		return false;


	}// Class regDir

	public static function updateDir($dir){

		$query = "UPDATE `directions` SET `dir_name` = :dir_name WHERE `directions`.`id_dir` = :id_dir;";

		self::getConection();

		$result = self::$cnx->prepare($query);

		//Obtain data from Model
		$id_dir = $dir->getId_dir();
		$result->bindParam(":id_dir", $id_dir);

		$dir_name = $dir->getDir_name();
		$result->bindParam(":dir_name", $dir_name);

		//Execute Query
		if($result->execute()){

			return true;
		}

		return false;


	}//updateDir

	public static function delDir($dir){
		$query = "UPDATE `directions` SET `status` = '0' WHERE `directions`.`id_dir` = :id_dir;";

		self::getConection();

		$result = self::$cnx->prepare($query);

		$id_dir = $dir->getId_dir();
		$result->bindParam(":id_dir", $id_dir);

		if($result->execute()){

			return true;
		}

		return false;

	}//delDir Method



}// Class DirDAO

?>
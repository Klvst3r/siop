<?php 

//Import conection
include 'HeadConnect.php';
include '../model/HeadUser.php';

class HeadUserDAO extends HeadConnect {
	protected static $cnx;


	private static function getConection(){

		self::$cnx = HeadConnect::connection();
		
	}


	private static function disconnect(){
	
		self::$cnx = null;
	}

	public static function getHeadPhoto($user){

	 	$query = "SELECT A.id_user, A.name, A.user_name, A.user_resume, A.user_photo, B.desc_priv FROM users A, privileges B WHERE id_user = :id_user and A.id_priv = B.id_priv";
	 	 

	 	self::getConection();

	 	$result = self::$cnx->prepare($query);

	 	$user_bd = $user->getId_user();
		$result->bindParam(":id_user", $user_bd);

		$result->execute();

		$data = $result->fetch();

		$user = new HeadUser();

		$user->setId_user($data["id_user"]);

		$user->setUser_photo($data["user_photo"]);

		$user->setName($data["name"]);
		
		$user->setUser_name($data["user_name"]);

		$user->setUser_resume($data["user_resume"]);

		$user->setDesc_priv($data["desc_priv"]);


		return $user;


	 }//function getHeadPhoto


} //class HeadUserDAO
<?php

include 'Reconnect.php';
include '../model/Privilege.php';

class OtherPrivDAO extends Reconnect {
	
	protected static $cnx;
	
	private static function getConection(){
		
		self::$cnx = Connect::connection();
		
	}

	private static function disconnect(){
		
		self::$cnx = null;
	}

	public static function getNumPriv(){
		$query = "SELECT count(*) as priv from privileges";

		self::getConection();

		$result = self::$cnx->prepare($query);

		if($result->execute()){

			$data = $result->fetch();

			$priv = new Privilege();			

			$priv->setRecords($data["priv"]);

			self::disconnect();

		}

		return $priv;

	}//getNumPriv

}

?>
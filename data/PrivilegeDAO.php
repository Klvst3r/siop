<?php
//Import conection
include 'Connect.php';
include '../model/Privilege.php';

class PrivilegeDAO extends Connect {
	protected static $cnx;

	private static function getConection(){

		self::$cnx = Connect::connection();

	}//function getConection

	private static function disconnect(){

		self::$cnx = null;
	}

	public static function regPrivilege($privilege){

		$query = "INSERT INTO privileges (id_priv, desc_priv, create_priv, select_priv, insert_priv, update_priv, delete_priv, backup_priv, report_priv) VALUES (NULL, :desc_priv, :create_priv, :select_priv, :insert_priv, :update_priv, :delete_priv, :backup_priv, :report_priv)";

		self::getConection();

		$result = self::$cnx->prepare($query);

		//Obtain data from Model
		$desc_priv = $privilege->getDesc_priv();
		$result->bindParam(":desc_priv", $desc_priv);

		$create_priv = $privilege->getCreate_priv();
		$result->bindParam(":create_priv", $create_priv);

		$select_priv = $privilege->getSelect_priv();
		$result->bindParam(":select_priv", $select_priv);

		$insert_priv = $privilege->getInsert_priv();
		$result->bindParam(":insert_priv", $insert_priv);

		$update_priv = $privilege->getUpdate_priv();
		$result->bindParam(":update_priv", $update_priv);

		$delete_priv = $privilege->getDelete_priv();
		$result->bindParam(":delete_priv", $delete_priv);

		$backup_priv = $privilege->getBackup_priv();
		$result->bindParam(":backup_priv", $backup_priv);

		$report_priv = $privilege->getReport_priv();
		$result->bindParam(":report_priv", $report_priv);

		//Execute Query
		if($result->execute()){
			//return true;
			//echo "Insercion Exitosa";
			return true;
		}

		return false;


	}// Class RegPrivilege

	public static function updatePrivilege($privilege){

		$query = "UPDATE `privileges` SET `desc_priv` = :desc_priv, `create_priv` = :create_priv, `select_priv` = :select_priv,
		 `insert_priv` = :insert_priv, `update_priv` = :update_priv, `delete_priv` = :delete_priv, `backup_priv` = :backup_priv,
		 `report_priv` = :report_priv WHERE `privileges`.`id_priv` = :id_priv;";

		self::getConection();

		$result = self::$cnx->prepare($query);

		//Obtain data from Model
		$id_priv = $privilege->getId_priv();
		$result->bindParam(":id_priv", $id_priv);

		$desc_priv = $privilege->getDesc_priv();
		$result->bindParam(":desc_priv", $desc_priv);

		$create_priv = $privilege->getCreate_priv();
		$result->bindParam(":create_priv", $create_priv);

		$select_priv = $privilege->getSelect_priv();
		$result->bindParam(":select_priv", $select_priv);

		$insert_priv = $privilege->getInsert_priv();
		$result->bindParam(":insert_priv", $insert_priv);

		$update_priv = $privilege->getUpdate_priv();
		$result->bindParam(":update_priv", $update_priv);

		$delete_priv = $privilege->getDelete_priv();
		$result->bindParam(":delete_priv", $delete_priv);

		$backup_priv = $privilege->getBackup_priv();
		$result->bindParam(":backup_priv", $backup_priv);

		$report_priv = $privilege->getReport_priv();
		$result->bindParam(":report_priv", $report_priv);

		//Execute Query
		if($result->execute()){
			//return true;
			//echo "Insercion Exitosa";
			return true;
		}

		return false;


	}//updatePrivilege

	public static function delPrivilege($privilege){
		$query = "UPDATE `privileges` SET `status` = '0' WHERE `privileges`.`id_priv` = :id_priv;";

		self::getConection();

		$result = self::$cnx->prepare($query);

		$id_priv = $privilege->getId_priv();
		$result->bindParam(":id_priv", $id_priv);

		if($result->execute()){

			return true;
		}

		return false;

	}//delPrivilege Method

	


}// Class PrivilegeDAO

?>
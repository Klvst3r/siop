<?php

include 'AnotherConnect.php';
include '../model/Track.php';

class TrackDAO extends AnotherConnect {
	protected static $cnx;


	private static function getConection(){

		self::$cnx = Connect::connection();

	}


	private static function disconnect(){

		self::$cnx = null;
	}

	public static function regTrack($track){

		$query = "INSERT INTO `documents_turned` (`id_turn`, `id_doc`, `id_dir`, `id_status_doc`, `id_dep`, `id_char`, `id_classif`, `id_desc`, 
					`folio_turned`, `doc_origin`, `doc_ccp`, `date_turned`, `instructions`, `answer`, `observation`) 
					VALUES (NULL, :id_doc, :id_dir, :id_status_doc, :id_dep, :id_char, :id_classif, :id_desc, 
					:folio_turned, :doc_origin, :doc_ccp, :date_turned, :instructions, '', '') ";

		


		self::getConection();
	
		$result = self::$cnx->prepare($query);


		$id_doc = $track->getId_doc();
		$result->bindParam(":id_doc", $id_doc);

		$id_dir = $track->getId_dir();
		$result->bindParam(":id_dir", $id_dir);

		$id_status_doc = $track->getId_status_doc();
		$result->bindParam(":id_status_doc", $id_status_doc);

		$id_dep = $track->getId_dep();
		$result->bindParam("id_dep", $id_dep);

		$id_char = $track->getId_char();
		$result->bindParam(":id_char", $id_char);

		$id_classif = $track->getId_classif();
		$result->bindParam(":id_classif", $id_classif);

		$id_desc = $track->getId_desc();
		$result->bindParam(":id_desc", $id_desc);

		$folio_turned = $track->getFolio_turned();
		$result->bindParam(":folio_turned", $folio_turned);

		$doc_origin = $track->getDoc_origin();
		$result->bindParam(":doc_origin", $doc_origin);

		$doc_ccp = $track->getDoc_cpp();
		$result->bindParam(":doc_ccp", $doc_ccp);

		$date_turned = $track->getDate_turned();
		$result->bindParam(":date_turned", $date_turned);

		$instructions = $track->getInstructions();
		$result->bindParam(":instructions", $instructions);

		if($result->execute()){
			
			self::disconnect();

			return true;
		}

		return false;				


	}//regTrack method

	public static function getTrack($track){

		

		$query = "SELECT A.id_doc, A.folio_doc, A.subject_doc, B.desc_destiny , B.destiny_position, A.send_doc, A.origin_doc, A.date_doc, A.date_recep, 
				  C.folio_turned, C.doc_origin, C.date_turned, C.instructions, D.dir_name, E.departament, C.answer, C.observation 
			FROM documents A, destiny_docs B, documents_turned C, directions D, departaments E  
			WHERE A.id_doc = :id_doc AND A.id_destiny = B.id_destiny AND A.id_doc = C.id_doc AND C.id_dir = D.id_dir and C.id_dep = E.id_dep 
		";	

		self::getConection();
	
		$result = self::$cnx -> prepare($query);

		$id_doc = $track -> getId_doc();
		$result -> bindParam(":id_doc", $id_doc);

		$result -> execute();

		$data = $result -> fetch();

		

		//$track->setId_doc($data["id_doc"]);
		$track -> setFolio_doc($data["folio_doc"]);
		$track -> setSubject_doc($data["subject_doc"]);
		$track -> setDesc_destiny($data["desc_destiny"]);
		$track -> setDestiny_position($data["destiny_position"]);
		$track -> setSend_doc($data["send_doc"]);
		$track -> setOrigin_doc($data["origin_doc"]);
		$track -> setDate_doc($data["date_doc"]);
		$track -> setDate_recep($data["date_recep"]);
		
		$track -> setDir_name($data["dir_name"]);
		$track -> setDepartament($data["departament"]);

		$track -> setFolio_turned($data["folio_turned"]);
		$track -> setDoc_origin($data["doc_origin"]);
		$track -> setDate_turned($data["date_turned"]);
		$track -> setInstructions($data["instructions"]);

		$track -> setAnswer( $data["answer"] );
		$track -> setObservation( $data["observation"] );

		
		

		self::disconnect();

		//Return the entity
		return $track;


	} //getDoc method




	public static function updateTrack($track){

		$query = "UPDATE `documents_turned` SET `answer` = :answer, `observation` = :observation WHERE `documents_turned`.`id_doc` = :id_turn;";

		self::getConection();
	
		$result = self::$cnx -> prepare($query);

		$id_turn = $track -> getId_turn();
		$result -> bindParam(":id_turn", $id_turn);

		$answer = $track -> getAnswer();
		$result -> bindParam(":answer", $answer);

		$observation = $track -> getObservation();
		$result -> bindParam(":observation", $observation);



		if($result->execute()){
			
			self::disconnect();

			return true;
		}

		return false;

	} //updateTrack method


}// class TrackDAO	

?>
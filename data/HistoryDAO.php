<?php
include 'Reconnect.php';
include '../model/History.php';
include '../model/UserHistory.php';

class HistoryDAO extends Reconnect {
	protected static $cnx;

	private static function getConection(){
			self::$cnx = Connect::connection();
		
	}

	private static function disconnect(){
		self::$cnx = null;
	}

	public static function regHistory($history){
		$query = "INSERT into history_access (id_history, ip, date_access, time_in, time_out) VALUES (NULL, :ip, :date_access, :time_in, :time_out)";

		self::getConection();
	
		$result = self::$cnx->prepare($query);

		$ip = $history->getIp();
		$result->bindParam(":ip", $ip);

		$date_access = $history->getDate_access();
		$result->bindParam(":date_access", $date_access);

		$time_in = $history->getTime_in();
		$result->bindParam(":time_in", $time_in);		

		$time_out = $history->getTime_out();
		$result->bindParam(":time_out", $time_out);

		if($result->execute()){
			//return true;
			//echo "Insercion Exitosa";
			//$lastId = $result->insert_id();
			/*$temp_result = $result->fetch(PDO::FETCH_ASSOC);
            return ( $temp_result ) ? $temp_result['last_value'] : false;
            */
           /* Haremos una seleccion del ultimo registro y lo enviamos a la tabla de Usuarios como una actualización */
           //echo "Last ID: " . $lastId;
			
			self::disconnect();
			//return $lastId;
			return true;
		}

		return false;

	}//method regHistory

	public static function updateHistory($user){
	//First Select las id
	$query = "SELECT id_history, ip, date_access, time_in, time_out FROM `history_access` ORDER BY `id_history` DESC LIMIT 1"; 

	self::getConection();
	
	$result = self::$cnx->prepare($query);



	if($result->execute()){

		$data = $result->fetch();

		$history = new History();

		$history->setId_history($data["id_history"]);
		$idHistory = $data["id_history"];

		$history->setIp($data["ip"]);
		$history->setDate_access($data["date_access"]);
		$history->setTime_in($data["time_in"]);
		$history->setTime_out($data["time_out"]);

		//echo "Id History: " . $idHistory;


		$update = "UPDATE `users` SET `id_history` = '$idHistory' WHERE `users`.`id_user` = :id_user";

		self::getConection();

		$resultUpdate = self::$cnx->prepare($update);

		//echo $update;
		//el objeto creado es $history y no $user con el metodo getId_history
		$id_user = $user->getId_history();
		$resultUpdate->bindParam(":id_user", $id_user);

		//echo "ID User: " . $id_user;
		//If doesn't function change last method to $idUser from object param

		

		$resultUpdate->execute();


		self::disconnect();
	return true;
	}

	return false;


	}//method updateHistory

	


}//Class HistoryDAO

?>
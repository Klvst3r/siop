<?php
//Import conection
include 'Connect.php';
include '../model/Departament.php';

class DepDAO extends Connect {
	protected static $cnx;

	private static function getConection(){

		self::$cnx = Connect::connection();

	}//function getConection

	private static function disconnect(){

		self::$cnx = null;
	}

	public static function regDep($dir){

		$query = "INSERT INTO departaments (id_dep, departament, status) VALUES (NULL, :departament, :status)";

		self::getConection();

		$result = self::$cnx->prepare($query);

		//Obtain data from Model
		$departament = $dir->getDepartament();
		$result->bindParam(":departament", $departament);

		$status = '1';
		$result->bindParam(":status", $status);


		//Execute Query
		if($result->execute()){
			//return true;
			//echo "Insercion Exitosa";
			return true;
		}

		return false;


	}// Class regDir

	public static function updateDep($dep){

		$query = "UPDATE `departaments` SET `departament` = :departament WHERE `departaments`.`id_dep` = :id_dep;";

		self::getConection();

		$result = self::$cnx->prepare($query);

		//Obtain data from Model
		$id_dep = $dep->getId_dep();
		$result->bindParam(":id_dep", $id_dep);

		$departament = $dep->getDepartament();
		$result->bindParam(":departament", $departament);

		//Execute Query
		if($result->execute()){

			return true;
		}

		return false;


	}//updateDir

	public static function delDep($dep){
		$query = "UPDATE `departaments` SET `status` = '0' WHERE `departaments`.`id_dep` = :id_dep;";

		self::getConection();

		$result = self::$cnx->prepare($query);

		$id_dep = $dep->getId_dep();
		$result->bindParam(":id_dep", $id_dep);

		if($result->execute()){

			return true;
		}

		return false;

	}//delDep Method



}// Class DirDAO

?>
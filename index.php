 <?php

include 'assets/partials/head.php';
/*include 'assets/partials/menu.php';*/
?>

<!-- HEADER -->
    <header>

      <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button><!-- .navbar-toggle -->

            <a class="navbar-brand" href="#">
                <img src="assets/img/logo/SECODUVI_small.jpg" alt="SIOP" class="logo-home">
            </a>

        </div><!-- .navbar-header -->
        <div class="collapse navbar-collapse">

        	<ul class="nav navbar-nav navbar-left">
                <li class="nav-title"> Sistema de Información de Oficialía de Partes</li>
                
            </ul>
            
        </div><!-- .collapse -->
    </div><!-- .container -->
</nav> <!-- .navbar -->   
</header >
<!-- HEADER -->
<!-- CONTENT -->
<section class="container-fluid wrap">
	<div class="starter-template" id="title-enroll">
		<div class="jumbotron">
			<div class="container-fluid">
				<h1 class="text-center">SIOP</h1>
				<p class="text-center"> Sistema de Información de Oficialía de Partes</p>
				<p class="text-center">
					<a href="login.php" class="btn btn-success btn-lg">Login</a>
				</p>
			</div>
		</div>
	</div>

</section><!-- /.container -->
<!-- END CONTENT -->

<!--  FEATURES -->
<section class="wrap" id="system-features">
    <div class="container">
        <div class="header-section">
            <img src="assets/img/features.png" class="img-size">
            <h2>Características del Sistema</h2>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <i><img src="assets/img/user_b.png" alt="User"/></i>
                <h4>Administración de Usuarios</h4>
            </div>
            <div class="col-sm-3">
                <i><img src="assets/img/task_b.png" alt="Task"/></i>
                <h4>Registro de Oficios</h4>
            </div>
            <div class="col-sm-3">
                <i><img src="assets/img/pdf_b.png" alt="PDF"/></i>
                <h4>Informes en PDF</h4>
            </div>
            <div class="col-sm-3">
                <i><img src="assets/img/mobil_b.png" alt="Mobile"/></i>
                <h4>Accesible localmente y  remoto multiplataforma</h4>
            </div>
        </div><!-- .row -->
    </div><!-- .container -->
</section>
<!-- END FEATURES -->

<!-- FOOTER -->
<footer id="footer">
    <div class="container-fluid">
        <p>SIOP - SECODUVI &copy; <?php echo date('Y'); ?> | Made by <a href="#" class="">Klvst3r</a></i></p>
        </div><!-- .container-fluid -->
    </footer>
    <!-- FOOTER -->

<?php 

include 'assets/partials/footer.php'; 
?>
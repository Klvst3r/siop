<?php

$a = $_GET['a'];

/**
 * Inside each option you should load the next session variable to stay inside in the system
 * this value will be put it after load menu
 */
//$_SESSION["user"]["code"] = "true";

if(isset($a)){
	switch($a){
		case 1:
			//Profile
			require_once("view/viewProfile.php");
			break;
		case 2:
			// Edit Profile
			require_once("view/editProfile.php");
			break;
		case 3:
			// Edit Profile User
			require_once("view/viewOrg.php");
			break;
		case 4:
			// Edit Config
			require_once("view/editConfig.php");
			break;
		case 5:
			// Change Profile User
			require_once("view/changeProfile.php");
			break;
		case 6:
			// Change Generals Profile
			require_once("view/changeGenerals.php");
			break;
		case 7:
			//Change About User
			require_once("view/changeAbout.php");
			break;
		case 8:
			//Change User Password
			require_once("view/changePassword.php");
			break;
		case 9:
			//view Users
			require_once("view/viewUsers.php");
			break;
		case 10:
			//Detail User
			require_once("view/detailsUser.php");
			break;
		case 11: /* Begin Section Edit User*/
			//Form New User
			require_once("view/wrapperFormUser.php");
			break;
		case 12:
		//proccess update user selected
			require_once("view/editUser.php");
			break; /* End Section Edit User*/
		case 13: /* Begin Seccion Add New User */
			require_once("view/UserFormProfile.php");
			break;
		case 14:
			require_once("view/UserFormGenerals.php");
			break;
		case 15:
			require_once("view/UserFormAbout.php");
			break;
		case 16:
			require_once("view/UserFormPassword.php");
			break;
		case 17:
			require_once("view/processProfile.php");
			break;
		case 18:
			require_once("view/processGenerals.php");
			break;
		case 19:
			require_once("view/processAbout.php");
			break;
		case 20:
			require_once("view/processPass.php");
			break; /* End Seccion Add New User */
		case 21: /* Begin section delete user */
			require_once("view/delUser.php");
			break; /* End Section Delete User */
		case 22: // <- 20 <- 14  /* Begin Section Privileges*/
				//view Profiles
			require_once("view/viewPrivileges.php");                   /*-----------------   Done   ----------------------*/
			break;
		case 23: // <- 21 <- 15
				// Privileges Form to Insert or Update
			require_once("view/privilegeForm.php");
			break;
		case 24: // <- 22 <- 16
				//Add new Profile or modify profile
			require_once("view/addNewPrivilege.php");
			break;
		case 25: // <- 23 <- 17
				//Delete Profile
			require_once("view/delPrivilege.php");
			break;		/* End Section Privileges*/
		case 26: /*  Begin Procces to update User Information" */
			// Change Profile User
			require_once("view/updateProfile.php");
			break;
		case 27: /*  Begin Procces to update User Information" */
			// Change Profile User
			require_once("view/updateGenerals.php");
			break;
		case 28:
			//Edit About user
			require_once("view/updateAbout.php");
			break;
		case 29:
			//Change Password of user selected
			require_once("view/updatePass.php");
			break;
		case 30:
			//View Register Profile first step
			require_once("view/regUserProfile.php");
			break;
		case 31:
		//view Register user Generals
			require_once("view/regUserGenerals.php");
			break;
		case 32:
			//view register user About Information
			require_once("view/regUserAbut.php");
			break;
		case 33:
			//view register User Password
			require_once("view/regUserPass.php");
			break;
		case 34:
			require_once("view/regUserPhoto.php");
			break;
		case 35:
			require_once("view/changeOrg.php");
			break;
		case 36:
			require_once("view/dirList.php");
			break;
		case 37:
			require_once("view/dirForm.php");
			break;			
		case 38:
			require_once("view/addNewDir.php");
			break;	
		case 39:
			require_once("view/delDir.php");
			break;	
		case 40:
			//Changue Pass
			require_once("view/viewChanguePass.php");
			break;
		case 41:
			require_once("view/depList.php");
			break;
		case 42:
			require_once("view/depForm.php");
			break;
		case 43:
			require_once("view/addNewDep.php");
			break;	
		case 44:
			require_once("view/delDep.php");
			break;	
		case 50:
			//Backup BD
			require_once("view/viewBackup.php");
			break;
		case 51:
			//Restore BD
			require_once("view/viewRestore.php");
			break;


		default:
			echo"No hay mas Opciones en el Menu del Administrador";
			break;

	}
}

?>

 <?php

include '../controller/PrivilegeController.php';
include '../helps/helps.php';


 ?>

 <section class="wrapper site-min-height">
<h3><i class="fa fa-angle-right"></i> Perfil | Administrador: <b><?php echo $user_name;  ?></b></h3>
<div class="row mt">
<div class="col-lg-12">
 			<!-- INICIO CONTENIDO -->
<div class="container-fluid">
<div class="panel">
<div class="panel-heading">
	<h3 class="panel-title">Agregar Perfil</h3>
 </div>
 <div class="panel-body">
<!-- ************************   Begin Process to Change About   **************************** -->

<?php
//echo "Processing....";
//Verify the method to receive form



if($_SERVER["REQUEST_METHOD"] == "POST"){
    //id_user does not exist so, register privilege
    
    if($_POST["update"] == "true"){

    	if(isset($_POST["id_priv"]) && isset($_POST["desc_priv"]) && isset($_POST["yes_no_create"]) && isset($_POST["yes_no_select"]) && isset($_POST["yes_no_insert"]) && isset($_POST["yes_no_update"]) && isset($_POST["yes_no_delete"]) && isset($_POST["yes_no_backup"]) && isset($_POST["yes_no_report"])){
		//Code to update 
		$id_priv 	 	=	validate_field($_POST["id_priv"]);
		$desc_priv	 	=	validate_field($_POST["desc_priv"]);
		$create_priv 	=	validate_field($_POST["yes_no_create"]);
		$select_priv	=	validate_field($_POST["yes_no_select"]);
		$insert_priv	= 	validate_field($_POST["yes_no_insert"]);
		$update_priv	=	validate_field($_POST["yes_no_update"]);
		$delete_priv	=	validate_field($_POST["yes_no_delete"]);
		$backup_priv	=	validate_field($_POST["yes_no_backup"]);
		$report_priv	=	validate_field($_POST["yes_no_report"]);

		PrivilegeController::updatePrivilege($id_priv, $desc_priv, $create_priv, $select_priv, $insert_priv, $update_priv, $delete_priv, $backup_priv, $report_priv);

		header("location:action.php?a=22");

		}else{
			header("location:action.php?a=23");
		}//if validate variables


    }else{

	if(isset($_POST["desc_priv"]) && isset($_POST["yes_no_create"]) && isset($_POST["yes_no_select"]) && isset($_POST["yes_no_insert"]) && isset($_POST["yes_no_update"]) && isset($_POST["yes_no_delete"]) && isset($_POST["yes_no_backup"]) && isset($_POST["yes_no_report"])){

	$desc_priv	 	=	validate_field($_POST["desc_priv"]);
	$create_priv 	=	validate_field($_POST["yes_no_create"]);
	$select_priv	=	validate_field($_POST["yes_no_select"]);
	$insert_priv	= 	validate_field($_POST["yes_no_insert"]);
	$update_priv	=	validate_field($_POST["yes_no_update"]);
	$delete_priv	=	validate_field($_POST["yes_no_delete"]);
	$backup_priv	=	validate_field($_POST["yes_no_backup"]);
	$report_priv	=	validate_field($_POST["yes_no_report"]);

	

	PrivilegeController::regPrivilege($desc_priv, $create_priv, $select_priv, $insert_priv, $update_priv, $delete_priv, $backup_priv, $report_priv);

	header("location:action.php?a=22");

	}else{
	//comeback to form new / update privilege
	header("location:action.php?a=23");
	}

	}// if($_POST["update"] == "true")

}//if($_SERVER["REQUEST_METHOD"] == "POST")
?>


<!-- ************************   End Process to Change About   **************************** -->
</div>   
</div>       
</div>        				

<!--TERMINO CONTENIDO -->
 			</div>

 		</div>
</section><!--/wrapper --> 			 	


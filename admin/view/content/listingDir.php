 <?php

include 'sql/DirSQL.php';

 ?>

 <section class="wrapper site-min-height">
<h3><i class="fa fa-angle-right"></i> Perfil | Administrador: <b><?php echo $user_name;  ?></b></h3>
<div class="row mt">
<div class="col-lg-12">
      <!-- INICIO CONTENIDO -->
<div class="container-fluid">
<div class="panel">
<div class="panel-heading">
  <h3 class="panel-title">Direcciones y Áreas administrativas SECODUVI</h3>
 </div>
 <div class="panel-body">
<!-- ************************   Begin List Privilegios   **************************** -->
<div class="col-lg-12">
	<?php  
	//last 14
	/*
	<h1 class="page-header">Listado de privilegios | <a href="action.php?a=14" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo privilegio</a>
    </h1>
	 */
	?>
    <h1 class="page-header">Listado de Direcciones | <a href="action.php?a=37" class="btn btn-primary"><i class="fa fa-plus"></i> Nueva Dirección</a>
    </h1>
</div>

<?php

$query_count = "SELECT COUNT(*) FROM directions WHERE status = '1'";
$query = " SELECT id_dir as ID, dir_name as Nombre FROM directions WHERE status = '1' ORDER BY id_dir";

$params = "";

DirSQL::getTableDir_Pag($query_count, $query, $params);

?>

</div>   
</div>       
</div>                

<!--TERMINO CONTENIDO -->
      </div>

    </div>
</section><!--/wrapper -->        


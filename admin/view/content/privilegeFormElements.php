<?php

include'../data/Form.php';
include 'sql/Combo.php';
include 'sql/Data.php';

if(isset($_GET["b"])){
		        $id_user_received = $_GET["b"];
		}

?>
<section class="wrapper site-min-height">
<h3><i class="fa fa-angle-right"></i> Perfil | Administrador: <b><?php echo $user_name;  ?></b></h3>
<div class="row mt">
<div class="col-lg-12">
      <!-- INICIO CONTENIDO -->
<div class="container-fluid">
<div class="panel">
<div class="panel-heading">
  <h3 class="panel-title">Privilegios</h3>
</div>
 <div class="panel-body">
<!-- ************************   Begin New Privilegios   **************************** -->

<!--************************- Begin Form New / Update Privilege  *******************-->
<div class="col-lg-12">
	<h1 class="page-header"><?php echo isset($id_user_received) ? 'Actualizar' : 'Nuevo' ?> Privilegio |
    <a href="action.php?a=23" class="btn btn-default">
        <i class="fa fa-list-ul"></i> Ver Listado</a>
    </h1>
</div> <!-- .col-lg-12 -->

<!-- <div class="row">
<div class="col-md-12"> -->
<!-- <div class="panel-body"> -->


<?php
$form = new Form('newPrivilege','POST','action.php?a=24', 'form', '', '');
?>

<legend>Datos del Privilegio</legend>
<div class="panel panel-default">
	<div class="panel-body">
<?php
//Datos del usuario si es edición
if(isset($id_user_received)) {
	//Update Process
      


 //Select data for values in form


 $query = "SELECT id_priv, desc_priv, create_priv, select_priv, insert_priv, update_priv, delete_priv, backup_priv, report_priv FROM privileges where id_priv = :id_priv";


 $select = new Data();

 $select::getConection();

  //$query = $sql;

  $result = $select::$cnx->prepare($query);

  //echo $id_user;

  //The parameters are sent the user in the form by user_name field, the object $user, to the user of the form
  $id_priv = $id_user_received;
  $result->bindParam(":id_priv", $id_priv);

  //We run the query the PDO connection
  $result->execute();

  $rows = $result->rowCount();

  if($rows > 0){
    $data = $result->fetch();

    //$privilegio = new Privilege();

    $id_priv = $data["id_priv"];
    $desc 	 = $data["desc_priv"];
    $create  = $data["create_priv"];
    $select  = $data["select_priv"];
    $insert  = $data["insert_priv"];
    $update  = $data["update_priv"];
    $delete  = $data["delete_priv"];
    $backup  = $data["backup_priv"];
    $report  = $data["report_priv"];
    //$desc = $id_user;

    //$privilegio->setDesc_priv($desc);

    //$desc = $privilegio->getDesc_priv($privilegio);

    //echo $desc;
	



  }else {
    echo "No Data in Query";
  }

  $value = $desc;

 $form -> addField(4, array(
        "field_name"    =>  "id_priv",
        "value"   =>  $id_priv
        ));
 
 $form -> addField(4, array(
        "field_name"    =>  "update",
        "value"   =>  "true"
        ));

  //Descripción del
$form -> addField(1, array(
"field_name"    =>  "desc_priv",
"class_label"   =>  "",
"label_field"   =>  "Nombre del privilegio",
"div_field"     =>  "",
"input_class"   =>  "col-md-8",
"readonly"      =>  "",
"disabled"      =>  "",
"value"         =>  $desc,
"maxlength"     =>  "",
"size"          =>  "",
"style"         =>  "",
"js"            =>  "",
"placeholder"   =>  "Descripción del privilegio",
"required"      =>  "required",
"autofocus"     =>  "autofocus"
));

echo '<div class="space"><br/></div>';

//echo $select;
if($create == "Y"){
	$valCreate = true;
}else{
	$valCreate = false;
}
$form -> addField(10,array(
	"field_name"	=>	"yes_no_radio_create",
	"selected"		=>  $valCreate,
	"class_label"	=>	"",
	"label_field"	=>	"¿Este privilegio puede crear contenido?",
	"name_option"	=>	"yes_no_create",
	"div_field"     =>  "",
	"input_class"   =>  "col-md-8",
	"readonly"      =>  "",
	"disabled"      =>  "",
	"value_no"		=>	"N",
	"value_yes"		=>	"Y"
	));

if($select == "Y"){
	$valSelect = true;
}else{
	$valSelect = false;
}
$form -> addField(10,array(
	"field_name"	=>	"yes_no_radio_select",
	"selected"		=> 	$valSelect,
	"class_label"	=>	"",
	"label_field"	=>	"¿Este privilegio puede generar consultas?",
	"name_option"	=>	"yes_no_select",
	"div_field"     =>  "",
	"input_class"   =>  "col-md-8",
	"readonly"      =>  "",
	"disabled"      =>  "",
	"value_no"		=>	"N",
	"value_yes"		=>	"Y"
	));

if($insert == "Y"){
	$valInsert = true;
}else{
	$valInsert = false;
}
$form -> addField(10,array(
	"field_name"	=>	"yes_no_radio_insert",
	"selected"		=> 	$valInsert,
	"class_label"	=>	"",
	"label_field"	=>	"¿Este privilegio puede insertar información?",
	"name_option"	=>	"yes_no_insert",
	"div_field"     =>  "",
	"input_class"   =>  "col-md-8",
	"readonly"      =>  "",
	"disabled"      =>  "",
	"value_no"		=>	"N",
	"value_yes"		=>	"Y"
	));

if($update == "Y"){
	$valUpdate = true;
}else{
	$valUpdate = false;
}
$form -> addField(10,array(
	"field_name"	=>	"yes_no_radio_update",
	"selected"		=> 	$valUpdate,
	"class_label"	=>	"",
	"label_field"	=>	"¿Este privilegio puede actualizar información?",
	"name_option"	=>	"yes_no_update",
	"div_field"     =>  "",
	"input_class"   =>  "col-md-8",
	"readonly"      =>  "",
	"disabled"      =>  "",
	"value_no"		=>	"N",
	"value_yes"		=>	"Y"
	));

if($delete == "Y"){
	$valDelete = true;
}else{
	$valDelete = false;
}
$form -> addField(10,array(
	"field_name"	=>	"yes_no_radio_delete",
	"selected"		=> 	$valDelete,
	"class_label"	=>	"",
	"label_field"	=>	"¿Este privilegio puede borrar información?",
	"name_option"	=>	"yes_no_delete",
	"div_field"     =>  "",
	"input_class"   =>  "col-md-8",
	"readonly"      =>  "",
	"disabled"      =>  "",
	"value_no"		=>	"N",
	"value_yes"		=>	"Y"
	));


if($backup == "Y"){
	$valBackup = true;
}else{
	$valBackup = false;
}
$form -> addField(10,array(
	"field_name"	=>	"yes_no_radio_backup",
	"selected"		=> 	$valBackup,
	"class_label"	=>	"",
	"label_field"	=>	"¿Este privilegio puede respaldar información?",
	"name_option"	=>	"yes_no_backup",
	"div_field"     =>  "",
	"input_class"   =>  "col-md-8",
	"readonly"      =>  "",
	"disabled"      =>  "",
	"value_no"		=>	"N",
	"value_yes"		=>	"Y"
	));

if($report == "Y"){
	$valReport = true;
}else{
	$valReport = false;
}
$form -> addField(10,array(
	"field_name"	=>	"yes_no_radio_report",
	"selected"		=> 	$valReport,
	"class_label"	=>	"",
	"label_field"	=>	"¿Este privilegio puede generar reportes?",
	"name_option"	=>	"yes_no_report",
	"div_field"     =>  "",
	"input_class"   =>  "col-md-8",
	"readonly"      =>  "",
	"disabled"      =>  "",
	"value_no"		=>	"N",
	"value_yes"		=>	"Y"
	));

echo '<div class="space"><br/></div>';

$form -> addField(3, array(
  "name" 			=>  "save_priv",
  "type_button"     =>  "btn btn-primary",
  "icon"		    =>  "fa fa-arrow-right",
  "disabled"      =>  "",
  "legend"    		=>  "Actualizar Privilegio"

  ));




  }//check if is update or register
  else{
    $value = "";

  

//Register Process

//Descripción del
$form -> addField(1, array(
"field_name"    =>  "desc_priv",
"class_label"   =>  "",
"label_field"   =>  "Nombre del privilegio",
"div_field"     =>  "",
"input_class"   =>  "col-md-8",
"readonly"      =>  "",
"disabled"      =>  "",
"value"         =>  "",
"maxlength"     =>  "",
"size"          =>  "",
"style"         =>  "",
"js"            =>  "",
"placeholder"   =>  "Descripción del privilegio",
"required"      =>  "required",
"autofocus"     =>  "autofocus"
));

echo '<div class="space"><br/></div>';



$form -> addField(10,array(
	"field_name"	=>	"yes_no_radio_create",
	"selected"		=> 	"",
	"class_label"	=>	"",
	"label_field"	=>	"¿Este privilegio puede crear contenido?",
	"name_option"	=>	"yes_no_create",
	"div_field"     =>  "",
	"input_class"   =>  "col-md-8",
	"readonly"      =>  "",
	"disabled"      =>  "",
	"value_no"		=>	"N",
	"value_yes"		=>	"Y"
	));

$form -> addField(10,array(
	"field_name"	=>	"yes_no_radio_select",
	"selected"		=> 	"",
	"class_label"	=>	"",
	"label_field"	=>	"¿Este privilegio puede generar consultas?",
	"name_option"	=>	"yes_no_select",
	"div_field"     =>  "",
	"input_class"   =>  "col-md-8",
	"readonly"      =>  "",
	"disabled"      =>  "",
	"value_no"		=>	"N",
	"value_yes"		=>	"Y"
	));

$form -> addField(10,array(
	"field_name"	=>	"yes_no_radio_insert",
	"selected"		=> 	"",
	"class_label"	=>	"",
	"label_field"	=>	"¿Este privilegio puede insertar información?",
	"name_option"	=>	"yes_no_insert",
	"div_field"     =>  "",
	"input_class"   =>  "col-md-8",
	"readonly"      =>  "",
	"disabled"      =>  "",
	"value_no"		=>	"N",
	"value_yes"		=>	"Y"
	));

$form -> addField(10,array(
	"field_name"	=>	"yes_no_radio_update",
	"selected"		=> 	"",
	"class_label"	=>	"",
	"label_field"	=>	"¿Este privilegio puede actualizar información?",
	"name_option"	=>	"yes_no_update",
	"div_field"     =>  "",
	"input_class"   =>  "col-md-8",
	"readonly"      =>  "",
	"disabled"      =>  "",
	"value_no"		=>	"N",
	"value_yes"		=>	"Y"
	));

$form -> addField(10,array(
	"field_name"	=>	"yes_no_radio_delete",
	"selected"		=> 	"",
	"class_label"	=>	"",
	"label_field"	=>	"¿Este privilegio puede borrar información?",
	"name_option"	=>	"yes_no_delete",
	"div_field"     =>  "",
	"input_class"   =>  "col-md-8",
	"readonly"      =>  "",
	"disabled"      =>  "",
	"value_no"		=>	"N",
	"value_yes"		=>	"Y"
	));

$form -> addField(10,array(
	"field_name"	=>	"yes_no_radio_backup",
	"selected"		=> 	"",
	"class_label"	=>	"",
	"label_field"	=>	"¿Este privilegio puede respaldar información?",
	"name_option"	=>	"yes_no_backup",
	"div_field"     =>  "",
	"input_class"   =>  "col-md-8",
	"readonly"      =>  "",
	"disabled"      =>  "",
	"value_no"		=>	"N",
	"value_yes"		=>	"Y"
	));

$form -> addField(10,array(
	"field_name"	=>	"yes_no_radio_report",
	"selected"		=> 	"",
	"class_label"	=>	"",
	"label_field"	=>	"¿Este privilegio puede generar reportes?",
	"name_option"	=>	"yes_no_report",
	"div_field"     =>  "",
	"input_class"   =>  "col-md-8",
	"readonly"      =>  "",
	"disabled"      =>  "",
	"value_no"		=>	"N",
	"value_yes"		=>	"Y"
	));





echo '<div class="space"><br/></div>';

$form -> addField(3, array(
  "name" 			=>	"save_reg",
  "type_button"    	=>  "btn btn-primary",
  "icon"		   	=>  "fa fa-save",
  "disabled"      	=>  "",
  "legend"    		=>  "Registrar"

  ));


  }//if(isset($id_user))



?>
</div>
</div><!-- .panel panel-default -->            
<?php

$form->closeForm();

?>
<!-- </div> --><!-- .panel-body -->

</div>
</div>


<!-- ************************   End New Privilegios   **************************** -->
</div><!-- .panel-body -->
</div><!-- .panel -->
</div><!-- .container-fluid -->

<!--TERMINO CONTENIDO -->
</div><!-- .container-fluid -->

</div><!-- .row mt -->
</section><!--/wrapper -->     
 <?php

include '../controller/UserController.php';
include '../helps/helps.php';


 ?>

 <section class="wrapper site-min-height">
<h3><i class="fa fa-angle-right"></i> Perfil | Administrador: <b><?php echo $user_name;  ?></b></h3>
<div class="row mt">
<div class="col-lg-12">
 			<!-- INICIO CONTENIDO -->
<div class="container-fluid">
<div class="panel">
<div class="panel-heading">
	<h3 class="panel-title">Editar Perfil</h3>
 </div>
 <div class="panel-body">
<!-- ************************   Begin Process to Change Password   **************************** -->

<?php
//Verify the method to receive form
if($_SERVER["REQUEST_METHOD"] == "POST"){

	if(isset($_POST["id_user"]) && isset($_POST["new_pass"])){
		//echo "Processing....";

	$id_user 	 	=	validate_field($_POST["id_user"]);
	$new_pass	 	=	validate_field($_POST["new_pass"]);


	UserController::updatePass($id_user, $new_pass);

	header("location:action.php?a=10&b=$id_user");

	 }

	}else{
header("location:action.php?a=9");
}//if($_SERVER["REQUEST_METHOD"] == "POST")

?>

<!-- ************************   End Process to Change Password   **************************** -->
</div>   
</div>       
</div>        				

<!--TERMINO CONTENIDO -->
 			</div>

 		</div>
</section><!--/wrapper --> 			 	


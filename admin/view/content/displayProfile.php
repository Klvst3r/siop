 <?php

include '../controller/UserController.php';
//include '../controller/HistoryController.php';

$user_name = $_SESSION["user"]["user_name"];

$id_user = $_SESSION["user"]["id_user"];

$user  = UserController::getProfile($id_user);

//$history = HistoryController::getLastAccess($id_user);

/* Insertar el ultimo acceso del usuario y mandarlo la tabla Users */


 ?>

 <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Perfil | Administrador: <b><?php echo $user_name;  ?></b></h3>
          	<?php //echo $user->getId_user(); ?>
          	<div class="row mt">
          		<div class="col-lg-12">
          		<!-- INICIO CONTENIDO -->
            
                <div class="container-fluid">
			


                    <div class="panel">
                    <div class="panel-heading">
                    <h3 class="panel-title">Perfil del Administrador</h3>
                    </div>
                    <div class="panel-body">
                        

                        <!--  begin div content -->
                        <div class="main-content">
            

<div class="row">
    <div class="col-md-3">

<div class="row">
    <div class="col-md-12 col-sm-5">

        <img src="../assets/img/users/<?php echo $user->getUser_photo(); ?>" style="max-width:100%;border-top-left-radius: 3px;border-top-right-radius: 3px;">
        


    
        <p>
        	

        	<div class="panel panel-primary">
                        <div class="panel-heading">
                        <h3 class="panel-title text-center"><?php echo $user->getName(); ?></h3>
                        </div>
                        
                    </div>   

        </p>

    </div>
    </div>

        <br>
        <div class="panel panel-default fadeInDown animation-delay2">
            <div class="panel-heading">
                Sobre el usuario
            </div>
            <div class="panel-body">
                <p class="text-sm"><span><span class="fa fa-calendar"></span> Registro desde:  <b><?php echo $user->getDate_reg();  ?></b></span></p>
                <p class="text-justify"><?php echo $user->getAbout(); ?></p>
            </div>
        </div>

    </div>
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Información del Usuario</div>
                    <div class="panel-body">
                        <ul class="list-unstyled list-info">
                            <li>
                                <span class="text-info fa fa-user fa-fw"></span>
                                <?php echo $user->getName(); ?>
                            </li>

                            <?php if($user->getUser_email() != ''){ ?>
                            <li>
                                <span class="text-info fa fa-envelope fa-fw"></span>
                                <?php echo $user->getUser_email(); ?>
                            </li>
                            <?php } ?>
                            
                            <li>
                                <span class="text-info fa fa-university fa-fw"></span>
                                Dirección: <?php echo $user->getDirection(); ?>
                            </li>
                            
                            <li>
                                <span class="text-info fa fa-home fa-fw"></span>
                                Departamento: <?php echo $user->getDepartament(); ?>
                            </li>
                            <li>
                                <span class="text-info fa fa-code"></span>
                                Cargo: <?php echo $user->getUser_position(); ?>
                            </li>
                            <li>
                                <span class="text-info fa fa-coffee"></span>
                                Resumen: <?php echo $user->getUser_resume(); ?>
                            </li>
                            
                            <?php if($user->getUser_tel() != ''){ ?>
                            <li>
                                <span class="text-info fa fa-phone fa-fw"></span>
                                <?php echo $user->getUser_tel(); 
                                
                                if($user->getUser_ext() != ''){ 
                                	echo " Ext. " . $user->getUser_ext(); 
                                }
                               ?>
                            </li>
                            <?php } ?>
                            
                            <?php if($user->getUser_movil() != ''){ ?>
                            <li>
                                <span class="text-info fa fa-mobile-phone fa-fw"></span>
                                2461645449
                            </li>
                            <?php } ?>

                        </ul>

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Privilegios | <?php echo $user->getDesc_priv(); ?>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item"><i class="fa fa-code "></i> Administrar | Crear 
                        <?php if($user->getCreate_priv() == 'Y'){ ?>
                        	<span class="pull-right label label-success"> <?php echo "Si"; ?> </span></li>
                        <?php }else{  ?>
							<span class="pull-right label label-danger"><?php echo "No"; ?></span></li>
                        <?php } ?>


                        <li class="list-group-item"><i class="fa fa-archive "></i> Realizar Consultas 
						<?php if($user->getSelect_priv() == 'Y'){ ?>
							<span class="pull-right label label-success"><?php echo "Si"; ?></span></li>
						<?php }else{  ?>
							<span class="pull-right label label-danger"><?php echo "No"; ?></span></li>
						<?php } ?>

                        	
                        <li class="list-group-item"><i class="fa fa-pencil "></i> Registrar Información 
                        <?php if($user->getInsert_priv() == 'Y'){ ?>
                        	<span class="pull-right label label-success"><?php echo "Si"; ?></span></li>
                        <?php }else{  ?>
							<span class="pull-right label label-danger"><?php echo "No"; ?></span></li>
						<?php } ?>
                        
						
                        <li class="list-group-item"><i class="fa fa-file-text "></i> Actualización | Modificación de información 
						<?php if($user->getUpdate_priv() == 'Y'){ ?>
                        	<span class="pull-right label label-success"><?php echo "Si"; ?></span></li>
                        <?php }else{  ?>
							<span class="pull-right label label-danger"><?php echo "No"; ?></span></li>
						<?php } ?>

                        <li class="list-group-item"><i class="fa fa-times "></i> Eliminación de información 
						<?php if($user->getDelete_priv() == 'Y'){ ?>
							<span class="pull-right label label-success"><?php echo "Si"; ?></span></li>
                        <?php }else{  ?>
							<span class="pull-right label label-danger"><?php echo "No"; ?></span></li>
						<?php } ?>


                        <li class="list-group-item"><i class="fa fa-database "></i> Respaldo de información 
						<?php if($user->getBackup_priv() == 'Y'){ ?>
                        	<span class="pull-right label label-success"><?php echo "Si"; ?></span></li>
                        <?php }else{  ?>
							<span class="pull-right label label-danger"><?php echo "No"; ?></span></li>
						<?php } ?>



                        <li class="list-group-item"><i class="fa fa-print "></i> Generación de reportes 
                        <?php if($user->getReport_priv() == 'Y'){ ?>
                        	<span class="pull-right label label-success"><?php echo "Si"; ?></span></li>
                        <?php }else{  ?>
							<span class="pull-right label label-danger"><?php echo "No"; ?></span></li>
						<?php } ?>



                    </ul>
                </div>
            </div>
        </div>


        <div class="panel panel-default">
            <div class="panel-heading">
                Actividad Reciente
            </div>
           
                <li class="list-group-item">
                    <div class="media">
                        <a class="pull-left" href="#">
                            <!-- <img src="images/faces/1a.png" class="media-object thumb-sm"> -->
                        </a>
                        <div class="media-body">
                            <p class="text-sm pull-right">Ultimo Acceso: <b><?php echo  $user->getDate_access(); ?></b></p>
                            <!-- SELECT * FROM `history_access` ORDER BY `date_access` DESC LIMIT 1 -->
                            <p class="media-heading">
                            	<div class="stats">
    							<p class="stat"><span class="label label-info"><?php echo $user->getId_history(); ?></span> Accesos al sistema</p>
    							<p class="stat"><span class="label label-success"><?php echo $user->getIp(); ?></span> Ip del ultimo acceso</p>
    							
                                
                                <?php 
                                //$fechaInicio = strftime("%Y-%m-%d %H:%M:%S", time());
                                $fechaInicio = $user->getDate_reg();

                                //echo "Esta es la fecha de Inicio Obtenida: " . $fechaInicio;

                                //echo "<br/>";

                                //$fechaFin = date('2017-11-07 09:53:00');
                                $fechaFin = date('Y-m-d H:i:s');
                                //$fechaFin = date('Y-');
                                //echo tiempoTranscurridoFechas($fechaInicio,$fechaFin);
                                
                                ?>

                                <p class="stat"><span class="label label-danger">
                                <?php $user  = UserController::getInSystem($fechaInicio, $fechaFin);    
                                ?>
                                </span>Tiempo dentro del sistema</p>

								</div>
                            </p> 
                            <p><!-- Ultimo Acceso: <b>01/11/2017</b> --></p>
                        </div>
                    </div>
                </li>
                
                
                    
                    

                </ul>
            </div>
        </div>
        <div class="col-sm-4 col-md-offset-3">
                        <!-- <button type="submit" class="btn btn-danger pull-right btn-block btn-sm">Modificar Información de Perfíl</button> -->
                        <div class="page-header"><a href="action.php?a=2" class="btn btn-danger"><i class="fa fa-edit"></i> Modificar Información de Perfíl</a>
                      </div>
    </div>

	

</div>



<!-- End Div Content -->

                        
                    

                        <!-- <a href="#" class="btn btn-default btn-large"><i class="fa fa-users"></i> Operaciones</a> -->
                        <div class="space"></div>
                        <p>
                        <!-- <button class="btn btn-danger btn-sm" onclick="confirmar('blank.html')">Eliminar</button> -->
                      </p>
                    </div>
                  </div>   
                </div>       

            <!--TERMINO CONTENIDO -->
          		</div>
              
          	</div>
			
		      </section><!--/wrapper -->
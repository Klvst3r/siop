<?php

include '../controller/OrgController.php';

$org  = OrgController::getOrg();

?>

<section class="wrapper site-min-height">
<h3><i class="fa fa-angle-right"></i> Admin Panel</h3>
<div class="row mt">
	
<div class="col-lg-12">
		<!-- INICIO CONTENIDO -->

    <div class="container-fluid">
        <div class="panel">
        <div class="panel-heading">
        <h3 class="panel-title">Organización</h3>
        </div>
        <div class="panel-body">


    <div class="space"></div>

        <div class="panel panel-primary">
        <div class="panel-heading">
        <h3 class="panel-title">Configuración</h3>
        </div>
        <div class="panel-body">
        <!-- Begin Content -->
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-briefcase"></i> </span>
            <h4><?php echo $org->getOrg_nick();  ?></h4>
          </div>
          <div class="widget-content">
            <div class="row-fluid">
              <div class="span6">
                <table class="">
                  <tbody>
                    <tr>
                      <td><h4><?php echo $org->getOrg_name(); ?></h4></td>
                    </tr>
                    <tr>
                      <td><?php echo $org->getOrg_dir(); ?></td>
                    </tr>
                    <tr>
                      <td><?php echo $org->getOrg_pob(); ?></td>
                    </tr>
                    <tr>
                      <td><?php echo "CP: " . $org->getOrg_cp(); ?></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="span6">
                <table class="table table-bordered table-invoice">
                  <tbody>
                    <tr>
                    </tr><tr>
                      <td class="width30">Telefono:</td>
                      <td class="width70"><strong><?php echo $org->getOrg_tel(); ?></strong></td>
                    </tr>
                    <tr>
                      <td>Ext:</td>
                      <td><strong><?php echo $org->getOrg_ext(); ?></strong></td>
                    </tr>
                    <tr>
                      <td>Email:</td>
                      <td><strong><?php echo $org->getOrg_email(); ?></strong></td>
                    </tr>
                  
                    </tbody>
                  
                </table>
                <div class="pull-right">
                  
                  <br>
                  <a class="btn btn-primary btn-large pull-right" href="action.php?a=4&id=<?php echo $org->getId_user();  ?>">Modificar</a> </div>
              </div>
              </div>
            </div>

          
          </div>
        </div>

        <!-- Finish Content -->        
        </div>
        </div>   
        

            
    <div class="space"></div>
    <p></p>
        </div>
      </div>   
    </div>       

<!--TERMINO CONTENIDO -->
</div>

</div>
</section><!--/wrapper -->
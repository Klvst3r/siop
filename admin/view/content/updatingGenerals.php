 <?php

include '../controller/UserController.php';
include '../helps/helps.php';

 ?>

 <section class="wrapper site-min-height">
<h3><i class="fa fa-angle-right"></i> Perfil | Administrador: <b><?php echo $user_name;  ?></b></h3>
<div class="row mt">
<div class="col-lg-12">
 			<!-- INICIO CONTENIDO -->
<div class="container-fluid">
<div class="panel">
<div class="panel-heading">
	<h3 class="panel-title">Editar Perfil</h3>
 </div>
 <div class="panel-body">
<!-- ************************   Begin Process to Change Generals   **************************** -->

<?php
//echo "Processing....";
//Verify the method to receive form
if($_SERVER["REQUEST_METHOD"] == "POST"){
	//Receive fields to validate first and update later
	if(isset($_POST["id_user"]) && isset($_POST["name"]) && isset($_POST["user_name"]) && isset($_POST["user_resume"]) && isset($_POST["user_movil"]) && isset($_POST["user_email"]) && isset($_POST["user_tel"]) && isset($_POST["user_ext"])){

	//Validate fields from the form and asign to the variables in order to work with them
	$id_user 	 =	validate_field($_POST["id_user"]);
	$name	 	 =	validate_field($_POST["name"]);
	$user_name 	 = 	validate_field($_POST["user_name"]);
	$user_resume = 	validate_field($_POST["user_resume"]);
	$user_movil  =	validate_field($_POST["user_movil"]);
	$user_email  = 	validate_field($_POST["user_email"]);
	$user_tel 	 =  validate_field($_POST["user_tel"]);
	$user_ext 	 = 	validate_field($_POST["user_ext"]);


	//Execute update
	UserController::updateGenerals($id_user, $name, $user_name, $user_resume, $user_movil, $user_email, $user_tel, $user_ext);

	//After Update view the values in the form continue updating values
	header("location:action.php?a=10&b=$id_user");

	}

}else{
//In case fail register the user send again to the view of Profile User
header("location:action.php?a=9");
}//if($_SERVER["REQUEST_METHOD"] == "POST")
?>


<!-- ************************   End Process to Change Generals   **************************** -->
</div>   
</div>       
</div>        				

<!--TERMINO CONTENIDO -->
 			</div>

 		</div>
</section><!--/wrapper --> 			 	


<?php

  include '../controller/UserController.php';

  include '../helps/helps.php';

  include'../data/Form.php';

  include 'sql/Combo.php';

  


 ?>
 <section class="wrapper site-min-height">
    
    <h3><i class="fa fa-angle-right"></i> Perfil | Administrador: <b><?php echo $user_name;  ?></b></h3>
    <div class="row mt">
        <div class="col-lg-12">
            <!-- INICIO CONTENIDO -->

            <div class="container-fluid">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Registrar Nuevo Usuario</h3>
                    </div>
                    <div class="panel-body">


            <?php //echo "ID User: " . $id_user; ?>

                        <div class="right_col" role="main"> <!-- page content -->
                            <div class="">
                                <div class="page-title">

                                    <!-- content -->
                                    <br>
                                    <div class="row">


                                        <!-- Begin Perfil Forms -->


                                        <div class="col-md-10 col-md-offset-1 col-xs-12 col-sm-12">
                                            <?php include "../assets/lib/alerts.php";
                                                profile(); //call alert function
                                            ?>
                                            <div class="x_panel">
                                                <div class=""><!-- x_title -->
                                                    <h2>Informacion personal</h2>
                                                    <ul class="nav navbar-right panel_toolbox">

                                                    </ul>

                                                </div>
                                                <!--  *************************** Begin Paneles  *****************************  -->
  <!-- ADMIN PROFILE -->
        <section class="wrap" id="admin-course">
          <div class="container">




            <div class="row">
              <div class="col-sm-12 ">
               <!-- <p>
                 <a id="btnTabAdmin" href="#form-add-section" aria-controls="form-add-section" data-toggle="tab" role="tab" class="btn btn-info">Añadir Sección</a>
               </p> -->
               <!-- NAV TABS -->
               <ul class="nav nav-tabs" role="tablist">
                 <li role="presentation">
                   <a id="tabAdmin1" href="#form-profile" aria-controls="form-profile" data-toggle="tab" role="tab">Perfil</a>
                 </li>
                 <li role="presentation">
                   <a id="tabAdmin2" href="#form-generals" aria-controls="form-generals" data-toggle="tab" role="tab">Generales</a>
                 </li>
                 <li role="presentation">
                  <a id="tabAdmin3" href="#form-about" aria-controls="form-about" data-toggle="tab" role="tab">Sobre el Usuario</a>
                </li>
                <li role="presentation" class="active">
                  <a id="tabAdmin4" href="#form-password" aria-controls="form-password" data-toggle="tab" role="tab">Password</a>
                </li>
              </ul>
              <!-- END NAV TABS -->
              <br/>
              <!-- TAB CONTENT -->
              <div class="tab-content">
            
<!-- SECTION PROFILE -->
<div role="tabpanel" class="panel panel-default tab-pane  fade" id="form-profile">
    <div class="panel-body">

    <h3>Perfíl de Usuario</h3>

       <ul class="list-group">
           <li class="list-group-item">

<?php
/* ---------------------------------------------------------*/

if($_SERVER["REQUEST_METHOD"] == "POST"){
  //Receive fields to validate first and update later
  if(isset($_POST["id_user"]) && isset($_POST["about"])){

  //Validate fields from the form and asign to the variables in order to work with them
  $id_user      =  validate_field($_POST["id_user"]);
  $about         =  validate_field($_POST["about"]);
  
  
  //Execute update
  UserController::updateAbout($id_user, $about);
  

  //Get all values of Profiles Selects 
  $select = UserController::getSelects($id_user);

  $id_priv  = $select->getId_priv(); 
  $position = $select->getUser_position();

  //Pull values to variables
  $id_privilege   = $select->getId_priv();  
  $desc_priv      = $select->getDesc_priv();
  $privilege      = $id_privilege . " - " . $desc_priv;


  $id_stat        = $select->getId_user_status();
  $desc_stat      = $select->getDesc_user_status();
  $status         = $id_stat . " - " - $desc_stat;

  $id_direction   = $select->getId_dir();
  $desc_direction = $select->getDir_name();
  $dir            = $id_direction . " - " . $desc_direction;

  $id_departament = $select->getId_dep();
  $desc_dep       = $select->getDepartament();
  $departament    = $id_departament . " - " . $desc_dep;


  $generals = UserController::getGenerals($id_user); 

  $get_name         = $generals->getName();
  $get_user_name    = $generals->getUser_name();
  $get_user_resume  = $generals->getUser_resume();
  $get_user_movil   = $generals->getUser_movil();
  $get_user_email   = $generals->getUser_email();
  $get_user_tel     = $generals->getUser_tel();
  $get_user_ext     = $generals->getUser_ext();

  

  $aboutUser = UserController::getAbout($id_user);

  $get_about        = $aboutUser->getAbout(); //check this
     

  
            //reference Action.php?a=5
            $form = new Form('regProfile','POST','', 'form', '', '');

            //$privilege obtain privilege
            //privilege
            $query = "select id_priv, desc_priv from privileges EXCEPT where id_priv <> '$id_priv'";
            $combo = new combo($query,"privilege","selectPriv", "$privilege" ,"Privilegio","","disabled","","1");
              
            //status
            $query = "select id_user_status, desc_user_status from status_user order by id_user_status";
            $combo = new combo($query,"status","selectStatus", "$status" ,"Estado en el Sistema","","disabled","","1");

             //Direction
            $query = "SELECT id_dir, dir_name FROM directions order by dir_name";
            $combo = new combo($query,"direction","selectDir", "$dir" ,"Direccion","","disabled","","1");

            //Departaments
            $query = "select id_dep, departament from departaments order by departament";
            $combo = new combo($query,"departament","SelectDep", "$departament" ,"Departamento","","disabled","","1");

            //Position
            //$position = $user->getUser_position();
            $form -> addField(1, array(
            "field_name"    =>  "user_position",
            "class_label"   =>  "",
            "label_field"   =>  "Puesto",
            "div_field"     =>  "",
            "input_class"   =>  "col-md-12",
            "readonly"      =>  "",
            "disabled"      =>  "disabled",
            "value"         =>  "$position",
            "maxlength"     =>  "",
            "size"          =>  "",
            "style"         =>  "",
            "js"            =>  "",
            "placeholder"   =>  "Escriba el puesto del usuario...",
            "required"      =>  "required",
            "autofocus"     =>  "autofocus"
            ));

            echo '<div class="space"></div>';
            echo '<div class="ln_solid"></div>';

              //Send
              $form -> addField(3, array(
              "name"           =>  "save_perfil",
              "type_button"    =>  "btn btn-success",
              "icon"           =>  "fa fa-save",
              "disabled"       =>  "disabled",
              "legend"         =>  "Guardar Perfil y continuar"

              ));

              $form->closeForm();

    }
  

 }else{
    header("location:action.php?a=9");
 }  
/* ---------------------------------------------------------*/
               ?>
               <!-- <button type="submit" class="btn btn-success"><i class=""></i> Actualizar Perfíl </button> -->
           </li>
       </ul><!-- ul  -->
    </div>
</div><!-- .form-list -->
<!-- END SECTION PROFILE -->

<!-- BEGIN SECTION GENERALS -->
<div role="tabpanel" class="panel panel-default tab-pane fade" id="form-generals">
  <div class="panel-body">
    <h3>Información General</h3>
    <?php
    $form = new Form('regGenerals','POST','', 'form', '', '');
        
    $form -> addField(1, array(
      "field_name"    =>  "name",
      "class_label"   =>  "",
      "label_field"   =>  "Nombre y Apellidos",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "disabled",
      "value"         =>  "$get_name",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Escriba Nombre y Apellidos",
      "required"      =>  "required",
      "autofocus"     =>  "autofocus"
      ));

    $form -> addField(1, array(
    "field_name"    =>  "user_name",
    "label_field"   =>  "Nombre de Usuario",
    "class_label"   =>  "",
    "div_field"     =>  "",
    "input_class"   =>  "col-md-12",
    "readonly"      =>  "",
    "disabled"      =>  "disabled",
    "value"         =>  "$get_user_name",
    "maxlength"     =>  "",
    "size"          =>  "",
    "style"         =>  "",
    "js"            =>  "",
    "placeholder"   =>  "Escriba su Nickname...",
    "required"      =>  "required",
    "autofocus"     =>  "autofocus"
    ));

    $form -> addField(1, array(
    "field_name"    =>  "user_resume",
    "label_field"   =>  "Resumen del Usuario",
    "class_label"   =>  "",
    "div_field"     =>  "",
    "input_class"   =>  "col-md-12",
    "readonly"      =>  "",
    "disabled"      =>  "disabled",
    "value"         =>  "$get_user_resume",
    "maxlength"     =>  "",
    "size"          =>  "",
    "style"         =>  "",
    "js"            =>  "",
    "placeholder"   =>  "Breve Resumen del Usuario...",
    "required"      =>  "required",
    "autofocus"     =>  "autofocus"
    ));

  $form -> addField(7, array(
    "field_name"    =>  "user_movil",
    "label_field"   =>  "Celular",
    "class_label"   =>  "",
    "div_field"     =>  "",
    "input_class"   =>  "col-md-12",
    "readonly"      =>  "",
    "disabled"      =>  "disabled",
    "value"         =>  "$get_user_movil",
    "maxlength"     =>  "10",
    "size"          =>  "",
    "style"         =>  "",
    "js"            =>  "",
    "placeholder"   =>  "Numero de celular...",
    "required"      =>  "required",
    "autofocus"     =>  "autofocus"
    ));


  $form -> addField(8, array(
  "field_name"    =>  "user_email",
  "label_field"   =>  "Correo Electrónico",
  "class_label"   =>  "",
  "div_field"     =>  "",
  "input_class"   =>  "col-md-12",
  "readonly"      =>  "",
  "disabled"      =>  "disabled",
  "value"         =>  "$get_user_email",
  "maxlength"     =>  "",
  "size"          =>  "",
  "style"         =>  "",
  "js"            =>  "",
  "placeholder"   =>  "Escriba su email...",
  "required"      =>  "required",
  "autofocus"     =>  "autofocus"
  ));


  $form -> addField(7, array(
    "field_name"    =>  "user_tel",
    "label_field"   =>  "Telefono",
    "class_label"   =>  "",
    "div_field"     =>  "",
    "input_class"   =>  "col-md-12",
    "readonly"      =>  "",
    "disabled"      =>  "disabled",
    "value"         =>  "$get_user_tel",
    "maxlength"     =>  "10",
    "size"          =>  "10",
    "style"         =>  "",
    "js"            =>  "",
    "placeholder"   =>  "Numero telefónico...",
    "required"      =>  "required",
    "autofocus"     =>  "autofocus"
    ));

  $form -> addField(7, array(
    "field_name"    =>  "user_ext",
    "label_field"   =>  "Extensión",
    "class_label"   =>  "",
    "div_field"     =>  "",
    "input_class"   =>  "col-md-12",
    "readonly"      =>  "",
    "disabled"      =>  "disabled",
    "value"         =>  "$get_user_ext",
    "maxlength"     =>  "",
    "size"          =>  "",
    "style"         =>  "",
    "js"            =>  "",
    "placeholder"   =>  "Extensión de oficina...",
    "required"      =>  "required",
    "autofocus"     =>  "autofocus"
  ));

//echo '<button type="submit" class="btn btn-success">Añadir </button> o <a href="" title="">Cancelar</a>';
//Send


echo '<div class="space"></div>';
echo '<div class="ln_solid"></div>';

 $form -> addField(3, array(
  "name"           =>  "save_generals",
  "type_button"    =>  "btn btn-success",
  "icon"           =>  "fa fa-save",
  "disabled"       =>  "disabled",
  "legend"         =>  "Guardar Datos Generales y continuar"

 ));

$form->closeForm();

//echo '</form>';
    ?>


  </div><!-- .panel-body -->
</div><!-- .form-add-section -->
<!-- END SECTION GENERALS -->

<!-- BEGIN SECTION ABOUT -->
<div role="tabpanel" class="panel panel-default tab-pane fade" id="form-about">
<div class="panel-body">
  <h3>Sobre el Usuario</h3>
  <?php
  $form = new Form('editAbout','POST','', 'form', '', '');



  $form -> addField(9, array(
          "field_name"    =>  "about",
          "label_field"   =>  "Breve descripción del Usuario:",
          "readonly"      =>  "",
          "disabled"      =>  "disabled",
          "value"         =>  "about",
          "rows"          =>  "3",
          "cols"          =>  "",
          "style"         =>  "",
          "js"            =>  "",
          "placeholder"   =>  "Sobre el usuario...",
          "content"       =>  "$get_about",
          "required"      =>  ""

        ));

 //echo '<button type="submit" class="btn btn-info">Añadir Informacion</button> ';
 echo '<div class="space"></div>';
echo '<div class="ln_solid"></div>';

 $form -> addField(3, array(
  "name"           =>  "save_info",
  "type_button"    =>  "btn btn-info",
  "icon"           =>  "fa fa-save",
  "disabled"       =>  "disabled",
  "legend"         =>  "Guardar Información y continuar"

 ));


$form->closeForm();
?>


  </div><!-- .panel-body -->
</div><!-- .form-add-lecture -->
<!-- END SECTION ABOUT -->

<!-- BEGIN SECTION PASSWORD -->
<div role="tabpanel" class="panel panel-default tab-pane active" id="form-password">
  <div class="panel-body">
    <h3>Asignar Contraseña</h3>
    <?php
    $form = new Form('editPass','POST','action.php?a=34', 'form', 'form-horizontal form-label-left', '');

      $form -> addField(4, array(
      "field_name"    =>  "id_user",
      "value"   =>  $id_user
      ));
              
      $form -> addField(2, array(
      "field_name"    =>  "password",
      "class_label"   =>  "",
      "label_field"   =>  "Escriba su Password",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  "",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "**********",
      "required"      =>  "required",
      "autofocus"     =>  "autofocus"
      ));

   
    echo '<div class="space"></div>';
    echo '<div class="ln_solid"></div>';


     $form -> addField(3, array(
      "name"           =>  "save_picture",
      "type_button"    =>  "btn btn-warning",
      "icon"           =>  "fa fa-save",
      "disabled"       =>  "",
      "legend"         =>  "Guardar y asignar foto de perfil"
     ));


    $form->closeForm();
    ?>

  </div><!-- .panel-body -->
</div><!-- .form-add-lecture -->
<!-- END SECTION PASSWORD -->


      </div><!-- .tab-content -->
      <!-- END TAB CONTENT -->

    </div><!-- .col-sm-12 -->
  </div><!-- .row -->
</div><!-- .container -->
</section> <!-- admin-course -->
<!-- END ADMIN PROFILE -->
<!--  *************************** End Paneles  *****************************  -->
            </div>
        </div>
        <!-- End Perfil Forms -->

        </div>
    </div>
</div><!-- /page content -->





            <!-- <a href="#" class="btn btn-default btn-large"><i class="fa fa-users"></i> Operaciones</a> -->
            <div class="space"></div>
            <p>
                <!-- <button class="btn btn-danger btn-sm" onclick="confirmar('blank.html')">Eliminar</button> -->
            </p>
        </div>
    </div>
</div>

<!--TERMINO CONTENIDO -->
</div>

</div>

</section><!--/wrapper -->

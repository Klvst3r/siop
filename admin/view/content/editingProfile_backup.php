 <?php

include '../controller/UserController.php';

include'../data/Form.php';
include 'sql/Combo.php';

//include '../controller/HistoryController.php';

 $user_name = $_SESSION["user"]["user_name"];

 $id_user = $_SESSION["user"]["id_user"];


 $user  = UserController::getProfile($id_user);

 //$combo = new combo();

 ?>
 <section class="wrapper site-min-height">
 	<h3><i class="fa fa-angle-right"></i> Perfil | Administrador: <b><?php echo $user_name;  ?></b></h3>
 	<div class="row mt">
 		<div class="col-lg-12">
 			<!-- INICIO CONTENIDO -->

 			<div class="container-fluid">
 				<div class="panel">
 					<div class="panel-heading">
 						<h3 class="panel-title">Editar Perfil</h3>
 					</div>
 					<div class="panel-body">



 						<div class="right_col" role="main"> <!-- page content -->
 							<div class="">
 								<div class="page-title">

 									<!-- content -->
 									<br>
 									<div class="row">

 										<!-- Begin Photo Section  -->
 										<div class="col-md-4">
 											<div class="image view view-first">
 												<!-- <img class="thumb-image" style="width: 100%; display: block;" src="../assets/img/users/<?php echo $user->getName(); ?>" alt="image" /> -->
 												<img src="../assets/img/users/<?php echo $user->getUser_photo(); ?>" style="max-width:100%;border-top-left-radius: 3px;border-top-right-radius: 3px;">
 											</div>
 											<span class="btn btn-my-button btn-file">
 												<form method="post" id="formulario" enctype="multipart/form-data">
 													<?php echo '<input type="hidden" name="id_photo" value="' . $id_user .'" />'; ?>
 													Cambiar Imagen de perfil: <input type="file" name="file">
 												</form>
 											</span>
 											<div id="respuesta"></div>
 										</div>
 										<!-- End Photo Section  -->

 										<!-- Begin Perfil Forms -->


 										<div class="col-md-8 col-xs-12 col-sm-12">
 											<?php include "../assets/lib/alerts.php";
                            					profile(); //call alert function
 											?>
 											<div class="x_panel">
 												<div class=""><!-- x_title -->
 													<h2>Informacion personal</h2>
 													<ul class="nav navbar-right panel_toolbox">

 													</ul>

 												</div>
 												<div class="">
 													<ul class="nav nav-tabs" role="tablist">
 														<li role="presentation" class="active"><a id="tabAdmin1" href="#form-perfil" data-toggle="tab" role="tab">Profile</a></li>
 														<li role="presentation"><a id="tabAdmin2" href="#form-general" aria-controls="form-general" data-toggle="tab" role="tab">Generales</a></li>
 														<li role="presentation"><a id="tabAdmin3" href="#form-change-about" aria-controls="form-change-about" data-toggle="tab" role="tab">Sobre el Usuario</a></li>
 														<li role="presentation"><a id="tabAdmin4" href="#form-change-pass" aria-controls="form-change-pass" data-toggle="tab" role="tab">Password</a></li>
 													</ul>

 													<div class="tab-content">
 														<br/>




 														<!-- BEGIN SECTION PASSWORD -->
 														<section class="wrap" id="">
 															<div class="container">


 																<div class="row">

 																		 <!-- <p>
 																			<a id="btnTabAdmin" href="#form-change-pass" aria-controls="form-change-pass" data-toggle="tab" role="tab" class="btn btn-danger">Cambiar Pass</a>
 																		</p>  -->

 																		<br/>
 																		<!-- TAB CONTENT -->
 																		<div class="tab-content">
 																			<!-- Begin User Profile -->
 																			<div role="tabpanel" class="panel panel-default tab-pane active" id="form-perfil">

 																				<!-- .Begin Perfil section -->
 																				<div class="panel-body">
 																					<h3>Perfíl de Usuario</h3>
 																					<form>
 																					<?php
																                    /*$form = new Form('updateProfileUser','POST','action.php?a=3', 'form', '');

																                if(isset($id_user)) {

																                	$form -> addField(4, array(
														                           	"field_name"    =>  "id_user",
														                           	"value"   =>  $id_user
														                           	));

																					*/

														                           	$query = "select id_dir, dir_name from directions order by dir_name";
     																				//$combo = new combo($query,"direccion","inputDir", "" ,"Direccion: ","","","","1");


																                    ?>
 																						<!-- <div class="form-group">
 																							<label for="privilegio">Privilegio</label>
 																							<select name="privilege" id="privilegio" class="form-control" required="required"><option value="1">Administrador</option><option value="2">Usuario</option><option value="3">Tester</option><option value="4">Prueba</option><option value="5">Supervisor</option><option value="11">Deleted</option></select>
 																						</div> -->




 																						<div class="form-group">
 																							<label for="dir">Dirección</label>
 																							<!-- <input type="text" class="form-control" id="inputSectionTitle" placeholder="Escribe el titulo de la sección..." /> -->
 																							<select name="dir" id="dir" class="form-control" required="required"><option value="1">Dir1</option><option value="2">dir2</option></select>
 																						</div>

 																						<div class="form-group">
 																							<label for="depto">Departamento</label>
 																							<!-- <input type="text" class="form-control" id="inputSectionTitle" placeholder="Escribe el titulo de la sección..." /> -->
 																							<select name="deptop" id="depto" class="form-control" required="required"><option value="1">Depto 1</option><option value="2">Depto 2</option></select>
 																						</div>

																						<div class="form-group">
 																							<label for="user_position">Cargo</label>
 																							<input type="text" class="form-control" id="user_position" placeholder="Escriba cargo del usuario..." />
 																						</div>

																						<div class="ln_solid"></div>

																						<div class="space"></div>

 																						<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Actualizar Perfíl </button>
 																					<?php
 																					//}
 																					?>
 																					</form>
 																				</div><!-- .panel-body -->
 																				<!-- .End Perfil section -->

 																			</div>

 																				<!-- Begin User Profile -->
 																			<div role="tabpanel" class="panel panel-default tab-pane" id="form-general">

 																				<!-- .Begin Perfil section -->
 																				<div class="panel-body">
 																					<h3>Información General</h3>
 																					<form>

 																						<div class="form-group">
 																							<label for="name">Nombre y Apellidos</label>
 																							<input type="text" class="form-control" id="name" placeholder="Escriba Nombre y Apellidos..." />
 																						</div>

 																						<div class="form-group">
 																							<label for="user_name">Nombre Usuario</label>
 																							<input type="text" class="form-control" id="user_name" placeholder="Escriba si Nickname..." />
 																						</div>

 																						<div class="form-group">
 																							<label for="user_movil">Celular</label>
 																							<input type="tel" class="form-control" id="user_movil" placeholder="Numero de Celular..." />
 																						</div>

 																						<div class="form-group">
 																							<label for="user_email">Correo Electrónico</label>
 																							<input type="email" class="form-control" id="user_email" placeholder="Numero de Celular..." />
 																						</div>

 																						<div class="form-group">
 																							<label for="user_tel">Teléfono Oficina</label>
 																							<input type="tel" class="form-control" id="user_movil" placeholder="Numero de Teléfono..." />

 																							<label for="user_ext">Extensión</label>
 																							<input type="tel" class="form-control" id="user_ext" placeholder="Numero de Extensión..." />
 																						</div>

 																						<div class="ln_solid"></div>

 																						<div class="space"></div>

 																						<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Actualizar Información </button>
 																					</form>
 																				</div><!-- .panel-body -->
 																				<!-- .End Perfil section -->

 																			</div>

 																			<!-- .begin about user section -->
 																			<div role="tabpanel" class="panel panel-default tab-pane" id="form-change-about">
 																				<div class="panel-body">
 																					<h3>Sobre el Usuario</h3>
 																					<form>
 																						<div class="form-group">
 																							<label for="inputSectionTitle">Breve descripción del Usuario: </label>
 																							<textarea value="Smith" rows="3" class="form-control">2817 S 49th
Apt 314
San Jose, CA 95101</textarea>
 																						</div>
 																						<div class="ln_solid"></div>

																						<div class="space"></div>

 																						<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Añadir Información</button>
 																					</form>
 																				</div><!-- .panel-body -->
 																			</div>
 																			<!-- .end about user section -->
 																			<!-- . begin change-password-section -->
 																			<div role="tabpanel" class="panel panel-default tab-pane" id="form-change-pass">
 																				<div class="panel-body">
 																						<h3>Cambiar Contraseña</h3>
 																					<div class="space"></div>
 																					<form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" action="action/upd_profile.php" method="post">

 																						<div class="form-group">
 																							<label class="control-label col-md-3 col-sm-3 col-xs-12">Contraseña antigua
 																							</label>
 																							<div class="col-md-6 col-sm-6 col-xs-12">
 																								<input id="birthday" name="password" class="date-picker form-control col-md-7 col-xs-12" type="text" placeholder="**********">
 																							</div>
 																						</div>
 																						<div class="form-group">
 																							<label class="control-label col-md-3 col-sm-3 col-xs-12">Nueva contraseña
 																							</label>
 																							<div class="col-md-6 col-sm-6 col-xs-12">
 																								<input id="birthday" name="new_password" class="date-picker form-control col-md-7 col-xs-12" type="text">
 																							</div>
 																						</div>
 																						<div class="form-group">
 																							<label class="control-label col-md-3 col-sm-3 col-xs-12">Confirmar contraseña nueva
 																							</label>
 																							<div class="col-md-6 col-sm-6 col-xs-12">
 																								<input id="birthday" name="confirm_new_password" class="date-picker form-control col-md-7 col-xs-12" type="text">
 																							</div>
 																						</div>

 																						<div class="ln_solid"></div>

																						<div class="space"></div>

 																						<div class="form-group">
 																							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
 																								<button type="submit" name="token" class="btn btn-info"><i class="fa fa-save"></i> Actualizar Contraseña</button>
 																							</div>
 																						</div>
 																					</form>



 																				</div>
 																			</div>
 																			<!-- . End change-password-section -->
 																		</div><!-- .tab-content -->
 																		<!-- END TAB CONTENT -->

 																	</div><!-- .col -->
 																</div><!-- .row -->
 															</div>
 														</section>
 														<!-- END SECTION PASSWORD -->



 													</div>
 												</div>
 											</div>
 										</div>
 										<!-- End Perfil Forms -->



 									</div>
 								</div>
 							</div><!-- /page content -->





 							<!-- <a href="#" class="btn btn-default btn-large"><i class="fa fa-users"></i> Operaciones</a> -->
 							<div class="space"></div>
 							<p>
 								<!-- <button class="btn btn-danger btn-sm" onclick="confirmar('blank.html')">Eliminar</button> -->
 							</p>
 						</div>
 					</div>
 				</div>

 				<!--TERMINO CONTENIDO -->
 			</div>

 		</div>

		      </section><!--/wrapper -->

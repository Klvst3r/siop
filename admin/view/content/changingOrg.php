<?php
include '../controller/ConfigController.php';
include '../helps/helps.php';
?>

<section class="wrapper site-min-height">
<h3><i class="fa fa-angle-right"></i> Perfil | Administrador: <b><?php echo $user_name;  ?></b></h3>
<div class="row mt">
<div class="col-lg-12">
 			<!-- INICIO CONTENIDO -->
<div class="container-fluid">
<div class="panel">
<div class="panel-heading">
	<h3 class="panel-title">Modificando Organización</h3>
 </div>
 <div class="panel-body">
<!-- ************************   Begin Process to Change Organization   **************************** -->
<?php
if($_SERVER["REQUEST_METHOD"] == "POST"){
	if(isset($_POST["id_org"]) && isset($_POST["org_nick"]) && isset($_POST["org_name"]) && isset($_POST["org_dir"]) && isset($_POST["org_pob"]) && isset($_POST["org_cp"]) && isset($_POST["org_tel"]) && isset($_POST["org_ext"]) && isset($_POST["org_email"])){

		$id_org		 = 	validate_field($_POST["id_org"]);
		$org_nick	 =	validate_field($_POST["org_nick"]);
		$org_name 	 = 	validate_field($_POST["org_name"]);
		$org_dir 	 = 	validate_field($_POST["org_dir"]);
		$org_pob 	 =	validate_field($_POST["org_pob"]);
		$org_cp 	 = 	validate_field($_POST["org_cp"]);
		$org_tel	 = 	validate_field($_POST["org_tel"]);
		$org_ext 	 = 	validate_field($_POST["org_ext"]);
		$org_email 	 =  validate_field($_POST["org_email"]);

		ConfigController::updateOrg($id_org, $org_nick, $org_name, $org_dir, $org_pob, $org_cp, $org_tel, $org_ext, $org_email);

		header("location:action.php?a=3");

	} //if(isset
}//if $_SERVER
?>
<!-- ************************   End Process to Change Organization   **************************** -->
</div>   
</div>       
</div>        				

<!--TERMINO CONTENIDO -->
 			</div>

 		</div>
</section><!--/wrapper --> 			 	

<?php

include'../data/Form.php';
include 'sql/Combo.php';
include 'sql/Data.php';

if(isset($_GET["b"])){
		        $id_dep_received = $_GET["b"];
		}

?>
<section class="wrapper site-min-height">
<h3><i class="fa fa-angle-right"></i> Perfil | Administrador: <b><?php echo $user_name;  ?></b></h3>
<div class="row mt">
<div class="col-lg-12">
      <!-- INICIO CONTENIDO -->
<div class="container-fluid">
<div class="panel">
<div class="panel-heading">
  <h3 class="panel-title">Departamentos</h3>
</div>
 <div class="panel-body">
<!-- ************************   Begin New Privilegios   **************************** -->

<!--************************- Begin Form New / Update Privilege  *******************-->
<div class="col-lg-12">
	<h1 class="page-header"><?php echo isset($id_user_received) ? 'Actualizar' : 'Nuevo' ?> Departamento |
    <a href="action.php?a=41" class="btn btn-default">
        <i class="fa fa-list-ul"></i> Ver Departamentos</a>
    </h1>
</div> <!-- .col-lg-12 -->

<!-- <div class="row">
<div class="col-md-12"> -->
<!-- <div class="panel-body"> -->


<?php
$form = new Form('newDep','POST','action.php?a=43', 'form', '', '');
?>

<legend>Datos del Departamento</legend>
<div class="panel panel-default">
	<div class="panel-body">
<?php
//Datos del usuario si es edición
if(isset($id_dep_received)) {
	//Update Process




 //Select data for values in form


 $query = "SELECT id_dep, departament FROM departaments where id_dep = :id_dep";

 $select = new Data();

 $select::getConection();

  //$query = $sql;

  $result = $select::$cnx->prepare($query);

  //echo $id_user;

  //The parameters are sent the user in the form by user_name field, the object $user, to the user of the form
  $id_dep = $id_dep_received;
  $result->bindParam(":id_dep", $id_dep);

  //We run the query the PDO connection
  $result->execute();

  $rows = $result->rowCount();

  if($rows > 0){
    $data = $result->fetch();

    //$privilegio = new Privilege();

    $id_dep     = $data["id_dep"];
    $departament 	= $data["departament"];




  }else {
    echo "No Data in Query";
  }

  $value = $departament;

 $form -> addField(4, array(
        "field_name"    =>  "id_dep",
        "value"   =>  $id_dep
        ));
 
 $form -> addField(4, array(
        "field_name"    =>  "update",
        "value"   =>  "true"
        ));

  //Descripción de la dirección
$form -> addField(1, array(
"field_name"    =>  "departament",
"class_label"   =>  "",
"label_field"   =>  "Nombre del departamento",
"div_field"     =>  "",
"input_class"   =>  "col-md-8",
"readonly"      =>  "",
"disabled"      =>  "",
"value"         =>  $departament,
"maxlength"     =>  "",
"size"          =>  "",
"style"         =>  "",
"js"            =>  "",
"placeholder"   =>  "Descripción del departamento",
"required"      =>  "required",
"autofocus"     =>  "autofocus"
));

echo '<div class="space"><br/></div>';


//echo '<div class="space"><br/></div>';

$form -> addField(3, array(
  "name" 			=>  "save_dep",
  "type_button"     =>  "btn btn-primary",
  "icon"		    =>  "fa fa-arrow-right",
  "disabled"      =>  "",
  "legend"    		=>  "Actualizar Departamento"

  ));




  }//check if is update or register
  else{
    $value = "";



//Register Process

//Descripción del
$form -> addField(1, array(
"field_name"    =>  "departament",
"class_label"   =>  "",
"label_field"   =>  "Nombre del departamento",
"div_field"     =>  "",
"input_class"   =>  "col-md-8",
"readonly"      =>  "",
"disabled"      =>  "",
"value"         =>  "",
"maxlength"     =>  "",
"size"          =>  "",
"style"         =>  "",
"js"            =>  "",
"placeholder"   =>  "Descripción del departamento",
"required"      =>  "required",
"autofocus"     =>  "autofocus"
));

echo '<div class="space"><br/></div>';


$form -> addField(3, array(
  "name" 			=>	"save_reg",
  "type_button"    	=>  "btn btn-primary",
  "icon"		   	=>  "fa fa-save",
  "disabled"      	=>  "",
  "legend"    		=>  "Registrar"

  ));


  }//if(isset($id_user))



?>
</div>
</div><!-- .panel panel-default -->            
<?php

$form->closeForm();

?>
<!-- </div> --><!-- .panel-body -->

</div>
</div>


<!-- ************************   End New Privilegios   **************************** -->
</div><!-- .panel-body -->
</div><!-- .panel -->
</div><!-- .container-fluid -->

<!--TERMINO CONTENIDO -->
</div><!-- .container-fluid -->

</div><!-- .row mt -->
</section><!--/wrapper -->     
<?php
include '../controller/UserController.php';
include '../helps/helps.php';
?>

<section class="wrapper site-min-height">
<h3><i class="fa fa-angle-right"></i> Perfil | Administrador: <b><?php echo $user_name;  ?></b></h3>
<div class="row mt">
<div class="col-lg-12">
 			<!-- INICIO CONTENIDO -->
<div class="container-fluid">
<div class="panel">
<div class="panel-heading">
	<h3 class="panel-title">Editar Perfil</h3>
 </div>
 <div class="panel-body">
<!-- ************************   Begin Process to Change Profile   **************************** -->
<?php
//echo "Processing....";
//Verify the method to receive form
if($_SERVER["REQUEST_METHOD"] == "POST"){
	//Receive fields to validate first and update later
	if(isset($_POST["id_user"]) && isset($_POST["privilege"]) && isset($_POST["status"]) && isset($_POST["direction"]) && isset($_POST["departament"]) && isset($_POST["user_position"])){

	//Validate fields from the form and asign to the variables in order to work with them
	$id_user 	 =	validate_field($_POST["id_user"]);
	$privilege	 =	validate_field($_POST["privilege"]);
	$status 	 = 	validate_field($_POST["status"]);
	$direction 	 = 	validate_field($_POST["direction"]);
	$departament =	validate_field($_POST["departament"]);
	$position 	 = 	validate_field($_POST["user_position"]);

	//Execute update
	UserController::updateProfile($id_user, $privilege, $status, $direction, $departament, $position);

	//After Update view the values in the form continue updating values
	header("location:action.php?a=10&b=$id_user");

	}
	


}else{
	//In case fail register the user send again to the view of Profile User
header("location:action.php?a=9");
}//if($_SERVER["REQUEST_METHOD"] == "POST")
?>


<!-- ************************   End Process to Change Profile   **************************** -->
</div>   
</div>       
</div>        				

<!--TERMINO CONTENIDO -->
 			</div>

 		</div>
</section><!--/wrapper --> 			 	


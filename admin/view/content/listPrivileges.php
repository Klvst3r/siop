 <?php

include 'sql/PrivilegeSQL.php';

 ?>

 <section class="wrapper site-min-height">
<h3><i class="fa fa-angle-right"></i> Perfil | Administrador: <b><?php echo $user_name;  ?></b></h3>
<div class="row mt">
<div class="col-lg-12">
      <!-- INICIO CONTENIDO -->
<div class="container-fluid">
<div class="panel">
<div class="panel-heading">
  <h3 class="panel-title">Privilegios</h3>
 </div>
 <div class="panel-body">
<!-- ************************   Begin List Privilegios   **************************** -->
<div class="col-lg-12">
	<?php  
	//last 14
	
	?>
    <h1 class="page-header">Listado de privilegios | <a href="action.php?a=23" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo privilegio</a>
    </h1>
</div>

<?php

$query_count = "SELECT COUNT(*) FROM privileges where status = '1'";
$query = " SELECT id_priv as ID, desc_priv as Privilegio, create_priv as Crear, select_priv as Seleccionar, update_priv as Actualizar, delete_priv as Borrar, backup_priv as Respaldo, report_priv as Reportes FROM privileges where status= '1' ORDER BY id_priv";

$params = "";

PrivilegeSQL::getTablePrivileges_Pag($query_count, $query, $params);

?>

</div>   
</div>       
</div>                

<!--TERMINO CONTENIDO -->
      </div>

    </div>
</section><!--/wrapper -->        


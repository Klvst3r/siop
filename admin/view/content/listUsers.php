<?php

include 'sql/UserSQL.php';

?>
<section class="wrapper site-min-height">
<h3><i class="fa fa-angle-right"></i> Perfil | Administrador: <b><?php echo $user_name;  ?></b></h3>
<div class="row mt">
<div class="col-lg-12">
      <!-- INICIO CONTENIDO -->
<div class="container-fluid">
<div class="panel">
<div class="panel-heading">
  <h3 class="panel-title">Usuarios</h3>
 </div>
 <div class="panel-body">
<!-- ************************   Begin List Users   **************************** -->

<div class="col-lg-12">
    <h1 class="page-header">Listado de usuarios | <a href="action.php?a=30" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo usuario</a>
    </h1>
</div>

      <!-- INICIO CONTENIDO -->
<?php

$query_count = "SELECT COUNT(*) FROM users where status ='1'";
$query = "SELECT A.id_user as ID, A.user_name as Nombre, A.user_email as Email, A.user_position as Cargo, B.desc_priv as Privilegio, A.date_reg as Registro FROM users A, privileges B WHERE A.id_priv = B.id_priv and A.complete = '1' and A.status = '1' ORDER BY id_user ";


$params = "";


UserSQL::getTableUsers_Pag($query_count, $query, $params);

?>


<!-- ************************   End List Users   **************************** -->
</div>
</div>
</div>

<!--TERMINO CONTENIDO -->
      </div>

    </div>
</section><!--/wrapper -->

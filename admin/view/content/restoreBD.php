<?php


require_once ("backupBD/config/config.inc.php");

require 'backupBD/class/phpBackup4MySQL.class.php';


// Initialize source and destination datable through variable
$source_db_name="siop";
$retore_to_db_name="siopRestored";



?>

<section class="wrapper site-min-height">
<h3><i class="fa fa-angle-right"></i> Admin Panel</h3>
<div class="row mt">
	
<div class="col-lg-12">
		<!-- INICIO CONTENIDO -->

    <div class="container-fluid">
        <div class="panel">
        <div class="panel-heading">
        <h3 class="panel-title">Restore BD</h3>
        </div>
        <div class="panel-body">


    <div class="space"></div>

        <div class="panel panel-primary">
        <div class="panel-heading">
        <h3 class="panel-title">Restore::return</h3>
        </div>
        <div class="panel-body">
        <!-- Begin Content -->
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-briefcase"></i> </span>
            <h4><?php //echo $org->getOrg_nick(); ?></h4>
          </div>
          <div class="widget-content">
            <div class="row-fluid">
              <div class="span6">
                <table class="">
                  <tbody>
                    <tr>
                      <td><h4><?php echo "Mensaje" ?></h4></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="span6">
                <table class="table table-bordered table-invoice">
                  <tbody>
                    <tr>
                    <tr>
                      <td colspan="2" >
                        Respaldos:
                      </td>
                    </tr>  
                    </tr><tr>
                      
                      <td class="width70"><strong>
<?php 

//List the availablble backups as links from "$source_db_name" backups
foreach (glob(BACKUP_DIRECTORY."/".$source_db_name."/*") as $val)
{
echo "<a href='action.php?a=51&sql=".str_replace(BACKUP_DIRECTORY.'/'.$source_db_name.'/','',$val)."'>".str_replace(BACKUP_DIRECTORY.'/','',$val)."</a><br/>";
}
if (isset($_GET['sql']))
{

//Create a new phpbackup4mysql instance
$pb4ms = new phpBackup4mysql();

//Make a new db connexion
//and restore db to new db "$retore_to_db_name"
$dbh = $pb4ms->dbconnect();
$sql_dump = $pb4ms->restoreSQL(BACKUP_DIRECTORY.'/'.$source_db_name.'/'.$_GET['sql'],array($retore_to_db_name,$dbh));

?>
</strong></td></tr>
<tr><td colspan="2" >
<?php
if ($sql_dump)
echo 'Restore '.$_GET['sql'].' <strong>OK</strong> <br/>';
else
echo '<b>Restore failed</b><br/>';
}

?>
</td></tr>              
                  
                    </tbody>
                  
                </table>
                <div class="pull-right">
                  
                 </div>
              </div>
              </div>
            </div>

          
          </div>
        </div>

        <!-- Finish Content -->        
        </div>
        </div>   
        

            
    <div class="space"></div>
    <p></p>
        </div>
      </div>   
    </div>       

<!--TERMINO CONTENIDO -->
</div>

</div>
</section><!--/wrapper -->
 <?php

  include '../controller/UserController.php';

  include '../helps/helps.php';

  include'../data/Form.php';

  include 'sql/Combo.php';

  

 ?>
 <section class="wrapper site-min-height">
    
    <h3><i class="fa fa-angle-right"></i> Perfil | Administrador: <b><?php echo $user_name;  ?></b></h3>
    <div class="row mt">
        <div class="col-lg-12">
            <!-- INICIO CONTENIDO -->

            <div class="container-fluid">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Registrar Nuevo Usuario</h3>
                    </div>
                    <div class="panel-body">


            <?php //echo "ID User: " . $id_user; ?>

                        <div class="right_col" role="main"> <!-- page content -->
                            <div class="">
                                <div class="page-title">

                                    <!-- content -->
                                    <br>
                                    <div class="row">


                                        <!-- Begin Perfil Forms -->


                                        <div class="col-md-10 col-md-offset-1 col-xs-12 col-sm-12">
                                            <?php include "../assets/lib/alerts.php";
                                                profile(); //call alert function
                                            ?>
                                            <div class="x_panel">
                                                <div class=""><!-- x_title -->
                                                    <h2>Informacion personal</h2>
                                                    
                                                    

                                                </div>
                                                <!--  *************************** Begin Paneles  *****************************  -->
  <!-- ADMIN PROFILE -->
        <section class="wrap" id="admin-course">
          <div class="container">

          





            <div class="row">
              <div class="col-sm-12 ">
               <!-- <p>
                 <a id="btnTabAdmin" href="#form-add-section" aria-controls="form-add-section" data-toggle="tab" role="tab" class="btn btn-info">Añadir Sección</a>
               </p> -->
           
            
              <div class="tab-content">
              <div class="space"></div>
              <div class="ln_solid"></div>

            


      


<?php
/* ---------------------------------------------------------*/

if($_SERVER["REQUEST_METHOD"] == "POST"){
  //Receive fields to validate first and update later
  if(isset($_POST["id_user"]) && isset($_POST["password"])){

    //Validate fields from the form and asign to the variables in order to work with them
    $id_user    =  validate_field($_POST["id_user"]);
    $password   =  validate_field($_POST["password"]);
    
    //Execute update
    UserController::regPass($id_user, $password);
    

    $pic = UserController::getPhoto($id_user);

    $pic->getUser_photo();



    }
/* ---------------------------------------------------------*/
 ?>
  
     <!-- Begin Photo Section  -->
                    <div class="col-md-7 col-md-offset-2">
                      <div class="image view view-first">
                        
                        <?php //echo '<img src="../assets/img/users/' . $pic->getUser_photo() . '"' . 'style="max-width:100%;border-top-left-radius: 3px;border-top-right-radius: 3px;">'; ?>
                        <img src="../assets/img/users/<?php echo $pic->getUser_photo(); ?>" style="max-width:10
                        0%;border-top-left-radius: 3px;border-top-right-radius: 3px;">
                      </div>
                      <span class="btn btn-my-button btn-file">
                        <form method="post" id="formulario" enctype="multipart/form-data">
                          <?php echo '<input type="hidden" name="id_photo" value="' . $id_user .'" />'; ?>
                          Cambiar Imagen de perfil: <input type="file" name="file">
                        </form>
                      </span>
                      <div id="respuesta"></div>
                    </div>
                    <!-- End Photo Section  -->
    
  <?php

  }else{
    header("location:action.php?a=9");
 }  

  echo "<br/>";

  ?>

      </div><!-- .tab-content -->
      <!-- END TAB CONTENT -->

    </div><!-- .col-sm-12 -->
  </div><!-- .row -->
</div><!-- .container -->
</section> <!-- admin-course -->
<!-- END ADMIN PROFILE -->
<!--  *************************** End Paneles  *****************************  -->

<div class="space"></div>
    <div class="ln_solid"></div>
    
    <div class="col-md-4 col-md-offset-4">
      
        <a class="btn btn-primary" href="action.php?a=9">Finalizar registro</a>
      
    </div>    



            </div>
        </div>
        <!-- End Perfil Forms -->

        </div>
    </div>
</div><!-- /page content -->





            <!-- <a href="#" class="btn btn-default btn-large"><i class="fa fa-users"></i> Operaciones</a> -->
            <div class="space"></div>
            <p>
                <!-- <button class="btn btn-danger btn-sm" onclick="confirmar('blank.html')">Eliminar</button> -->
            </p>
        </div>
    </div>
</div>

<!--TERMINO CONTENIDO -->
</div>

</div>

</section><!--/wrapper -->

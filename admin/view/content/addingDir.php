<?php

include'../data/Form.php';
include 'sql/Combo.php';
include 'sql/Data.php';

if(isset($_GET["b"])){
		        $id_dir_received = $_GET["b"];
		}

?>
<section class="wrapper site-min-height">
<h3><i class="fa fa-angle-right"></i> Perfil | Administrador: <b><?php echo $user_name;  ?></b></h3>
<div class="row mt">
<div class="col-lg-12">
      <!-- INICIO CONTENIDO -->
<div class="container-fluid">
<div class="panel">
<div class="panel-heading">
  <h3 class="panel-title">Direcciones</h3>
</div>
 <div class="panel-body">
<!-- ************************   Begin New Privilegios   **************************** -->

<!--************************- Begin Form New / Update Privilege  *******************-->
<div class="col-lg-12">
	<h1 class="page-header"><?php echo isset($id_user_received) ? 'Actualizar' : 'Nueva' ?> Dirección |
    <a href="action.php?a=36" class="btn btn-default">
        <i class="fa fa-list-ul"></i> Ver Direcciones</a>
    </h1>
</div> <!-- .col-lg-12 -->

<!-- <div class="row">
<div class="col-md-12"> -->
<!-- <div class="panel-body"> -->


<?php
$form = new Form('newDir','POST','action.php?a=38', 'form', '', '');
?>

<legend>Datos de la Dirección</legend>
<div class="panel panel-default">
	<div class="panel-body">
<?php
//Datos del usuario si es edición
if(isset($id_dir_received)) {
	



 //Select data for values in form


 $query = "SELECT id_dir, dir_name FROM directions where id_dir = :id_dir";

 $select = new Data();

 $select::getConection();

  //$query = $sql;

  $result = $select::$cnx->prepare($query);

  //echo $id_user;

  //The parameters are sent the user in the form by user_name field, the object $user, to the user of the form
  $id_dir = $id_dir_received;
  $result->bindParam(":id_dir", $id_dir);

  //We run the query the PDO connection
  $result->execute();

  $rows = $result->rowCount();

  if($rows > 0){
    $data = $result->fetch();

    //$privilegio = new Privilege();

    $id_dir     = $data["id_dir"];
    $dir_name 	= $data["dir_name"];




  }else {
    echo "No Data in Query";
  }

  $value = $dir_name;

 $form -> addField(4, array(
        "field_name"    =>  "id_dir",
        "value"   =>  $id_dir
        ));
 
 $form -> addField(4, array(
        "field_name"    =>  "update",
        "value"   =>  "true"
        ));

  //Descripción de la dirección
$form -> addField(1, array(
"field_name"    =>  "dir_name",
"class_label"   =>  "",
"label_field"   =>  "Nombre de la dirección",
"div_field"     =>  "",
"input_class"   =>  "col-md-8",
"readonly"      =>  "",
"disabled"      =>  "",
"value"         =>  $dir_name,
"maxlength"     =>  "",
"size"          =>  "",
"style"         =>  "",
"js"            =>  "",
"placeholder"   =>  "Descripción de la dirección",
"required"      =>  "required",
"autofocus"     =>  "autofocus"
));

echo '<div class="space"><br/></div>';


//echo '<div class="space"><br/></div>';

$form -> addField(3, array(
  "name" 			=>  "save_dir",
  "type_button"     =>  "btn btn-primary",
  "icon"		    =>  "fa fa-arrow-right",
  "disabled"      =>  "",
  "legend"    		=>  "Actualizar Dirección"

  ));




  }//check if is update or register
  else{
    $value = "";



//Register Process

//Descripción del
$form -> addField(1, array(
"field_name"    =>  "dir_name",
"class_label"   =>  "",
"label_field"   =>  "Nombre de la dirección",
"div_field"     =>  "",
"input_class"   =>  "col-md-8",
"readonly"      =>  "",
"disabled"      =>  "",
"value"         =>  "",
"maxlength"     =>  "",
"size"          =>  "",
"style"         =>  "",
"js"            =>  "",
"placeholder"   =>  "Descripción de la dirección",
"required"      =>  "required",
"autofocus"     =>  "autofocus"
));

echo '<div class="space"><br/></div>';


$form -> addField(3, array(
  "name" 			=>	"save_reg",
  "type_button"    	=>  "btn btn-primary",
  "icon"		   	=>  "fa fa-save",
  "disabled"      	=>  "",
  "legend"    		=>  "Registrar"

  ));


  }//if(isset($id_user))



?>
</div>
</div><!-- .panel panel-default -->            
<?php

$form->closeForm();

?>
<!-- </div> --><!-- .panel-body -->

</div>
</div>


<!-- ************************   End New Privilegios   **************************** -->
</div><!-- .panel-body -->
</div><!-- .panel -->
</div><!-- .container-fluid -->

<!--TERMINO CONTENIDO -->
</div><!-- .container-fluid -->

</div><!-- .row mt -->
</section><!--/wrapper -->     
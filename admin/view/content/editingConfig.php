 <?php

include '../controller/ConfigController.php';
//include '../controller/OtherController.php';

include'../data/Form.php';
include 'sql/Combo.php';

//include '../controller/HistoryController.php';

if(isset($_GET["id"])){

  $user = $_GET["id"];

}
 // $user  = UserController::getProfile($id_user);
 
 $org = ConfigController::getConfig();


?>
<section class="wrapper site-min-height">
<h3><i class="fa fa-angle-right"></i> Perfil | Administrador: <b><?php echo $user_name;  ?></b></h3>
<div class="row mt">
<div class="col-lg-12">
 			<!-- INICIO CONTENIDO -->
<div class="container-fluid">
<div class="panel">
<div class="panel-heading">
	<h3 class="panel-title">Editar Perfil</h3>
 </div>
 	<div class="panel-body">

<!--  *************************** Begin Paneles  *****************************  -->
<!-- ADMIN COURSE -->
<section class="wrap" id="admin-course">
  <div class="container">
    
  <p class="lead">
    Edición de Información de la Organización.
  </p>
<div class="row">
<div class="col-sm-9 col-sm-offset-1">
 <!-- <p>
   <a id="btnTabAdmin" href="#form-add-section" aria-controls="form-add-section" data-toggle="tab" role="tab" class="btn btn-info">Añadir Sección</a>
 </p> -->
<br/>
<!-- TAB CONTENT -->
<div class="tab-content">
<div role="tabpanel" class="panel panel-primary tab-pane active" id="form-list">
<!-- SECTION COURSE -->

 <div class="panel-heading">
     Edición de la Organización
 </div><!-- .panel-heading -->
 <ul class="list-group">
     <li class="list-group-item">
<?php
/*$query = "select id_dir, dir_name from directions order by dir_name";
$combo = new combo($query,"direccion","inputDir", "" ,"Direccion: ","","","","1");*/
//echo $org->getId_user(); 

$form = new Form('editConfig','POST','action.php?a=35', 'form', '', '');

?>
<div class="widget-box">
  <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
    <h5>Información de la Organización</h5>
  </div>


</div>
      
 <div class="col-md-10 col-md-offset-1">
    <div class="panel panel-default">
      <div class="panel-heading">Información del Usuario</div>
      <div class="panel-body">
        

<?php

$id_org = $org->getId_org();
 $form -> addField(4, array(
        "field_name"    =>  "id_org",
        "value"   =>  $id_org
        ));

$org_nick = $org->getOrg_nick();
$form -> addField(1, array(
            "field_name"    =>  "org_nick",
            "class_label"   =>  "",
            "label_field"   =>  "Siglas",
            "div_field"     =>  "",
            "input_class"   =>  "col-md-12",
            "readonly"      =>  "",
            "disabled"      =>  "",
            "value"         =>  "$org_nick",
            "maxlength"     =>  "",
            "size"          =>  "",
            "style"         =>  "",
            "js"            =>  "",
            "placeholder"   =>  "Escriba siglas de la organización...",
            "required"      =>  "required",
            "autofocus"     =>  "autofocus"
            ));


//echo '<div class="space"><br/></div>';            
$org_name = $org->getOrg_name();
$form -> addField(1, array(
            "field_name"    =>  "org_name",
            "class_label"   =>  "",
            "label_field"   =>  "Nombre",
            "div_field"     =>  "",
            "input_class"   =>  "col-md-12",
            "readonly"      =>  "",
            "disabled"      =>  "",
            "value"         =>  "$org_name",
            "maxlength"     =>  "",
            "size"          =>  "",
            "style"         =>  "",
            "js"            =>  "",
            "placeholder"   =>  "Escriba el nombre de la organización...",
            "required"      =>  "required",
            "autofocus"     =>  ""
            ));

//echo '<div class="space"><br/></div>';            
$org_dir = $org->getOrg_dir();
$form -> addField(1, array(
            "field_name"    =>  "org_dir",
            "class_label"   =>  "",
            "label_field"   =>  "Dirección",
            "div_field"     =>  "",
            "input_class"   =>  "col-md-12",
            "readonly"      =>  "",
            "disabled"      =>  "",
            "value"         =>  "$org_dir",
            "maxlength"     =>  "",
            "size"          =>  "",
            "style"         =>  "",
            "js"            =>  "",
            "placeholder"   =>  "Escriba la dirección de la organización...",
            "required"      =>  "required",
            "autofocus"     =>  ""
            ));

$org_pob = $org->getOrg_pob();
$form -> addField(1, array(
            "field_name"    =>  "org_pob",
            "class_label"   =>  "",
            "label_field"   =>  "Población",
            "div_field"     =>  "",
            "input_class"   =>  "col-md-12",
            "readonly"      =>  "",
            "disabled"      =>  "",
            "value"         =>  "$org_pob",
            "maxlength"     =>  "",
            "size"          =>  "",
            "style"         =>  "",
            "js"            =>  "",
            "placeholder"   =>  "Escriba la dirección de la organización...",
            "required"      =>  "required",
            "autofocus"     =>  "autofocus"
            ));

$org_cp = $org->getOrg_cp();
$form -> addField(1, array(
            "field_name"    =>  "org_cp",
            "class_label"   =>  "",
            "label_field"   =>  "C.P.",
            "div_field"     =>  "",
            "input_class"   =>  "col-md-12",
            "readonly"      =>  "",
            "disabled"      =>  "",
            "value"         =>  "$org_cp",
            "maxlength"     =>  "",
            "size"          =>  "",
            "style"         =>  "",
            "js"            =>  "",
            "placeholder"   =>  "Escriba código postal de la organización...",
            "required"      =>  "required",
            "autofocus"     =>  "autofocus"
            ));

$org_tel = $org->getOrg_tel();
$form -> addField(1, array(
            "field_name"    =>  "org_tel",
            "class_label"   =>  "",
            "label_field"   =>  "Teléfono",
            "div_field"     =>  "",
            "input_class"   =>  "col-md-12",
            "readonly"      =>  "",
            "disabled"      =>  "",
            "value"         =>  "$org_tel",
            "maxlength"     =>  "",
            "size"          =>  "",
            "style"         =>  "",
            "js"            =>  "",
            "placeholder"   =>  "Escriba teléfono...",
            "required"      =>  "required",
            "autofocus"     =>  "autofocus"
            ));

$org_ext = $org->getOrg_ext();
$form -> addField(1, array(
            "field_name"    =>  "org_ext",
            "class_label"   =>  "",
            "label_field"   =>  "Extensión",
            "div_field"     =>  "",
            "input_class"   =>  "col-md-12",
            "readonly"      =>  "",
            "disabled"      =>  "",
            "value"         =>  "$org_ext",
            "maxlength"     =>  "",
            "size"          =>  "",
            "style"         =>  "",
            "js"            =>  "",
            "placeholder"   =>  "Escriba extensión telefonica...",
            "required"      =>  "required",
            "autofocus"     =>  "autofocus"
            ));

$org_email = $org->getOrg_email();
$form -> addField(1, array(
            "field_name"    =>  "org_email",
            "class_label"   =>  "",
            "label_field"   =>  "Email",
            "div_field"     =>  "",
            "input_class"   =>  "col-md-12",
            "readonly"      =>  "",
            "disabled"      =>  "",
            "value"         =>  "$org_email",
            "maxlength"     =>  "",
            "size"          =>  "",
            "style"         =>  "",
            "js"            =>  "",
            "placeholder"   =>  "Escriba correo electrónico...",
            "required"      =>  "required",
            "autofocus"     =>  "autofocus"
            ));

/*
$id_user = $org->getId_user();
$user = UserController::getUsername($id_user);

$desc_user = $user->getUsername($id_user);

echo $id_user;*/





echo '<div class="space"><br/></div>';            

$form -> addField(3, array(
  "name"           =>  "update",
  "type_button"    =>  "btn btn-primary",
  "icon"           =>  "fa fa-save",
  "disabled"       =>  "",
  "legend"         =>  "Actualizar"

  ));


$form->closeForm();

?>                        

                    
                </div>
            </div>
    


     </li>
 </ul><!-- ul  -->
  <!-- END SECTION COURSE -->
</div><!-- .form-list -->
  
</div><!-- .tab-content -->
<!-- END TAB CONTENT -->

</div><!-- .col -->
</div><!-- .row -->
</div>
</section>
<!-- END ADMIN COURSE -->
<!--  *************************** End Paneles  *****************************  -->
	</div>   
</div>       
</div>        				

<!--TERMINO CONTENIDO -->
 			</div>

 		</div>
</section><!--/wrapper --> 			
 <?php

include '../controller/DepController.php';
include '../helps/helps.php';


 ?>

 <section class="wrapper site-min-height">
<h3><i class="fa fa-angle-right"></i> Perfil | Administrador: <b><?php echo $user_name;  ?></b></h3>
<div class="row mt">
<div class="col-lg-12">
 			<!-- INICIO CONTENIDO -->
<div class="container-fluid">
<div class="panel">
<div class="panel-heading">
	<h3 class="panel-title">Agregando Departamento</h3>
 </div>
 <div class="panel-body">
<!-- ************************   Begin Process to Change About   **************************** -->

<?php
if($_SERVER["REQUEST_METHOD"] == "POST"){

	if($_POST["update"] == "true"){

		if( isset($_POST["id_dep"]) && isset($_POST["departament"]) ){
			$id_dep 	 	=	validate_field($_POST["id_dep"]);
			$departament	 	=	validate_field($_POST["departament"]);

			DepController::updateDep($id_dep, $departament);

			header("location:action.php?a=41");
		}else{
			header("location:action.php?a=42");
		}

	}else{
		if( isset($_POST["departament"]) ){

			$departament = validate_field($_POST["departament"]);


			DepController::regDep($departament);

			header("location:action.php?a=41");
		}else{
			header("location:action.php?a=42");
		}

	}// else($_POST["update"] == "true")


}//if($_SERVER["REQUEST_METHOD"] == "POST")
?>

<!-- ************************   End Process to Change About   **************************** -->
</div>   
</div>       
</div>        				

<!--TERMINO CONTENIDO -->
 			</div>

 		</div>
</section><!--/wrapper --> 			 	


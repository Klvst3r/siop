 <?php

  include '../controller/UserController.php';

  include'../data/Form.php';

  include 'sql/Combo.php';


$id_user = $_REQUEST["b"];


$user  = UserController::getProfile($id_user);

$userSelect =  UserController::getSelectProfile($id_user);

 //$combo = new combo();

 ?>
 <section class="wrapper site-min-height">
    
    <h3><i class="fa fa-angle-right"></i> Perfil | Administrador: <b><?php echo $user_name;  ?></b></h3>
    <div class="row mt">
        <div class="col-lg-12">
            <!-- INICIO CONTENIDO -->

            <div class="container-fluid">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Registrar Nuevo Usuario</h3>
                    </div>
                    <div class="panel-body">


            <?php //echo "ID User: " . $id_user; ?>

                        <div class="right_col" role="main"> <!-- page content -->
                            <div class="">
                                <div class="page-title">

                                    <!-- content -->
                                    <br>
                                    <div class="row">


                                        <!-- Begin Perfil Forms -->


                                        <div class="col-md-10 col-md-offset-1 col-xs-12 col-sm-12">
                                            <?php include "../assets/lib/alerts.php";
                                                profile(); //call alert function
                                            ?>
                                            <div class="x_panel">
                                                <div class=""><!-- x_title -->
                                                    <h2>Informacion personal</h2>
                                                    <ul class="nav navbar-right panel_toolbox">

                                                    </ul>

                                                </div>
                                                <!--  *************************** Begin Paneles  *****************************  -->
  <!-- ADMIN PROFILE -->
        <section class="wrap" id="admin-course">
          <div class="container">




            <div class="row">
              <div class="col-sm-12 ">
               <!-- <p>
                 <a id="btnTabAdmin" href="#form-add-section" aria-controls="form-add-section" data-toggle="tab" role="tab" class="btn btn-info">Añadir Sección</a>
               </p> -->
               <!-- NAV TABS -->
               <ul class="nav nav-tabs" role="tablist">
                 <li role="presentation" class="active" >
                   <a id="tabAdmin1" href="#form-profile" aria-controls="form-profile" data-toggle="tab" role="tab">Perfil</a>
                 </li>
                 <li role="presentation">
                   <a id="tabAdmin2" href="#form-generals" aria-controls="form-generals" data-toggle="tab" role="tab">Generales</a>
                 </li>
                 <li role="presentation">
                  <a id="tabAdmin3" href="#form-about" aria-controls="form-about" data-toggle="tab" role="tab">Sobre el Usuario</a>
                </li>
                <li role="presentation">
                  <a id="tabAdmin4" href="#form-password" aria-controls="form-password" data-toggle="tab" role="tab">Password</a>
                </li>
              </ul>
              <!-- END NAV TABS -->
              <br/>
              <!-- TAB CONTENT -->
              <div class="tab-content">
            
<!-- SECTION PROFILE -->
<div role="tabpanel" class="panel panel-default tab-pane  active" id="form-profile">
    <div class="panel-body">

    <h3>Perfíl de Usuario</h3>




       <ul class="list-group">
           <li class="list-group-item">
            <?php
            //reference Action.php?a=5
            $form = new Form('regProfile','POST','action.php?a=31', 'form', '', '');

            //privilege
            $query = "SELECT id_priv, desc_priv FROM privileges WHERE status = '1' order by id_priv";
            $combo = new combo($query,"privilege","selectPriv", "" ,"Privilegio","","","","1");
              
            //status
            $query = "select id_user_status, desc_user_status from status_user order by id_user_status";
            $combo = new combo($query,"status","selectStatus", "" ,"Estado en el Sistema","","","","1");

             //Direction
            $query = "SELECT id_dir, dir_name FROM directions WHERE status = '1' order by dir_name";
            $combo = new combo($query,"direction","selectDir", "" ,"Direccion","","","","1");

            //Departaments
            $query = "SELECT id_dep, departament FROM departaments WHERE status = '1' order by departament";
            $combo = new combo($query,"departament","SelectDep", "" ,"Departamento","","","","1");

            //Position
            $position = $user->getUser_position();
            $form -> addField(1, array(
            "field_name"    =>  "user_position",
            "class_label"   =>  "",
            "label_field"   =>  "Puesto",
            "div_field"     =>  "",
            "input_class"   =>  "col-md-12",
            "readonly"      =>  "",
            "disabled"      =>  "",
            "value"         =>  "",
            "maxlength"     =>  "",
            "size"          =>  "",
            "style"         =>  "",
            "js"            =>  "",
            "placeholder"   =>  "Escriba el puesto del usuario...",
            "required"      =>  "required",
            "autofocus"     =>  "autofocus"
            ));

            echo '<div class="space"></div>';
            echo '<div class="ln_solid"></div>';

              //Send
              $form -> addField(3, array(
              "name"          =>  "save",
              "type_button"   =>  "btn btn-success",
              "icon"          =>  "fa fa-save",
              "disabled"      =>  "",
              "legend"        =>  "Guardar Perfil y continuar"

              ));

              $form->closeForm();

               ?>
               <!-- <button type="submit" class="btn btn-success"><i class=""></i> Actualizar Perfíl </button> -->
           </li>
       </ul><!-- ul  -->
    </div>
</div><!-- .form-list -->
<!-- END SECTION PROFILE -->

<!-- BEGIN SECTION GENERALS -->
<div role="tabpanel" class="panel panel-default tab-pane fade" id="form-generals">
  <div class="panel-body">
    <h3>Información General</h3>
    <?php
    $form = new Form('regGenerals','POST','', 'form', '', '');
        
    $form -> addField(1, array(
      "field_name"    =>  "name",
      "class_label"   =>  "",
      "label_field"   =>  "Nombre y Apellidos",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "disabled",
      "value"         =>  "",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Escriba Nombre y Apellidos",
      "required"      =>  "required",
      "autofocus"     =>  "autofocus"
      ));

    $form -> addField(1, array(
    "field_name"    =>  "user_name",
    "label_field"   =>  "Nombre de Usuario",
    "class_label"   =>  "",
    "div_field"     =>  "",
    "input_class"   =>  "col-md-12",
    "readonly"      =>  "",
    "disabled"      =>  "disabled",
    "value"         =>  "",
    "maxlength"     =>  "",
    "size"          =>  "",
    "style"         =>  "",
    "js"            =>  "",
    "placeholder"   =>  "Escriba su Nickname...",
    "required"      =>  "required",
    "autofocus"     =>  "autofocus"
    ));

    $form -> addField(1, array(
    "field_name"    =>  "user_resume",
    "label_field"   =>  "Resumen del Usuario",
    "class_label"   =>  "",
    "div_field"     =>  "",
    "input_class"   =>  "col-md-12",
    "readonly"      =>  "",
    "disabled"      =>  "disabled",
    "value"         =>  "",
    "maxlength"     =>  "",
    "size"          =>  "",
    "style"         =>  "",
    "js"            =>  "",
    "placeholder"   =>  "Breve Resumen del Usuario...",
    "required"      =>  "required",
    "autofocus"     =>  "autofocus"
    ));

  $form -> addField(7, array(
    "field_name"    =>  "user_movil",
    "label_field"   =>  "Celular",
    "class_label"   =>  "",
    "div_field"     =>  "",
    "input_class"   =>  "col-md-12",
    "readonly"      =>  "",
    "disabled"      =>  "disabled",
    "value"         =>  "",
    "maxlength"     =>  "10",
    "size"          =>  "",
    "style"         =>  "",
    "js"            =>  "",
    "placeholder"   =>  "Numero de celular...",
    "required"      =>  "required",
    "autofocus"     =>  "autofocus"
    ));


  $form -> addField(8, array(
  "field_name"    =>  "user_email",
  "label_field"   =>  "Correo Electrónico",
  "class_label"   =>  "",
  "div_field"     =>  "",
  "input_class"   =>  "col-md-12",
  "readonly"      =>  "",
  "disabled"      =>  "disabled",
  "value"         =>  "",
  "maxlength"     =>  "",
  "size"          =>  "",
  "style"         =>  "",
  "js"            =>  "",
  "placeholder"   =>  "Escriba su email...",
  "required"      =>  "required",
  "autofocus"     =>  "autofocus"
  ));


  $form -> addField(7, array(
    "field_name"    =>  "user_tel",
    "label_field"   =>  "Telefono",
    "class_label"   =>  "",
    "div_field"     =>  "",
    "input_class"   =>  "col-md-12",
    "readonly"      =>  "",
    "disabled"      =>  "disabled",
    "value"         =>  "",
    "maxlength"     =>  "10",
    "size"          =>  "10",
    "style"         =>  "",
    "js"            =>  "",
    "placeholder"   =>  "Numero telefónico...",
    "required"      =>  "required",
    "autofocus"     =>  "autofocus"
    ));

  $form -> addField(7, array(
    "field_name"    =>  "user_ext",
    "label_field"   =>  "Extensión",
    "class_label"   =>  "",
    "div_field"     =>  "",
    "input_class"   =>  "col-md-12",
    "readonly"      =>  "",
    "disabled"      =>  "disabled",
    "value"         =>  "",
    "maxlength"     =>  "",
    "size"          =>  "",
    "style"         =>  "",
    "js"            =>  "",
    "placeholder"   =>  "Extensión de oficina...",
    "required"      =>  "required",
    "autofocus"     =>  "autofocus"
  ));

//echo '<button type="submit" class="btn btn-success">Añadir </button> o <a href="" title="">Cancelar</a>';
//Send


echo '<div class="space"></div>';
echo '<div class="ln_solid"></div>';



 $form -> addField(3, array(
      "name"          =>  "save_general",
      "type_button"   =>  "btn btn-success",
      "icon"          =>  "fa fa-save",
      "disabled"      =>  "",
      "legend"        =>  "Guardar Datos Generales y continuar"

      ));



$form->closeForm();

//echo '</form>';
    ?>


  </div><!-- .panel-body -->
</div><!-- .form-add-section -->
<!-- END SECTION GENERALS -->

<!-- BEGIN SECTION ABOUT -->
<div role="tabpanel" class="panel panel-default tab-pane fade" id="form-about">
<div class="panel-body">
  <h3>Sobre el Usuario</h3>
  <?php
  $form = new Form('editAbout','POST','', 'form', '', '');


  $form -> addField(9, array(
          "field_name"    =>  "about",
          "label_field"   =>  "Breve descripción del Usuario:",
          "readonly"      =>  "",
          "disabled"      =>  "disabled",
          "value"         =>  "about",
          "rows"          =>  "3",
          "cols"          =>  "",
          "style"         =>  "",
          "js"            =>  "",
          "placeholder"   =>  "Sobre el usuario...",
          "content"       =>  "",
          "required"      =>  ""

        ));

 //echo '<button type="submit" class="btn btn-info">Añadir Informacion</button> ';
 echo '<div class="space"></div>';
echo '<div class="ln_solid"></div>';

 $form -> addField(3, array(
  "name"           =>  "save_info",
  "type_button"    =>  "btn btn-info",
  "icon"           =>  "fa fa-save",
  "disabled"       =>  "disabled",
  "legend"         =>  "Guardar Información y continuar"

 ));


$form->closeForm();
?>


  </div><!-- .panel-body -->
</div><!-- .form-add-lecture -->
<!-- END SECTION ABOUT -->

<!-- BEGIN SECTION PASSWORD -->
<div role="tabpanel" class="panel panel-default tab-pane fade" id="form-password">
  <div class="panel-body">
    <h3>Cambiar Contraseña</h3>
    <?php
    $form = new Form('editPass','POST','', 'form', '', 'form-horizontal form-label-left');
                       
              
      $form -> addField(2, array(
      "field_name"    =>  "new_pass",
      "class_label"   =>  "",
      "label_field"   =>  "Nuevo Password",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "disabled",
      "value"         =>  "",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "**********",
      "required"      =>  "required",
      "autofocus"     =>  "autofocus"
      ));

   
    echo '<div class="space"></div>';
    echo '<div class="ln_solid"></div>';


     $form -> addField(3, array(
      "name"           =>  "save_config",
      "type_button"    =>  "btn btn-warning",
      "icon"           =>  "fa fa-save",
      "disabled"       =>  "disabled",
      "legend"         =>  "Guardar Configuración de seguridad"
     ));


    $form->closeForm();
    ?>

  </div><!-- .panel-body -->
</div><!-- .form-add-lecture -->
<!-- END SECTION PASSWORD -->


      </div><!-- .tab-content -->
      <!-- END TAB CONTENT -->

    </div><!-- .col-sm-12 -->
  </div><!-- .row -->
</div><!-- .container -->
</section> <!-- admin-course -->
<!-- END ADMIN PROFILE -->
<!--  *************************** End Paneles  *****************************  -->
            </div>
        </div>
        <!-- End Perfil Forms -->

        </div>
    </div>
</div><!-- /page content -->





            <!-- <a href="#" class="btn btn-default btn-large"><i class="fa fa-users"></i> Operaciones</a> -->
            <div class="space"></div>
            <p>
                <!-- <button class="btn btn-danger btn-sm" onclick="confirmar('blank.html')">Eliminar</button> -->
            </p>
        </div>
    </div>
</div>

<!--TERMINO CONTENIDO -->
</div>

</div>

</section><!--/wrapper -->

 <?php

include '../controller/UserController.php';
include '../helps/helps.php';

 ?>

 <section class="wrapper site-min-height">
<h3><i class="fa fa-angle-right"></i> Perfil | Administrador: <b><?php echo $user_name;  ?></b></h3>
<div class="row mt">
<div class="col-lg-12">
 			<!-- INICIO CONTENIDO -->
<div class="container-fluid">
<div class="panel">
<div class="panel-heading">
	<h3 class="panel-title">Editar Perfil</h3>
 </div>
 <div class="panel-body">
<!-- ************************   Begin Process to Change About   **************************** -->

<?php
//echo "Processing....";
//Verify the method to receive form
if($_SERVER["REQUEST_METHOD"] == "POST"){

	if(isset($_POST["id_user"]) && isset($_POST["about"])){

	$id_user 	 =	validate_field($_POST["id_user"]);
	$about	 	 =	validate_field($_POST["about"]);

	UserController::updateAbout($id_user, $about);

	header("location:action.php?a=2");

	}

}else{
header("location:action.php?a=1");
}//if($_SERVER["REQUEST_METHOD"] == "POST")
?>


<!-- ************************   End Process to Change About   **************************** -->
</div>   
</div>       
</div>        				

<!--TERMINO CONTENIDO -->
 			</div>

 		</div>
</section><!--/wrapper --> 			 	


 <?php
include '../controller/PrivilegeController.php';
include '../controller/OtherController.php';
?>

 <section class="wrapper site-min-height">
<h3><i class="fa fa-angle-right"></i> Perfil | Administrador: <b><?php echo $user_name;  ?></b></h3>
<div class="row mt">
<div class="col-lg-12">
 			<!-- INICIO CONTENIDO -->
<div class="container-fluid">
<div class="panel">
<div class="panel-heading">
	<h3 class="panel-title">Borrar Privilegio</h3>
 </div>
 <div class="panel-body">
<!-- ************************   Begin Process to Delete Privilege   **************************** -->

<?php
if($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["b"])){
	$id = $_GET["b"];   
	OtherController::verifyUser($id);

 }else{
	//In case fail register the user send again to the list of Privileges
	//last 13 -> 14
	header("location:action.php?a=22");
	//echo"<meta HTTP-EQUIV='Refresh' CONTENT='2; URL=action.php?a=13'<head/>";
	}

?>
<!-- ************************   End Process to Delete Privilege   **************************** -->
</div>   
</div>       
</div>        				

<!--TERMINO CONTENIDO -->
 			</div>

 		</div>
</section><!--/wrapper --> 			 	

 <?php

include '../controller/DirController.php';
include '../helps/helps.php';


 ?>

 <section class="wrapper site-min-height">
<h3><i class="fa fa-angle-right"></i> Perfil | Administrador: <b><?php echo $user_name;  ?></b></h3>
<div class="row mt">
<div class="col-lg-12">
 			<!-- INICIO CONTENIDO -->
<div class="container-fluid">
<div class="panel">
<div class="panel-heading">
	<h3 class="panel-title">Agregando Dirección</h3>
 </div>
 <div class="panel-body">
<!-- ************************   Begin Process to Change About   **************************** -->

<?php
if($_SERVER["REQUEST_METHOD"] == "POST"){

	if($_POST["update"] == "true"){

		if( isset($_POST["id_dir"]) && isset($_POST["dir_name"]) ){
			$id_dir 	 	=	validate_field($_POST["id_dir"]);
			$dir_name	 	=	validate_field($_POST["dir_name"]);

			DirController::updateDir($id_dir, $dir_name);

			header("location:action.php?a=36");
		}else{
			header("location:action.php?a=37");
		}

	}else{
		if( isset($_POST["dir_name"]) ){

			$dir_name = validate_field($_POST["dir_name"]);


			DirController::regDir($dir_name);

			header("location:action.php?a=36");
		}else{
			header("location:action.php?a=37");
		}

	}// else($_POST["update"] == "true")


}//if($_SERVER["REQUEST_METHOD"] == "POST")
?>

<!-- ************************   End Process to Change About   **************************** -->
</div>   
</div>       
</div>        				

<!--TERMINO CONTENIDO -->
 			</div>

 		</div>
</section><!--/wrapper --> 			 	


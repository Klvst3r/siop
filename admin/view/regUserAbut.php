<?php
ob_start();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
   
  </head>
  <?php include'inc/adminHead.php'; ?>
  <body>

  <section id="container" class="">
      
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
        <?php include'inc/adminHeader.php'; ?>
        <?php


        ?>

            
        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
       <!--sidebar start-->
      <aside>
         <?php include'inc/adminAside.php'; ?>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
      <?php  
          $_SESSION["user"]["code"] = "true";
      ?>
      <?php
      /*------------------------------------------------  Begin Profile View Conten   ---------------------------------------------*/

        include 'content/registeringUserAbout.php'; 

      /*------------------------------------------------  Ends Profile View Conten   ---------------------------------------------*/
     ?>

         
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
     <?php include'inc/adminFooter.php'; ?>
     <!--footer ends-->

     <script>
     $(function(){
      $("input[name='file']").on("change", function(){
        var formData = new FormData($("#formulario")[0]);
        var ruta = "../assets/class/upload-picture.php";
        $.ajax({
          url: ruta,
          type: "POST",
          data: formData,
          contentType: false,
          processData: false,
          success: function(datos)
          {
            $("#respuesta").html(datos);
          }
        });
      });
    });
     </script>


    <script type="text/javascript">
          $("#btnTabAdmin").click(function(){
          $("#tabAdmin4").removeClass();
          $("#tabAdmin1").removeClass();
          $("#tabAdmin2").removeClass();
          $("#tabAdmin3").removeClass().addClass("active").trigger('click');
        });

        </script>

  </body>
</html>
<?php
ob_end_flush();
?>  

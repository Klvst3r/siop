<?php
ob_start();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
   
  </head>
  <?php include'inc/adminHead.php'; ?>
  <body>

  <section id="container" class="">
      
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
        <?php include'inc/adminHeader.php'; ?>
            
        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
       <!--sidebar start-->
      <aside>
         <?php include'inc/adminAside.php'; ?>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
      <?php  
          $_SESSION["user"]["code"] = "true";
      ?>
      <?php
      /*------------------------------------------------  Begin Profile View Conten   ---------------------------------------------*/

        include 'content/editingConfig.php'; 

      /*------------------------------------------------  Ends Profile View Conten   ---------------------------------------------*/
     ?>

         
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
     <?php include'inc/adminFooter.php'; ?>
     <!--footer ends-->

         <script type="text/javascript">
          $("#btnTabAdmin").click(function(){
          $("#tabAdmin1").removeClass();
          $("#tabAdmin3").removeClass();
          $("#tabAdmin2").removeClass().addClass("active").trigger('click');
        });

        </script>
     

  </body>
</html>
<?php
ob_end_flush();
?>  

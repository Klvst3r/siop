 <?php


if(isset($_SESSION["user"]) and isset($_SESSION["user"]["url"])){

  $code = $_SESSION["user"]["code"];
//  echo $code;
  if($code == "true" and $_SESSION["user"]["code"] == "true"){
  //Definition of Paht principal
  define("PATH", $_SESSION["user"]["url"]);


include '../controller/HeadUserPhotoController.php';

$user_name = $_SESSION["user"]["user_name"];

$id_user = $_SESSION["user"]["id_user"];  

$headUser = HeadUserPhotoController::getHeadPhoto($id_user);

?>

 <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Ocultar / Mostrar Menu"></div>
              </div>
            <!--logo start-->
            <a href="#" class="logo"><b>SIOP</b></a>
            <!--logo end-->
            <div class="nav pull-right notify-row" id="top_menu">
                <!--  notification start -->
                <ul class="nav top-menu">
                   
                    <!-- inbox dropdown start-->
                    <li id="header_inbox_bar" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <i class="fa fa-user"> <?php echo $_SESSION["user"]["user_name"];  ?> </i>
                            <span class="badge bg-theme"></span>
                        </a>
                        <ul class="dropdown-menu extended inbox">
                            <div class="notify-arrow notify-arrow-green"></div>
                            <li>
                                <p class="green"><?php echo $headUser->getUser_name(); ?></p>
                                
                                <p class="blue"><span class="time">Usuario desde: <?php echo date("Y") ?></span></p>
                            </li>
            
                            <li>
                                <a href="#">
                                    <span class="photo">
                                      <img alt="avatar" src="../assets/img/users/<?php echo $headUser->getUser_photo(); ?>">
                                    </span>
                                    <span class="subject">
                                    <span class="from"><?php echo $headUser->getDesc_priv(); ?></span>
                                    
                                         
                                    </span>
                                    <span class="message">
                                        <!-- CEO Web Developer in SECODUVI -->
                                        <?php echo $headUser->getUser_resume(); ?>

                                    </span>
                                </a>
                            </li>
                            
                            <li class="user-footer">
                              <div class="pull-left">
                                <a href="action.php?a=1" class="btn btn-default btn-flat">User Profile 
                                  <i class="fa fa-cog fa-spin fa-1x fa-fw text-green"></i></a>
                              </div>
                              <div class="pull-right">
                                <a href="action.php?a=3" class="btn btn-default btn-flat">Org. Config  
                                  <i class="fa fa-gears text-blue"></i></a>
                              </div>
                          </li>
                        </ul>
                    </li>
                    <!-- inbox dropdown end -->
                    <li><a href="../view/signout.php"><i class="fa fa-power-off fa-1x text-red"></i></a></li>
                </ul>
                <!--  notification end -->
            </div>

<?php

//After login turn off code
$_SESSION["user"]["code"] = "false";

}else{
      $_SESSION["user"]["code"] = "false";
      $code ="false";
        header("location:../login.php");
      }

$_SESSION["user"]["code"] = "false";

}else {

header("location:../login.php");
}
?>
            
 <footer class="site-footer">
          <div class="text-center">
              SIOP - SECODUVI &copy; <?php echo date('Y');  ?> | Made by <a href="#" class="">Klvst3r</a></i>
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../assets/js/jquery.js"></script>
  
    <script src="../assets/js/jquery-confirm.min.js"></script>


    <script src="../assets/js/bootstrap.min.js"></script>
    <!--<script src="assets/js/jquery-ui-1.9.2.custom.min.js"></script>-->
    <script src="../assets/js/jquery.ui.touch-punch.min.js"></script>
    <script class="include" type="text/javascript" src="../assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../assets/js/jquery.scrollTo.min.js"></script>
    <script src="../assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="../assets/js/common-scripts.js"></script>

    <!--script for this page-->
    
  <script>
      //custom select box



  </script>

  <script>

function confirmar(url){
  $.confirm({
    title: '¿Deseas eliminar este registro?',
    content: 'Se eliminará completamente del sistema. \n Si elimino por error, esta acción se cancelara en 5 segundos.', 
    icon: 'fa fa-question',
    theme: 'modern', //modern, material, bootstrap, supervan
    closeIcon: true,
    animation: 'scale',
    type: 'red',
    autoClose: 'cancelAction|5000',
      escapeKey: 'cancelAction',
    buttons: {
      confirm: {
        btnClass: 'btn-red',
        text: 'Confirmar',
        action: function() {
          window.location.href=url;
        }
      },
      cancelAction: {
        btnClass: 'btn-success',
        text: 'Cancelar',
        action: function() {
          //$.alert('Cancelado!');
        }
      }
    }

  });
}

/*function confirmar(url){
  alert("Prepare to Delete Record");
}*/
 </script>
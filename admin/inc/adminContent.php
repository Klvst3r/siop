 <?php
include '../controller/UserController.php';
include '../controller/OtherPrivController.php';
include '../controller/OtherHistoryController.php';


 ?>

 <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Admin Panel</h3>
          	<div class="row mt">
          		<div class="col-lg-12">
          		<!-- INICIO CONTENIDO -->
            
                <div class="container-fluid">
                    <div class="panel">
                    <div class="panel-heading">
                    <h3 class="panel-title">DashBoard</h3>
                    </div>
                    <div class="panel-body">

                        
                        <section class="content">
<?php
$live  = UserController::getUserActive(1);

$down = UserController::getUserDeleted(0);

$priv = OtherPrivController::getNumPriv();

$history = OtherHistoryController::getNumAccess();

?>                      
    
    <div class="row">
        <div class="col-xl-3 col-md-6 col-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="ion ion-person-stalker"></i></span>

            <div class="info-box-content">
              <span class="info-box-number"><?php echo $live->getLive();  ?><small> </small></span>
              <span class="info-box-text">Usuarios Activos </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-xl-3 col-md-6 col-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-card"></i></span>

            <div class="info-box-content">
              <span class="info-box-number"><?php echo $priv->getRecords();  ?></span>
              <span class="info-box-text">Privilegios</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-xl-3 col-md-6 col-12">
          <a href="#">
          <div class="info-box">
            
            <span class="info-box-icon bg-purple"><i class="ion ion-android-checkmark-circle"></i></span>

              
            <div class="info-box-content">
                <span class="info-box-number"><?php echo $history->getRecords();  ?></span>
                <span class="info-box-text">Accesos</span>
            </div>
              
            <!-- /.info-box-content -->
          </div>
          </a>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-xl-3 col-md-6 col-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-close-circled"></i></span>

            <div class="info-box-content">
              <span class="info-box-number"><?php echo $down->getDeleted();  ?></span>
              <span class="info-box-text">Bajas</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>


  </section> 



                         <div class="space"></div>

                        


                        <div class="panel panel-primary">
                        <div class="panel-heading">
                        <h3 class="panel-title">Catalogos</h3>
                        </div>
                        <div class="panel-body">
                            <a href="action.php?a=9" class="btn btn-default btn-large">
                              <i class="fa fa-users"></i> Usuario</a>

                            <a href="action.php?a=22" class="btn btn-default btn-large">
                              <i class="fa fa-user" aria-hidden="true"></i>
                           Privilegios</a>

                           <a href="action.php?a=36" class="btn btn-default btn-large">
                              <i class="fa fa-user" aria-hidden="true"></i>
                           Direcciones</a>

                           <a href="action.php?a=41" class="btn btn-default btn-large">
                              <i class="fa fa-user" aria-hidden="true"></i>
                           Departamentos</a>

                        </div>
                    </div>   
                    

                        <!-- <a href="#" class="btn btn-default btn-large"><i class="fa fa-users"></i> Operaciones</a> -->
                        <div class="space"></div>
                        <p>
                        <!-- <button class="btn btn-danger btn-sm" onclick="confirmar('blank.html')">Eliminar</button> -->
                      </p>
                    </div>
                  </div>   
                </div>       

            <!--TERMINO CONTENIDO -->
          		</div>
              
          	</div>
			
		      </section><!--/wrapper -->
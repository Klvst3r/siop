<?php

include '../controller/UserController.php';
include '../controller/HistoryController.php';

include '../helps/helps.php';

session_start();

header('Content-type: application/json');


$result = array();


if($_SERVER["REQUEST_METHOD"] == "POST"){

	if(isset($_POST["user_name"]) && isset($_POST["user_password"])){

		$code = $_POST["code"];
		$user_name = validate_field($_POST["user_name"]);
		$user_password = validate_field($_POST["user_password"]);

		//Data for History
		$ip = getenv("REMOTE_ADDR");
		if ($ip = '::1'){
			$ip = '127.0.0.1';
		}
		//$ip = getRealIP();
		$timezone  = -7; //(UTC/GMT -7:00) Zona Horaria Estandar (México) - Tiempo del Pacífico: UTC–7 
		$date = date("Y-m-d");
		//$time_in = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I"))); 
		$time_in = date('Y-m-d H:i:s');
		//$time_out = gmdate("Y-m-d H:i:s", time() + 4600*($timezone+date("I"))); 
		$time_out = NULL;


		
		if(UserController::login($user_name, $user_password)){
		//return print "Logeado";
		
		
			//If access is garanted the history is registered
			HistoryController::regHistory($ip, $date, $time_in, $time_out);

			//After verify user and reg history, return status to acces at system
			$user  = UserController::getUser($user_name, $user_password);

			$idUser = $user->getId_user();

			//Update History in User Table
			$history = HistoryController::updateHistory($idUser);


			//echo $user->getUser_name();
			$_SESSION["user"] = array (
				"id_user"		=> $user->getId_user(),
				"name"			=> $user->getName(),
				"user_name"		=> $user->getUser_name(),
				"user_email"	=> $user->getUser_email(),
				"id_priv"		=> $user->getId_Priv(),
				"code"			=> $code,
				"op"      		=> "false",
				//"url" 			=> ""
				"url" 			=> "/secoduvi/siop"
				
			);//$_SESSION

			

			$result = array(
			"status" => "true"
			);

			return print(json_encode($result));
			
		}//if $UserController


	}

}//if $_SERVER

$result = array("status" => "false");
return print(json_encode($result));



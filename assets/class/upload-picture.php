<?php

//upload file by abisoft https://github.com/amnersaucedososa 
//Connection to BD
include "config.php";

if (isset($_FILES["file"]))
{
    $file = $_FILES["file"];
    $name = $file["name"];
    $id_photo = $_POST["id_photo"];
    $type = $file["type"];
    $tmp_n = $file["tmp_name"];
    $size = $file["size"];
    $folder = "../img/users/";
    
    if ($type != 'image/jpg' && $type != 'image/jpeg' && $type != 'image/png' && $type != 'image/gif')
    {
      echo "Error, el archivo no es una imagen"; 
    }
    else if ($size > 1024*1024)
    {
      echo "Error, el tamaño máximo permitido es un 1MB";
    }
    else
    {
        $src = $folder.$name;
       @move_uploaded_file($tmp_n, $src);

       /*
       UPDATE `user` SET `profile_pic` = 'klvst3r.jpg' WHERE `user`.`id` = 1;
        */
       $query=mysqli_query($con, "UPDATE users set user_photo=\"$name\" WHERE id_user=\"$id_photo\" ");
       if($query){
        echo "<div class='alert alert-success' role='alert'>
            <button type='button' class='close' data-dismiss='alert'>&times;</button>
            <strong>¡Bien hecho!</strong> Perfil Actualizado Correctamente
        </div>";
       }
    }
}
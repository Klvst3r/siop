/*
Created		23/10/2017
Modified		28/03/2018
Project		SIOP
Model		E-R
Company		SECODUVI
Author		Klvst3r
Version		2.2.1
Database		mySQL 5 
*/




Create table organizations (
	id_org Int NOT NULL AUTO_INCREMENT,
	org_nick Varchar(80),
	org_name Varchar(80),
	org_dir Varchar(99),
	org_pob Varchar(35),
	org_cp Varchar(6),
	org_tel Varchar(20),
	org_ext Char(6),
	org_email Varchar(60),
	id_user Int NOT NULL,
 Primary Key (id_org)) ENGINE = InnoDB;

Create table departaments (
	id_dep Int NOT NULL AUTO_INCREMENT,
	departament Varchar(80),
 Primary Key (id_dep)) ENGINE = InnoDB;

Create table directions (
	id_dir Int NOT NULL AUTO_INCREMENT,
	dir_name Varchar(80),
 Primary Key (id_dir)) ENGINE = InnoDB;

Create table users (
	id_user Int NOT NULL AUTO_INCREMENT,
	id_priv Int NOT NULL,
	id_user_status Int NOT NULL,
	id_dir Int NOT NULL,
	id_dep Int NOT NULL,
	id_history Int NOT NULL,
	name Varchar(120) NOT NULL,
	user_name Varchar(60) NOT NULL,
	user_pass Varchar(255) NOT NULL,
	user_movil Varchar(10),
	user_email Varchar(80),
	user_photo Varchar(80),
	user_position Varchar(40) NOT NULL,
	user_resume Varchar(120),
	user_tel Varchar(20),
	user_ext Varchar(6),
	active Varchar(2),
	user_online Varchar(20),
	date_reg Timestamp,
	about Text,
	status Enum('1','0'),
	complete Enum('1','0'),
 Primary Key (id_user)) ENGINE = InnoDB
COMMENT = 'This is the table where register users, his privileges, her status in the system, his departament, his history and the characteristics of the count''s owner.';

Create table privileges (
	id_priv Int NOT NULL AUTO_INCREMENT,
	desc_priv Varchar(60),
	create_priv Enum('N','Y'),
	select_priv Enum('N','Y'),
	insert_priv Enum('N','Y'),
	update_priv Enum('N','Y'),
	delete_priv Enum('N','Y'),
	backup_priv Enum('N','Y'),
	report_priv Enum('N','Y'),
	status Enum('1','0'),
 Primary Key (id_priv)) ENGINE = InnoDB;

Create table status_user (
	id_user_status Int NOT NULL AUTO_INCREMENT,
	desc_user_status Varchar(20),
 Primary Key (id_user_status)) ENGINE = InnoDB;

Create table history_access (
	id_history Int NOT NULL AUTO_INCREMENT,
	ip Varchar(20),
	date_access Date,
	time_in Time,
	time_out Time,
 Primary Key (id_history)) ENGINE = InnoDB;

Create table type_docs (
	id_type_doc Int NOT NULL AUTO_INCREMENT,
	desc_type_doc Varchar(20),
 Primary Key (id_type_doc)) ENGINE = InnoDB;

Create table status_docs (
	id_status_doc Int NOT NULL AUTO_INCREMENT,
	desc_status_docs Varchar(25),
 Primary Key (id_status_doc)) ENGINE = InnoDB;

Create table municipalities (
	id_muni Int NOT NULL AUTO_INCREMENT,
	desc_muni Varchar(65),
 Primary Key (id_muni)) ENGINE = InnoDB;

Create table destiny_docs (
	id_destiny Int NOT NULL AUTO_INCREMENT,
	desc_destiny Varchar(50),
	destiny_position Varchar(60),
 Primary Key (id_destiny)) ENGINE = InnoDB;

Create table answer_docs (
	id_answer Int NOT NULL AUTO_INCREMENT,
	id_status_doc Int NOT NULL,
	folio_doc Int,
	date_answer Date,
	oficio_answer Varchar(255),
 Primary Key (id_answer)) ENGINE = InnoDB;

Create table documents (
	id_doc Int NOT NULL AUTO_INCREMENT,
	id_destiny Int NOT NULL,
	id_muni Int NOT NULL,
	id_type_doc Int NOT NULL,
	id_status_doc Int NOT NULL,
	id_classif Int NOT NULL,
	subject_doc Text,
	desc_send_position Varchar(120),
	folio_doc Varchar(20),
	send_doc Varchar(100),
	origin_doc Varchar(100),
	dir_sender Varchar(150),
	movil_doc Varchar(11),
	tel_doc Varchar(11),
	ext_doc Varchar(6),
	email_doc Varchar(80),
	date_doc Date,
	date_recep Date,
	reference Varchar(100),
	status Enum('0','1','2'),
	answer Varchar(40) COMMENT 'This field is for the document of answer from each direction',
	observation Text,
 Primary Key (id_doc)) ENGINE = InnoDB
COMMENT = 'This is the principal Table to register the information of the documents who arrive to the organization; is accompanied for the information of the origin like municipalities, typo of document, who is the sender, what is the status of the document.';

Create table documents_turned (
	id_turn Int NOT NULL AUTO_INCREMENT,
	id_doc Int NOT NULL,
	id_dir Int NOT NULL,
	id_status_doc Int NOT NULL,
	id_dep Int NOT NULL,
	id_char Int NOT NULL,
	id_classif Int NOT NULL,
	id_desc Int NOT NULL,
	folio_turned Varchar(20),
	doc_origin Varchar(50),
	doc_ccp Varchar(50),
	date_turned Date,
	instructions Text,
 Primary Key (id_turn)) ENGINE = InnoDB
COMMENT = 'After tegistration of the document, the user turn the document at the organization and register the action, here will register the information about the document, the status, chracter of tamit and classificatin of the document';

Create table char_tramits (
	id_char Int NOT NULL AUTO_INCREMENT,
	desc_char Varchar(25),
 Primary Key (id_char)) ENGINE = InnoDB;

Create table classifications (
	id_classif Int NOT NULL AUTO_INCREMENT,
	desc_classif Varchar(50),
 Primary Key (id_classif)) ENGINE = InnoDB;

Create table descriptions (
	id_desc Int NOT NULL AUTO_INCREMENT,
	content_desc Varchar(60),
 Primary Key (id_desc)) ENGINE = InnoDB;









Alter table users add Foreign Key (id_dep) references departaments (id_dep) on delete  restrict on update  restrict;
Alter table documents_turned add Foreign Key (id_dep) references departaments (id_dep) on delete  restrict on update  restrict;
Alter table users add Foreign Key (id_dir) references directions (id_dir) on delete  restrict on update  restrict;
Alter table documents_turned add Foreign Key (id_dir) references directions (id_dir) on delete  restrict on update  restrict;
Alter table organizations add Foreign Key (id_user) references users (id_user) on delete  restrict on update  restrict;
Alter table users add Foreign Key (id_priv) references privileges (id_priv) on delete  restrict on update  restrict;
Alter table users add Foreign Key (id_user_status) references status_user (id_user_status) on delete  restrict on update  restrict;
Alter table users add Foreign Key (id_history) references history_access (id_history) on delete  restrict on update  restrict;
Alter table documents add Foreign Key (id_type_doc) references type_docs (id_type_doc) on delete  restrict on update  restrict;
Alter table answer_docs add Foreign Key (id_status_doc) references status_docs (id_status_doc) on delete  restrict on update  restrict;
Alter table documents add Foreign Key (id_status_doc) references status_docs (id_status_doc) on delete  restrict on update  restrict;
Alter table documents_turned add Foreign Key (id_status_doc) references status_docs (id_status_doc) on delete  restrict on update  restrict;
Alter table documents add Foreign Key (id_muni) references municipalities (id_muni) on delete  restrict on update  restrict;
Alter table documents add Foreign Key (id_destiny) references destiny_docs (id_destiny) on delete  restrict on update  restrict;
Alter table documents_turned add Foreign Key (id_doc) references documents (id_doc) on delete  restrict on update  restrict;
Alter table documents_turned add Foreign Key (id_char) references char_tramits (id_char) on delete  restrict on update  restrict;
Alter table documents_turned add Foreign Key (id_classif) references classifications (id_classif) on delete  restrict on update  restrict;
Alter table documents add Foreign Key (id_classif) references classifications (id_classif) on delete  restrict on update  restrict;
Alter table documents_turned add Foreign Key (id_desc) references descriptions (id_desc) on delete  restrict on update  restrict;







-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 03-04-2018 a las 15:38:40
-- Versión del servidor: 5.7.21-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `siop`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `answer_docs`
--

CREATE TABLE `answer_docs` (
  `id_answer` int(11) NOT NULL,
  `id_status_doc` int(11) NOT NULL,
  `folio_doc` int(11) DEFAULT NULL,
  `date_answer` date DEFAULT NULL,
  `oficio_answer` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `char_tramits`
--

CREATE TABLE `char_tramits` (
  `id_char` int(11) NOT NULL,
  `desc_char` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `char_tramits`
--

INSERT INTO `char_tramits` (`id_char`, `desc_char`) VALUES
(1, 'Importante'),
(2, 'Urgente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `classifications`
--

CREATE TABLE `classifications` (
  `id_classif` int(11) NOT NULL,
  `desc_classif` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `classifications`
--

INSERT INTO `classifications` (`id_classif`, `desc_classif`) VALUES
(1, 'Informativo'),
(2, 'Solicitud');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departaments`
--

CREATE TABLE `departaments` (
  `id_dep` int(11) NOT NULL,
  `departament` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `departaments`
--

INSERT INTO `departaments` (`id_dep`, `departament`) VALUES
(1, 'Departemento de Recursos Humanos y Desarrollo Administrativo'),
(2, 'Departamento de Adquisiciones,\r\nRecursos Materiales y Servicios'),
(3, 'Departamento de Contabilidad y Finanzas'),
(4, 'Informática'),
(5, 'Archivo'),
(6, 'Almacen 1'),
(7, 'Almacen 2'),
(8, 'Seguimiento a Auditorias'),
(9, 'Departamento de Gestión de Derecho de Vía y Afectaciones'),
(10, 'Departamento de Obras por Administración y Equipamiento'),
(11, 'Departamento de Infraestructura Vial'),
(12, 'Departamento de Concertación Social y Apoyos de Maquinaria'),
(13, 'Departamento de Eventos Especiales'),
(14, 'Departamento de Agua Potable, Alcantarillado y Saneamiento'),
(15, 'Departamento de Infraestructura Urbana'),
(16, 'Departamento Jurídico'),
(17, 'Departamento de Planeación y Coordinación de Programas'),
(18, 'Departamento de Proyectos'),
(19, 'Departamento de Estadística e Información Geográfica'),
(20, 'Departamento de Programas y Registro del Desarrollo Urbano'),
(21, 'Departamento de Licitaciones y Contratos'),
(22, 'Precios Unitarios y Ajuste de Costos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descriptions`
--

CREATE TABLE `descriptions` (
  `id_desc` int(11) NOT NULL,
  `content_desc` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `descriptions`
--

INSERT INTO `descriptions` (`id_desc`, `content_desc`) VALUES
(1, 'Notificaciones'),
(2, 'Dictamen de Congruencia'),
(3, 'Eventos Especiales'),
(4, 'Invitaciones'),
(5, 'Maquinaria'),
(6, 'Correo Electrónico'),
(7, 'Para su Conocimiento'),
(8, 'Tarjeta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `destiny_docs`
--

CREATE TABLE `destiny_docs` (
  `id_destiny` int(11) NOT NULL,
  `desc_destiny` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `destiny_position` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `destiny_docs`
--

INSERT INTO `destiny_docs` (`id_destiny`, `desc_destiny`, `destiny_position`) VALUES
(1, 'Arq. Francisco Javier Romero Ahuactzi', 'Secretario '),
(2, 'C.P. Carlos Sánchez Tamayo', 'Director Administrativo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directions`
--

CREATE TABLE `directions` (
  `id_dir` int(11) NOT NULL,
  `dir_name` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `directions`
--

INSERT INTO `directions` (`id_dir`, `dir_name`) VALUES
(1, 'Dirección Administrativa'),
(2, 'Dirección de Obras Públicas'),
(3, 'Dirección de Desarrollo Urbano'),
(4, 'Dirección de Licitaciones, Precios Unitarios y Contratos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documents`
--

CREATE TABLE `documents` (
  `id_doc` int(11) NOT NULL,
  `id_destiny` int(11) NOT NULL,
  `id_muni` int(11) NOT NULL,
  `id_type_doc` int(11) NOT NULL,
  `id_status_doc` int(11) NOT NULL,
  `id_classif` int(11) NOT NULL,
  `subject_doc` text COLLATE utf8_spanish_ci,
  `desc_send_position` varchar(120) COLLATE utf8_spanish_ci DEFAULT NULL,
  `folio_doc` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `send_doc` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `origin_doc` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dir_sender` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `movil_doc` varchar(11) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tel_doc` varchar(11) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ext_doc` varchar(6) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email_doc` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `date_doc` date DEFAULT NULL,
  `date_recep` date DEFAULT NULL,
  `reference` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `status` enum('0','1','2') COLLATE utf8_spanish_ci DEFAULT NULL,
  `answer` varchar(40) COLLATE utf8_spanish_ci DEFAULT NULL,
  `observation` text COLLATE utf8_spanish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `documents`
--

INSERT INTO `documents` (`id_doc`, `id_destiny`, `id_muni`, `id_type_doc`, `id_status_doc`, `id_classif`, `subject_doc`, `desc_send_position`, `folio_doc`, `send_doc`, `origin_doc`, `dir_sender`, `movil_doc`, `tel_doc`, `ext_doc`, `email_doc`, `date_doc`, `date_recep`, `reference`, `status`, `answer`, `observation`) VALUES
(1, 1, 1, 7, 1, 2, 'Asunto 1', 'Cargo Remitente 1', '1', 'Remitente 1', 'Origen 1', 'Domicilio 1', '987654321', '1234567890', '123456', 'primero@gmail.com', '2018-03-02', '2018-03-27', 'Referencia 1', '2', '', ''),
(2, 1, 2, 7, 1, 2, 'Asunto 2', 'Cargo Remitente 2', '2', 'Remitente 2', 'Origen 2', 'Domicilio 2', '9876543213', '9876543216', '654321', 'segundo@gmail.com', '2018-03-03', '2018-03-27', 'Referencia 2', '1', '', ''),
(3, 1, 3, 2, 1, 1, 'Asunto 3', 'Cargo Remitente 3', '3', 'Remitente 3', 'Origen 3', 'Domicilio 3', '789456321', '987654321', '654', 'tercero@gmail.com', '2018-03-05', '2018-03-28', 'Referencia 3', '2', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documents_turned`
--

CREATE TABLE `documents_turned` (
  `id_turn` int(11) NOT NULL,
  `id_doc` int(11) NOT NULL,
  `id_dir` int(11) NOT NULL,
  `id_status_doc` int(11) NOT NULL,
  `id_dep` int(11) NOT NULL,
  `id_char` int(11) NOT NULL,
  `id_classif` int(11) NOT NULL,
  `id_desc` int(11) NOT NULL,
  `folio_turned` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `doc_origin` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `doc_ccp` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `date_turned` date DEFAULT NULL,
  `instructions` text COLLATE utf8_spanish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `documents_turned`
--

INSERT INTO `documents_turned` (`id_turn`, `id_doc`, `id_dir`, `id_status_doc`, `id_dep`, `id_char`, `id_classif`, `id_desc`, `folio_turned`, `doc_origin`, `doc_ccp`, `date_turned`, `instructions`) VALUES
(1, 1, 1, 3, 1, 2, 2, 5, '1', 'Turnado 1', 'Copia turnado 1', '2018-03-27', 'Instrucciones 1'),
(2, 3, 2, 1, 14, 1, 1, 4, '3', 'Arq. Texis', 'C.P. Carlos Sánchez Tamayo', '2018-03-28', 'Invitación a curso de capacitación');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `history_access`
--

CREATE TABLE `history_access` (
  `id_history` int(11) NOT NULL,
  `ip` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `date_access` date DEFAULT NULL,
  `time_in` time DEFAULT NULL,
  `time_out` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `history_access`
--

INSERT INTO `history_access` (`id_history`, `ip`, `date_access`, `time_in`, `time_out`) VALUES
(1, '127.0.1.1', '2018-03-26', '14:28:26', NULL),
(2, '127.0.0.1', '2018-03-26', '14:38:54', NULL),
(3, '127.0.0.1', '2018-03-26', '15:34:08', NULL),
(4, '127.0.0.1', '2018-03-26', '18:09:09', NULL),
(5, '127.0.0.1', '2018-03-26', '18:14:35', NULL),
(6, '127.0.0.1', '2018-03-26', '18:17:00', NULL),
(7, '127.0.0.1', '2018-03-26', '18:17:14', NULL),
(8, '127.0.0.1', '2018-03-26', '18:17:35', NULL),
(9, '127.0.0.1', '2018-03-26', '20:51:11', NULL),
(10, '127.0.0.1', '2018-03-27', '10:12:03', NULL),
(11, '127.0.0.1', '2018-03-27', '10:58:25', NULL),
(12, '127.0.0.1', '2018-03-27', '11:54:30', NULL),
(13, '127.0.0.1', '2018-03-27', '12:00:23', NULL),
(14, '127.0.0.1', '2018-03-28', '10:51:32', NULL),
(15, '127.0.0.1', '2018-03-28', '12:18:00', NULL),
(16, '127.0.0.1', '2018-03-28', '12:35:17', NULL),
(17, '127.0.0.1', '2018-03-28', '15:03:20', NULL),
(18, '127.0.0.1', '2018-03-28', '15:08:30', NULL),
(19, '127.0.0.1', '2018-03-28', '18:15:53', NULL),
(20, '127.0.0.1', '2018-03-28', '18:44:52', NULL),
(21, '127.0.0.1', '2018-03-28', '19:06:58', NULL),
(22, '127.0.0.1', '2018-04-03', '13:46:13', NULL),
(23, '127.0.0.1', '2018-04-03', '13:46:24', NULL),
(24, '127.0.0.1', '2018-04-03', '15:36:34', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipalities`
--

CREATE TABLE `municipalities` (
  `id_muni` int(11) NOT NULL,
  `desc_muni` varchar(65) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `municipalities`
--

INSERT INTO `municipalities` (`id_muni`, `desc_muni`) VALUES
(1, 'Amaxac de Guerrero'),
(2, 'Apetatitlan de Antonio Carvajal'),
(3, 'Atlangatepec'),
(4, 'Atltzayanca'),
(5, 'Apizaco'),
(6, 'Calpulalpan'),
(7, 'El Carmen Tequexquitla'),
(8, 'Cuapiaxtla'),
(9, 'Cuaxomulco'),
(10, 'Chiautempan'),
(11, 'Muñoz de Domingo Arenas'),
(12, 'Españita'),
(13, 'Huamantla'),
(14, 'Hueyotlipan'),
(15, 'Ixtacuixtla de Mariano Matamoros'),
(16, 'Ixtenco'),
(17, 'Mazatecochco de José María Morelos'),
(18, 'Contla de Juan Cuamatzi'),
(19, 'Tepetitla de Lardizábal'),
(20, 'Sanctórum de Lázaro Cárdenas'),
(21, 'Nanacamilpa de Mariano Arista'),
(22, 'Acuamanala de Miguel Hidalgo'),
(23, 'Natívitas'),
(24, 'Panotla'),
(25, 'San Pablo del Monte'),
(26, 'Santa Cruz Tlaxcala'),
(27, 'Tenancingo'),
(28, 'Teolocholco'),
(29, 'Tepeyanco'),
(30, 'Terrenate'),
(31, 'Tetla de la Solidaridad'),
(32, 'Tetlatlahuca'),
(33, 'Tlaxcala'),
(34, 'Tlaxco'),
(35, 'Tocatlán'),
(36, 'Totolac'),
(37, 'Ziltlaltépec de Trinidad Sánchez '),
(38, 'Tzompantepec'),
(39, 'Xaloztoc'),
(40, 'Xaltocan'),
(41, 'Papalotla de Xicohténcatl'),
(42, 'Xicohtzinco'),
(43, 'Yauhquemehcan'),
(44, 'Zacatelco'),
(45, 'Benito Juárez'),
(46, 'Emiliano Zapata'),
(47, 'Lázaro Cárdenas'),
(48, 'La Magdalena Tlaltelulco'),
(49, 'San Damián Texóloc'),
(50, 'San Francisco Tetlanohcan'),
(51, 'San Jerónimo Zacualpan'),
(52, 'San José Teacalco'),
(53, 'San Juan Huactzinco'),
(54, 'San Lorenzo Axocomanitla'),
(55, 'San Lucas Tecopilco'),
(56, 'Santa Ana Nopalucan'),
(57, 'Santa Apolonia Teacalco'),
(58, 'Santa Catarina Ayometla'),
(59, 'Santa Cruz Quilehtla'),
(60, 'Santa Isabel Xiloxoxtla');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `organizations`
--

CREATE TABLE `organizations` (
  `id_org` int(11) NOT NULL,
  `org_nick` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_name` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_dir` varchar(99) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_pob` varchar(35) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_cp` varchar(6) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_tel` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_ext` char(6) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_email` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privileges`
--

CREATE TABLE `privileges` (
  `id_priv` int(11) NOT NULL,
  `desc_priv` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL,
  `create_priv` enum('N','Y') COLLATE utf8_spanish_ci DEFAULT NULL,
  `select_priv` enum('N','Y') COLLATE utf8_spanish_ci DEFAULT NULL,
  `insert_priv` enum('N','Y') COLLATE utf8_spanish_ci DEFAULT NULL,
  `update_priv` enum('N','Y') COLLATE utf8_spanish_ci DEFAULT NULL,
  `delete_priv` enum('N','Y') COLLATE utf8_spanish_ci DEFAULT NULL,
  `backup_priv` enum('N','Y') COLLATE utf8_spanish_ci DEFAULT NULL,
  `report_priv` enum('N','Y') COLLATE utf8_spanish_ci DEFAULT NULL,
  `status` enum('1','0') COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `privileges`
--

INSERT INTO `privileges` (`id_priv`, `desc_priv`, `create_priv`, `select_priv`, `insert_priv`, `update_priv`, `delete_priv`, `backup_priv`, `report_priv`, `status`) VALUES
(1, 'Administración del sistema', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'N', '1'),
(2, 'Dev', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', '1'),
(3, 'Registro de Información', 'N', 'Y', 'Y', 'Y', 'N', 'N', 'N', '1'),
(4, 'Consulta de Información', 'N', 'Y', 'N', 'N', 'N', 'N', 'Y', '1'),
(5, 'Borrado de Información', 'N', 'Y', 'Y', 'Y', 'Y', 'N', 'N', '1'),
(6, 'Respaldo de Información', 'N', 'Y', 'N', 'N', 'N', 'Y', 'N', '1'),
(7, 'deleted', 'N', 'Y', 'N', 'Y', 'N', 'Y', 'N', '0'),
(8, 'modify', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', '0'),
(9, 'alert', 'N', 'N', 'N', 'N', 'N', 'N', 'N', '0'),
(10, 'New Privilege', 'N', 'N', 'N', 'N', 'N', 'N', 'N', '0'),
(11, 'Privilege reedited', 'N', 'Y', 'N', 'Y', 'N', 'Y', 'N', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_docs`
--

CREATE TABLE `status_docs` (
  `id_status_doc` int(11) NOT NULL,
  `desc_status_docs` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `status_docs`
--

INSERT INTO `status_docs` (`id_status_doc`, `desc_status_docs`) VALUES
(1, 'Iniciado'),
(2, 'Enterado'),
(3, 'En Trámite'),
(4, 'Pendiente'),
(5, 'Concluido');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_user`
--

CREATE TABLE `status_user` (
  `id_user_status` int(11) NOT NULL,
  `desc_user_status` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `status_user`
--

INSERT INTO `status_user` (`id_user_status`, `desc_user_status`) VALUES
(1, 'Active'),
(2, 'Inactive');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_docs`
--

CREATE TABLE `type_docs` (
  `id_type_doc` int(11) NOT NULL,
  `desc_type_doc` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `type_docs`
--

INSERT INTO `type_docs` (`id_type_doc`, `desc_type_doc`) VALUES
(1, 'Circular'),
(2, 'Convocatoria'),
(3, 'Email'),
(4, 'Engargolado'),
(5, 'Factura'),
(6, 'Invitación'),
(7, 'Oficio'),
(8, 'Sobre'),
(9, 'Tarjeta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `id_priv` int(11) NOT NULL,
  `id_user_status` int(11) NOT NULL,
  `id_dir` int(11) NOT NULL,
  `id_dep` int(11) NOT NULL,
  `id_history` int(11) NOT NULL,
  `name` varchar(120) COLLATE utf8_spanish_ci NOT NULL,
  `user_name` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `user_pass` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `user_movil` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_email` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_photo` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_position` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `user_resume` varchar(120) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_tel` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_ext` varchar(6) COLLATE utf8_spanish_ci DEFAULT NULL,
  `active` varchar(2) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_online` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `date_reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `about` text COLLATE utf8_spanish_ci,
  `status` enum('1','0') COLLATE utf8_spanish_ci DEFAULT NULL,
  `complete` enum('1','0') COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id_user`, `id_priv`, `id_user_status`, `id_dir`, `id_dep`, `id_history`, `name`, `user_name`, `user_pass`, `user_movil`, `user_email`, `user_photo`, `user_position`, `user_resume`, `user_tel`, `user_ext`, `active`, `user_online`, `date_reg`, `about`, `status`, `complete`) VALUES
(2, 1, 1, 1, 4, 22, 'Klvst3r', 'klvst3r', '81dc9bdb52d04dc20036dbd8313ed055', '2461645449', 'klvst3r@gmail.com', 'ui-danro.jpg', 'Dev', 'CEO Web developer + Edx + UX + UI', '1234567890', '0', '0', '0', '2018-04-03 18:46:13', 'I am admin user dev for Internet lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eros nibh, viverra a dui a, gravida varius velit. Vivamus tristique est non ipsum dapibus lacinia sed nec metus.', '1', '1'),
(3, 2, 1, 1, 4, 1, 'Kozlov', 'kozlov', 'user_pass', '2461645449', 'kozlov@gmail.com', 'ui-danro.jpg', 'Dev', 'Admin', '123456789', '123', '0', '0', '2018-03-26 23:14:50', 'CEO Web developer + Edx + UX + UI', '0', '1'),
(4, 2, 1, 1, 4, 1, 'name', 'user_name', 'user_pass', 'user_movil', 'user_email', 'default.png', 'Dev', 'user_resume', 'user_tel', 'ext', '0', '0', '2018-03-26 23:12:32', 'about', '0', '1'),
(5, 5, 1, 1, 4, 1, 'name', 'user_name', 'user_pass', 'user_movil', 'user_email', 'default.png', 'Delete', 'user_resume', 'user_tel', 'ext', '0', '0', '2018-03-26 19:46:17', 'about', '1', '1'),
(6, 3, 1, 1, 4, 1, 'name', 'user_name', 'user_pass', 'user_movil', 'user_email', 'default.png', 'Registro', 'user_resume', 'user_tel', 'ext', '0', '0', '2018-03-26 19:51:29', 'about', '1', '1'),
(7, 3, 1, 1, 4, 1, 'name', 'user_name', 'user_pass', 'user_movil', 'user_email', 'default.png', 'Registro', 'user_resume', 'user_tel', 'ext', '0', '0', '2018-03-26 23:14:57', 'about', '0', '1'),
(8, 4, 1, 1, 4, 1, 'Consulta', 'consulta', '81dc9bdb52d04dc20036dbd8313ed055', '123456879', 'consulta@gmail.com', 'user_b.png', 'Consulta', '1234', '321654987', '321', '0', '0', '2018-03-26 20:35:57', 'Consulta en el Sistema', '1', '1'),
(9, 6, 1, 1, 4, 1, 'name', 'user_name', 'user_pass', 'user_movil', 'user_email', 'default.png', 'Respaldo', 'user_resume', 'user_tel', 'ext', '0', '0', '2018-03-26 20:37:14', 'about', '1', '1'),
(10, 6, 1, 1, 4, 1, 'name', 'user_name', 'user_pass', 'user_movil', 'user_email', 'default.png', 'Respaldo', 'user_resume', 'user_tel', 'ext', '0', '0', '2018-03-26 23:12:21', 'about', '0', '1'),
(11, 6, 1, 1, 4, 1, 'respaldo 2', 'respaldo2', '81dc9bdb52d04dc20036dbd8313ed055', '321654987', 'respaldo2@gmail.com', 'klvst3r.png', 'respaldo 2', 'respaldo2', '98754321', '321', '0', '0', '2018-03-26 23:12:15', 'Para respaldar Informacion', '0', '1'),
(12, 2, 1, 1, 4, 24, 'Kozlov', 'kozlov', '81dc9bdb52d04dc20036dbd8313ed055', '2461645449', 'kozlov@gmail.com', 'ui-danro.jpg', 'Dev', 'CEO Web developer + Edx + UX + UI', '123456789', '123', '0', '0', '2018-04-03 20:36:34', 'I am admin user dev for Internet lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eros nibh, viverra a dui a, gravida varius velit. Vivamus tristique est non ipsum dapibus lacinia sed nec metus.', '1', '1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `answer_docs`
--
ALTER TABLE `answer_docs`
  ADD PRIMARY KEY (`id_answer`),
  ADD KEY `id_status_doc` (`id_status_doc`);

--
-- Indices de la tabla `char_tramits`
--
ALTER TABLE `char_tramits`
  ADD PRIMARY KEY (`id_char`);

--
-- Indices de la tabla `classifications`
--
ALTER TABLE `classifications`
  ADD PRIMARY KEY (`id_classif`);

--
-- Indices de la tabla `departaments`
--
ALTER TABLE `departaments`
  ADD PRIMARY KEY (`id_dep`);

--
-- Indices de la tabla `descriptions`
--
ALTER TABLE `descriptions`
  ADD PRIMARY KEY (`id_desc`);

--
-- Indices de la tabla `destiny_docs`
--
ALTER TABLE `destiny_docs`
  ADD PRIMARY KEY (`id_destiny`);

--
-- Indices de la tabla `directions`
--
ALTER TABLE `directions`
  ADD PRIMARY KEY (`id_dir`);

--
-- Indices de la tabla `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id_doc`),
  ADD KEY `id_type_doc` (`id_type_doc`),
  ADD KEY `id_status_doc` (`id_status_doc`),
  ADD KEY `id_muni` (`id_muni`),
  ADD KEY `id_destiny` (`id_destiny`),
  ADD KEY `id_classif` (`id_classif`);

--
-- Indices de la tabla `documents_turned`
--
ALTER TABLE `documents_turned`
  ADD PRIMARY KEY (`id_turn`),
  ADD KEY `id_dep` (`id_dep`),
  ADD KEY `id_dir` (`id_dir`),
  ADD KEY `id_status_doc` (`id_status_doc`),
  ADD KEY `id_doc` (`id_doc`),
  ADD KEY `id_char` (`id_char`),
  ADD KEY `id_classif` (`id_classif`),
  ADD KEY `id_desc` (`id_desc`);

--
-- Indices de la tabla `history_access`
--
ALTER TABLE `history_access`
  ADD PRIMARY KEY (`id_history`);

--
-- Indices de la tabla `municipalities`
--
ALTER TABLE `municipalities`
  ADD PRIMARY KEY (`id_muni`);

--
-- Indices de la tabla `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id_org`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `privileges`
--
ALTER TABLE `privileges`
  ADD PRIMARY KEY (`id_priv`);

--
-- Indices de la tabla `status_docs`
--
ALTER TABLE `status_docs`
  ADD PRIMARY KEY (`id_status_doc`);

--
-- Indices de la tabla `status_user`
--
ALTER TABLE `status_user`
  ADD PRIMARY KEY (`id_user_status`);

--
-- Indices de la tabla `type_docs`
--
ALTER TABLE `type_docs`
  ADD PRIMARY KEY (`id_type_doc`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_dep` (`id_dep`),
  ADD KEY `id_dir` (`id_dir`),
  ADD KEY `id_priv` (`id_priv`),
  ADD KEY `id_user_status` (`id_user_status`),
  ADD KEY `id_history` (`id_history`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `answer_docs`
--
ALTER TABLE `answer_docs`
  MODIFY `id_answer` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `char_tramits`
--
ALTER TABLE `char_tramits`
  MODIFY `id_char` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `classifications`
--
ALTER TABLE `classifications`
  MODIFY `id_classif` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `departaments`
--
ALTER TABLE `departaments`
  MODIFY `id_dep` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `descriptions`
--
ALTER TABLE `descriptions`
  MODIFY `id_desc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `destiny_docs`
--
ALTER TABLE `destiny_docs`
  MODIFY `id_destiny` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `directions`
--
ALTER TABLE `directions`
  MODIFY `id_dir` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `documents`
--
ALTER TABLE `documents`
  MODIFY `id_doc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `documents_turned`
--
ALTER TABLE `documents_turned`
  MODIFY `id_turn` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `history_access`
--
ALTER TABLE `history_access`
  MODIFY `id_history` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `municipalities`
--
ALTER TABLE `municipalities`
  MODIFY `id_muni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT de la tabla `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id_org` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `privileges`
--
ALTER TABLE `privileges`
  MODIFY `id_priv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `status_docs`
--
ALTER TABLE `status_docs`
  MODIFY `id_status_doc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `status_user`
--
ALTER TABLE `status_user`
  MODIFY `id_user_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `type_docs`
--
ALTER TABLE `type_docs`
  MODIFY `id_type_doc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `answer_docs`
--
ALTER TABLE `answer_docs`
  ADD CONSTRAINT `answer_docs_ibfk_1` FOREIGN KEY (`id_status_doc`) REFERENCES `status_docs` (`id_status_doc`);

--
-- Filtros para la tabla `documents`
--
ALTER TABLE `documents`
  ADD CONSTRAINT `documents_ibfk_1` FOREIGN KEY (`id_type_doc`) REFERENCES `type_docs` (`id_type_doc`),
  ADD CONSTRAINT `documents_ibfk_2` FOREIGN KEY (`id_status_doc`) REFERENCES `status_docs` (`id_status_doc`),
  ADD CONSTRAINT `documents_ibfk_3` FOREIGN KEY (`id_muni`) REFERENCES `municipalities` (`id_muni`),
  ADD CONSTRAINT `documents_ibfk_4` FOREIGN KEY (`id_destiny`) REFERENCES `destiny_docs` (`id_destiny`),
  ADD CONSTRAINT `documents_ibfk_5` FOREIGN KEY (`id_classif`) REFERENCES `classifications` (`id_classif`);

--
-- Filtros para la tabla `documents_turned`
--
ALTER TABLE `documents_turned`
  ADD CONSTRAINT `documents_turned_ibfk_1` FOREIGN KEY (`id_dep`) REFERENCES `departaments` (`id_dep`),
  ADD CONSTRAINT `documents_turned_ibfk_2` FOREIGN KEY (`id_dir`) REFERENCES `directions` (`id_dir`),
  ADD CONSTRAINT `documents_turned_ibfk_3` FOREIGN KEY (`id_status_doc`) REFERENCES `status_docs` (`id_status_doc`),
  ADD CONSTRAINT `documents_turned_ibfk_4` FOREIGN KEY (`id_doc`) REFERENCES `documents` (`id_doc`),
  ADD CONSTRAINT `documents_turned_ibfk_5` FOREIGN KEY (`id_char`) REFERENCES `char_tramits` (`id_char`),
  ADD CONSTRAINT `documents_turned_ibfk_6` FOREIGN KEY (`id_classif`) REFERENCES `classifications` (`id_classif`),
  ADD CONSTRAINT `documents_turned_ibfk_7` FOREIGN KEY (`id_desc`) REFERENCES `descriptions` (`id_desc`);

--
-- Filtros para la tabla `organizations`
--
ALTER TABLE `organizations`
  ADD CONSTRAINT `organizations_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_dep`) REFERENCES `departaments` (`id_dep`),
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`id_dir`) REFERENCES `directions` (`id_dir`),
  ADD CONSTRAINT `users_ibfk_3` FOREIGN KEY (`id_priv`) REFERENCES `privileges` (`id_priv`),
  ADD CONSTRAINT `users_ibfk_4` FOREIGN KEY (`id_user_status`) REFERENCES `status_user` (`id_user_status`),
  ADD CONSTRAINT `users_ibfk_5` FOREIGN KEY (`id_history`) REFERENCES `history_access` (`id_history`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

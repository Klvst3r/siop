-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 23-03-2018 a las 19:50:06
-- Versión del servidor: 5.7.21-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `siop`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `answer_docs`
--


id_destiny
id_muni
id_type_doc
id_status_doc


utf8_spanish_ci



CREATE TABLE `answer_docs` (
  `id_answer` int(11) NOT NULL,
  `id_status_doc` int(11) NOT NULL,
  `folio_doc` int(11) DEFAULT NULL,
  `date_answer` date DEFAULT NULL,
  `oficio_answer` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `char_tramits`
--

CREATE TABLE `char_tramits` (
  `id_char` int(11) NOT NULL,
  `desc_char` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `char_tramits`
--

INSERT INTO `char_tramits` (`id_char`, `desc_char`) VALUES
(1, 'Importante'),
(2, 'Urgente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `classifications`
--

CREATE TABLE `classifications` (
  `id_classif` int(11) NOT NULL,
  `desc_classif` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `classifications`
--

INSERT INTO `classifications` (`id_classif`, `desc_classif`) VALUES
(1, 'Informativo'),
(2, 'Solicitud');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departaments`
--

CREATE TABLE `departaments` (
  `id_dep` int(11) NOT NULL,
  `departament` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `departaments`
--

INSERT INTO `departaments` (`id_dep`, `departament`) VALUES
(1, 'Departemento de Recursos Humanos y Desarrollo Administrativo'),
(2, 'Departamento de Adquisiciones,\r\nRecursos Materiales y Servicios'),
(3, 'Departamento de Contabilidad y Finanzas'),
(4, 'Informática'),
(5, 'Archivo'),
(6, 'Almacen 1'),
(7, 'Almacen 2'),
(8, 'Seguimiento a Auditorias'),
(9, 'Departamento de Gestión de Derecho de Vía y Afectaciones'),
(10, 'Departamento de Obras por Administración y Equipamiento'),
(11, 'Departamento de Infraestructura Vial'),
(12, 'Departamento de Concertación Social y Apoyos de Maquinaria'),
(13, 'Departamento de Eventos Especiales'),
(14, 'Departamento de Agua Potable, Alcantarillado y Saneamiento'),
(15, 'Departamento de Infraestructura Urbana'),
(16, 'Departamento Jurídico'),
(17, 'Departamento de Planeación y Coordinación de Programas'),
(18, 'Departamento de Proyectos'),
(19, 'Departamento de Estadística e Información Geográfica'),
(20, 'Departamento de Programas y Registro del Desarrollo Urbano'),
(21, 'Departamento de Licitaciones y Contratos'),
(22, 'Precios Unitarios y Ajuste de Costos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descriptions`
--

CREATE TABLE `descriptions` (
  `id_desc` int(11) NOT NULL,
  `content_desc` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `descriptions`
--

INSERT INTO `descriptions` (`id_desc`, `content_desc`) VALUES
(1, 'Notificaciones'),
(2, 'Dictamen de Congruencia'),
(3, 'Eventos Especiales'),
(4, 'Invitaciones'),
(5, 'Maquinaria'),
(6, 'Correo Electrónico'),
(7, 'Para su Conocimiento'),
(8, 'Tarjeta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `destiny_docs`
--

CREATE TABLE `destiny_docs` (
  `id_destiny` int(11) NOT NULL,
  `desc_destiny` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `destiny_position` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `destiny_docs`
--

INSERT INTO `destiny_docs` (`id_destiny`, `desc_destiny`, `destiny_position`) VALUES
(1, 'Arq. Francisco Javier Romero Ahuactzi', 'Secretario '),
(2, 'C.P. Carlos Sánchez Tamayo', 'Director Administrativo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directions`
--

CREATE TABLE `directions` (
  `id_dir` int(11) NOT NULL,
  `dir_name` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `directions`
--

INSERT INTO `directions` (`id_dir`, `dir_name`) VALUES
(1, 'Dirección Administrativa'),
(2, 'Dirección de Obras Públicas'),
(3, 'Dirección de Desarrollo Urbano'),
(4, 'Dirección de Licitaciones, Precios Unitarios y Contratos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documents`
--

CREATE TABLE `documents` (
  `id_doc` int(11) NOT NULL,
  `id_destiny` int(11) NOT NULL,
  `id_muni` int(11) NOT NULL,
  `id_type_doc` int(11) NOT NULL,
  `id_status_doc` int(11) NOT NULL,
  `id_classif` int(11) NOT NULL,
  `subject_doc` text COLLATE utf8_spanish_ci,
  `desc_send_position` varchar(120) COLLATE utf8_spanish_ci DEFAULT NULL,
  `folio_doc` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `send_doc` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `origin_doc` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dir_sender` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `movil_doc` int(11) DEFAULT NULL,
  `tel_doc` int(11) DEFAULT NULL,
  `ext_doc` varchar(6) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email_doc` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `date_doc` date DEFAULT NULL,
  `date_recep` date DEFAULT NULL,
  `reference` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `status` enum('0','1','2') COLLATE utf8_spanish_ci DEFAULT NULL,
  `answer` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `observation` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='This is the principal Table to register the information of the documents who arrive to the organization; is accompanied for the information of the origin like municipalities, typo of document, who is the sender, what is the status of the document.';

--
-- Volcado de datos para la tabla `documents`
--

INSERT INTO `documents` (`id_doc`, `id_destiny`, `id_muni`, `id_type_doc`, `id_status_doc`, `id_classif`, `subject_doc`, `desc_send_position`, `folio_doc`, `send_doc`, `origin_doc`, `dir_sender`, `movil_doc`, `tel_doc`, `ext_doc`, `email_doc`, `date_doc`, `date_recep`, `reference`, `status`, `answer`, `observation`) VALUES
(11, 1, 1, 1, 1, 0, 'Test', 'CEO', '1', 'Klvst3r', 'Dashboard', 'Internet', 123456789, 987654321, '4321', 'klvst3r@gmail.com', '2018-02-01', '2018-02-05', 'Dev', '2', '', ''),
(12, 2, 33, 2, 2, 0, 'Asunto 2', 'Dev', '2', 'Tester', 'Edificio', 'Internet', 987654321, 123456789, '4321', 'tester@gmail.com', '2018-02-01', '2018-02-07', 'Referencia 2', '2', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documents_turned`
--

CREATE TABLE `documents_turned` (
  `id_turn` int(11) NOT NULL,
  `id_doc` int(11) NOT NULL,
  `id_dir` int(11) NOT NULL,
  `id_status_doc` int(11) NOT NULL,
  `id_dep` int(11) NOT NULL,
  `id_char` int(11) NOT NULL,
  `id_classif` int(11) NOT NULL,
  `id_desc` int(11) NOT NULL,
  `folio_turned` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `doc_origin` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `doc_ccp` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `date_turned` date DEFAULT NULL,
  `instructions` text COLLATE utf8_spanish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='After tegistration of the document, the user turn the document at the organization and register the action, here will register the information about the document, the status, chracter of tamit and classificatin of the document';

--
-- Volcado de datos para la tabla `documents_turned`
--

INSERT INTO `documents_turned` (`id_turn`, `id_doc`, `id_dir`, `id_status_doc`, `id_dep`, `id_char`, `id_classif`, `id_desc`, `folio_turned`, `doc_origin`, `doc_ccp`, `date_turned`, `instructions`) VALUES
(1, 11, 1, 1, 1, 1, 1, 1, 'folio_turned', 'doc_origin', 'doc_ccp', '2018-03-20', 'Instructions'),
(3, 12, 2, 2, 2, 2, 2, 2, '2', 'Original 2', 'Copia 2', '2018-03-22', 'Intrucciones 2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `history_access`
--

CREATE TABLE `history_access` (
  `id_history` int(11) NOT NULL,
  `ip` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `date_access` date DEFAULT NULL,
  `time_in` time DEFAULT NULL,
  `time_out` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `history_access`
--

INSERT INTO `history_access` (`id_history`, `ip`, `date_access`, `time_in`, `time_out`) VALUES
(1, '127.0.0.1', '2017-10-25', '18:00:00', NULL),
(2, '::1', '2017-10-30', '18:39:43', '06:00:00'),
(3, '::1', '2017-10-30', '18:43:01', '06:00:00'),
(4, '::1', '2017-10-30', '18:44:59', '06:00:00'),
(5, '::1', '2017-10-30', '18:45:18', '06:00:00'),
(6, '::1', '2017-10-30', '18:46:02', '06:00:00'),
(7, '::1', '2017-10-30', '18:47:56', '06:00:00'),
(8, '::1', '2017-10-30', '18:55:56', '06:00:00'),
(9, '::1', '2017-10-30', '18:56:46', '06:00:00'),
(10, '10.50.30.50', '2017-10-30', '18:59:04', '06:00:00'),
(11, '10.50.30.50', '2017-10-30', '19:07:45', '06:00:00'),
(12, '::1', '2017-10-30', '19:15:48', '06:00:00'),
(13, '::1', '2017-10-30', '19:16:11', '06:00:00'),
(14, NULL, '2017-10-30', '19:18:00', '06:00:00'),
(15, '::1', '2017-10-30', '19:19:34', '06:00:00'),
(16, '::1', '2017-10-30', '19:20:01', '06:00:00'),
(17, '::1', '2017-10-30', '19:20:35', '06:00:00'),
(18, '::1', '2017-10-30', '19:21:11', '06:00:00'),
(19, '::1', '2017-10-30', '19:22:08', '06:00:00'),
(20, '::1', '2017-10-30', '19:22:43', '06:00:00'),
(21, '::1', '2017-10-30', '19:23:36', '06:00:00'),
(22, '127.0.0.1', '2017-10-30', '19:24:54', '06:00:00'),
(23, '127.0.0.1', '2017-10-30', '19:25:41', '06:00:00'),
(24, '127.0.0.1', '2017-10-30', '19:26:11', '06:00:00'),
(25, '127.0.0.1', '2017-10-30', '19:26:23', '06:00:00'),
(26, '127.0.0.1', '2017-10-30', '19:26:36', '06:00:00'),
(27, '127.0.0.1', '2017-10-30', '19:27:12', '06:00:00'),
(28, '127.0.0.1', '2017-10-30', '19:28:41', '06:00:00'),
(29, '127.0.0.1', '2017-10-30', '19:28:54', '06:00:00'),
(30, '127.0.0.1', '2017-10-30', '19:29:31', '06:00:00'),
(31, '127.0.0.1', '2017-10-30', '19:33:00', '06:00:00'),
(32, '127.0.0.1', '2017-10-30', '19:35:02', '06:00:00'),
(33, '127.0.0.1', '2017-10-30', '19:35:39', '06:00:00'),
(34, '127.0.0.1', '2017-10-30', '19:38:32', '06:00:00'),
(35, '127.0.0.1', '2017-10-30', '19:38:54', '06:00:00'),
(36, '127.0.0.1', '2017-10-30', '19:39:18', '06:00:00'),
(37, '127.0.0.1', '2017-10-30', '19:39:44', '06:00:00'),
(38, '127.0.0.1', '2017-10-30', '19:40:11', '06:00:00'),
(39, '127.0.0.1', '2017-10-30', '19:41:49', '06:00:00'),
(40, '127.0.0.1', '2017-10-30', '19:42:14', '06:00:00'),
(41, '127.0.0.1', '2017-10-30', '19:42:46', '06:00:00'),
(42, '127.0.0.1', '2017-10-30', '19:44:45', '06:00:00'),
(43, '127.0.0.1', '2017-10-30', '19:45:55', '06:00:00'),
(44, '127.0.0.1', '2017-10-30', '19:47:15', '06:00:00'),
(45, '127.0.0.1', '2017-10-30', '20:45:16', '06:00:00'),
(46, '127.0.0.1', '2017-10-30', '20:45:40', '06:00:00'),
(47, '127.0.0.1', '2017-10-30', '20:46:20', '06:00:00'),
(48, '127.0.0.1', '2017-10-31', '16:56:35', '06:00:00'),
(49, '127.0.0.1', '2017-10-31', '16:56:39', '06:00:00'),
(50, '127.0.0.1', '2017-10-31', '16:56:47', '06:00:00'),
(51, '127.0.0.1', '2017-10-31', '16:59:23', '06:00:00'),
(52, '127.0.0.1', '2017-10-31', '16:59:39', '06:00:00'),
(53, '127.0.0.1', '2017-10-31', '17:00:27', '06:00:00'),
(54, '127.0.0.1', '2017-10-31', '17:01:06', '06:00:00'),
(55, '127.0.0.1', '2017-10-31', '17:02:15', '06:00:00'),
(56, '127.0.0.1', '2017-10-31', '17:02:17', '06:00:00'),
(57, '127.0.0.1', '2017-10-31', '17:02:18', '06:00:00'),
(58, '127.0.0.1', '2017-10-31', '17:02:18', '06:00:00'),
(59, '127.0.0.1', '2017-10-31', '17:02:18', '06:00:00'),
(60, '127.0.0.1', '2017-10-31', '17:02:18', '06:00:00'),
(61, '127.0.0.1', '2017-10-31', '17:02:18', '06:00:00'),
(62, '127.0.0.1', '2017-10-31', '17:02:19', '06:00:00'),
(63, '127.0.0.1', '2017-10-31', '17:02:19', '06:00:00'),
(64, '127.0.0.1', '2017-10-31', '17:02:30', '06:00:00'),
(65, '127.0.0.1', '2017-10-31', '17:03:43', '06:00:00'),
(66, '127.0.0.1', '2017-10-31', '17:04:09', '06:00:00'),
(67, '127.0.0.1', '2017-11-01', '15:19:22', '06:00:00'),
(68, '127.0.0.1', '2017-11-01', '15:21:18', '06:00:00'),
(69, '127.0.0.1', '2017-11-01', '15:21:23', '06:00:00'),
(70, '127.0.0.1', '2017-11-01', '15:21:29', '06:00:00'),
(71, '127.0.0.1', '2017-11-01', '15:26:49', '06:00:00'),
(72, '127.0.0.1', '2017-11-01', '15:26:57', '06:00:00'),
(73, '127.0.0.1', '2017-11-01', '15:29:31', '06:00:00'),
(74, '127.0.0.1', '2017-11-01', '15:29:43', '06:00:00'),
(75, '127.0.0.1', '2017-11-01', '15:30:27', '06:00:00'),
(76, '127.0.0.1', '2017-11-01', '15:30:46', '06:00:00'),
(77, '127.0.0.1', '2017-11-01', '15:45:19', '06:00:00'),
(78, '127.0.0.1', '2017-11-01', '15:47:10', '06:00:00'),
(79, '127.0.0.1', '2017-11-01', '15:47:16', '06:00:00'),
(80, '127.0.0.1', '2017-11-01', '15:59:27', '06:00:00'),
(81, '127.0.0.1', '2017-11-01', '16:07:35', '06:00:00'),
(82, '127.0.0.1', '2017-11-01', '16:07:40', '06:00:00'),
(83, '127.0.0.1', '2017-11-01', '16:08:06', '06:00:00'),
(84, '127.0.0.1', '2017-11-01', '16:11:27', '06:00:00'),
(85, '127.0.0.1', '2017-11-01', '16:11:32', '06:00:00'),
(86, '127.0.0.1', '2017-11-01', '16:11:54', '06:00:00'),
(87, '127.0.0.1', '2017-11-01', '16:12:12', '06:00:00'),
(88, '127.0.0.1', '2017-11-01', '16:12:52', '06:00:00'),
(89, '127.0.0.1', '2017-11-01', '16:13:15', '06:00:00'),
(90, '127.0.0.1', '2017-11-01', '16:13:36', '06:00:00'),
(91, '127.0.0.1', '2017-11-01', '16:14:16', '06:00:00'),
(92, '127.0.0.1', '2017-11-01', '16:15:04', '06:00:00'),
(93, '127.0.0.1', '2017-11-01', '17:10:37', '06:00:00'),
(94, '127.0.0.1', '2017-11-01', '17:11:59', '06:00:00'),
(95, '127.0.0.1', '2017-11-01', '17:13:14', '06:00:00'),
(96, '127.0.0.1', '2017-11-01', '17:14:25', '06:00:00'),
(97, '127.0.0.1', '2017-11-01', '17:14:46', '06:00:00'),
(98, '127.0.0.1', '2017-11-01', '17:15:26', '06:00:00'),
(99, '127.0.0.1', '2017-11-01', '17:15:52', '06:00:00'),
(100, '127.0.0.1', '2017-11-01', '17:16:14', '06:00:00'),
(101, '127.0.0.1', '2018-03-06', '17:11:16', NULL),
(102, '127.0.0.1', '2018-03-07', '19:42:10', NULL),
(103, '127.0.0.1', '2018-03-08', '09:08:46', NULL),
(104, '127.0.0.1', '2018-03-08', '14:19:47', NULL),
(105, '127.0.0.1', '2018-03-08', '19:25:58', NULL),
(106, '127.0.0.1', '2018-03-08', '19:26:19', NULL),
(107, '127.0.0.1', '2018-03-08', '19:50:32', NULL),
(108, '127.0.0.1', '2018-03-12', '10:19:32', NULL),
(109, '127.0.0.1', '2018-03-12', '10:46:14', NULL),
(110, '127.0.0.1', '2018-03-12', '14:49:34', NULL),
(111, '127.0.0.1', '2018-03-12', '20:53:57', NULL),
(112, '127.0.0.1', '2018-03-13', '10:04:58', NULL),
(113, '127.0.0.1', '2018-03-13', '11:54:45', NULL),
(114, '127.0.0.1', '2018-03-13', '14:14:15', NULL),
(115, '127.0.0.1', '2018-03-13', '14:14:46', NULL),
(116, '127.0.0.1', '2018-03-16', '20:45:17', NULL),
(117, '127.0.0.1', '2018-03-20', '13:18:11', NULL),
(118, '127.0.0.1', '2018-03-20', '14:55:12', NULL),
(119, '127.0.0.1', '2018-03-20', '15:15:20', NULL),
(120, '127.0.0.1', '2018-03-20', '20:00:52', NULL),
(121, '127.0.0.1', '2018-03-20', '20:01:22', NULL),
(122, '127.0.0.1', '2018-03-21', '12:23:42', NULL),
(123, '127.0.0.1', '2018-03-21', '12:24:58', NULL),
(124, '127.0.0.1', '2018-03-22', '10:15:00', NULL),
(125, '127.0.0.1', '2018-03-22', '13:10:29', NULL),
(126, '127.0.0.1', '2018-03-22', '13:58:58', NULL),
(127, '127.0.0.1', '2018-03-22', '14:30:35', NULL),
(128, '127.0.0.1', '2018-03-22', '15:38:02', NULL),
(129, '127.0.0.1', '2018-03-22', '18:46:27', NULL),
(130, '127.0.0.1', '2018-03-22', '18:53:05', NULL),
(131, '127.0.0.1', '2018-03-22', '20:23:14', NULL),
(132, '127.0.0.1', '2018-03-23', '10:07:25', NULL),
(133, '127.0.0.1', '2018-03-23', '11:54:39', NULL),
(134, '127.0.0.1', '2018-03-23', '11:55:12', NULL),
(135, '127.0.0.1', '2018-03-23', '13:17:12', NULL),
(136, '127.0.0.1', '2018-03-23', '13:19:44', NULL),
(137, '127.0.0.1', '2018-03-23', '15:32:28', NULL),
(138, '127.0.0.1', '2018-03-23', '15:51:26', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipalities`
--

CREATE TABLE `municipalities` (
  `id_muni` int(11) NOT NULL,
  `desc_muni` varchar(65) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `municipalities`
--

INSERT INTO `municipalities` (`id_muni`, `desc_muni`) VALUES
(1, 'Amaxac de Guerrero'),
(2, 'Apetatitlan de Antonio Carvajal'),
(3, 'Atlangatepec'),
(4, 'Atltzayanca'),
(5, 'Apizaco'),
(6, 'Calpulalpan'),
(7, 'El Carmen Tequexquitla'),
(8, 'Cuapiaxtla'),
(9, 'Cuaxomulco'),
(10, 'Chiautempan'),
(11, 'Muñoz de Domingo Arenas'),
(12, 'Españita'),
(13, 'Huamantla'),
(14, 'Hueyotlipan'),
(15, 'Ixtacuixtla de Mariano Matamoros'),
(16, 'Ixtenco'),
(17, 'Mazatecochco de José María Morelos'),
(18, 'Contla de Juan Cuamatzi'),
(19, 'Tepetitla de Lardizábal'),
(20, 'Sanctórum de Lázaro Cárdenas'),
(21, 'Nanacamilpa de Mariano Arista'),
(22, 'Acuamanala de Miguel Hidalgo'),
(23, 'Natívitas'),
(24, 'Panotla'),
(25, 'San Pablo del Monte'),
(26, 'Santa Cruz Tlaxcala'),
(27, 'Tenancingo'),
(28, 'Teolocholco'),
(29, 'Tepeyanco'),
(30, 'Terrenate'),
(31, 'Tetla de la Solidaridad'),
(32, 'Tetlatlahuca'),
(33, 'Tlaxcala'),
(34, 'Tlaxco'),
(35, 'Tocatlán'),
(36, 'Totolac'),
(37, 'Ziltlaltépec de Trinidad Sánchez '),
(38, 'Tzompantepec'),
(39, 'Xaloztoc'),
(40, 'Xaltocan'),
(41, 'Papalotla de Xicohténcatl'),
(42, 'Xicohtzinco'),
(43, 'Yauhquemehcan'),
(44, 'Zacatelco'),
(45, 'Benito Juárez'),
(46, 'Emiliano Zapata'),
(47, 'Lázaro Cárdenas'),
(48, 'La Magdalena Tlaltelulco'),
(49, 'San Damián Texóloc'),
(50, 'San Francisco Tetlanohcan'),
(51, 'San Jerónimo Zacualpan'),
(52, 'San José Teacalco'),
(53, 'San Juan Huactzinco'),
(54, 'San Lorenzo Axocomanitla'),
(55, 'San Lucas Tecopilco'),
(56, 'Santa Ana Nopalucan'),
(57, 'Santa Apolonia Teacalco'),
(58, 'Santa Catarina Ayometla'),
(59, 'Santa Cruz Quilehtla'),
(60, 'Santa Isabel Xiloxoxtla');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `organizations`
--

CREATE TABLE `organizations` (
  `id_org` int(11) NOT NULL,
  `org_nick` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_name` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_dir` varchar(99) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_pob` varchar(35) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_cp` varchar(6) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_tel` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_ext` char(6) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_email` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `organizations`
--

INSERT INTO `organizations` (`id_org`, `org_nick`, `org_name`, `org_dir`, `org_pob`, `org_cp`, `org_tel`, `org_ext`, `org_email`, `id_user`) VALUES
(1, 'SECODUVI', 'Secretaria de Obras Públicas, Desarrollo Urvano y Vivienda', 'Km. 1.5 Carretera Tlaxcala-Puebla', 'Tlaxcala, Tlax.', '90000', '01(246)4652960', '3907', 'secoduvi@tlaxcala.gob.mx', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privileges`
--

CREATE TABLE `privileges` (
  `id_priv` int(11) NOT NULL,
  `desc_priv` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL,
  `create_priv` enum('N','Y') COLLATE utf8_spanish_ci DEFAULT NULL,
  `select_priv` enum('N','Y') COLLATE utf8_spanish_ci DEFAULT NULL,
  `insert_priv` enum('N','Y') COLLATE utf8_spanish_ci DEFAULT NULL,
  `update_priv` enum('N','Y') COLLATE utf8_spanish_ci DEFAULT NULL,
  `delete_priv` enum('N','Y') COLLATE utf8_spanish_ci DEFAULT NULL,
  `backup_priv` enum('N','Y') COLLATE utf8_spanish_ci DEFAULT NULL,
  `report_priv` enum('N','Y') COLLATE utf8_spanish_ci DEFAULT NULL,
  `status` enum('1','0') COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `privileges`
--

INSERT INTO `privileges` (`id_priv`, `desc_priv`, `create_priv`, `select_priv`, `insert_priv`, `update_priv`, `delete_priv`, `backup_priv`, `report_priv`, `status`) VALUES
(1, 'Administración del sistema', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'N', '1'),
(2, 'Dev', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', '1'),
(3, 'Registro de Información', 'N', 'Y', 'Y', 'Y', 'N', 'N', 'N', '1'),
(4, 'Consulta de Información', 'N', 'Y', 'N', 'N', 'N', 'N', 'Y', '1'),
(5, 'Borrado de Información', 'N', 'Y', 'Y', 'Y', 'Y', 'N', 'N', '1'),
(6, 'Respaldo de Información', 'N', 'Y', 'N', 'N', 'N', 'Y', 'N', '1'),
(7, 'deleted', 'N', 'Y', 'N', 'Y', 'N', 'Y', 'N', '0'),
(8, 'modify', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', '0'),
(9, 'alert', 'N', 'N', 'N', 'N', 'N', 'N', 'N', '0'),
(10, 'New Privilege', 'N', 'N', 'N', 'N', 'N', 'N', 'N', '0'),
(11, 'Privilege reedited', 'N', 'Y', 'N', 'Y', 'N', 'Y', 'N', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_docs`
--

CREATE TABLE `status_docs` (
  `id_status_doc` int(11) NOT NULL,
  `desc_status_docs` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `status_docs`
--

INSERT INTO `status_docs` (`id_status_doc`, `desc_status_docs`) VALUES
(1, 'Iniciado'),
(2, 'Enterado'),
(3, 'En Trámite'),
(4, 'Pendiente'),
(5, 'Concluido');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_user`
--

CREATE TABLE `status_user` (
  `id_user_status` int(11) NOT NULL,
  `desc_user_status` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `status_user`
--

INSERT INTO `status_user` (`id_user_status`, `desc_user_status`) VALUES
(1, 'Active'),
(2, 'Inactive');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_docs`
--

CREATE TABLE `type_docs` (
  `id_type_doc` int(11) NOT NULL,
  `desc_type_doc` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `type_docs`
--

INSERT INTO `type_docs` (`id_type_doc`, `desc_type_doc`) VALUES
(1, 'Circular'),
(2, 'Convocatoria'),
(3, 'Email'),
(4, 'Engargolado'),
(5, 'Factura'),
(6, 'Invitación'),
(7, 'Oficio'),
(8, 'Sobre'),
(9, 'Tarjeta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `id_priv` int(11) NOT NULL,
  `id_user_status` int(11) NOT NULL,
  `id_dir` int(11) NOT NULL,
  `id_dep` int(11) NOT NULL,
  `id_history` int(11) NOT NULL,
  `name` varchar(120) COLLATE utf8_spanish_ci NOT NULL,
  `user_name` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `user_pass` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `user_movil` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_email` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_photo` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_position` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `user_resume` varchar(120) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_tel` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_ext` varchar(6) COLLATE utf8_spanish_ci DEFAULT NULL,
  `active` varchar(2) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_online` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `date_reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `about` text COLLATE utf8_spanish_ci,
  `status` enum('1','0') COLLATE utf8_spanish_ci DEFAULT NULL,
  `complete` enum('1','0') COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='This is the table where register users, his privileges, her status in the system, his departament, his history and the characteristics of the count''s owner.';

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id_user`, `id_priv`, `id_user_status`, `id_dir`, `id_dep`, `id_history`, `name`, `user_name`, `user_pass`, `user_movil`, `user_email`, `user_photo`, `user_position`, `user_resume`, `user_tel`, `user_ext`, `active`, `user_online`, `date_reg`, `about`, `status`, `complete`) VALUES
(2, 1, 1, 3, 1, 100, 'Klvst3r Strk', 'klvst3r', '81dc9bdb52d04dc20036dbd8313ed055', '2461645449', 'klvst3r@gmail.com', 'ui-danro.jpg', 'Dev', 'CEO Web developer + Edx + UX + UI', '1234567890', '0', '0', '0', '2018-03-05 22:48:47', 'I am admin user dev for Internet lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eros nibh, viverra a dui a, gravida varius velit. Vivamus tristique est non ipsum dapibus lacinia sed nec metus.', '1', '1'),
(3, 2, 1, 1, 1, 138, 'Kozlov', 'kozlov', '81dc9bdb52d04dc20036dbd8313ed055', '2461645449', 'kozlov@gmail.com', 'ui-danro.jpg', 'Register', 'CEO Web developer + Edx + UX + UI', '2461645449', '0', '0', '0', '2018-03-23 20:51:26', 'Register of Information in System', '1', '1'),
(6, 1, 1, 1, 1, 1, 'delete', 'user_name', 'user_pass', 'user_movil', 'delete@gmail', 'lOTiyr.jpg', 'user_position', 'User to delete', 'user_tel', 'ext', '0', '0', '2018-01-03 13:26:04', 'about', '0', '1'),
(7, 2, 1, 1, 1, 1, 'Kozlov', 'kozlov', 'user_pass', '159487263', 'kozlov@gmail.com', '8154073189_39c2dd8d49_z.jpg', 'user_position', 'kozlov =&gt; Tester', '357241689', '357', '0', '0', '2018-01-03 13:26:00', 'Marketng on Line', '0', '1'),
(8, 3, 1, 1, 1, 1, 'User Register', 'register', '81dc9bdb52d04dc20036dbd8313ed055', '123456789', 'register@gmail.com', 'default.png', 'user_position', 'Registration in the system', '987654321', '1234', '0', '0', '2018-01-03 13:25:58', 'User + Register', '0', '1'),
(9, 4, 1, 1, 1, 1, 'Consultas User', 'consulta', '81dc9bdb52d04dc20036dbd8313ed055', '123456789', 'consulta@gmail.com', 'ui-danro.jpg', 'Consultas', 'queries', '987654321', '1234', '0', '0', '2017-12-13 06:47:01', 'Consulta de información', '0', '1'),
(10, 1, 1, 1, 1, 1, 'otro', 'otro', '81dc9bdb52d04dc20036dbd8313ed055', '123456789', 'otro@gmail.com', 'default.png', 'Otro', 'otro', '987654321', '1234', '0', '0', '2017-12-13 06:43:16', 'otro', '0', '1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `answer_docs`
--
ALTER TABLE `answer_docs`
  ADD PRIMARY KEY (`id_answer`),
  ADD KEY `id_status_doc` (`id_status_doc`);

--
-- Indices de la tabla `char_tramits`
--
ALTER TABLE `char_tramits`
  ADD PRIMARY KEY (`id_char`);

--
-- Indices de la tabla `classifications`
--
ALTER TABLE `classifications`
  ADD PRIMARY KEY (`id_classif`);

--
-- Indices de la tabla `departaments`
--
ALTER TABLE `departaments`
  ADD PRIMARY KEY (`id_dep`);

--
-- Indices de la tabla `descriptions`
--
ALTER TABLE `descriptions`
  ADD PRIMARY KEY (`id_desc`);

--
-- Indices de la tabla `destiny_docs`
--
ALTER TABLE `destiny_docs`
  ADD PRIMARY KEY (`id_destiny`);

--
-- Indices de la tabla `directions`
--
ALTER TABLE `directions`
  ADD PRIMARY KEY (`id_dir`);

--
-- Indices de la tabla `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id_doc`),
  ADD KEY `id_type_doc` (`id_type_doc`),
  ADD KEY `id_status_doc` (`id_status_doc`),
  ADD KEY `id_muni` (`id_muni`),
  ADD KEY `id_destiny` (`id_destiny`);

--
-- Indices de la tabla `documents_turned`
--
ALTER TABLE `documents_turned`
  ADD PRIMARY KEY (`id_turn`),
  ADD KEY `id_dep` (`id_dep`),
  ADD KEY `id_dir` (`id_dir`),
  ADD KEY `id_status_doc` (`id_status_doc`),
  ADD KEY `id_doc` (`id_doc`),
  ADD KEY `id_char` (`id_char`),
  ADD KEY `id_classif` (`id_classif`),
  ADD KEY `id_desc` (`id_desc`);

--
-- Indices de la tabla `history_access`
--
ALTER TABLE `history_access`
  ADD PRIMARY KEY (`id_history`);

--
-- Indices de la tabla `municipalities`
--
ALTER TABLE `municipalities`
  ADD PRIMARY KEY (`id_muni`);

--
-- Indices de la tabla `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id_org`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `privileges`
--
ALTER TABLE `privileges`
  ADD PRIMARY KEY (`id_priv`);

--
-- Indices de la tabla `status_docs`
--
ALTER TABLE `status_docs`
  ADD PRIMARY KEY (`id_status_doc`);

--
-- Indices de la tabla `status_user`
--
ALTER TABLE `status_user`
  ADD PRIMARY KEY (`id_user_status`);

--
-- Indices de la tabla `type_docs`
--
ALTER TABLE `type_docs`
  ADD PRIMARY KEY (`id_type_doc`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_dep` (`id_dep`),
  ADD KEY `id_dir` (`id_dir`),
  ADD KEY `id_priv` (`id_priv`),
  ADD KEY `id_user_status` (`id_user_status`),
  ADD KEY `id_history` (`id_history`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `answer_docs`
--
ALTER TABLE `answer_docs`
  MODIFY `id_answer` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `char_tramits`
--
ALTER TABLE `char_tramits`
  MODIFY `id_char` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `classifications`
--
ALTER TABLE `classifications`
  MODIFY `id_classif` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `departaments`
--
ALTER TABLE `departaments`
  MODIFY `id_dep` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `descriptions`
--
ALTER TABLE `descriptions`
  MODIFY `id_desc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `destiny_docs`
--
ALTER TABLE `destiny_docs`
  MODIFY `id_destiny` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `directions`
--
ALTER TABLE `directions`
  MODIFY `id_dir` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `documents`
--
ALTER TABLE `documents`
  MODIFY `id_doc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `documents_turned`
--
ALTER TABLE `documents_turned`
  MODIFY `id_turn` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `history_access`
--
ALTER TABLE `history_access`
  MODIFY `id_history` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;
--
-- AUTO_INCREMENT de la tabla `municipalities`
--
ALTER TABLE `municipalities`
  MODIFY `id_muni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT de la tabla `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id_org` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `privileges`
--
ALTER TABLE `privileges`
  MODIFY `id_priv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `status_docs`
--
ALTER TABLE `status_docs`
  MODIFY `id_status_doc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `status_user`
--
ALTER TABLE `status_user`
  MODIFY `id_user_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `type_docs`
--
ALTER TABLE `type_docs`
  MODIFY `id_type_doc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `answer_docs`
--
ALTER TABLE `answer_docs`
  ADD CONSTRAINT `answer_docs_ibfk_1` FOREIGN KEY (`id_status_doc`) REFERENCES `status_docs` (`id_status_doc`);

--
-- Filtros para la tabla `documents`
--
ALTER TABLE `documents`
  ADD CONSTRAINT `documents_ibfk_1` FOREIGN KEY (`id_type_doc`) REFERENCES `type_docs` (`id_type_doc`),
  ADD CONSTRAINT `documents_ibfk_2` FOREIGN KEY (`id_status_doc`) REFERENCES `status_docs` (`id_status_doc`),
  ADD CONSTRAINT `documents_ibfk_3` FOREIGN KEY (`id_muni`) REFERENCES `municipalities` (`id_muni`),
  ADD CONSTRAINT `documents_ibfk_4` FOREIGN KEY (`id_destiny`) REFERENCES `destiny_docs` (`id_destiny`);

--
-- Filtros para la tabla `documents_turned`
--
ALTER TABLE `documents_turned`
  ADD CONSTRAINT `documents_turned_ibfk_1` FOREIGN KEY (`id_dep`) REFERENCES `departaments` (`id_dep`),
  ADD CONSTRAINT `documents_turned_ibfk_2` FOREIGN KEY (`id_dir`) REFERENCES `directions` (`id_dir`),
  ADD CONSTRAINT `documents_turned_ibfk_3` FOREIGN KEY (`id_status_doc`) REFERENCES `status_docs` (`id_status_doc`),
  ADD CONSTRAINT `documents_turned_ibfk_4` FOREIGN KEY (`id_doc`) REFERENCES `documents` (`id_doc`),
  ADD CONSTRAINT `documents_turned_ibfk_5` FOREIGN KEY (`id_char`) REFERENCES `char_tramits` (`id_char`),
  ADD CONSTRAINT `documents_turned_ibfk_6` FOREIGN KEY (`id_classif`) REFERENCES `classifications` (`id_classif`),
  ADD CONSTRAINT `documents_turned_ibfk_7` FOREIGN KEY (`id_desc`) REFERENCES `descriptions` (`id_desc`);

--
-- Filtros para la tabla `organizations`
--
ALTER TABLE `organizations`
  ADD CONSTRAINT `organizations_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_dep`) REFERENCES `departaments` (`id_dep`),
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`id_dir`) REFERENCES `directions` (`id_dir`),
  ADD CONSTRAINT `users_ibfk_3` FOREIGN KEY (`id_priv`) REFERENCES `privileges` (`id_priv`),
  ADD CONSTRAINT `users_ibfk_4` FOREIGN KEY (`id_user_status`) REFERENCES `status_user` (`id_user_status`),
  ADD CONSTRAINT `users_ibfk_5` FOREIGN KEY (`id_history`) REFERENCES `history_access` (`id_history`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
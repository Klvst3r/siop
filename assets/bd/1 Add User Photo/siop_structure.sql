-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 25-10-2017 a las 10:47:59
-- Versión del servidor: 5.7.20-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `siop`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `answer_docs`
--

CREATE TABLE `answer_docs` (
  `id_answer` int(11) NOT NULL,
  `folio_doc` int(11) DEFAULT NULL,
  `date_answer` date DEFAULT NULL,
  `oficio_answer` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_status_doc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `char_tramits`
--

CREATE TABLE `char_tramits` (
  `id_char` int(11) NOT NULL,
  `desc_char` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `classifications`
--

CREATE TABLE `classifications` (
  `id_classif` int(11) NOT NULL,
  `desc_classif` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departaments`
--

CREATE TABLE `departaments` (
  `id_dep` int(11) NOT NULL,
  `departament` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_dir` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descriptions`
--

CREATE TABLE `descriptions` (
  `id_desc` int(11) NOT NULL,
  `content_desc` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `destiny_docs`
--

CREATE TABLE `destiny_docs` (
  `id_destiny` int(11) NOT NULL,
  `desc_destiny` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `destiny_position` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directions`
--

CREATE TABLE `directions` (
  `id_dir` int(11) NOT NULL,
  `dir_name` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documents`
--

CREATE TABLE `documents` (
  `id_doc` int(11) NOT NULL,
  `id_destiny` int(11) NOT NULL,
  `id_muni` int(11) NOT NULL,
  `id_type_doc` int(11) NOT NULL,
  `id_status_doc` int(11) NOT NULL,
  `id_sender` int(11) NOT NULL,
  `folio_doc` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `send_doc` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `origin_doc` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `movil_doc` int(11) DEFAULT NULL,
  `tel_doc` int(11) DEFAULT NULL,
  `ext_doc` varchar(6) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email_doc` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `date_doc` date DEFAULT NULL,
  `date_recep` date DEFAULT NULL,
  `reference` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='This is the principal Table to register the information of the documents who arrive to the organization; is accompanied for the information of the origin like municipalities, typo of document, who is the sender, what is the status of the document.';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documents_turned`
--

CREATE TABLE `documents_turned` (
  `id_turn` int(11) NOT NULL,
  `id_doc` int(11) NOT NULL,
  `id_status_doc` int(11) NOT NULL,
  `id_dep` int(11) NOT NULL,
  `id_char` int(11) NOT NULL,
  `id_classif` int(11) NOT NULL,
  `id_desc` int(11) NOT NULL,
  `folio_turned` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `doc_origin` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `doc_ccp` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `date_turned` date DEFAULT NULL,
  `instructions` text COLLATE utf8_spanish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='After tegistration of the document, the user turn the document at the organization and register the action, here will register the information about the document, the status, chracter of tamit and classificatin of the document';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `history_access`
--

CREATE TABLE `history_access` (
  `id_history` int(11) NOT NULL,
  `ip` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `date_access` date DEFAULT NULL,
  `time_in` time DEFAULT NULL,
  `time_out` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipalities`
--

CREATE TABLE `municipalities` (
  `id_muni` int(11) NOT NULL,
  `desc_muni` varchar(65) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `organizations`
--

CREATE TABLE `organizations` (
  `id_org` int(11) NOT NULL,
  `org_name` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_dir` varchar(99) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_col` varchar(35) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_cp` varchar(6) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_tel` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_ext` char(6) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_email` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privileges`
--

CREATE TABLE `privileges` (
  `id_priv` int(11) NOT NULL,
  `desc_priv` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `senders`
--

CREATE TABLE `senders` (
  `id_sender` int(11) NOT NULL,
  `desc_sender_position` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_docs`
--

CREATE TABLE `status_docs` (
  `id_status_doc` int(11) NOT NULL,
  `desc_status_docs` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_user`
--

CREATE TABLE `status_user` (
  `id_user_status` int(11) NOT NULL,
  `desc_user_status` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_docs`
--

CREATE TABLE `type_docs` (
  `id_type_doc` int(11) NOT NULL,
  `desc_type_doc` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `id_priv` int(11) NOT NULL,
  `id_user_status` int(11) NOT NULL,
  `id_dep` int(11) NOT NULL,
  `id_history` int(11) NOT NULL,
  `name` varchar(120) COLLATE utf8_spanish_ci NOT NULL,
  `user_name` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `user_pass` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `user_movil` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_email` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_photo` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `user_position` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `user_tel` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_ext` varchar(6) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_online` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='This is the table where register users, his privileges, her status in the system, his departament, his history and the characteristics of the count''s owner.';

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `answer_docs`
--
ALTER TABLE `answer_docs`
  ADD PRIMARY KEY (`id_answer`),
  ADD KEY `id_status_doc` (`id_status_doc`);

--
-- Indices de la tabla `char_tramits`
--
ALTER TABLE `char_tramits`
  ADD PRIMARY KEY (`id_char`);

--
-- Indices de la tabla `classifications`
--
ALTER TABLE `classifications`
  ADD PRIMARY KEY (`id_classif`);

--
-- Indices de la tabla `departaments`
--
ALTER TABLE `departaments`
  ADD PRIMARY KEY (`id_dep`),
  ADD KEY `id_dir` (`id_dir`);

--
-- Indices de la tabla `descriptions`
--
ALTER TABLE `descriptions`
  ADD PRIMARY KEY (`id_desc`);

--
-- Indices de la tabla `destiny_docs`
--
ALTER TABLE `destiny_docs`
  ADD PRIMARY KEY (`id_destiny`);

--
-- Indices de la tabla `directions`
--
ALTER TABLE `directions`
  ADD PRIMARY KEY (`id_dir`);

--
-- Indices de la tabla `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id_doc`),
  ADD KEY `id_type_doc` (`id_type_doc`),
  ADD KEY `id_status_doc` (`id_status_doc`),
  ADD KEY `id_muni` (`id_muni`),
  ADD KEY `id_destiny` (`id_destiny`),
  ADD KEY `id_sender` (`id_sender`);

--
-- Indices de la tabla `documents_turned`
--
ALTER TABLE `documents_turned`
  ADD PRIMARY KEY (`id_turn`),
  ADD KEY `id_dep` (`id_dep`),
  ADD KEY `id_status_doc` (`id_status_doc`),
  ADD KEY `id_doc` (`id_doc`),
  ADD KEY `id_char` (`id_char`),
  ADD KEY `id_classif` (`id_classif`),
  ADD KEY `id_desc` (`id_desc`);

--
-- Indices de la tabla `history_access`
--
ALTER TABLE `history_access`
  ADD PRIMARY KEY (`id_history`);

--
-- Indices de la tabla `municipalities`
--
ALTER TABLE `municipalities`
  ADD PRIMARY KEY (`id_muni`);

--
-- Indices de la tabla `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id_org`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `privileges`
--
ALTER TABLE `privileges`
  ADD PRIMARY KEY (`id_priv`);

--
-- Indices de la tabla `senders`
--
ALTER TABLE `senders`
  ADD PRIMARY KEY (`id_sender`);

--
-- Indices de la tabla `status_docs`
--
ALTER TABLE `status_docs`
  ADD PRIMARY KEY (`id_status_doc`);

--
-- Indices de la tabla `status_user`
--
ALTER TABLE `status_user`
  ADD PRIMARY KEY (`id_user_status`);

--
-- Indices de la tabla `type_docs`
--
ALTER TABLE `type_docs`
  ADD PRIMARY KEY (`id_type_doc`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_dep` (`id_dep`),
  ADD KEY `id_priv` (`id_priv`),
  ADD KEY `id_user_status` (`id_user_status`),
  ADD KEY `id_history` (`id_history`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `answer_docs`
--
ALTER TABLE `answer_docs`
  MODIFY `id_answer` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `char_tramits`
--
ALTER TABLE `char_tramits`
  MODIFY `id_char` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `classifications`
--
ALTER TABLE `classifications`
  MODIFY `id_classif` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `departaments`
--
ALTER TABLE `departaments`
  MODIFY `id_dep` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `descriptions`
--
ALTER TABLE `descriptions`
  MODIFY `id_desc` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `destiny_docs`
--
ALTER TABLE `destiny_docs`
  MODIFY `id_destiny` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `directions`
--
ALTER TABLE `directions`
  MODIFY `id_dir` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `documents`
--
ALTER TABLE `documents`
  MODIFY `id_doc` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `documents_turned`
--
ALTER TABLE `documents_turned`
  MODIFY `id_turn` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `history_access`
--
ALTER TABLE `history_access`
  MODIFY `id_history` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `municipalities`
--
ALTER TABLE `municipalities`
  MODIFY `id_muni` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id_org` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `privileges`
--
ALTER TABLE `privileges`
  MODIFY `id_priv` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `senders`
--
ALTER TABLE `senders`
  MODIFY `id_sender` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `status_docs`
--
ALTER TABLE `status_docs`
  MODIFY `id_status_doc` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `status_user`
--
ALTER TABLE `status_user`
  MODIFY `id_user_status` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `type_docs`
--
ALTER TABLE `type_docs`
  MODIFY `id_type_doc` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `answer_docs`
--
ALTER TABLE `answer_docs`
  ADD CONSTRAINT `answer_docs_ibfk_1` FOREIGN KEY (`id_status_doc`) REFERENCES `status_docs` (`id_status_doc`),
  ADD CONSTRAINT `answer_docs_ibfk_2` FOREIGN KEY (`id_status_doc`) REFERENCES `status_docs` (`id_status_doc`);

--
-- Filtros para la tabla `departaments`
--
ALTER TABLE `departaments`
  ADD CONSTRAINT `departaments_ibfk_1` FOREIGN KEY (`id_dir`) REFERENCES `directions` (`id_dir`),
  ADD CONSTRAINT `departaments_ibfk_2` FOREIGN KEY (`id_dir`) REFERENCES `directions` (`id_dir`);

--
-- Filtros para la tabla `documents`
--
ALTER TABLE `documents`
  ADD CONSTRAINT `documents_ibfk_1` FOREIGN KEY (`id_type_doc`) REFERENCES `type_docs` (`id_type_doc`),
  ADD CONSTRAINT `documents_ibfk_10` FOREIGN KEY (`id_sender`) REFERENCES `senders` (`id_sender`),
  ADD CONSTRAINT `documents_ibfk_2` FOREIGN KEY (`id_status_doc`) REFERENCES `status_docs` (`id_status_doc`),
  ADD CONSTRAINT `documents_ibfk_3` FOREIGN KEY (`id_muni`) REFERENCES `municipalities` (`id_muni`),
  ADD CONSTRAINT `documents_ibfk_4` FOREIGN KEY (`id_destiny`) REFERENCES `destiny_docs` (`id_destiny`),
  ADD CONSTRAINT `documents_ibfk_5` FOREIGN KEY (`id_sender`) REFERENCES `senders` (`id_sender`),
  ADD CONSTRAINT `documents_ibfk_6` FOREIGN KEY (`id_type_doc`) REFERENCES `type_docs` (`id_type_doc`),
  ADD CONSTRAINT `documents_ibfk_7` FOREIGN KEY (`id_status_doc`) REFERENCES `status_docs` (`id_status_doc`),
  ADD CONSTRAINT `documents_ibfk_8` FOREIGN KEY (`id_muni`) REFERENCES `municipalities` (`id_muni`),
  ADD CONSTRAINT `documents_ibfk_9` FOREIGN KEY (`id_destiny`) REFERENCES `destiny_docs` (`id_destiny`);

--
-- Filtros para la tabla `documents_turned`
--
ALTER TABLE `documents_turned`
  ADD CONSTRAINT `documents_turned_ibfk_1` FOREIGN KEY (`id_dep`) REFERENCES `departaments` (`id_dep`),
  ADD CONSTRAINT `documents_turned_ibfk_10` FOREIGN KEY (`id_desc`) REFERENCES `descriptions` (`id_desc`),
  ADD CONSTRAINT `documents_turned_ibfk_2` FOREIGN KEY (`id_status_doc`) REFERENCES `status_docs` (`id_status_doc`),
  ADD CONSTRAINT `documents_turned_ibfk_3` FOREIGN KEY (`id_doc`) REFERENCES `documents` (`id_doc`),
  ADD CONSTRAINT `documents_turned_ibfk_4` FOREIGN KEY (`id_char`) REFERENCES `char_tramits` (`id_char`),
  ADD CONSTRAINT `documents_turned_ibfk_5` FOREIGN KEY (`id_dep`) REFERENCES `departaments` (`id_dep`),
  ADD CONSTRAINT `documents_turned_ibfk_6` FOREIGN KEY (`id_status_doc`) REFERENCES `status_docs` (`id_status_doc`),
  ADD CONSTRAINT `documents_turned_ibfk_7` FOREIGN KEY (`id_doc`) REFERENCES `documents` (`id_doc`),
  ADD CONSTRAINT `documents_turned_ibfk_8` FOREIGN KEY (`id_char`) REFERENCES `char_tramits` (`id_char`),
  ADD CONSTRAINT `documents_turned_ibfk_9` FOREIGN KEY (`id_classif`) REFERENCES `classifications` (`id_classif`);

--
-- Filtros para la tabla `organizations`
--
ALTER TABLE `organizations`
  ADD CONSTRAINT `organizations_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`),
  ADD CONSTRAINT `organizations_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_dep`) REFERENCES `departaments` (`id_dep`),
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`id_priv`) REFERENCES `privileges` (`id_priv`),
  ADD CONSTRAINT `users_ibfk_3` FOREIGN KEY (`id_user_status`) REFERENCES `status_user` (`id_user_status`),
  ADD CONSTRAINT `users_ibfk_4` FOREIGN KEY (`id_history`) REFERENCES `history_access` (`id_history`),
  ADD CONSTRAINT `users_ibfk_5` FOREIGN KEY (`id_dep`) REFERENCES `departaments` (`id_dep`),
  ADD CONSTRAINT `users_ibfk_6` FOREIGN KEY (`id_priv`) REFERENCES `privileges` (`id_priv`),
  ADD CONSTRAINT `users_ibfk_7` FOREIGN KEY (`id_user_status`) REFERENCES `status_user` (`id_user_status`),
  ADD CONSTRAINT `users_ibfk_8` FOREIGN KEY (`id_history`) REFERENCES `history_access` (`id_history`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 23-01-2018 a las 13:53:14
-- Versión del servidor: 5.7.20-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `siop`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_docs`
--

CREATE TABLE `type_docs` (
  `id_type_doc` int(11) NOT NULL,
  `desc_type_doc` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `type_docs`
--

INSERT INTO `type_docs` (`id_type_doc`, `desc_type_doc`) VALUES
(1, 'Circular'),
(2, 'Convocatoria'),
(3, 'Email'),
(4, 'Engargolado'),
(5, 'Factura'),
(6, 'Invitación'),
(7, 'Oficio'),
(8, 'Sobre'),
(9, 'Tarjeta');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `type_docs`
--
ALTER TABLE `type_docs`
  ADD PRIMARY KEY (`id_type_doc`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `type_docs`
--
ALTER TABLE `type_docs`
  MODIFY `id_type_doc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

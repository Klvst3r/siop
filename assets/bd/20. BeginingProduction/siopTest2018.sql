-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 18, 2018 at 12:52 PM
-- Server version: 10.1.26-MariaDB-0+deb9u1
-- PHP Version: 7.0.30-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siop`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer_docs`
--

CREATE TABLE `answer_docs` (
  `id_answer` int(11) NOT NULL,
  `id_status_doc` int(11) NOT NULL,
  `folio_doc` int(11) DEFAULT NULL,
  `date_answer` date DEFAULT NULL,
  `oficio_answer` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `char_tramits`
--

CREATE TABLE `char_tramits` (
  `id_char` int(11) NOT NULL,
  `desc_char` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `char_tramits`
--

INSERT INTO `char_tramits` (`id_char`, `desc_char`) VALUES
(1, 'Importante'),
(2, 'Urgente');

-- --------------------------------------------------------

--
-- Table structure for table `classifications`
--

CREATE TABLE `classifications` (
  `id_classif` int(11) NOT NULL,
  `desc_classif` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `classifications`
--

INSERT INTO `classifications` (`id_classif`, `desc_classif`) VALUES
(1, 'Informativo'),
(2, 'Solicitud');

-- --------------------------------------------------------

--
-- Table structure for table `departaments`
--

CREATE TABLE `departaments` (
  `id_dep` int(11) NOT NULL,
  `departament` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `departaments`
--

INSERT INTO `departaments` (`id_dep`, `departament`) VALUES
(1, 'Departemento de Recursos Humanos y Desarrollo Administrativo'),
(2, 'Departamento de Adquisiciones,\r\nRecursos Materiales y Servicios'),
(3, 'Departamento de Contabilidad y Finanzas'),
(4, 'Informática'),
(5, 'Archivo'),
(6, 'Almacen 1'),
(7, 'Almacen 2'),
(8, 'Seguimiento a Auditorias'),
(9, 'Departamento de Gestión de Derecho de Vía y Afectaciones'),
(10, 'Departamento de Obras por Administración y Equipamiento'),
(11, 'Departamento de Infraestructura Vial'),
(12, 'Departamento de Concertación Social y Apoyos de Maquinaria'),
(13, 'Departamento de Eventos Especiales'),
(14, 'Departamento de Agua Potable, Alcantarillado y Saneamiento'),
(15, 'Departamento de Infraestructura Urbana'),
(16, 'Departamento Jurídico'),
(17, 'Departamento de Planeación y Coordinación de Programas'),
(18, 'Departamento de Proyectos'),
(19, 'Departamento de Estadística e Información Geográfica'),
(20, 'Departamento de Programas y Registro del Desarrollo Urbano'),
(21, 'Departamento de Licitaciones y Contratos'),
(22, 'Precios Unitarios y Ajuste de Costos');

-- --------------------------------------------------------

--
-- Table structure for table `descriptions`
--

CREATE TABLE `descriptions` (
  `id_desc` int(11) NOT NULL,
  `content_desc` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `descriptions`
--

INSERT INTO `descriptions` (`id_desc`, `content_desc`) VALUES
(1, 'Notificaciones'),
(2, 'Dictamen de Congruencia'),
(3, 'Eventos Especiales'),
(4, 'Invitaciones'),
(5, 'Maquinaria'),
(6, 'Correo Electrónico'),
(7, 'Para su Conocimiento'),
(8, 'Tarjeta');

-- --------------------------------------------------------

--
-- Table structure for table `destiny_docs`
--

CREATE TABLE `destiny_docs` (
  `id_destiny` int(11) NOT NULL,
  `desc_destiny` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `destiny_position` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `destiny_docs`
--

INSERT INTO `destiny_docs` (`id_destiny`, `desc_destiny`, `destiny_position`) VALUES
(1, 'Arq. Francisco Javier Romero Ahuactzi', 'Secretario ');

-- --------------------------------------------------------

--
-- Table structure for table `directions`
--

CREATE TABLE `directions` (
  `id_dir` int(11) NOT NULL,
  `dir_name` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `directions`
--

INSERT INTO `directions` (`id_dir`, `dir_name`) VALUES
(1, 'Dirección Administrativa'),
(2, 'Dirección de Obras Públicas'),
(3, 'Dirección de Desarrollo Urbano'),
(4, 'Dirección de Licitaciones, Precios Unitarios y Contratos'),
(5, 'Despacho');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id_doc` int(11) NOT NULL,
  `id_destiny` int(11) NOT NULL,
  `id_muni` int(11) NOT NULL,
  `id_type_doc` int(11) NOT NULL,
  `id_status_doc` int(11) NOT NULL,
  `id_classif` int(11) NOT NULL,
  `subject_doc` text COLLATE utf8_spanish_ci,
  `desc_send_position` varchar(120) COLLATE utf8_spanish_ci DEFAULT NULL,
  `folio_doc` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `send_doc` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `origin_doc` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dir_sender` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `movil_doc` varchar(11) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tel_doc` varchar(11) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ext_doc` varchar(6) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email_doc` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `date_doc` date DEFAULT NULL,
  `date_recep` date DEFAULT NULL,
  `reference` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `status` enum('0','1','2') COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id_doc`, `id_destiny`, `id_muni`, `id_type_doc`, `id_status_doc`, `id_classif`, `subject_doc`, `desc_send_position`, `folio_doc`, `send_doc`, `origin_doc`, `dir_sender`, `movil_doc`, `tel_doc`, `ext_doc`, `email_doc`, `date_doc`, `date_recep`, `reference`, `status`) VALUES
(1, 1, 13, 7, 1, 2, 'SOLICITA PRÉSTAMO DE UNA MÁQUINA PESADA PARA REALIZAR TRABAJOS DE ZANJEO Y JAGÜEYES', 'SOLICITANTE', '2477', 'C. JOSÉ LEOPOLDO GONZÁLEZ MORALES', 'JOSÉ MARÍA MORELOS BUENAVISTA', 'JOSÉ MARÍA MORELOS BUENAVISTA', '0', '0', '0', 'satquiel@msn.com', '2018-07-14', '2018-07-16', '0', '1'),
(2, 1, 3, 7, 1, 2, 'SOLICITA PRÉSTAMO DE UNA MÁQUINA TRACTO CAMIÓN VACTOR PARA DESAZOLVAR 180 M3 DE AGUAS NEGRAS RESIDUALES', 'CORONEL DE LA FUERZA AÉREA', '2595', 'RUBÉN BERNARDO CORTÉS HERNÁNDEZ', 'ATLANGATEPEC', 'CMTE. ACC DE LA ESTACIÓN AÉREA MILITAR No.9', '2212824595', '2414960249', '0', 'sincorreo@msn.com', '2018-07-23', '2018-07-26', 'Sin referencia', '1'),
(3, 1, 6, 7, 1, 2, 'SOLICITA PROYECTO EJECUTIVO, ACTA E ENTREGA RECEPCIÓN DE &quot;PLANTA DE TRATAMIENTO RESIDUALES&quot; SAN ANTONIO MAZAPA', 'DIRECTOR CAPAM', '2598', 'LIC. JESÚS FERMÍN GARCÍA ORTEGA', 'CALPAULALPAN', 'PLAZA DE LA CONSTITUCIPON No.5 CALPULALPAN', '9180853', '7499180495', '0', 'Sin correo electrónico', '2018-07-12', '2018-07-27', 'CAPAM/349/018', '1'),
(4, 1, 31, 7, 1, 1, 'SE INFORMA QUE SE DESIGNO AL ING. LIZZI IXTLAPALE URBANO CON No. DE REGISTRO D.R.O.357 COMO DIRECTOR RESPONSABLE DE OBRA DE LOS TRABAJOS DE &quot;RECONSTRUCCIÓN DE PAVIMENTO DE CARPETA ASFÁLTICA EN CALLE TLAXCALA&quot; CONTRATO PF/PDR/025/18', 'ADMINISTRADOR ÚNICO', '2631', 'ING. JAVIER CUATEPOTZO CARRETO', 'CUATEPOTZO CONSTRUCCIONES S.A. DE C.V.', 'BOULEVARD IGNACIO ZARAGOZA No. 24-A CHAUTZINGO TETLA TLAXCALA', '0', '2414121108', '0', 'Sin correo electrónico', '2018-07-28', '2018-08-01', 'Sin referencia', '1'),
(5, 1, 31, 7, 1, 2, 'SE INFORMA QUE CON FECHA 28 DE JULIO DE 2018 SE DA INICIO A LOS TRABAJOS DE &quot;RECONSTRUCCIÓN DE PAVIMENTO DE CARPETA ASFÁLTICA EN CALLE TLAXCALA&quot; CONTRATO PF/PDR/025/18', 'ADMINISTRADOR ÚNICO', '2632', 'ING. JAVIER CUATEPOTZO CARRETO', 'CUATEPOTZO CONSTRUCCIONES', 'BOULEVARD IGNACIO ZARAGOZA No.24-A CHAUTZINGO TETLA', '0', '2414121108', '0', 'Sin correo electrónico', '2018-07-28', '2018-08-01', 'Sin referencia', '1'),
(6, 1, 33, 7, 1, 2, 'EXPEDIENTE 110/2018, POBLADO EL ROSARIO, MUNICIPIO DE TLAXCO TLAXCALA', 'ACTUARIO', '2958', 'LIC. DANIEL AMBRIZ ROMERO', 'TRIBUNAL UNITARIO AGRARIO DISTRITO TREINTA Y TRES', 'Domicilio no especificado', '0', '0', '0', 'Sin correo electrónico', '2018-08-30', '2018-08-30', '110/2018', '1'),
(7, 1, 33, 7, 1, 1, 'EXPEDIENTE 356/2018, POBLADO N.C.P.E. PLUTARCO ELIAS CALLES, MUNICIPIO DE TLAXCO TLAXCALA.', 'ACTUARIO', '2959', 'LIC. DANIEL AMBRIZ ROMERO', 'TRIBUNAL UNITARIO AGRARIO DISTRITO TREINTA Y TRES', 'TLAXCALA', '0', '0', '0', 'Sin correo electrónico', '2018-08-30', '2018-08-30', '356/2018', '1'),
(8, 1, 33, 7, 1, 1, 'EXPEDIENTE 355/2018, POBLADO N.C.P.E. PLUTARCO ELÍAS CALLES, MUNICIPIO DE TLAXCO TLAXCALA', 'ACTUARIO', '2960', 'LIC. DANIEL AMBRIZ ROMERO', 'TRIBUNAL UNITARIO AGRARIO DISTRITO TREINTA Y TRES', 'TLAXCALA', '0', '0', '0', 'Sin correo electrónico', '2018-08-30', '2018-08-30', '355/2018', '1'),
(9, 1, 33, 7, 1, 1, 'EXPEDIENTE 80/2018, POBLADO EL ROSARIO DEL MUNICIPIO DE TLAXCO TLAXCALA', 'ACTUARIO', '2961', 'LIC. DANIEL AMBRIZ ROMERO', 'TRIBUNAL UNITARIO AGRARIO DISTRITO TREINTA Y TRES', 'TLAXCALA', '0', '0', '0', 'Sin correo electrónico', '2018-08-30', '2018-08-30', '80/2018', '1'),
(10, 1, 33, 7, 1, 1, 'EXPEDIENTE 111/2018, POBLADO EL ROSARIO DEL MUNICIPIO DE TLAXCO TLAXCALA', 'ACTUARIO', '2961', 'LIC. DANIEL AMBRIZ ROMERO', 'TRIBUNAL UNITARIO AGRARIO DISTRITO TREINTA Y TRES', 'TLAXCALA', '0', '0', '0', 'Sin correo electrónico', '2018-08-30', '2018-08-30', '111/2018', '1'),
(11, 1, 33, 4, 1, 2, 'EN LOS AUTOS DEL RECURSO DE REVISIÓN R.R.193/2018-P-1', 'SECRETARIO DE ESTUDIO Y CUENTA', '3185', 'LIC. PABLO CORTÉS FLORES', 'IAIP', 'CALLE 17No.236 COL. LOMA XICOHTÉNCATL TLAXCALA', '0', '246 462 00', '0', 'contacto@iaiptlaxcala.org.mx', '2018-09-10', '2018-09-18', 'Sin referencia', '1'),
(12, 1, 33, 7, 1, 2, 'SOLICITA PRÉSTAMO DE MÁQUINA MOTOCONFORMADORA PARA REALIZAR TRABAJOS DE RASTREO Y NIVELACIÓN DE VARIAS CALLES DE LA COMUNIDAD DE SAN FRANCISCO TEMETZONTLA, PANOTLA TLAXCALA.', 'DIPUTADO LOCAL', '3186', 'LIC. JESÚS ROLANDO PÉREZ SAAVEDRA', 'CONGRESO DEL ESTADO', 'CALLE IGNACIO ALLENDE No.31 COL. CENTRO', '0', '2464666082', '0', 'Sin correo electrónico', '2018-09-17', '2018-09-18', 'Sin referencia', '1'),
(13, 1, 15, 7, 1, 2, 'SOLICITA CONTINUACIÓN DE ENCARPETAMIENTO', 'PRESIDENTE DE COMUNIDAD', '3187', 'DELFINO PÉREZ SÁNCHEZ', 'LA TRINIDAD TENEXYECAC', 'CALLE DE DIOS PADRE S/N COL. CENTRO', '2461929207', '2462387234', '0', 'Sin correo electrónico', '2018-09-17', '2018-09-18', 'Sin referencia', '1'),
(14, 1, 33, 7, 1, 2, 'SOLICITA APOYO CON LA COLOCACIÓN DE REDUCTORES DE VELOCIDAD A LO LARGO DE LA CALLE ANTIGUO CAMINO A ZAUTLA.', 'JEFA DE DIVISIÓN DE ESTUDIOS PROFESIONALES CAMPUS 3', '3188', 'DRA. MARÍA DE LA LUZ MARTÍNEZ MALDONADO', 'UNAM', 'EX FABRICA DE SAN MANUEL S/N COL. SAN MANUEL', '0', '2464651800', '0', 'Sin correo electrónico', '2018-09-18', '2018-09-18', 'Sin referencia', '1'),
(15, 1, 2, 7, 1, 2, 'NOTIFICACIÓN DE REVISIÓN FÍSICA PARA EL 19 DE SEPTIEMBRE DE 2018 DEL CONTRATO PF/PR/106/17 EN EL LUGAR DE LA OBRA.', 'CONTRALOR DEL EJECUTIVO', '3189', 'C.P. MARÍA MARICELA ESCOBAR SÁNCHEZ', 'CONTRALORÍA DEL EJECUTIVO', 'EX RANCHO LA AGUANAJA S/N SAN PABLO APETATITLÁN', '2114', '4650900', '2113', 'Sin correo electrónico', '2018-09-13', '2018-09-18', 'CE/DSFCA/AOPS/18-09-2329', '1'),
(16, 1, 33, 7, 1, 2, 'SE REMITE PROYECTO EJECUTIVO DE LA OBRA DENOMINADA CONSTRUCCIÓN DEL RAMAL MAZAQUIAHUAC - GRACIANO SÁNCHEZ Y RAMAL A SANTIAGO TETLAPAYAC, DEBIDAMENTE AUTORIZADO PARA LOS TRÁMITES ADMINISTRATIVOS CORRESPONDIENTES.', 'SUBDIRECTOR DE OBRAS', '3190', 'ING. RUBEN HERNÁNDEZ AGAVO', 'SCT', 'Domicilio no especificado', '0', '0', '0', 'Sin correo electrónico', '2018-09-11', '2018-09-18', '6.28.407.1.-0752/2018', '1'),
(17, 1, 48, 7, 1, 2, 'SOLICITA PREÉSTAMO DE MÁQUINA VACTOR PARA DESAZOLVE DE DRENAJES DE LA LOCALIDAD DEL MUNICIPIO DE TLALTELULCO', 'DIRECTOR DE OBRAS', '3191', 'ING. VICTOR EDUARDO ARENAS MORALES', 'TLALTELULCO', 'CALLE 16 DE SEPTIEMBRE No.2 COL. CENTRO', '0', '4617140', '0', 'Sin correo electrónico', '2018-09-18', '2018-09-18', 'PMMT-DOP2018/103', '1'),
(18, 1, 5, 4, 1, 2, 'EN LOS AUTOS DEL JUICIO DE AMPARO 809/2017-V PROMOVIDO POR JOSÉ RESÉNDIZ GARCÍA', 'SECRETARIO DE JUZGADO', '3192', 'LIC. CHRISTOPER RODRÍGUEZ ESCOBAR', 'PODER JUDICIAL DE LA FEDERACIÓN', 'PREDIO RÚSTICO TERCER PISO ALA B SANTA ANITA HUILOAC', '0', '2414188190', '1302', 'Sin correo electrónico', '2018-09-17', '2018-09-18', '809/2017-V', '1');

-- --------------------------------------------------------

--
-- Table structure for table `documents_turned`
--

CREATE TABLE `documents_turned` (
  `id_turn` int(11) NOT NULL,
  `id_doc` int(11) NOT NULL,
  `id_dir` int(11) NOT NULL,
  `id_status_doc` int(11) NOT NULL,
  `id_dep` int(11) NOT NULL,
  `id_char` int(11) NOT NULL,
  `id_classif` int(11) NOT NULL,
  `id_desc` int(11) NOT NULL,
  `folio_turned` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `doc_origin` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `doc_ccp` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `date_turned` date DEFAULT NULL,
  `instructions` text COLLATE utf8_spanish_ci,
  `answer` varchar(40) COLLATE utf8_spanish_ci DEFAULT NULL,
  `observation` text COLLATE utf8_spanish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `history_access`
--

CREATE TABLE `history_access` (
  `id_history` int(11) NOT NULL,
  `ip` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `date_access` date DEFAULT NULL,
  `time_in` time DEFAULT NULL,
  `time_out` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `history_access`
--

INSERT INTO `history_access` (`id_history`, `ip`, `date_access`, `time_in`, `time_out`) VALUES
(1, '127.0.0.1', '2018-07-12', '12:00:00', '12:00:00'),
(3, '127.0.0.1', '2018-07-13', '18:23:30', NULL),
(4, '127.0.0.1', '2018-07-13', '18:30:43', NULL),
(5, '127.0.0.1', '2018-07-13', '18:40:38', NULL),
(6, '127.0.0.1', '2018-07-13', '19:15:05', NULL),
(7, '127.0.0.1', '2018-07-13', '19:16:24', NULL),
(8, '127.0.0.1', '2018-07-14', '09:22:34', NULL),
(9, '127.0.0.1', '2018-07-16', '10:14:53', NULL),
(10, '127.0.0.1', '2018-07-16', '10:21:35', NULL),
(11, '127.0.0.1', '2018-07-16', '10:22:28', NULL),
(12, '127.0.0.1', '2018-07-16', '10:23:08', NULL),
(13, '127.0.0.1', '2018-07-16', '10:23:42', NULL),
(14, '127.0.0.1', '2018-07-16', '10:30:03', NULL),
(15, '127.0.0.1', '2018-07-16', '10:46:47', NULL),
(16, '127.0.0.1', '2018-07-16', '10:54:46', NULL),
(17, '127.0.0.1', '2018-07-19', '09:21:29', NULL),
(18, '127.0.0.1', '2018-07-21', '11:40:52', NULL),
(19, '127.0.0.1', '2018-07-21', '11:47:10', NULL),
(20, '127.0.0.1', '2018-07-21', '11:51:53', NULL),
(21, '127.0.0.1', '2018-07-23', '09:53:54', NULL),
(22, '127.0.0.1', '2018-07-26', '17:21:04', NULL),
(23, '127.0.0.1', '2018-07-27', '10:08:38', NULL),
(24, '127.0.0.1', '2018-07-27', '11:34:37', NULL),
(25, '127.0.0.1', '2018-07-27', '11:36:26', NULL),
(26, '127.0.0.1', '2018-07-27', '12:16:05', NULL),
(27, '127.0.0.1', '2018-07-27', '12:22:02', NULL),
(28, '127.0.0.1', '2018-07-27', '12:35:32', NULL),
(29, '127.0.0.1', '2018-07-27', '13:46:05', NULL),
(30, '127.0.0.1', '2018-08-01', '10:35:30', NULL),
(31, '127.0.0.1', '2018-08-06', '13:33:42', NULL),
(32, '127.0.0.1', '2018-08-06', '13:36:01', NULL),
(33, '127.0.0.1', '2018-08-06', '14:13:56', NULL),
(34, '127.0.0.1', '2018-08-13', '14:52:40', NULL),
(35, '127.0.0.1', '2018-08-13', '14:52:49', NULL),
(36, '127.0.0.1', '2018-08-14', '13:56:29', NULL),
(37, '127.0.0.1', '2018-08-14', '18:52:45', NULL),
(38, '127.0.0.1', '2018-08-15', '17:48:36', NULL),
(39, '127.0.0.1', '2018-08-15', '17:49:14', NULL),
(40, '127.0.0.1', '2018-08-28', '19:53:53', NULL),
(41, '127.0.0.1', '2018-08-28', '19:56:06', NULL),
(42, '127.0.0.1', '2018-08-30', '09:38:10', NULL),
(43, '127.0.0.1', '2018-08-31', '13:06:54', NULL),
(44, '127.0.0.1', '2018-08-31', '13:08:50', NULL),
(45, '127.0.0.1', '2018-09-03', '09:20:42', NULL),
(46, '127.0.0.1', '2018-09-03', '12:45:08', NULL),
(47, '127.0.0.1', '2018-09-03', '12:45:11', NULL),
(48, '127.0.0.1', '2018-09-03', '17:38:22', NULL),
(49, '127.0.0.1', '2018-09-05', '09:30:25', NULL),
(50, '127.0.0.1', '2018-09-10', '19:58:46', NULL),
(51, '127.0.0.1', '2018-09-18', '10:25:57', NULL),
(52, '127.0.0.1', '2018-09-18', '11:49:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `municipalities`
--

CREATE TABLE `municipalities` (
  `id_muni` int(11) NOT NULL,
  `desc_muni` varchar(65) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `municipalities`
--

INSERT INTO `municipalities` (`id_muni`, `desc_muni`) VALUES
(1, 'Amaxac de Guerrero'),
(2, 'Apetatitlan de Antonio Carvajal'),
(3, 'Atlangatepec'),
(4, 'Atltzayanca'),
(5, 'Apizaco'),
(6, 'Calpulalpan'),
(7, 'El Carmen Tequexquitla'),
(8, 'Cuapiaxtla'),
(9, 'Cuaxomulco'),
(10, 'Chiautempan'),
(11, 'Muñoz de Domingo Arenas'),
(12, 'Españita'),
(13, 'Huamantla'),
(14, 'Hueyotlipan'),
(15, 'Ixtacuixtla de Mariano Matamoros'),
(16, 'Ixtenco'),
(17, 'Mazatecochco de José María Morelos'),
(18, 'Contla de Juan Cuamatzi'),
(19, 'Tepetitla de Lardizábal'),
(20, 'Sanctórum de Lázaro Cárdenas'),
(21, 'Nanacamilpa de Mariano Arista'),
(22, 'Acuamanala de Miguel Hidalgo'),
(23, 'Natívitas'),
(24, 'Panotla'),
(25, 'San Pablo del Monte'),
(26, 'Santa Cruz Tlaxcala'),
(27, 'Tenancingo'),
(28, 'Teolocholco'),
(29, 'Tepeyanco'),
(30, 'Terrenate'),
(31, 'Tetla de la Solidaridad'),
(32, 'Tetlatlahuca'),
(33, 'Tlaxcala'),
(34, 'Tlaxco'),
(35, 'Tocatlán'),
(36, 'Totolac'),
(37, 'Ziltlaltépec de Trinidad Sánchez '),
(38, 'Tzompantepec'),
(39, 'Xaloztoc'),
(40, 'Xaltocan'),
(41, 'Papalotla de Xicohténcatl'),
(42, 'Xicohtzinco'),
(43, 'Yauhquemehcan'),
(44, 'Zacatelco'),
(45, 'Benito Juárez'),
(46, 'Emiliano Zapata'),
(47, 'Lázaro Cárdenas'),
(48, 'La Magdalena Tlaltelulco'),
(49, 'San Damián Texóloc'),
(50, 'San Francisco Tetlanohcan'),
(51, 'San Jerónimo Zacualpan'),
(52, 'San José Teacalco'),
(53, 'San Juan Huactzinco'),
(54, 'San Lorenzo Axocomanitla'),
(55, 'San Lucas Tecopilco'),
(56, 'Santa Ana Nopalucan'),
(57, 'Santa Apolonia Teacalco'),
(58, 'Santa Catarina Ayometla'),
(59, 'Santa Cruz Quilehtla'),
(60, 'Santa Isabel Xiloxoxtla');

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations` (
  `id_org` int(11) NOT NULL,
  `org_nick` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_name` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_dir` varchar(99) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_pob` varchar(35) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_cp` varchar(6) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_tel` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_ext` char(6) COLLATE utf8_spanish_ci DEFAULT NULL,
  `org_email` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `privileges`
--

CREATE TABLE `privileges` (
  `id_priv` int(11) NOT NULL,
  `desc_priv` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL,
  `create_priv` enum('N','Y') COLLATE utf8_spanish_ci DEFAULT NULL,
  `select_priv` enum('N','Y') COLLATE utf8_spanish_ci DEFAULT NULL,
  `insert_priv` enum('N','Y') COLLATE utf8_spanish_ci DEFAULT NULL,
  `update_priv` enum('N','Y') COLLATE utf8_spanish_ci DEFAULT NULL,
  `delete_priv` enum('N','Y') COLLATE utf8_spanish_ci DEFAULT NULL,
  `backup_priv` enum('N','Y') COLLATE utf8_spanish_ci DEFAULT NULL,
  `report_priv` enum('N','Y') COLLATE utf8_spanish_ci DEFAULT NULL,
  `status` enum('1','0') COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `privileges`
--

INSERT INTO `privileges` (`id_priv`, `desc_priv`, `create_priv`, `select_priv`, `insert_priv`, `update_priv`, `delete_priv`, `backup_priv`, `report_priv`, `status`) VALUES
(1, 'Administración del sistema', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'N', '1'),
(2, 'Oficialia de Partes', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', '1'),
(3, 'Registro de Información', 'N', 'Y', 'Y', 'Y', 'N', 'N', 'N', '1'),
(4, 'Consulta de Información', 'N', 'Y', 'N', 'N', 'N', 'N', 'Y', '1'),
(5, 'Borrado de Información', 'N', 'Y', 'Y', 'Y', 'Y', 'N', 'N', '1'),
(6, 'Respaldo de Información', 'N', 'Y', 'N', 'N', 'N', 'Y', 'N', '1'),
(7, 'Oficialia de Partes', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', '1'),
(8, 'modify', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', '0'),
(9, 'alert', 'N', 'N', 'N', 'N', 'N', 'N', 'N', '0'),
(10, 'New Privilege', 'N', 'N', 'N', 'N', 'N', 'N', 'N', '0'),
(11, 'Privilege reedited', 'N', 'Y', 'N', 'Y', 'N', 'Y', 'N', '0');

-- --------------------------------------------------------

--
-- Table structure for table `status_docs`
--

CREATE TABLE `status_docs` (
  `id_status_doc` int(11) NOT NULL,
  `desc_status_docs` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `status_docs`
--

INSERT INTO `status_docs` (`id_status_doc`, `desc_status_docs`) VALUES
(1, 'Iniciado'),
(2, 'Enterado'),
(3, 'En Trámite'),
(4, 'Pendiente'),
(5, 'Concluido');

-- --------------------------------------------------------

--
-- Table structure for table `status_user`
--

CREATE TABLE `status_user` (
  `id_user_status` int(11) NOT NULL,
  `desc_user_status` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `status_user`
--

INSERT INTO `status_user` (`id_user_status`, `desc_user_status`) VALUES
(1, 'Active'),
(2, 'Inactive');

-- --------------------------------------------------------

--
-- Table structure for table `type_docs`
--

CREATE TABLE `type_docs` (
  `id_type_doc` int(11) NOT NULL,
  `desc_type_doc` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `type_docs`
--

INSERT INTO `type_docs` (`id_type_doc`, `desc_type_doc`) VALUES
(1, 'Circular'),
(2, 'Convocatoria'),
(3, 'Email'),
(4, 'Notificación'),
(5, 'Factura'),
(6, 'Invitación'),
(7, 'Oficio'),
(8, 'Sobre'),
(9, 'Tarjeta');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `id_priv` int(11) NOT NULL,
  `id_user_status` int(11) NOT NULL,
  `id_dir` int(11) NOT NULL,
  `id_dep` int(11) NOT NULL,
  `id_history` int(11) NOT NULL,
  `name` varchar(120) COLLATE utf8_spanish_ci NOT NULL,
  `user_name` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `user_pass` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `user_movil` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_email` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_photo` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_position` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `user_resume` varchar(120) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_tel` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_ext` varchar(6) COLLATE utf8_spanish_ci DEFAULT NULL,
  `active` varchar(2) COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_online` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `date_reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `about` text COLLATE utf8_spanish_ci,
  `status` enum('1','0') COLLATE utf8_spanish_ci DEFAULT NULL,
  `complete` enum('1','0') COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `id_priv`, `id_user_status`, `id_dir`, `id_dep`, `id_history`, `name`, `user_name`, `user_pass`, `user_movil`, `user_email`, `user_photo`, `user_position`, `user_resume`, `user_tel`, `user_ext`, `active`, `user_online`, `date_reg`, `about`, `status`, `complete`) VALUES
(1, 1, 1, 1, 4, 39, 'Klvst3r', 'klvst3r', '81dc9bdb52d04dc20036dbd8313ed055', '12345678', 'klvst3r@gmail.com', 'ui-danro.jpg', 'Dev', 'CEO Web developer + Edx + UX + UI', '12345678', '0', '0', '0', '2018-08-15 22:49:14', 'I am admin user dev for Internet lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eros nibh, viverra a dui a, gravida varius velit. Vivamus tristique est non ipsum dapibus lacinia sed nec metus.', '1', '1'),
(2, 2, 1, 1, 4, 50, 'Kozlov', 'kozlov', 'c3c6249a73c9d53912e0ed24c35d5783', '2461645449', 'kozlov@gmail.com', 'pp_010.jpg', 'Web Dev', 'Dev + UI + Dx + Tster', '12345678', '0', '0', '0', '2018-09-11 00:58:46', 'Web developer + Edx + UX + UI', '1', '1'),
(3, 2, 1, 5, 5, 52, 'Rosa Isela Suarez Delgadillo', 'Rous', '27af73ef66176960eec3fdf2d5d116ad', '2462189693', 'fenyxrous@gmail.com', 'girl_login.png', 'Oficialia de Partes', 'Control de Documentos de Oficialia de Partes', '2464650900', '3907', '0', '0', '2018-09-18 16:49:48', 'Control y Administración de Oficios, documentos que se reciben en el Despacho y remiten hacia las unidades asministrativas de SECODUVI.', '1', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answer_docs`
--
ALTER TABLE `answer_docs`
  ADD PRIMARY KEY (`id_answer`),
  ADD KEY `id_status_doc` (`id_status_doc`);

--
-- Indexes for table `char_tramits`
--
ALTER TABLE `char_tramits`
  ADD PRIMARY KEY (`id_char`);

--
-- Indexes for table `classifications`
--
ALTER TABLE `classifications`
  ADD PRIMARY KEY (`id_classif`);

--
-- Indexes for table `departaments`
--
ALTER TABLE `departaments`
  ADD PRIMARY KEY (`id_dep`);

--
-- Indexes for table `descriptions`
--
ALTER TABLE `descriptions`
  ADD PRIMARY KEY (`id_desc`);

--
-- Indexes for table `destiny_docs`
--
ALTER TABLE `destiny_docs`
  ADD PRIMARY KEY (`id_destiny`);

--
-- Indexes for table `directions`
--
ALTER TABLE `directions`
  ADD PRIMARY KEY (`id_dir`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id_doc`),
  ADD KEY `id_type_doc` (`id_type_doc`),
  ADD KEY `id_status_doc` (`id_status_doc`),
  ADD KEY `id_muni` (`id_muni`),
  ADD KEY `id_destiny` (`id_destiny`),
  ADD KEY `id_classif` (`id_classif`);

--
-- Indexes for table `documents_turned`
--
ALTER TABLE `documents_turned`
  ADD PRIMARY KEY (`id_turn`),
  ADD KEY `id_dep` (`id_dep`),
  ADD KEY `id_dir` (`id_dir`),
  ADD KEY `id_status_doc` (`id_status_doc`),
  ADD KEY `id_doc` (`id_doc`),
  ADD KEY `id_char` (`id_char`),
  ADD KEY `id_classif` (`id_classif`),
  ADD KEY `id_desc` (`id_desc`);

--
-- Indexes for table `history_access`
--
ALTER TABLE `history_access`
  ADD PRIMARY KEY (`id_history`);

--
-- Indexes for table `municipalities`
--
ALTER TABLE `municipalities`
  ADD PRIMARY KEY (`id_muni`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id_org`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `privileges`
--
ALTER TABLE `privileges`
  ADD PRIMARY KEY (`id_priv`);

--
-- Indexes for table `status_docs`
--
ALTER TABLE `status_docs`
  ADD PRIMARY KEY (`id_status_doc`);

--
-- Indexes for table `status_user`
--
ALTER TABLE `status_user`
  ADD PRIMARY KEY (`id_user_status`);

--
-- Indexes for table `type_docs`
--
ALTER TABLE `type_docs`
  ADD PRIMARY KEY (`id_type_doc`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_dep` (`id_dep`),
  ADD KEY `id_dir` (`id_dir`),
  ADD KEY `id_priv` (`id_priv`),
  ADD KEY `id_user_status` (`id_user_status`),
  ADD KEY `id_history` (`id_history`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answer_docs`
--
ALTER TABLE `answer_docs`
  MODIFY `id_answer` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `char_tramits`
--
ALTER TABLE `char_tramits`
  MODIFY `id_char` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `classifications`
--
ALTER TABLE `classifications`
  MODIFY `id_classif` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `departaments`
--
ALTER TABLE `departaments`
  MODIFY `id_dep` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `descriptions`
--
ALTER TABLE `descriptions`
  MODIFY `id_desc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `destiny_docs`
--
ALTER TABLE `destiny_docs`
  MODIFY `id_destiny` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `directions`
--
ALTER TABLE `directions`
  MODIFY `id_dir` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id_doc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `documents_turned`
--
ALTER TABLE `documents_turned`
  MODIFY `id_turn` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `history_access`
--
ALTER TABLE `history_access`
  MODIFY `id_history` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `municipalities`
--
ALTER TABLE `municipalities`
  MODIFY `id_muni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id_org` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `privileges`
--
ALTER TABLE `privileges`
  MODIFY `id_priv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `status_docs`
--
ALTER TABLE `status_docs`
  MODIFY `id_status_doc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `status_user`
--
ALTER TABLE `status_user`
  MODIFY `id_user_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `type_docs`
--
ALTER TABLE `type_docs`
  MODIFY `id_type_doc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `answer_docs`
--
ALTER TABLE `answer_docs`
  ADD CONSTRAINT `answer_docs_ibfk_1` FOREIGN KEY (`id_status_doc`) REFERENCES `status_docs` (`id_status_doc`);

--
-- Constraints for table `documents`
--
ALTER TABLE `documents`
  ADD CONSTRAINT `documents_ibfk_1` FOREIGN KEY (`id_type_doc`) REFERENCES `type_docs` (`id_type_doc`),
  ADD CONSTRAINT `documents_ibfk_2` FOREIGN KEY (`id_status_doc`) REFERENCES `status_docs` (`id_status_doc`),
  ADD CONSTRAINT `documents_ibfk_3` FOREIGN KEY (`id_muni`) REFERENCES `municipalities` (`id_muni`),
  ADD CONSTRAINT `documents_ibfk_4` FOREIGN KEY (`id_destiny`) REFERENCES `destiny_docs` (`id_destiny`),
  ADD CONSTRAINT `documents_ibfk_5` FOREIGN KEY (`id_classif`) REFERENCES `classifications` (`id_classif`);

--
-- Constraints for table `documents_turned`
--
ALTER TABLE `documents_turned`
  ADD CONSTRAINT `documents_turned_ibfk_1` FOREIGN KEY (`id_dep`) REFERENCES `departaments` (`id_dep`),
  ADD CONSTRAINT `documents_turned_ibfk_2` FOREIGN KEY (`id_dir`) REFERENCES `directions` (`id_dir`),
  ADD CONSTRAINT `documents_turned_ibfk_3` FOREIGN KEY (`id_status_doc`) REFERENCES `status_docs` (`id_status_doc`),
  ADD CONSTRAINT `documents_turned_ibfk_4` FOREIGN KEY (`id_doc`) REFERENCES `documents` (`id_doc`),
  ADD CONSTRAINT `documents_turned_ibfk_5` FOREIGN KEY (`id_char`) REFERENCES `char_tramits` (`id_char`),
  ADD CONSTRAINT `documents_turned_ibfk_6` FOREIGN KEY (`id_classif`) REFERENCES `classifications` (`id_classif`),
  ADD CONSTRAINT `documents_turned_ibfk_7` FOREIGN KEY (`id_desc`) REFERENCES `descriptions` (`id_desc`);

--
-- Constraints for table `organizations`
--
ALTER TABLE `organizations`
  ADD CONSTRAINT `organizations_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_dep`) REFERENCES `departaments` (`id_dep`),
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`id_dir`) REFERENCES `directions` (`id_dir`),
  ADD CONSTRAINT `users_ibfk_3` FOREIGN KEY (`id_priv`) REFERENCES `privileges` (`id_priv`),
  ADD CONSTRAINT `users_ibfk_4` FOREIGN KEY (`id_user_status`) REFERENCES `status_user` (`id_user_status`),
  ADD CONSTRAINT `users_ibfk_5` FOREIGN KEY (`id_history`) REFERENCES `history_access` (`id_history`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
ob_start();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
   
  </head>
  <?php include'inc/userHeadDashboard.php'; ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- ********** Begin HEADER ********** -->      
      <!-- ***********************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      **************************************************************************************************************************** -->
        <?php include'inc/userHeader.php'; ?>
    <!-- ********** Finish HEADER ********** -->        
  </header>
      <!--header end-->
      
      <!-- ***********************************************************************************************************************
      MAIN SIDEBAR MENU
      **************************************************************************************************************************** -->
       <!--sidebar start-->
      <aside class="main-sidebar">
      <!-- ********** Begin Aside ********** --> 
      <?php include 'chkmenu.php'; ?>     
        
      <!-- ********** Finish Aside ********** -->        
      </aside>
      <!--sidebar end-->
      
      <!-- ************************************************************************************************************************
      MAIN CONTENT
      ***************************************************************************************************************************** -->


      <!--main content start-->
      <section id="main-content">
      
      

      <?php  
          $_SESSION["user"]["code"] = "true";
      ?>
      
      <?php
        include'inc/userContent.php'; 
      ?>

         
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
     <?php include'inc/userFooter.php'; ?>
     <!--footer ends-->

  </body>
</html>
<?php
ob_end_flush();
?>  

<!DOCTYPE html>
<html lang="en">
<head>
 
</head>
<!-- ********** Begins Meta and Links ********** -->      
   

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, SECODUVI, SIOP">
  
  <title> - TLX | SECODUVI -</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../panel/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../panel/components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../panel/components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../panel/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<!--   <link rel="stylesheet" href="../panel/dist/css/skins/_all-skins.min.css"> -->

  <link rel="stylesheet" href="../panel/dist/css/skins/_all-skins.css">
  <link rel="stylesheet" href="../panel/dist/css/custom.css">
  

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> --><!-- ********** Finish Meta and Links ********** -->          
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<header class="main-header">
<!-- ********** Begin HEADER ********** -->      
<!-- ********** Begin HEADER ********** -->
    <!-- Logo -->
    <div class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b><span class="dark_green">T</span><span class="light_green">L<span class="red">X</span></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><span class="dark_green">T</span><span class="light_green">L<span class="red">X</span></b> <span class="grey">SECODUVI</span></span>
    </div>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
         
          <!-- Tasks: style can be found in dropdown.less -->
         
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- <img src="../panel/dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> -->
              <span class="hidden-xs">kozlov</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../assets/img/users/ui-danro.jpg" class="img-circle" alt="User Image">

                <p>
                  Kozlov - Register 
                  <small>CEO Web developer + Edx + UX + UI</small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-6 text-center">
                    <span>kozlov@gmail.com</span>
                  </div>
                  <div class="col-xs-6 text-center">
                    <span href="#">12 / 01 / 2018</span>
                  </div>
                  
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat"><i class="fa fa-cog fa-spin fa-1x fa-fw text-green"></i> Perfíl</a>

                </div>
                <div class="pull-right">
                  <a href="#" class="btn btn-default btn-flat" data-toggle="control-sidebar"><i class="fa fa-gears text-blue"></i> - Configuración</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="../view/signout.php"><i class="fa fa-power-off fa-1x text-red"></i></a>


          </li>
        </ul>
      </div>
    </nav>

<!-- ********** Finish HEADER ********** -->
<!-- ********** Finish HEADER ********** -->        
<!--header end-->
</header>
<!--sidebar start-->
<aside class="main-sidebar">
<!-- ********** Begin Aside ********** -->      
  
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
        	          <img src="../assets/img/users/ui-danro.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
                  <p>Kozlov</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <div>
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"> - SIOP | MAIN NAVIGATION - </li>
        <li class="treeview">
                  <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              
            </span>
          </a>
       
        </li>
      </ul>
      <div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Buscar Folio...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <ul class="sidebar-menu" data-widget="tree">
        

         <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Registro</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="op.php?a=1"><i class="fa fa-circle-o"></i> Documentos</a></li>
            <!-- <li><a href="#"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Editors</a></li> -->
          </ul>
        </li>       
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>UI Elements</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> General</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Icons</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Buttons</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Sliders</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Modals</a></li>
          </ul>
        </li>
        
        <li>
          <a href="#">
            <i class="fa fa-th"></i> <span>Widgets</span>
            
          </a>
        </li>

        <li><a href="#"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar --><!-- ********** Finish Aside ********** -->        
</aside>
<!--sidebar end-->
<!--- ####################################################### Begin Content ##################################################################### -->


<!--main content start-->
<section id="main-content">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      SIOP 
      <small>Sistema de Información de Oficialia de Partes</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="/secoduvi/siop/user/"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Registro</a></li>
      <li class="active"><i class="fa fa-edit"></i> Documentos</li>
    </ol>
  </section>

<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <i class="ion ion-aperture"></i> Registro de Documentos
        <small class="pull-right">12-01-2018</small>
      </h2>
    </div>
    <!-- /.col -->
  </div>
  

  <div class="row">
    
  </div>

  <div class="row invoice-info">
  <div class="col-xs-12">
      <p class="lead text-center">Registro</p>
    </div>
    <!-- /.col -->
   <form name='editProfile' method='POST' action='action.php?a=5' role='form'  class=''>
    <div class="col-sm-6">
    <div class="form-group">
                <label class="" for="folio_doc"><b> Folio</b></label> 
                <div class="">
                  <input type="text" name="folio_doc" class="col-md-12" id="folio_doc"   value="" maxlength="" size="" style=""     placeholder="Folio del documento" required autofocus autocomplete="off" />
                </div>
    </div>
    <div class="space">&nbsp;</div>
    <div class="form-group"><label for="SelectDestiny"><b>Dirigido a</b></label>
      <select name="destiny" id="SelectDestiny" class="form-control" required=""  >
        <option value='0'>'No hay opciones !!'</option>
      </select>
    </div>
    </div>
    <!-- /.col -->

    <div class="col-sm-6 invoice-col">
    <div class="form-group">
                <label class="" for="folio_doc"><b> Folio</b></label> 
                <div class="">
                  <input type="text" name="folio_doc" class="col-md-12" id="folio_doc"   value="" maxlength="" size="" style=""     placeholder="Folio del documento" required autofocus autocomplete="off" />
                </div>
              </div>
    </div>
    <!-- /.col -->

    <div class="space">&nbsp;</div>
    <div class="ln_solid"></div>

    <div class="col-md-5 col-md-offset-9">
      <button type="submit" class="btn btn-primary" ><i class="fa fa-save"></i> &nbsp;&nbsp;Registrar Información</button>          <!-- button type="button" class="btn btn-primary " style="margin-right: 5px;">
            <i class="fa fa-save"></i> Registrar
          </button> -->
    </div>
      

    </form> </div>


 
</section>
<!-- /.content -->
  
<div class="pad margin no-print"></div>
<div class="clearfix"></div>
</div>
  <!-- /.content-wrapper -->
</section><!-- /MAIN CONTENT -->

<!--- ####################################################### Finish Content ##################################################################### -->
<!--main content end-->

<!--footer start-->
  <footer class="main-footer no-print">
    <div class="pull-right hidden-xs">
      <b>V.</b> 1.0.1
    </div>
    <strong>SIOP - SECODUVI &copy; 2018.</strong> - by <i class="text-blue">Klvst3r & <i class="ion ion-music-note"></i></i>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Actividad Reciente</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cumpleaños de Kozlov</h4>

                <p>Será el 23 de April de 2017</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Kozlov Actualizo su perfi</h4>

                <p>Nuevo Telefono +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora se unió a la lista de correo</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 ejecutado</h4>

                <p>tiempo de execución 5 segundos</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Progreso de tareas</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Diseño de plantilla personalizada
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Actualizar información
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Integraciónn de archivo
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Generación de informes
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Contenido de pestaña</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">Configuración General</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Reporte del uso del panel
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Parte de la información sobre esta opción de configuración general
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
             Permitir redirección de correo
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
             Otros conjuntos de opciones están disponibles
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Exponer el nombre del autor en publicaciones
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Permitir que el usuario muestre su nombre en publicaciones de blog
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Configuración del Chat</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Muestrame en linea
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Apagar notificaciones
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
             Borrar el historial del chat
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../panel/components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../panel/components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../panel/components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../panel/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../panel/dist/js/demo.js"></script><!--footer ends-->

</body>
</html>
  





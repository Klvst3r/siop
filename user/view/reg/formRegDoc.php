<?php
ob_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>

</head>
<!-- ********** Begins Meta and Links ********** -->
<?php include'inc/userHeadDashboard.php'; ?>
<!-- ********** Finish Meta and Links ********** -->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<header class="main-header">
<!-- ********** Begin HEADER ********** -->
<?php include'inc/userHeader.php'; ?>
<!-- ********** Finish HEADER ********** -->
<!--header end-->
</header>
<!--sidebar start-->
<aside class="main-sidebar">
<!-- ********** Begin Aside ********** -->
 <?php 
 //include'inc/userAside.php'; 
   include 'chkmenu.php';
  ?>
<!-- ********** Finish Aside ********** -->
</aside>
<!--sidebar end-->
<!--- ####################################################### Begin Content ##################################################################### -->


<!--main content start-->
<section id="main-content">
<?php
  $_SESSION["user"]["code"] = "true";

/* ********************************************************************************************************************************************************
Begin Main Content
*********************************************************************************************************************************************************** */
/*
  After this will be adquire all document info and its tables
 */
include '../controller/DestinyController.php';

include '../controller/DocumentController.php';

include'../data/Form.php';

include '../sql/Combo.php';

include '../helps/helps.php';

include ("../assets/calendario/calendario.php");

echo '<script language="JavaScript" src="../assets/calendario/javascripts.js"></script>
    <link rel="STYLESHEET" type="text/css" href="../assets/calendario/estilo.css">';


?>


<script type="text/javascript">
$(function(){
  $('#formInscripcion').validate({
    rules: {
      'classif': 'required',
    },
    messages: {
        'classif': 'Debe seleccionar la clasificación del documento',

    },
    //debug: true,
    /*errorElement: 'div',*/
    //errorContainer: $('#errores'),

  });
});
</script>



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      SIOP
      <small>Sistema de Información de Oficialia de Partes</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="/secoduvi/siop/user/"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Registro</a></li>
      <li class="active"><i class="fa fa-edit"></i> Documentos</li>
    </ol>
  </section>

<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <i class="ion ion-aperture"></i> Registro de Documentos
        <small class="pull-right"><?php echo date("d-m-Y") ?></small>
      </h2>
    </div>
    <!-- /.col -->
  </div>


  <div class="row">

  </div>

  <div class="row invoice-info">
  <!-- <div class="col-xs-12">
      <p class="lead text-center">Registro</p>
    </div> -->
    <!-- /.col -->
    <?php

    

     if (isset($_POST["save"])){ //If the user wants save data

        //In optional cases
        if($_POST["dir_sender"] == ''){
          $_POST["dir_sender"] = "Domicilio no especificado";
        }

        if($_POST["tel_doc"] == ''){
          $_POST["tel_doc"] = "0";
        }

        if($_POST["ext_doc"] == ''){
          $_POST["ext_doc"] = "0";
        }
        
        if($_POST["movil_doc"] == ''){
          $_POST["movil_doc"] = "0";
        }
        
        if($_POST["email_doc"] == ''){
          $_POST["email_doc"] = "Sin correo electrónico";
        }

        if($_POST["reference"] == ''){
          $_POST["reference"] = "Sin referencia";
        }
        
        

      if(isset($_POST["folio_doc"]) && isset($_POST["destiny"]) && isset($_POST["destiny_position"]) && isset($_POST["subject"]) && isset($_POST["send_doc"]) && isset($_POST["desc_send_position"]) && isset($_POST["origin_doc"]) && isset($_POST["dir_sender"]) && isset($_POST["municipality"]) && isset($_POST["tel_doc"]) && isset($_POST["ext_doc"]) && isset($_POST["movil_doc"]) && isset($_POST["email_doc"]) && isset($_POST["date_doc"]) && isset($_POST["date_recep"]) && isset($_POST["reference"]) && isset($_POST["desc_type_doc"]) && isset($_POST["desc_status_doc"]) and isset($_POST["classif"])){

        $id_destiny           =   validate_field($_POST["destiny"]);
        $id_muni              =   validate_field($_POST["municipality"]);
        $id_type_doc          =   validate_field($_POST["desc_type_doc"]);
        $id_status_doc        =   validate_field($_POST["desc_status_doc"]);
        $id_classif           =   validate_field($_POST["classif"]);
        $subject_doc          =   validate_field($_POST["subject"]);
        $desc_send_position   =   validate_field($_POST["desc_send_position"]);
        $folio_doc            =   validate_field($_POST["folio_doc"]);
        $send_doc             =   validate_field($_POST["send_doc"]);
        $origin_doc           =   validate_field($_POST["origin_doc"]);
        $dir_sender           =   validate_field($_POST["dir_sender"]);
        $movil_doc            =   validate_field($_POST["movil_doc"]);
        $tel_doc              =   validate_field($_POST["tel_doc"]);
        $ext_doc              =   validate_field($_POST["ext_doc"]);
        $email_doc            =   validate_field($_POST["email_doc"]);
        $date_doc             =   validate_field($_POST["date_doc"]);
        $date_doc             =   format_date($date_doc);

        $date_recep           =   validate_field($_POST["date_recep"]);
        $date_recep           =   format_date($date_recep);

        $reference            =   validate_field($_POST["reference"]);


        DocumentController::regDocument($id_destiny, $id_muni, $id_type_doc, $id_status_doc, $id_classif, $subject_doc, $desc_send_position, $folio_doc, $send_doc, $origin_doc, $dir_sender, $movil_doc, $tel_doc, $ext_doc, $email_doc, $date_doc, $date_recep, $reference);


        header("location:index.php");

      }else{
        header("location:index.php");
      }

   


        
    }else{

      
        
      



    $form = new Form('regDoc','POST','', 'form', '', '');
    
    
      
    
    ?>

    <div class="col-sm-6">
    <?php
  /* ------------------------------------    Begin First Column  ----------------------------------*/      

    if(isset($_POST["folio_doc"])){
      $folio_doc = $_POST["folio_doc"];
    }else{
      $folio_doc = '';
    }

    $form -> addField(1, array(
      "field_name"    =>  "folio_doc",
      "class_label"   =>  "",
      "label_field"   =>  "Folio",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  $folio_doc,
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Folio del documento",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));



   

     if(isset($_POST["destiny"])){
      $destiny = $_POST["destiny"];
    }else{
      $destiny = '';
    }
     $query = "SELECT id_destiny, desc_destiny, destiny_position from destiny_docs order by id_destiny";
     $combo = new combo($query,"destiny","SelectDestiny", $destiny, "Dirigido a","","","onChange='this.form.submit();'","1");
     //$combo = new combo($query,"destiny","SelectDestiny", "", "Dirigido a","","","","1");


     $id_dest = $destiny;

     $data = DestinyController::getDestiny($id_dest);
     $position = $data->getDestiny_position();


   
      
      if(isset($position)){
        $destiny_position = $position;
      }else{
        $destiny_position = '';
      }

      $field = $form -> addField(1, array(
      "field_name"    =>  "destiny_position",
      "class_label"   =>  "",
      "label_field"   =>  "Cargo",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  $destiny_position,
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Cargo",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));



     if(isset($_POST["subject"])){
      $subject = $_POST["subject"];
    }else{
      $subject = '';
    } 
      $form -> addField(9, array(
        "field_name"    =>  "subject",
        "label_field"   =>  "Asunto",
        "readonly"      =>  "",
        "disabled"      =>  "",
        "value"         =>  "subject",
        "rows"          =>  "5",
        "cols"          =>  "",
        "style"         =>  "",
        "js"            =>  "",
        "placeholder"   =>  "Breve descripción del Asunto...",
        "content"       =>  "$subject",
        "required"      =>  "required"

      ));




    if(isset($_POST["send_doc"])){
      $send_doc = $_POST["send_doc"];
    }else{
      $send_doc = '';
    }
    $form -> addField(1, array(
      "field_name"    =>  "send_doc",
      "class_label"   =>  "",
      "label_field"   =>  "Enviado por",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  $send_doc,
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Remitente",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));

    
    if(isset($_POST["desc_send_position"])){
      $desc_send_position = $_POST["desc_send_position"];
    }else{
      $desc_send_position = '';
    }
    $form -> addField(1, array(
      "field_name"    =>  "desc_send_position",
      "class_label"   =>  "",
      "label_field"   =>  "Cargo Remitente",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  $desc_send_position,
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Cargo remitente",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));

    if(isset($_POST["classif"])){
      $classif = $_POST["classif"];
    }else{
      $classif = '';
    }
    $query = "SELECT id_classif, desc_classif from classifications order by id_classif";
    $combo = new combo($query,"classif","classif", $classif, "Clasificación del documento","required","","","1");


    
    if(isset($_POST["origin_doc"])){
      $origin_doc = $_POST["origin_doc"];
    }else{
      $origin_doc = '';
    }
    $form -> addField(1, array(
      "field_name"    =>  "origin_doc",
      "class_label"   =>  "",
      "label_field"   =>  "Lugar de origen",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  $origin_doc,
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Origen",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));



    if(isset($_POST["dir_sender"])){
      $dir_sender = $_POST["dir_sender"];
    }else{
      $dir_sender = '';
    }
    $form -> addField(1, array(
      "field_name"    =>  "dir_sender",
      "class_label"   =>  "",
      "label_field"   =>  "Domicilio",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  $dir_sender,
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Dirección",
      "required"      =>  "",
      "autofocus"     =>  ""
      ));


      if(isset($_POST["municipality"])){
        $municipality = $_POST["municipality"];
      }else{
        $municipality = '';
      }
     $query = "SELECT id_muni, desc_muni from municipalities order by id_muni";
     $combo = new combo($query,"municipality","SelectMuni", $municipality, "Municipio","","","","1");

      
      

       


    /* ------------------------------------    End First Column  ----------------------------------*/
    ?>
    </div>
    <!-- /.col -->


    <div class="col-sm-6 invoice-col">
    <?php
    /* ------------------------------------    Second Column  ----------------------------------*/

    
    

    if(isset($_POST["tel_doc"])){
        $tel_doc = $_POST["tel_doc"];
      }else{
        $tel_doc = '';
      }    
      $form -> addField(7, array(
                "field_name"    =>  "tel_doc",
                "label_field"   =>  "Telefono",
                "class_label"   =>  "",
                  "div_field"     =>  "",
                "input_class"   =>  "col-md-12",
                "readonly"      =>  "",
                "disabled"      =>  "",
                "value"         =>  $tel_doc,
                "maxlength"     =>  "10",
                "size"          =>  "10",
                "style"         =>  "",
                "js"            =>  "",
                "placeholder"   =>  "Numero telefónico...",
                "required"      =>  "",
                "autofocus"     =>  ""
                ));


       if(isset($_POST["ext_doc"])){
        $ext_doc = $_POST["ext_doc"];
      }else{
        $ext_doc = '';
      }    
      $form -> addField(7, array(
                  "field_name"    =>  "ext_doc",
                  "label_field"   =>  "Extensión",
                  "class_label"   =>  "",
                  "div_field"     =>  "",
                  "input_class"   =>  "col-md-12",
                  "readonly"      =>  "",
                  "disabled"      =>  "",
                  "value"         =>  $ext_doc,
                  "maxlength"     =>  "6",
                  "size"          =>  "",
                  "style"         =>  "",
                  "js"            =>  "",
                  "placeholder"   =>  "Extensión de oficina...",
                  "required"      =>  "",
                  "autofocus"     =>  ""
                ));



    if(isset($_POST["movil_doc"])){
        $movil_doc = $_POST["movil_doc"];
      }else{
        $movil_doc = '';
      }        
     $form -> addField(7, array(
                "field_name"    =>  "movil_doc",
                "label_field"   =>  "Otro teléfono",
                "class_label"   =>  "",
                  "div_field"     =>  "",
                "input_class"   =>  "col-md-12",
                "readonly"      =>  "",
                "disabled"      =>  "",
                "value"         =>  $movil_doc,
                "maxlength"     =>  "10",
                "size"          =>  "",
                "style"         =>  "",
                "js"            =>  "",
                "placeholder"   =>  "Otro telefono o número de celular...",
                "required"      =>  "",
                "autofocus"     =>  ""
                ));

        if(isset($_POST["email_doc"])){
          $email_doc = $_POST["email_doc"];
        }else{
          $email_doc = '';
        }       

        $form -> addField(1, array(
            "field_name"    =>  "email_doc",
            "class_label"   =>  "",
            "label_field"   =>  "Correo Electrónico",
            "div_field"     =>  "",
            "input_class"   =>  "col-md-12",
            "readonly"      =>  "",
            "disabled"      =>  "",
            "value"         =>  $email_doc,
            "maxlength"     =>  "",
            "size"          =>  "",
            "style"         =>  "",
            "js"            =>  "",
            "placeholder"   =>  "Escriba su email o sitio web...",
            "required"      =>  "",
            "autofocus"     =>  ""
            ));

        

    echo '<div class="space">&nbsp;</div>';

    if(isset($_POST["date_doc"])){
      $date_doc = $_POST["date_doc"];
    }else{
      $date_doc = date("d/m/Y");
    }     
    escribe_formulario_fecha_vacio(
      "date_doc",
      $date_doc,
      "Fecha de Documento",
      "regDoc"
      );

     if(isset($_POST["date_recep"])){
      $date_recep = $_POST["date_recep"];
    }else{
      $date_recep = date("d/m/Y");
    }   
    escribe_formulario_fecha_vacio(
      "date_recep",
      $date_recep,
      "Fecha de Recepción",
      "regDoc"
      );

     if(isset($_POST["reference"])){
      $reference = $_POST["reference"];
    }else{
      $reference = '';
    } 
    $form -> addField(1, array(
      "field_name"    =>  "reference",
      "class_label"   =>  "",
      "label_field"   =>  "Referencia asunto",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  $reference,
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Referencia",
      "required"      =>  "",
      "autofocus"     =>  ""
      ));

    if(isset($_POST["desc_type_doc"])){
        $desc_type_doc = $_POST["desc_type_doc"];
      }else{
        $desc_type_doc = '';
      }
    $query = "SELECT id_type_doc, desc_type_doc from type_docs order by id_type_doc";
    $combo = new combo($query,"desc_type_doc","SelectTypeDoc", $desc_type_doc, "Tipo de Documento","required","","","1");

    /*if(isset($_POST["desc_status_doc"])){
        $desc_status_doc = $_POST["desc_status_doc"];
      }else{
        $desc_status_doc = '';
      }
    $query = "SELECT id_status_doc, desc_status_docs from status_docs order by id_status_doc";
    $combo = new combo($query,"desc_status_doc","SelectStatusDoc", $desc_status_doc, "Estatus Documento","","","","1");*/

    //Al documents registred begin in 1
    $form -> addField(4, array(
                           "field_name"   =>  "desc_status_doc",
                           "value"        =>  "1"
                           ));
    



    ?>

    </div>
    <!-- /.col -->


    <div class="space">&nbsp;</div>
    <div class="ln_solid"></div>

    <?php

    echo '<div class="space">&nbsp;</div>
    <div class="ln_solid"></div>

    

    <div class="space">&nbsp;</div>
    ';

    echo '<div class="col-md-6 ">';

     $form -> addField(3, array(
      "name"          =>  "save",
      "type_button"   =>  "btn btn-primary pull-right",
      "icon"          =>  "fa fa-save",
      "disabled"      =>  "",
      "legend"        =>  "Registrar Información",
      "tooltip"       =>  ""

      ));

    echo '</div>';


    ?>

    <div class="col-md-5 col-md-offset-9">
    

    </div>


  <?php 
    
  $form->closeForm(); 

  }//if(isset($_POST('save'));

    ?>


  </div>



</section>
<!-- /.content -->

<div class="pad margin no-print"></div>
<div class="clearfix"></div>
</div>
  <!-- /.content-wrapper -->
<?php
/* **********************************************************************************************************************************************************
Begin Main Content
*********************************************************************************************************************************************************** */
?>
</section><!-- /MAIN CONTENT -->

<!--- ####################################################### Finish Content ##################################################################### -->
<!--main content end-->

<!--footer start-->
<?php include'inc/userFooter.php'; ?>
<!--footer ends-->

</body>
</html>
<?php
ob_end_flush();
?>


<?php
ob_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>

</head>
<!-- ********** Begins Meta and Links ********** -->
<?php include'inc/userHeadDashboard.php'; ?>
<!-- ********** Finish Meta and Links ********** -->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<header class="main-header">
<!-- ********** Begin HEADER ********** -->
<?php include'inc/userHeader.php'; ?>
<!-- ********** Finish HEADER ********** -->
<!--header end-->
</header>
<!--sidebar start-->
<aside class="main-sidebar">
<!-- ********** Begin Aside ********** -->
 <?php include'inc/userAside.php'; ?>
<!-- ********** Finish Aside ********** -->
</aside>
<!--sidebar end-->
<!--- ####################################################### Begin Content ##################################################################### -->


<!--main content start-->
<section id="main-content">
<?php
  $_SESSION["user"]["code"] = "true";

/* ********************************************************************************************************************************************************
Begin Main Content
*********************************************************************************************************************************************************** */
/*
  After this will be adquire all document info and its tables
 */
include '../controller/DocumentController.php';

include '../controller/TrackController.php';

include'../data/Form.php';

include '../sql/Combo.php';

include '../helps/helps.php';

include ("../assets/calendario/calendario.php");

echo '<script language="JavaScript" src="../assets/calendario/javascripts.js"></script>
    <link rel="STYLESHEET" type="text/css" href="../assets/calendario/estilo.css">';

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      SIOP
      <small>Sistema de Información de Oficialia de Partes</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="/secoduvi/siop/user/"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Registro</a></li>
      <li class="active"><i class="fa fa-edit"></i> Documentos</li>
    </ol>
  </section>

<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <i class="ion ion-aperture"></i> Registro de Seguimiento
        <small class="pull-right"><?php echo date("d-m-Y") ?></small>
      </h2>
    </div>
    <!-- /.col -->
  </div>


  <div class="row">

  </div>

  <div class="row invoice-info">
  <!-- <div class="col-xs-12">
      <p class="lead text-center">Registro</p>
    </div> -->
    <!-- /.col -->
    <?php
    

     if (isset($_POST["save"])){ //If the user wants save data

      


      
          if( isset($_POST["answer"] ) && isset ($_POST["observation"]) ){

            $id_turn         =   validate_field($_POST["folio"]);
            $answer          =   validate_field($_POST["answer"]);         
            $observation     =   validate_field($_POST["observation"]);    


/*                echo $answer;
        echo "<br />";
        echo $observation;
            */
           
          }

          if( $_POST["answer"] == " "  && $_POST["observation"] == " " ){            

              $answer          =   "Sin Respuesta";         
              $observation     =   "Sin comentario"; 

          }



        TrackController::updateTrack( $id_turn, $answer, $observation );

      header("location:index.php");

        
    }else{


//$docSelect =  DocumentController::getInfoDoc($d);

$form = new Form('trackDoc','POST','', 'form', '', '');
?>

    <div class="col-sm-6">

<?php


  /* ------------------------------------    Begin First Column  ----------------------------------*/      

if(isset($_GET["b"])){
  $folio = $_GET["b"];
}else{
  $folio = '';
}


   $form -> addField(4, array(
       "field_name"    =>  "folio",
       "value"   =>  $folio
       ));    


   $track = TrackController::getTrack($folio);




   //$answer = $track -> TrackController::getAnswer();
   

   $answer = $track -> getAnswer();

   if( empty($answer) ){

     $form -> addField(9, array(
        "field_name"    =>  "answer",
        "label_field"   =>  "Respuesta",
        "readonly"      =>  "",
        "disabled"      =>  "",
        "value"         =>  "",
        "rows"          =>  "3",
        "cols"          =>  "",
        "style"         =>  "",
        "js"            =>  "",
        "placeholder"   =>  "Escriba respuesta de seguimiento a documento",
        "content"       =>  "",
        "required"      =>  ""
      ));     


   }else{


       $form -> addField(9, array(
        "field_name"    =>  "answer",
        "label_field"   =>  "Respuesta",
        "readonly"      =>  "",
        "disabled"      =>  "",
        "value"         =>  "",
        "rows"          =>  "3",
        "cols"          =>  "",
        "style"         =>  "",
        "js"            =>  "",
        "placeholder"   =>  "Escriba respuesta de seguimiento a documento",
        "content"       =>  "$answer",
        "required"      =>  ""
      ));     




   }


     

?>
  
  <hr />

<?php

    
  /*  $form -> addField(1, array(
      "field_name"    =>  "id_doc",
      "class_label"   =>  "",
      "label_field"   =>  "ID",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "disabled",
      "value"         =>  $id_doc,
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "ID del documento",
      "required"      =>  "",
      "autofocus"     =>  ""
      ));*/

    $desc_destiny = $track -> getDesc_destiny();
    $form -> addField(1, array(
      "field_name"    =>  "desc_destiny",
      "class_label"   =>  "",
      "label_field"   =>  "Destinatario",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "disabled",
      "value"         =>  "$desc_destiny",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Nombre del Destinatario",
      "required"      =>  "",
      "autofocus"     =>  ""
      ));
   
    $destiny_position = $track -> getDestiny_position();
    $form -> addField(1, array(
      "field_name"    =>  "destiny_position",
      "class_label"   =>  "",
      "label_field"   =>  "Cargo",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "disabled",
      "value"         =>  "$destiny_position",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Cargo del Destinatario",
      "required"      =>  "",
      "autofocus"     =>  ""
      ));


    //$folio_doc = $track->getFolio_doc();
    $form -> addField(1, array(
      "field_name"    =>  "folio_doc",
      "class_label"   =>  "",
      "label_field"   =>  "Folio",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "disabled",
      "value"         =>  "$folio",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Folio del documento",
      "required"      =>  "",
      "autofocus"     =>  ""
      ));

    $subject_doc = $track -> getSubject_doc();
    $form -> addField(9, array(
        "field_name"    =>  "subject",
        "label_field"   =>  "Asunto",
        "readonly"      =>  "",
        "disabled"      =>  "disabled",
        "value"         =>  "subject",
        "rows"          =>  "3",
        "cols"          =>  "",
        "style"         =>  "",
        "js"            =>  "",
        "placeholder"   =>  "Breve descripción del Asunto...",
        "content"       =>  "$subject_doc",
        "required"      =>  "required"
      ));

     $send_doc = $track -> getSend_doc();
     $form -> addField(1, array(
      "field_name"    =>  "send_doc",
      "class_label"   =>  "",
      "label_field"   =>  "Enviado por",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "disabled",
      "value"         =>  "$send_doc",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Remitente",
      "required"      =>  "",
      "autofocus"     =>  ""
      ));

     $origin_doc = $track -> getOrigin_doc();
     $form -> addField(1, array(
      "field_name"    =>  "origin_doc",
      "class_label"   =>  "",
      "label_field"   =>  "Lugar de origen",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "disabled",
      "value"         =>  "$origin_doc",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Origen",
      "required"      =>  "",
      "autofocus"     =>  ""
      ));


    $date_doc = $track -> getDate_doc();
     $form -> addField(1, array(
      "field_name"    =>  "date_doc",
      "class_label"   =>  "",
      "label_field"   =>  "Fecha Documento",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "disabled",
      "value"         =>  "$date_doc",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Fecha Elaboración",
      "required"      =>  "",
      "autofocus"     =>  ""
      ));
   
  
    /* ------------------------------------    End First Column  ----------------------------------*/
    ?>
    </div>
    <!-- /.col -->

    <div class="col-sm-6 invoice-col">
    <?php
    /* ------------------------------------    Second Column  ----------------------------------*/

    $observation = $track -> getObservation();

    if( empty( $observation ) ){

      $form -> addField(9, array(
        "field_name"    =>  "observation",
        "label_field"   =>  "Observaciones",
        "readonly"      =>  "", 
        "disabled"      =>  "",
        "value"         =>  "",
        "rows"          =>  "3",
        "cols"          =>  "",
        "style"         =>  "",
        "js"            =>  "",
        "placeholder"   =>  "Observación o comentario sobre el seguimiento del documento",
        "content"       =>  "",
        "required"      =>  ""
      )); 

    }else{

      $form -> addField(9, array(
        "field_name"    =>  "observation",
        "label_field"   =>  "Observaciones",
        "readonly"      =>  "", 
        "disabled"      =>  "",
        "value"         =>  "",
        "rows"          =>  "3",
        "cols"          =>  "",
        "style"         =>  "",
        "js"            =>  "",
        "placeholder"   =>  "Observación o comentario sobre el seguimiento del documento",
        "content"       =>  "$observation",
        "required"      =>  ""
      )); 

    }




      

      ?>

    <hr />

  <?php

   
    

  
    $date_recep = $track -> getDate_recep();
     $form -> addField(1, array(
      "field_name"    =>  "date_recep",
      "class_label"   =>  "",
      "label_field"   =>  "Fecha Recepción",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "disabled",
      "value"         =>  "$date_recep",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Fecha Recepción",
      "required"      =>  "",
      "autofocus"     =>  ""
      ));


    $dir_name = $track -> getDir_name();
     $form -> addField(1, array(
      "field_name"    =>  "dir_name",
      "class_label"   =>  "",
      "label_field"   =>  "Dirección Turnada",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "disabled",
      "value"         =>  "$dir_name",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Dirección",
      "required"      =>  "",
      "autofocus"     =>  ""
      ));

    $departament = $track -> getDepartament();
    $form -> addField(1, array(
      "field_name"    =>  "departament",
      "class_label"   =>  "",
      "label_field"   =>  "Departamento",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "disabled",
      "value"         =>  "$departament",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Departamento",
      "required"      =>  "",
      "autofocus"     =>  ""
      ));     


   /* $folio_turned = $track -> getFolio_turned();
     $form -> addField(1, array(
      "field_name"    =>  "folio_turned",
      "class_label"   =>  "",
      "label_field"   =>  "Folio Turnado",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "disabled",
      "value"         =>  "$folio_turned",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Folio Turnado",
      "required"      =>  "",
      "autofocus"     =>  ""
      ));      */


     $doc_origin = $track -> getDoc_origin();
     $form -> addField(1, array(
      "field_name"    =>  "doc_origin",
      "class_label"   =>  "",
      "label_field"   =>  "Turnado a",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "disabled",
      "value"         =>  "$doc_origin",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Origen",
      "required"      =>  "",
      "autofocus"     =>  ""
      ));     

     $date_turned = $track -> getDate_turned();
     $form -> addField(1, array(
      "field_name"    =>  "date_turned",
      "class_label"   =>  "",
      "label_field"   =>  "Fecha Turnado",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "disabled",
      "value"         =>  "$date_turned",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Fecha en que se turno",
      "required"      =>  "",
      "autofocus"     =>  ""
      ));

     $instructions = $track -> getInstructions();
     $form -> addField(9, array(
        "field_name"    =>  "instructions",
        "label_field"   =>  "Instrucciones",
        "readonly"      =>  "",
        "disabled"      =>  "disabled",
        "value"         =>  "subject",
        "rows"          =>  "3",
        "cols"          =>  "",
        "style"         =>  "",
        "js"            =>  "",
        "placeholder"   =>  "Breve descripción de las instrucciones...",
        "content"       =>  "$instructions",
        "required"      =>  "required"
      ));    


     
?>

</div>
<!-- /.col -->


<div class="space">&nbsp;</div>
<div class="ln_solid"></div>

<?php

echo '<div class="space">&nbsp;</div>
<div class="ln_solid"></div>



<div class="space">&nbsp;</div>
';

echo '<div class="col-md-6 ">';

 $form -> addField(3, array(
  "name"          =>  "save",
  "type_button"   =>  "btn btn-primary pull-right",
  "icon"          =>  "fa fa-save",
  "disabled"      =>  "",
  "legend"        =>  "Registrar Información",
  "tooltip"       =>  "",

  ));

echo '</div>';


?>

<div class="col-md-5 col-md-offset-9">


</div>


  <?php 
    
  $form->closeForm(); 

  }//if(isset($_POST('save'));

    ?>


  </div>



</section>
<!-- /.content -->

<div class="pad margin no-print"></div>
<div class="clearfix"></div>
</div>
  <!-- /.content-wrapper -->
<?php
/* **********************************************************************************************************************************************************
Begin Main Content
*********************************************************************************************************************************************************** */
?>
</section><!-- /MAIN CONTENT -->

<!--- ####################################################### Finish Content ##################################################################### -->
<!--main content end-->

<!--footer start-->
<?php include'inc/userFooter.php'; ?>
<!--footer ends-->

</body>
</html>
<?php
ob_end_flush();
?>

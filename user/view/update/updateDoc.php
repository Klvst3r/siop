<?php
ob_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>

</head>
<!-- ********** Begins Meta and Links ********** -->
<?php include'inc/userHeadDashboard.php'; ?>
<!-- ********** Finish Meta and Links ********** -->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<header class="main-header">
<!-- ********** Begin HEADER ********** -->
<?php 
    //include'chkmenu.php'; 
    include'inc/userHeader.php';
?>
<!-- ********** Finish HEADER ********** -->
<!--header end-->
</header>
<!--sidebar start-->
<aside class="main-sidebar">
<!-- ********** Begin Aside ********** -->
 <?php include'inc/userAside.php'; ?>
<!-- ********** Finish Aside ********** -->
</aside>
<!--sidebar end-->
<!--- ####################################################### Begin Content ##################################################################### -->


<!--main content start-->
<section id="main-content">
<?php
  $_SESSION["user"]["code"] = "true";

/* ********************************************************************************************************************************************************
Begin Main Content
*********************************************************************************************************************************************************** */
/*
  After this will be adquire all document info and its tables
 */
include '../controller/DestinyController.php';

include '../controller/DocumentController.php';

include'../data/Form.php';

include '../sql/Combo.php';

include '../helps/helps.php';

include 'inc/conn.php';

include ("../assets/calendario/calendario.php");

echo '<script language="JavaScript" src="../assets/calendario/javascripts.js"></script>
    <link rel="STYLESHEET" type="text/css" href="../assets/calendario/estilo.css">';




?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      SIOP
      <small>Sistema de Información de Oficialia de Partes</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="/secoduvi/siop/user/"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Actualización</a></li>
      <li class="active"><i class="fa fa-edit"></i> Documentos</li>
    </ol>
  </section>

<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <i class="ion ion-aperture"></i> Actualización de Información
        <small class="pull-right"><?php echo date("d-m-Y") ?></small>
      </h2>
    </div>
    <!-- /.col -->
  </div>


  <div class="row">

  </div>

  <div class="row invoice-info">

<!-- ************************   Begin List Documents   **************************** -->
<?php

  
  if (isset($_POST["update"])){ //If the user wants update data



   /* echo "Updating Document Data <br/>";

    echo "ID: ". $_POST["id"] . "<br/>";
    echo "Folio: ". $_POST["folio_doc"] . "<br/>";
    echo "Dirigido a: " . $_POST["destiny"] . "<br/>";
    echo "Cargo: " . $_POST["destiny_position"] . "<br/>";
    echo "Asunto: " . $_POST["subject"] . "<br/>";
    echo "Enviado por: " . $_POST["send_doc"] . "<br/>";
    echo "Cargo Remitente: " . $_POST["desc_send_position"] . "<br/>";
    echo "Lugar de origen: " . $_POST["origin_doc"] . "<br/>";
    echo "Domicilio: " . $_POST["dir_sender"] . "<br/>";
    echo "Municipio: " . $_POST["municipality"] . "<br/>";
    echo "Telefono: " . $_POST["tel_doc"] . "<br/>";
    echo "Extensión: " . $_POST["ext_doc"] . "<br/>";
    echo "Otro Telefono: " . $_POST["movil_doc"] . "<br/>";
    echo "Correo Electrónico: " . $_POST["email_doc"] . "<br/>";
    echo "Fecha de Documento: " . $_POST["date_doc"] . "<br/>";
    echo "Fecha de Recepción: " . $_POST["date_recep"] . "<br/>";
    echo "Referencia del asunto: " . $_POST["reference"] . "<br/>";
    echo "Tipo de documento: " . $_POST["desc_type_doc"] . "<br/>";
    echo "Status del documento: " . $_POST["desc_status_doc"] . "<br/>";
*/
   
    if($_POST["email_doc"] == ''){
          $_POST["email_doc"] = "Sin correo electrónico";
        }
        
    
    if(isset($_POST["id"]) && isset($_POST["folio_doc"]) &&isset($_POST["destiny"]) && isset($_POST["destiny_position"]) && isset($_POST["subject"]) && isset($_POST["send_doc"]) && isset($_POST["desc_send_position"]) && isset($_POST["origin_doc"]) && isset($_POST["dir_sender"]) && isset($_POST["municipality"]) && isset($_POST["tel_doc"]) && isset($_POST["ext_doc"]) && isset($_POST["movil_doc"]) && isset($_POST["email_doc"]) && isset($_POST["date_doc"]) && isset($_POST["date_recep"]) && isset($_POST["reference"]) && isset($_POST["desc_type_doc"]) && isset($_POST["desc_status_doc"]) 
      ){

    
/*     
    echo "Updating Document Data <br/>";

    echo "ID: ". $_POST["id"] . "<br/>";
    echo "Folio: ". $_POST["folio_doc"] . "<br/>";
    echo "Dirigido a: " . $_POST["destiny"] . "<br/>";
    echo "Cargo: " . $_POST["destiny_position"] . "<br/>";
    echo "Asunto: " . $_POST["subject"] . "<br/>";
    echo "Enviado por: " . $_POST["send_doc"] . "<br/>";
    echo "Cargo Remitente: " . $_POST["desc_send_position"] . "<br/>";
    echo "Lugar de origen: " . $_POST["origin_doc"] . "<br/>";
    echo "Domicilio: " . $_POST["dir_sender"] . "<br/>";
    echo "Municipio: " . $_POST["municipality"] . "<br/>";
    echo "Telefono: " . $_POST["tel_doc"] . "<br/>";
    echo "Extensión: " . $_POST["ext_doc"] . "<br/>";
    echo "Otro Telefono: " . $_POST["movil_doc"] . "<br/>";
    echo "Correo Electrónico: " . $_POST["email_doc"] . "<br/>";
    echo "Fecha de Documento: " . $_POST["date_doc"] . "<br/>";
    echo "Fecha de Recepción: " . $_POST["date_recep"] . "<br/>";
    echo "Referencia del asunto: " . $_POST["reference"] . "<br/>";
    echo "Tipo de documento: " . $_POST["desc_type_doc"] . "<br/>";
    echo "Status del documento: " . $_POST["desc_status_doc"] . "<br/>";
*/
    
      $id                 = validate_field($_POST["id"]);
      $id_destiny         = validate_field($_POST["destiny"]);
      $id_muni            = validate_field($_POST["municipality"]);
      //$desc_muni          = validate_field($_POST["desc_muni"]);
      $id_type_doc        = validate_field($_POST["desc_type_doc"]);
      $id_status_doc      = validate_field($_POST["desc_status_doc"]);
      
      $subject_doc        = validate_field($_POST["subject"]);
      //$subject_doc        = str_replace('"', '"', $subject_doc);
      $subject_doc        = addslashes($subject_doc);

      $desc_send_position = validate_field($_POST["desc_send_position"]);
      $folio_doc          = validate_field($_POST["folio_doc"]);
      $send_doc           = validate_field($_POST["send_doc"]);
      $origin_doc         = validate_field($_POST["origin_doc"]);
      $dir_sender         = validate_field($_POST["dir_sender"]);
      $movil_doc          = validate_field($_POST["movil_doc"]);
      $tel_doc            = validate_field($_POST["tel_doc"]);
      $ext_doc            = validate_field($_POST["ext_doc"]);
      $email_doc          = validate_field($_POST["email_doc"]);
      $date_doc           = reinverse_date($_POST["date_doc"]);
      $date_recep         = reinverse_date($_POST["date_recep"]);
      $reference          = validate_field($_POST["reference"]);
  
 /*
       echo "id: " . $id . "<br/>";
      echo "id_destiny: " . $id_destiny . "<br/>";      
      echo "id_muni: " . $id_muni . "<br/>";    
      echo "id_type_doc: " . $id_type_doc . "<br/>";        
      echo "id_status_doc: " . $id_status_doc . "<br/>";      
      echo "subject_doc: " . $subject_doc . "<br/>";        
      echo "desc_send_position: " . $desc_send_position . "<br/>";
      echo "folio_doc: " . $folio_doc . "<br/>";          
      echo "send_doc: " . $send_doc  . "<br/>";          
      echo "origin_doc: " . $origin_doc  . "<br/>";        
      echo "dir_sender: " . $dir_sender  . "<br/>";        
      echo "movil_doc: " . $movil_doc  . "<br/>";         
      echo "tel_doc: " . $tel_doc  . "<br/>";           
      echo "ext_doc: " . $ext_doc . "<br/>";            
      echo "email_doc: " . $email_doc . "<br/>";         
      echo "date_doc: " . $date_doc . "<br/>";          
      echo "date_recep: " . $date_recep . "<br/>";        
      echo "reference: " . $reference . "<br/>";       
      */
      //echo "Asunto" . $subject_doc;

      DocumentController::updateDoc($id, $id_destiny, $id_muni, $id_type_doc, $id_status_doc, $subject_doc, $desc_send_position, $folio_doc, $send_doc, $origin_doc, $dir_sender, $movil_doc, $tel_doc, $ext_doc, $email_doc, $date_doc, $date_recep, $reference);


    
      header("location:index.php");





    }
    else{
      header("location:index.php");
    }








  }else{
  $form = new Form('updateDoc','POST','', 'form', '', '');

?>

    <div class="col-sm-6">
    <?php
  /* ------------------------------------    Begin First Column  ----------------------------------*/

   $id_doc = $_GET['b'];

    $form -> addField(4, array(
                           "field_name"    =>  "id",
                           "value"   =>  $id_doc
                           ));

    $doc = DocumentController::getDoc($id_doc);

    $form -> addField(1, array(
      "field_name"    =>  "id_doc",
      "class_label"   =>  "",
      "label_field"   =>  "ID",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "disabled",
      "value"         =>  $id_doc,
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "ID del documento",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));


    if(isset($_POST['folio_doc'])){
      $folio_doc = $_POST["folio_doc"];
    }else
    {
      $folio_doc = $doc->getFolio_doc();
    }

    $form -> addField(1, array(
      "field_name"    =>  "folio_doc",
      "class_label"   =>  "",
      "label_field"   =>  "Folio",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  $folio_doc,
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Folio del documento",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));


/*      $queryCount = "SELECT count(*) FROM destiny_docs";

      $runQuery = mysqli_query($link, $queryCount);

      $datos = mysqli_fetch_array($runQuery);
*/
      //$regs = mysqli_num_rows($runQuery); 
      

      //echo "datos" . $datos[0];

      //echo $position = $doc->getId_destiny();

     $position = $doc->getId_destiny();
        $desc_position = $doc->getDesc_destiny();
        $val_position = $position . " - " . $desc_position; 
        $destiny_position = $doc->getDestiny_position(); 


        //echo $position;
        
        $query = "SELECT id_destiny, desc_destiny, destiny_position from destiny_docs EXCEPT where id_destiny = '$position' order by id_destiny";
        //$query = "SELECT id_destiny, desc_destiny, destiny_position from destiny_docs order by id_destiny";
        $combo = new combo($query,"destiny","SelectDestiny", $val_position, "Dirigido a","","","onChange='this.form.submit();'","1");




     /*if(isset($_POST["destiny"])){
        $destiny = $_POST["destiny"];

        $query = "SELECT id_destiny, desc_destiny, destiny_position from destiny_docs order by id_destiny";
        $combo = new combo($query,"destiny","SelectDestiny", $destiny, "Dirigido a","","","onChange='this.form.submit();'","1");

        $data = DestinyController::getDestiny($destiny);
        $position = $data->getDestiny_position();
        $destiny_position = $position;


     }else{
        $position = $doc->getId_destiny();
        $desc_position = $doc->getDesc_destiny();
        $val_position = $position . " - " . $desc_position; 
        $destiny_position = $doc->getDestiny_position(); 

        //echo $position;
        
        $query = "SELECT id_destiny, desc_destiny, destiny_position from destiny_docs EXCEPT where id_destiny <> '$position' order by id_destiny";
        $combo = new combo($query,"destiny","SelectDestiny", $val_position, "Dirigido a","","","onChange='this.form.submit();'","1");
        
     }
*/



      $field = $form -> addField(1, array(
      "field_name"    =>  "destiny_position",
      "class_label"   =>  "",
      "label_field"   =>  "Cargo",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  $destiny_position,
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Cargo",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));


      if(isset($_POST['subject'])){
        $subject = $_POST["subject"];
      }else
      {
        $subject = $doc->getSubject_doc();  
      }
      $form -> addField(9, array(
        "field_name"    =>  "subject",
        "label_field"   =>  "Asunto",
        "readonly"      =>  "",
        "disabled"      =>  "",
        "value"         =>  $subject,
        "rows"          =>  "7",
        "cols"          =>  "",
        "style"         =>  "",
        "js"            =>  "",
        "placeholder"   =>  "Breve descripción del Asunto...",
        "content"       =>  "$subject",
        "required"      =>  ""

      ));

    if(isset($_POST['send_doc'])){
        $send_doc = $_POST["send_doc"];
    }else{
        $send_doc = $doc->getSend_doc();
    }
    $form -> addField(1, array(
      "field_name"    =>  "send_doc",
      "class_label"   =>  "",
      "label_field"   =>  "Enviado por",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  $send_doc,
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Remitente",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));

    if(isset($_POST['desc_send_position'])){
        $desc_send_position = $_POST["desc_send_position"];
    }else{
        $desc_send_position = $doc->getDesc_send_position();
    }
    $form -> addField(1, array(
      "field_name"    =>  "desc_send_position",
      "class_label"   =>  "",
      "label_field"   =>  "Cargo Remitente",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  $desc_send_position,
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Cargo remitente",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));

    if(isset($_POST['origin_doc'])){
        $origin_doc = $_POST["origin_doc"];
    }else{
        $origin_doc = $doc->getOrigin_doc();
    }
    $form -> addField(1, array(
      "field_name"    =>  "origin_doc",
      "class_label"   =>  "",
      "label_field"   =>  "Lugar de origen",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  $origin_doc,
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Origen",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));


    
     if(isset($_POST['dir_sender'])){
        $dir_sender = $_POST["dir_sender"];
     }else{
        $dir_sender = $doc->getDir_sender();
     }
      $form -> addField(9, array(
        "field_name"    =>  "dir_sender",
        "label_field"   =>  "Dirección",
        "readonly"      =>  "",
        "disabled"      =>  "",
        "value"         =>  $dir_sender,
        "rows"          =>  "3",
        "cols"          =>  "",
        "style"         =>  "",
        "js"            =>  "",
        "placeholder"   =>  "Dirección de Origen...",
        "content"       =>  "$dir_sender",
        "required"      =>  ""

      ));


    /* ------------------------------------    End First Column  ----------------------------------*/
    ?>
    </div>
    <!-- /.col -->


    <div class="col-sm-6 invoice-col">
    <?php
    /* ------------------------------------    Second Column  ----------------------------------*/

    if(isset($_POST['municipality'])){
        $id_muni = $_POST["municipality"];
        $desc_muni = $doc->getDesc_muni();
        //$desc_muni = $_POST["desc_muni"];
        $val_muni = $id_muni . " - " . $desc_muni;


       $query = "SELECT id_muni, desc_muni from municipalities EXCEPT where id_muni <> '$id_muni' order by id_muni";
       $combo = new combo($query,"municipality","SelectMuni", $val_muni, "Municipio","","","","1");

       //$desc_muni = $_POST["desc_muni"];

       /*$form -> addField(4, array(
                           "field_name"    =>  "desc_muni",
                           "value"   =>  $desc_muni
                           ));
*/


     }else{
        $id_muni = $doc->getId_muni();
        $desc_muni = $doc->getDesc_muni();
    $val_muni = $id_muni . " - " . $desc_muni;

     $query = "SELECT id_muni, desc_muni from municipalities EXCEPT where id_muni <> '$id_muni' order by id_muni";
     $combo = new combo($query,"municipality","SelectMuni", $val_muni, "Municipio","","","","1");
        
     }


    /* if(isset($_POST["municipality"])){
        //$desc_muni = $_POST["desc_muni"];
        $desc_muni = $id_muni;
     }else{
        $desc_mni = $doc->getDesc_muni();
     }
     $form -> addField(1, array(
      "field_name"    =>  "municipio",
      "class_label"   =>  "",
      "label_field"   =>  "Municipio",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  $desc_muni,
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Municipio",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));*/
       

  /*    if(isset($_POST['folio_doc'])){
      $folio_doc = $_POST["folio_doc"];
    }else
    {
      $folio_doc = $doc->getFolio_doc();
    }*/


    if(isset($_POST['tel_doc'])){
        $tel_doc = $_POST["tel_doc"];
     }else{
        $tel_doc = $doc->getTel_doc();
     }
    $form -> addField(7, array(
                "field_name"    =>  "tel_doc",
                "label_field"   =>  "Telefono",
                "class_label"   =>  "",
                "div_field"     =>  "",
                "input_class"   =>  "col-md-12",
                "readonly"      =>  "",
                "disabled"      =>  "",
                "value"         =>  $tel_doc,
                "maxlength"     =>  "10",
                "size"          =>  "10",
                "style"         =>  "",
                "js"            =>  "",
                "placeholder"   =>  "Numero telefónico...",
                "required"      =>  "required",
                "autofocus"     =>  ""
                ));

    if(isset($_POST['ext_doc'])){
        $ext_doc = $_POST["ext_doc"];
     }else{
        $ext_doc = $doc->getExt_doc();
     }
     $form -> addField(7, array(
                  "field_name"    =>  "ext_doc",
                  "label_field"   =>  "Extensión",
                  "class_label"   =>  "",
                  "div_field"     =>  "",
                  "input_class"   =>  "col-md-12",
                  "readonly"      =>  "",
                  "disabled"      =>  "",
                  "value"         =>  $ext_doc,
                  "maxlength"     =>  "",
                  "size"          =>  "",
                  "style"         =>  "",
                  "js"            =>  "",
                  "placeholder"   =>  "Extensión de oficina...",
                  "required"      =>  "required",
                  "autofocus"     =>  ""
                ));

     if(isset($_POST['movil_doc'])){
        $movil_doc = $_POST["movil_doc"];
     }else{
        $movil_doc = $doc->getMovil_doc();
     }
     $form -> addField(7, array(
                "field_name"    =>  "movil_doc",
                "label_field"   =>  "Otro teléfono",
                "class_label"   =>  "",
                  "div_field"     =>  "",
                "input_class"   =>  "col-md-12",
                "readonly"      =>  "",
                "disabled"      =>  "",
                "value"         =>  $movil_doc,
                "maxlength"     =>  "10",
                "size"          =>  "",
                "style"         =>  "",
                "js"            =>  "",
                "placeholder"   =>  "Otro telefono o número de celular...",
                "required"      =>  "required",
                "autofocus"     =>  ""
                ));


        if(isset($_POST['email_doc'])){
          $email_doc = $_POST["email_doc"];
        }else{
           $email_doc = $doc->getEmail_doc();
        }
        $form -> addField(1, array(
            "field_name"    =>  "email_doc",
            "class_label"   =>  "",
            "label_field"   =>  "Correo Electrónico",
            "div_field"     =>  "",
            "input_class"   =>  "col-md-12",
            "readonly"      =>  "",
            "disabled"      =>  "",
            "value"         =>  $email_doc,
            "maxlength"     =>  "",
            "size"          =>  "",
            "style"         =>  "",
            "js"            =>  "",
            "placeholder"   =>  "Escriba su email o sitio web...",
            "required"      =>  "",
            "autofocus"     =>  ""
            ));

        
        

    echo '<div class="space">&nbsp;</div>';


    

    if(isset($_POST["date_doc"])){
      $date_doc = $_POST["date_doc"];
    }else{
      $date_doc = inverse_date($doc->getDate_doc());
    }     
    escribe_formulario_fecha_vacio(
      "date_doc",
      $date_doc,
      "Fecha de Documento",
      "updateDoc"
      );


    if(isset($_POST["date_recep"])){
      $date_recep = $_POST["date_recep"];
    }else{
      $date_recep = inverse_date($doc->getDate_recep());
    }
    escribe_formulario_fecha_vacio(
      "date_recep",
      $date_recep,
      "Fecha de Recepción",
      "updateDoc"
      );

     if(isset($_POST['reference'])){
          $reference = $_POST["reference"];
      }else{
          $reference = $doc->getReference();
      }
    $form -> addField(1, array(
      "field_name"    =>  "reference",
      "class_label"   =>  "",
      "label_field"   =>  "Referencia asunto",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  $reference,
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Referencia",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));


    $id_type_doc = $doc->getId_type_doc();
    $desc_type_doc = $doc->getDesc_type_doc();
    $val_type_doc = $id_type_doc . " - " . $desc_type_doc;

    $query = "SELECT id_type_doc, desc_type_doc from type_docs EXCEPT where id_type_doc <> '$id_type_doc' order by id_type_doc";
    $combo = new combo($query,"desc_type_doc","SelectTypeDoc", $val_type_doc, "Tipo de Documento","","","","1");

    
    $id_status_doc = $doc->getId_status_doc();
    $desc_status_docs = $doc->getDesc_status_doc();
    $val_status_doc = $id_status_doc . " - " . $desc_status_docs;

    $query = "SELECT id_status_doc, desc_status_docs from status_docs EXCEPT where id_status_doc <> '$id_status_doc' order by id_status_doc";
    $combo = new combo($query,"desc_status_doc","SelectStatusDoc", $val_status_doc, "Estatus Documento","","","","1");


    ?>

    </div>
    <!-- /.col -->

    <div class="space">&nbsp;</div>
    <div class="ln_solid"></div>

    <?php

    echo '<div class="space">&nbsp;</div>
    <div class="ln_solid"></div>



    <div class="space">&nbsp;</div>
    ';

    echo '<div class="col-md-6 ">';

     $form -> addField(3, array(
      "name"          =>  "update",
      "type_button"   =>  "btn btn-primary pull-right",
      "icon"          =>  "fa fa-save",
      "disabled"      =>  "",
      "legend"        =>  "Actualizar Información",
      "tooltip"       =>  "",

      ));

    echo '</div>';


    ?>

    <div class="col-md-5 col-md-offset-9">


    </div>


<?php

  $form->closeForm();
}//else of if(isset(_POST['update']));

?>






<!-- ************************   End List Documents   **************************** -->


  <?php

  //modal


  ?>




  </div>


  	</section>
<!-- /.content -->

<div class="pad margin no-print"></div>
<div class="clearfix"></div>
</div>
  <!-- /.content-wrapper -->
<?php
/* **********************************************************************************************************************************************************
Begin Main Content
*********************************************************************************************************************************************************** */
?>
</section><!-- /MAIN CONTENT -->

<!--- ####################################################### Finish Content ##################################################################### -->
<!--main content end-->

<!--footer start-->
<?php include'inc/userFooter.php'; ?>
<!--footer ends-->

</body>
</html>
<?php
ob_end_flush();
?>

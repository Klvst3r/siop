<?php
ob_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>

</head>
<!-- ********** Begins Meta and Links ********** -->
<?php include'inc/userHeadDashboard.php'; ?>
<!-- ********** Finish Meta and Links ********** -->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<header class="main-header">
<!-- ********** Begin HEADER ********** -->
<?php include'inc/userHeader.php'; ?>
<!-- ********** Finish HEADER ********** -->
<!--header end-->
</header>
<!--sidebar start-->
<aside class="main-sidebar">
<!-- ********** Begin Aside ********** -->
 <?php include'inc/userAside.php'; ?>
<!-- ********** Finish Aside ********** -->
</aside>
<!--sidebar end-->
<!--- ####################################################### Begin Content ##################################################################### -->


<!--main content start-->
<section id="main-content">
<?php
  $_SESSION["user"]["code"] = "true";

/* ********************************************************************************************************************************************************
Begin Main Content
*********************************************************************************************************************************************************** */
/*
  After this will be adquire all document info and its tables
 */
include '../controller/DocumentController.php';

include '../controller/TrackController.php';

include'../data/Form.php';

include '../sql/Combo.php';

include '../helps/helps.php';

include ("../assets/calendario/calendario.php");

echo '<script language="JavaScript" src="../assets/calendario/javascripts.js"></script>
    <link rel="STYLESHEET" type="text/css" href="../assets/calendario/estilo.css">';



?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      SIOP
      <small>Sistema de Información de Oficialia de Partes</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="/secoduvi/siop/user/"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Registro</a></li>
      <li class="active"><i class="fa fa-edit"></i> Documentos</li>
    </ol>
  </section>

<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <i class="ion ion-aperture"></i> Registro de Seguimiento
        <small class="pull-right"><?php echo date("d-m-Y") ?></small>
      </h2>
    </div>
    <!-- /.col -->
  </div>


  <div class="row">

  </div>

  <div class="row invoice-info">
  <!-- <div class="col-xs-12">
      <p class="lead text-center">Registro</p>
    </div> -->
    <!-- /.col -->
    <?php

    

     if (isset($_POST["save"])){ //If the user wants save data

      //In optional cases
        if($_POST["turned_copy"] == ''){
          $_POST["turned_copy"] = "Turnado con copia sin especificar";
        }

      if(isset($_POST["id_doc"]) && isset($_POST["dir"]) && isset($_POST["status_doc"]) &&  isset($_POST["departament"]) && isset($_POST["charTramit"]) && isset($_POST["classif"]) && isset($_POST["id_desc"]) && isset($_POST["folio_doc"]) && isset($_POST["turned_original"]) && isset($_POST["turned_copy"]) && isset($_POST["date_doc"]) && isset($_POST["instructions"])){

/*        echo "<br/>id_doc: " . $id_doc                      =   validate_field($_POST["id_doc"]); //1
        echo "<br/>Dirección: " . $dir                      =   validate_field($_POST["dir"]); //2
        echo "<br/>Status_doc: " . $status_doc              =   validate_field($_POST["status_doc"]);//3
        echo "<br/> Departamento: " . $departamen           =   validate_field($_POST["departament"]);//4                
        echo "<br/>Char Tramit: " . $charTramit             =   validate_field($_POST["charTramit"]);//5
        echo "<br/>Clasification: " . $classif              =   validate_field($_POST["classif"]);//6
        echo "<br/>Descrip Doc: " . $descrip_doc            =   validate_field($_POST["id_desc"]);//7
        echo "<br/>folio: " . $folio_doc                    =   validate_field($_POST["folio"]); //8
        echo "<br/>turned_original: " . $turned_original    =   validate_field($_POST["turned_original"]); //9
        echo "<br/>turned_copy: " . $turned_copy            =   validate_field($_POST["turned_copy"]); //10
        echo "<br/>Fecha_doc: " . $date_doc                 =   format_date($date_doc); //11
        $date_doc                                           =   validate_field($_POST["date_doc"]);//11
        echo "<br/>Instructions: " . $instructions          =   validate_field($_POST["instructions"]); //12*/

        $id_doc          =   validate_field($_POST["id_doc"]);          //1
        $dir             =   validate_field($_POST["dir"]);             //2
        $status_doc      =   validate_field($_POST["status_doc"]);      //3
        $departament     =   validate_field($_POST["departament"]);     //4                
        $charTramit      =   validate_field($_POST["charTramit"]);      //5
        $classif         =   validate_field($_POST["classif"]);         //6
        $descrip_doc     =   validate_field($_POST["id_desc"]);         //7
        $folio_doc       =   validate_field($_POST["folio_doc"]);           //8
        $turned_original =   validate_field($_POST["turned_original"]); //9
        $turned_copy     =   validate_field($_POST["turned_copy"]);     //10
        $date_doc        =   validate_field($_POST["date_doc"]);
        $date_doc        =   format_date($date_doc);                    //11
        $instructions    =   validate_field($_POST["instructions"]);    //12
        

        TrackController::regTrack($id_doc, $dir, $status_doc, $departament, $charTramit, $classif, $descrip_doc, $folio_doc, $turned_original, $turned_copy, $date_doc, $instructions);

        
        $value = '2';

        DocumentController::updateTrack($id_doc, $value);

        header("location:index.php");

      }else{

        header("location:index.php");
      
      }

   
        
    }else{


//$docSelect =  DocumentController::getInfoDoc($d);

$form = new Form('trackDoc','POST','', 'form', '', '');
    

?>

    <div class="col-sm-6">
    <?php

    

  /* ------------------------------------    Begin First Column  ----------------------------------*/      


    if(isset($_GET["b"])){
      $id_doc = $_GET["b"];
    }else{
      $id_doc = '';
    }


    $form -> addField(4, array(
                           "field_name"    =>  "id_doc",
                           "value"   =>  $id_doc
                           ));


    $form -> addField(1, array(
      "field_name"    =>  "id_doc",
      "class_label"   =>  "",
      "label_field"   =>  "ID",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "disabled",
      "value"         =>  $id_doc,
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "ID del documento",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));

   


    if(isset($_GET["c"])){
      $folio_doc = $_GET["c"];
    }else{
      $folio_doc = '';
    }


    $form -> addField(4, array(
                           "field_name"    =>  "folio_doc",
                           "value"   =>  $folio_doc
                           ));


    $form -> addField(1, array(
      "field_name"    =>  "folio_doc",
      "class_label"   =>  "",
      "label_field"   =>  "Folio",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "disabled",
      "value"         =>  $folio_doc,
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Folio del documento",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));

    $doc = DocumentController::getSelect($id_doc);


     $query = "SELECT id_dir, dir_name from directions order by id_dir";
     $combo = new combo($query,"dir","SelectDir", "", "Turnar a","","","","1");

     $query = "SELECT id_dep, departament from departaments order by id_dep";
     $combo = new combo($query,"departament","SelectDep", "", "Departamento","","","","1");


      $field = $form -> addField(1, array(
      "field_name"    =>  "turned_original",
      "class_label"   =>  "",
      "label_field"   =>  "Turnado Original",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  "",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Nombre a quien se turna el documento",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));


      $field = $form -> addField(1, array(
      "field_name"    =>  "turned_copy",
      "class_label"   =>  "",
      "label_field"   =>  "Turnado con copia",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  "",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Copia del documento turnado",
      "required"      =>  "",
      "autofocus"     =>  ""
      ));



      $form -> addField(9, array(
        "field_name"    =>  "instructions",
        "label_field"   =>  "Instrucciones",
        "readonly"      =>  "",
        "disabled"      =>  "",
        "value"         =>  "",
        "rows"          =>  "5",
        "cols"          =>  "",
        "style"         =>  "",
        "js"            =>  "",
        "placeholder"   =>  "Breve descripción de las instrucciones...",
        "content"       =>  "",
        "required"      =>  "required"

      ));



    /* ------------------------------------    End First Column  ----------------------------------*/
    ?>
    </div>
    <!-- /.col -->


    <div class="col-sm-6 invoice-col">
    <?php
    /* ------------------------------------    Second Column  ----------------------------------*/

    
    if(isset($_GET["b"])){
      $id_doc = $_GET["b"];
    }else{
      $id_doc = '';
    }



    if(isset($_GET["b"])){

      $status = $doc->getId_status_doc();
      
      $desc_status = $doc->getDesc_status_doc();

      $val_status = $status . " - " . $desc_status;
      
      $query = "SELECT id_status_doc, desc_status_docs from status_docs EXCEPT where id_status_doc <> '$status'";
      $combo = new combo($query,"status_doc","SelectStatusDoc", $val_status, "Estatus Documento","required","","","1");

    }else{
      $query = "SELECT id_status_doc, desc_status_docs from status_docs order by id_status_doc";
      $combo = new combo($query,"status_doc","SelectStatusDoc", "", "Estatus Documento","","","","1");
    }


    $query = "SELECT id_char, desc_char from char_tramits order by id_char";
    $combo = new combo($query,"charTramit","SelectCharDoc", "", "Carácter del trámite","","","","1");





  if(isset($_GET["b"])){    

    $classif = $doc->getId_classif();
    $desc_classif = $doc->getDesc_classif();

    $val_classif = $classif . " - " . $desc_classif;

    $query = "SELECT id_classif, desc_classif from classifications EXCEPT WHERE id_classif <> '$classif'";
    $combo = new combo($query,"classif","SelectClassif", $val_classif, "Clasificación del documento","required","","","1");

  }else{
    $query = "SELECT id_classif, desc_classif from classifications order by id_classif";
    $combo = new combo($query,"classif","SelectClassif", "", "Clasificación del documento","","","","1");

  }



    $query = "SELECT id_desc, content_desc FROM descriptions ORDER BY id_desc";
    $combo = new combo($query,"id_desc","SelectDescrip", "", "Descripción del documento ","","","","1");


    $date_doc = date("d/m/Y");
    escribe_formulario_fecha_vacio(
      "date_doc",
      $date_doc,
      "Fecha en que se turna documento",
      "trackDoc"
      );


?>

</div>
<!-- /.col -->


<div class="space">&nbsp;</div>
<div class="ln_solid"></div>

<?php

echo '<div class="space">&nbsp;</div>
<div class="ln_solid"></div>



<div class="space">&nbsp;</div>
';

echo '<div class="col-md-6 ">';

 $form -> addField(3, array(
  "name"          =>  "save",
  "type_button"   =>  "btn btn-primary pull-right",
  "icon"          =>  "fa fa-save",
  "disabled"      =>  "",
  "legend"        =>  "Registrar Información",
  "tooltip"       =>  "",

  ));

echo '</div>';


?>

<div class="col-md-5 col-md-offset-9">


</div>


  <?php 
    
  $form->closeForm(); 

  }//if(isset($_POST('save'));

    ?>


  </div>



</section>
<!-- /.content -->

<div class="pad margin no-print"></div>
<div class="clearfix"></div>
</div>
  <!-- /.content-wrapper -->
<?php
/* **********************************************************************************************************************************************************
Begin Main Content
*********************************************************************************************************************************************************** */
?>
</section><!-- /MAIN CONTENT -->

<!--- ####################################################### Finish Content ##################################################################### -->
<!--main content end-->

<!--footer start-->
<?php include'inc/userFooter.php'; ?>
<!--footer ends-->

</body>
</html>
<?php
ob_end_flush();
?>

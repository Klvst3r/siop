﻿<?php

require('mysql_table.php');
include 'inc/conn.php';

if(isset($_POST['query'])){


$sql 	= $_POST['query'];
$first 	= $_POST['first'];
$second = $_POST['second'];

class PDF extends PDF_MySQL_Table{

	function Header()
	{
		$this->Image('../assets/img/logo/SECODUVI.jpg',8,5,60);
		

		//Title
		$this->SetFont('Arial','',10);
		$this->setX(15);
		$this->Cell(0,6,'SECODUVI',0,1,'C');
		$this->Ln(1);
		//$this->SetFont('Arial','',8);
		//$this->Cell(200, 7, utf8_decode('Documentos registrados como solicitudes entre el ' . $first . ' y el' . $second),0, 1,'C');

		$this->Line(20, 28, 195, 28); // 20mm from each edge
		$this->Ln(12);
		//Ensure table header is output
		parent::Header();
	}//function Heade

	function Footer()
	{
		$this->Line(20, 285, 200, 285); // 20mm from each edge
	    // Posición: a 1,5 cm del final
	    $this->SetY(-12);
	    // Arial italic 8
	    $this->SetFont('Arial','I',8);
	    // Número de página
	    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	}//function footer

}//Class PDF



$pdf=new PDF();
$pdf->AliasNbPages();
//$pdf->AddPage();
//First table: put all columns automatically
//$query = "SELECT A.id_task as ID, B.desc_area as Area, C.name as Usuario FROM tasks A, areas B, users C WHERE A.id_area = B.id_area AND A.id_user = C.id_user and A.status = '0' ORDER BY id_task";
//$pdf->Table($link, $query);
$pdf->AddPage();
//Table: specify 3 columns
$title = array(
	//'ID' => 'ID',
	'Folio' => utf8_decode('Folio'),
	'Asunto' => 'Asunto',
	'Referencia' => 'Referencia',
	'Origen' => 'Origen',
	'Respuesta' => '',
	'Observación' => ''
	);





//$pdf->AddCol('ID',6,$title["ID"],'C.name');
$pdf->AddCol('Folio',8,$title["Folio"],'L');
$pdf->AddCol('Asunto',35,$title["Asunto"],'C');
$pdf->AddCol('Referencia',20,$title["Referencia"],'L');
$pdf->AddCol('Origen',35,$title["Origen"],'L');
$pdf->AddCol('Respuesta',45,'','L');
$pdf->AddCol('Observacion',45,'','L');




$prop=array('HeaderColor'=>array(230,230,230),
			'color1'=>array(248,249,250),
			'color2'=>array(255,255,245),
			'padding'=>2);


$query = $sql;


//$this->Cell(200, 7, utf8_decode('Documentos registrados como solicitudes entre el ' . $first . ' y el' . $second),0, 1,'C');    
$pdf->Ln(1);
$pdf->SetFont('Arial','',8);
$pdf->Cell(189, 7, utf8_decode('Documentos registrados como solicitudes entre el ' . $first . ' y el ' . $second),0, 1,'C');    

//$pdf->Table($link, $query, $prop);
$pdf->Table($link2, $query, $prop);
$fileNamePDF="Respuesta"; 
$pdf->Output($fileNamePDF,'I');
}else{
	echo ' 
		<script type="text/javascript">
			
			  window.close();
			
		</script>
	';
}

?>
<?php
session_start(); 

require('mysql_table.php');
include 'inc/conn.php';
include '../helps/helps.php';

/*------ Verify if user exist --------*/
if(isset($_SESSION["user"]) and isset($_SESSION["user"]["url"])){


/*------ Verify if ID exist --------*/
if(isset($_POST['q'])){

	//echo $_POST['q'];
	$folio = $_POST['q'];
	


class PDF extends PDF_MySQL_Table{

	function Header()
	{
		$this->Image('../assets/img/logo/SECODUVI.jpg',16,10,60);
		

		//Title
		$this->SetFont('Arial','B',10);
		$this->setX(180);
		$this->Cell(0,6,'DESPACHO',0,2,'C');
		$this->SetFont('Arial','',8);
		//$this->Line(20, 20, 195, 20); // 20mm from each edge
		$this->Ln(1);
		//Ensure table header is output
		parent::Header();
	}//function Heade


	function Footer()
	{
		$this->SetDrawColor(0,0,0);
		$this->Line(26, 268, 192, 268); // 20mm from each edge
	    // Posición: a 1,5 cm del final
	    $this->SetY(-12);
	    // Arial italic 8
	    $this->SetFont('Arial','',7);
	    // Número de página
	    //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	    $this->Cell(0,10,utf8_decode('Secretaría de Obras Publicas, Desarrollo Urbano y Vivienda - K.M. 1.5 Carretera Tlaxcala - Puebla, Tlaxcala. C.P. 90000'),0,0,'C');
	}//function footer

}//class PDF extends PDF_MySQL_Table



	$query = "SELECT A.id_doc as ID, A.folio_doc as Folio, B.desc_destiny as Destinatario, B.destiny_position as Cargo, A.subject_doc as Asunto, A.send_doc as Remitente, A.origin_doc as Origen, A.date_doc as Elaborado, A.date_recep as Recibido, D.desc_type_doc as Tipo, A.id_destiny as ID_Destinatario, A.id_muni as Muni, C.desc_muni as Municipio, A.desc_send_position as Send_position, A.dir_sender as Domicilio, A.tel_doc as Telefono, A.ext_doc as Extension, A.movil_doc as Movil, A.email_doc as Email, A.reference as Referencia, E.desc_status_docs as Status_doc FROM documents A, destiny_docs B, municipalities C, type_docs D, status_docs E WHERE A.id_muni = C.id_muni and A.id_type_doc = D.id_type_doc and A.id_destiny = B.id_destiny and A.id_status_doc = E.id_status_doc and A.folio_doc = '$folio' AND A.status <> '0'";


	$runQuery = mysqli_query($link2, $query);

	$datos = mysqli_fetch_array($runQuery);

	$regs = mysqli_num_rows($runQuery);	


	if ($regs > 0){

		//----------- Config Page ---------------
		$pdf=new PDF('P','mm','Letter');
		$pdf->AliasNbPages();
		//Thin Line
		//$pdf->SetLineWidth(0.1);
		$pdf->SetLineWidth(0.2);

		$pdf->AddPage();

		$pdf->SetFont('Arial', '', 8);
}else{  //if isset $_POST[q]
	echo ' 
		<script type="text/javascript">
			
			  window.close();
			
		</script>
	';
}

/*----------- End Config Page ---------------*/


/*----------- Fill Data from DB ---------------*/
$pdf->Cell(93, 6, ' ', 0);
$pdf->Cell(8, 6, ' ', 0);


//$id = $datos["ID"];
//$pdf->Cell(47, 6, 'ID:  ' . $id, 0);
$pdf->Cell(45, 6, ' ', 0);
$pdf->SetFont('Arial', 'B', 9);
$pdf->Cell(28, 6, '   Folio:  ', 0, 0);
$pdf->Cell(14, 6, $datos["Folio"], 0, 0,'R');
$pdf->SetFont('Arial', '', 9);
$pdf->Ln(6);

$destinatario = strtoupper( $datos["Destinatario"] );
$cargo = strtoupper( $datos["Cargo"] );
$remitente = strtoupper( $datos["Remitente"] );

$email = $datos["Email"];

$find = strpos($email, "@");
if ($find !== false){ 
	//$es_corre = "Es correo";
	$email = strtolower( $datos["Email"] );

}else{
	$es_correo = "no es correo";
	
	//$acents = array("ó");
	//$email = str_replace("ó", "Ó", $email);
	//$email = preg_replace("ó", "Ó", $email);
	
	
	
	/* $data = explode('Ó', $email);
	$first = $data[0];*/
    
    //$second = $data[1];

    //$email = $first . "Ó" . $second;
    //$email = $first;

    //$email = strtoupper( $email );
    $email = strtolower( $email );
	
    /*$vowels = array("a", "e", "i", "o", "u", "A", "E", "I", "O", "U");
	$acents = array("á", "é", "í", "ó", "ú", "Á", "É", "Í", "Ó", "Ú");
	$email = str_replace($vowels, $acents, $email);*/
}



$cargo_remitente = strtoupper( $datos["Send_position"] );
$origen = strtoupper( $datos["Origen"] );

$recibido = $datos["Recibido"];
$recibido = inverse_date($recibido);
$elaborado = $datos["Elaborado"];
$elaborado = inverse_date($elaborado);

$domicilio = strtoupper( $datos["Domicilio"] );
$referencia = strtoupper( $datos["Referencia"] );
$municipio = strtoupper( $datos["Municipio"] );
$tipo = strtoupper( $datos["Tipo"] );

$asunto = $datos["Asunto"];
$asunto = str_replace('&quot;', '"', $asunto);
$asunto = strtoupper( $asunto );

/*$pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 4, 'color' => array(255, 0, 0)));*/
$pdf->SetLineWidth(0.1);
//$pdf->SetFillColor(0,120,0);
//Establece a color negro el color de borde de los rectangulos
$pdf->SetDrawColor(0,0,0);
//$pdf->SetTextColor(0,0,128);

$pdf->Cell(5, 6, ' ', 0);

$txt_destinatario = 'DIRIGIDO A: ';

$pdf->SetFont('Arial', 'B', 7);
$pdf->Cell(19, 5,  $txt_destinatario, 0);
$pdf->SetFont('Arial', '', 7);
$pdf->Cell(74, 5, utf8_decode( $destinatario ), 1, 0);
$pdf->Cell(7, 6, ' ', 0);
$pdf->SetFont('Arial', 'B', 7);
$pdf->Cell(17, 6, 'MUNICIPIO:  ', 0);
$pdf->SetFont('Arial', '', 7);
$pdf->Cell(72, 5, utf8_decode( $municipio ), 1);
$pdf->Cell(7, 6, ' ', 0);

$pdf->Ln(6);

$pdf->SetFont('Arial', 'B', 7);
$pdf->Cell(5, 5, ' ', 0);
$pdf->Cell(21, 6, 'CARGO:  ', 0);
$pdf->SetFont('Arial', '', 7);
$pdf->Cell(72, 5, utf8_decode( $cargo ), 1);
$pdf->Cell(7, 6, ' ', 0);

$pdf->SetFont('Arial', 'B', 7);
$pdf->Cell(18, 6, 'TELEFONO:  ', 0);
$pdf->SetFont('Arial', '', 7);
$pdf->Cell(71, 5, $datos["Telefono"], 1);
$pdf->SetFont('Arial', 'B', 7);
/*$pdf->Cell(16, 6, utf8_decode( '           EXT.:  ' ), 0);
$pdf->SetFont('Arial', '', 7);
$pdf->Cell(29, 5, utf8_decode( $datos["Extension"] ), 1);*/
$pdf->Ln(6);

$pdf->SetFont('Arial', 'B', 7);
$pdf->Cell(5, 5, ' ', 0);
$pdf->Cell(21, 6, utf8_decode('ENVIADO POR:  ' ), 0);
$pdf->SetFont('Arial', '', 7);
$pdf->Cell(72, 5, utf8_decode( $remitente ), 1);
$pdf->Cell(7, 6, ' ', 0);

$pdf->SetFont('Arial', 'B', 7);
$pdf->Cell(18, 6, utf8_decode( 'EXTENSIÓN:   ' ), 0);
$pdf->SetFont('Arial', '', 7);
$pdf->Cell(71, 5, utf8_decode( $datos["Extension"] ), 1);
$pdf->Ln(6);

$pdf->SetFont('Arial', 'B', 7);
$pdf->Cell(5, 5, ' ', 0);
$pdf->Cell(93, 6, 'CARGO:  ', 0);
$pdf->SetFont('Arial', '', 7);
//$pdf->Cell(80, 11, '', 1);
$pdf->Rect(30, 41, 78 , 11, 'D');



//utf8_decode( $cargo_remitente )
$pdf->Cell(7, 6, ' ', 0);

$pdf->SetFont('Arial', 'B', 7);
$pdf->Cell(25, 6, utf8_decode('OTRO TELEFONO:  ' ), 0);
$pdf->SetFont('Arial', '', 7);
$pdf->Cell(64, 5, utf8_decode( $datos["Movil"] ), 1);
$pdf->Ln(6);

$pdf->SetFont('Arial', 'B', 7);
//$pdf->Cell(26, 6, 'LUGAR DE ORIGEN:  ', 0);
$pdf->SetFont('Arial', '', 7);
$pdf->Cell(93, 5, " ", 0);
$pdf->Cell(7, 6, ' ', 0);

$pdf->SetFont('Arial', 'B', 7);
$pdf->Cell(5, 5, ' ', 0);
$pdf->Cell(12, 6, 'EMAIL:  '  , 0);
$pdf->SetFont('Arial', '', 7);
$pdf->Cell(77, 5,  utf8_decode($email) , 1);
$pdf->Ln(6);

/*rectangulo de lugar de origen*/
$pdf->SetFont('Arial', 'B', 7);
$pdf->Cell(5, 5, ' ', 0);
$pdf->Cell(26, 6, 'LUGAR DE ORIGEN:  ', 0);
$pdf->Rect(41, 53, 67, 11, 'D');
$pdf->SetFont('Arial', '', 7);
/*$pdf->Cell(67, 5, utf8_decode( $origen ), 1);*/

$pdf->Cell(67, 5, '', 0);
$pdf->Cell(7, 6, ' ', 0);

$pdf->SetFont('Arial', 'B', 7);
$pdf->Cell(29, 6, 'FECHA DOCUMENTO:  ', 0);
$pdf->SetFont('Arial', '', 7);
$pdf->Cell(60, 5, $elaborado, 1);

$pdf->Ln(6);

/*RECTANGULO DOMICILIO*/
$pdf->SetFont('Arial', 'B', 7);
$pdf->Cell(16, 6, ' ' , 0);
$pdf->SetFont('Arial', '', 7);
$pdf->Rect(31, 65, 77, 11, 'D');
$pdf->Cell(77, 5, '', 0);
//$pdf->Cell(77, 5, utf8_decode( $domicilio ), 1);



$pdf->Cell(5, 5, ' ', 0);
$pdf->Cell(7, 6, ' ', 0);
$pdf->SetFont('Arial', 'B', 7);

$pdf->Cell(28, 6, utf8_decode('FECHA RECEPCIÓN:  ' ), 0);
$pdf->SetFont('Arial', '', 7);
$pdf->Cell(61, 5, utf8_decode( $recibido), 1);

$pdf->Ln(6);


$pdf->SetFont('Arial', 'B', 7);
$pdf->Cell(5, 5, ' ', 0);
$pdf->Cell(16, 5, 'DOMICILIO:  ' , 0);
$pdf->SetFont('Arial', '', 7);
/*$pdf->Rect(26, 59, 77, 11, 'D');*/
$pdf->Cell(77, 5, '', 0);
/*$pdf->SetFont('Arial', 'B', 7);
$pdf->Cell(16, 6, ' ' , 0);
$pdf->SetFont('Arial', '', 7);
$pdf->Cell(77, 5, ' ', 0);*/
//$pdf->Cell(7, 6, ' ', 1);
//$pdf->Cell(7, 6, ' ', 0);

$pdf->Cell(7, 6, ' ', 0);
$pdf->SetFont('Arial', 'B', 7);
$pdf->Cell(35, 6, 'REFERENCIA DEL ASUNTO:  ', 0);
$pdf->SetFont('Arial', '', 7);
$pdf->Cell(54, 5, utf8_decode( $referencia ), 1);
//$pdf->Ln(6);

$pdf->Ln(6);

$pdf->SetFont('Arial', 'B', 7);
$pdf->Cell(16, 6, ' ' , 0);
$pdf->SetFont('Arial', '', 7);
$pdf->Cell(5, 5, ' ', 0);
$pdf->Cell(77, 5, ' ', 0);
$pdf->Cell(7, 6, ' ', 0);

$pdf->SetFont('Arial', 'B', 7);
$pdf->Cell(30, 6, 'TIPO DE DOCUMENTO:  ', 0);
$pdf->SetFont('Arial', '', 7);
$pdf->Cell(59, 5, utf8_decode( $tipo ), 1);

/*----------- End Fill Data from DB ---------------*/
/*----------- Subject  ---------------*/

$pdf->Ln(6);

/*$lorem = "Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500.Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500.printReg.php las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500.printReg.php las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500.printReg.php Lorem Ipsum ha sido el texto de relleno estándar de Lorem Ipsum ha sido el texto de.
";*/

/*Seccion de los campos de texto en rectangulos*/
$pdf->SetXY(30, 42);
$pdf->multiCell(77, 4, utf8_decode( $cargo_remitente ), 0, "L", 0, 5, 5, 5, 5 );


$pdf->SetXY(41, 54);
$pdf->multiCell(75, 4, utf8_decode( $origen ), 0, "L", 0, 5, 5, 5, 5 );

$pdf->SetXY(32, 66);
$pdf->multiCell(75, 4, utf8_decode( $domicilio ), 0, "L", 0, 5, 5, 5, 5 );

$pdf->SetLineWidth(0.2);
$pdf->SetXY(18, 86);
$pdf->multiCell(184, 5, utf8_decode( $asunto ), 0, "L", 0, 5, 5, 5, 5 );
//$pdf->multiCell(184, 5, utf8_decode($lorem), 0, "L", 0, 5, 5, 5, 5 );



//Establece El color de Fondo "Blanco" para todos los titulos de los rectangulos
$pdf->SetFillColor(255, 255, 255);
//Establece el color de los rectangulos a un color negro
$pdf->SetDrawColor(0,0,0);

//Rectangulo de Asunto
$pdf->Rect(16, 83, 188 , 30, 'D');

//rectangulo blanco del titulo de ASUNTO
$pdf->Rect(21, 80, 18 , 4, 'F');

$pdf->SetXY(20, 79);
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(18, 8, utf8_decode('   ASUNTO: '), 0);
$pdf->SetFont('Arial', '', 7);
/*----------- End Subject  ---------------*/
/*----------- Turn & Observations  -------------*/

$pdf->SetXY(18, 112);

// Rectangulo 1 y 2

$pdf->Rect(16, 118, 99 , 72, 'D');

$pdf->Rect(118, 118, 86 , 72, 'D');


//Rectangulos del titulo de la segunda seccion
$pdf->Rect(18, 116, 21 , 4, 'F');

$pdf->Rect(120, 116, 42, 4, 'F');

$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(95, 12, utf8_decode('  TURNAR A: '), 0);
$pdf->Cell(20, 12, utf8_decode('            CARÁCTER DEL TRÁMITE: '), 0);
$pdf->SetFont('Arial', '', 7);

$pdf->Ln(10);
$pdf->Cell(7, 5, ' ', 0);
$pdf->Cell(90, 5, utf8_decode('     [      ]    DIRECCIÓN ADMINISTRATIVA '), 0);
$pdf->Ln(5);
$pdf->Cell(7, 5, ' ', 0);
$pdf->Cell(90, 5, utf8_decode('     [      ]    DIRECCIÓN DE LICITACIONES, CONCURSOS Y PRECIOS UNITARIOS '), 0);
$pdf->Ln(5);
$pdf->Cell(7, 5, ' ', 0);
$pdf->Cell(90, 5, utf8_decode('     [      ]    DIRECCIÓN DE DESARROLLO URBANO '), 0);
$pdf->Ln(5);
$pdf->Cell(7, 5, ' ', 0);
$pdf->Cell(90, 5, utf8_decode('     [      ]    DIRECCIÓN DE OBRAS PÚBLICAS '), 0);
$pdf->Ln(5);
$pdf->Cell(7, 5, ' ', 0);
$pdf->Cell(90, 5, utf8_decode('     [      ]    DEPARTAMENTO JURÍDICO '), 0);
$pdf->Ln(5);
$pdf->Cell(7, 5, ' ', 0);
$pdf->Cell(90, 5, utf8_decode('     [      ]    DEPARTAMENTO DE PLANEACIÓN '), 0);
$pdf->Ln(5);
$pdf->Cell(7, 5, ' ', 0);
$pdf->Cell(90, 5, utf8_decode('     [      ]    DEPTO. DE AGUA POTABLE, ALCANTARILLADO Y SANEAMIENTO '), 0);
$pdf->Ln(5);
$pdf->Cell(7, 5, ' ', 0);
$pdf->Cell(90, 5, utf8_decode('     [      ]    DEPARTAMENTO DE INFRAESTRUCTURA VIAL '), 0);
$pdf->Ln(5);
$pdf->Cell(7, 5, ' ', 0);
$pdf->Cell(90, 5, utf8_decode('     [      ]    EVENTOS ESPECIALES '), 0);
$pdf->Ln(5);
$pdf->Cell(7, 5, ' ', 0);
$pdf->Cell(90, 5, utf8_decode('     [      ]    MAQUINARIA '), 0);
$pdf->Ln(5);
$pdf->Cell(7, 5, ' ', 0);
$pdf->Cell(90, 5, utf8_decode('     [      ]    DEPARTAMENTO DE DERECHO DE VÍA '), 0);
$pdf->Ln(5);
$pdf->Cell(7, 5, ' ', 0);
$pdf->Cell(90, 5, utf8_decode('     [      ]    DESPACHO '), 0);
$pdf->Ln(5);
$pdf->Cell(7, 5, ' ', 0);
$pdf->Cell(90, 5, utf8_decode('     [      ]    OTROS '), 0);

/*----------- End Turn & Observations  -------------*/
$pdf->SetXY(18, 114);

$pdf->Rect(124, 126, 3 , 3, '');
$pdf->Rect(124, 136, 3 , 3, '');
$pdf->Ln(10);
$pdf->Rect(124, 147, 3 , 3, '');


$pdf->Cell(114, 5, ' ', 0);
$pdf->Cell(5, 5, ' ', 0);
$pdf->Cell(25, 8, 'URGENTE ', 0);
$pdf->Ln(10);
$pdf->Cell(114, 5, ' ', 0);
$pdf->Cell(5, 5, ' ', 0);
$pdf->Cell(25, 8, 'IMPORTANTE ', 0);
$pdf->Ln(10);
$pdf->Cell(114, 5, ' ', 0);
$pdf->Cell(5, 5, ' ', 0);
$pdf->Cell(25, 8, 'NORMAL ', 0);


/*----------- Rectangles 3 & 4 -------------*/



// Rectangulo 3 y 4

$pdf->Rect(16, 196, 99 , 66, 'D');

$pdf->Rect(118, 196, 86 , 66, 'D');

//Rectangulos del titulo de la tercera seccion
$pdf->Rect(18, 194, 29 , 4, 'F');

$pdf->Rect(120, 194, 25 , 4, 'F');


$pdf->SetXY(12, 192);
//$pdf->Ln(5);
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(6, 5, ' ', 0);
$pdf->Cell(95, 9, utf8_decode('  INSTRUCCIONES: '), 0);
$pdf->Cell(94, 9, utf8_decode('            RESOLUCIÓN: '), 0);
$pdf->SetFont('Arial', '', 7);


$pdf->SetXY(1, 1);

//$pdf->Rect(0, 0, 215 , 279, 'D');



//$pdf->Ln(88);
/*----------- End Rectangles 3 & 4 -------------*/
/*----------- Sign and unity -------------*/

/*$pdf->Cell(188, 8, utf8_decode('Unidad de Correspondencia'), 0,0,'C');
$pdf->Ln(5);
$pdf->Cell(188, 8, utf8_decode('Nombre y Firma'), 0,0,'C');
*/
/*----------- End Sign and unity -------------*/


$fileNamePDF="Respuesta"; 

$pdf->Output($fileNamePDF,'I');

}else{  //if isset $_POST[q]
	echo ' 
		<script type="text/javascript">
			
			  window.close();
			
		</script>
	';
}
/*------ End Verify if ID exist --------*/


/*------ End of Verify if user exist  --------*/
   }else {

//Close Session
session_destroy();
session_unset();

//Close the format PDF 
echo ' 
		<script type="text/javascript">
			
			  window.close();
			
		</script>
	';
}

?>
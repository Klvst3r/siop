<?php
ob_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>


<style>
 

.styled-select {
   background: url(http://i62.tinypic.com/15xvbd5.png) no-repeat 96% 0;
   height: 29px;
   overflow: hidden;
   width: 240px;
}

.styled-select select {
   background: transparent;
   border: none;
   font-size: 14px;
   height: 29px;
   padding: 5px; /* If you add too much padding here, the options won't show in IE */
   width: 268px;
}





</style>  

</head>
<!-- ********** Begins Meta and Links ********** -->
<?php include'inc/userHeadDashboard.php'; ?>
<!-- ********** Finish Meta and Links ********** -->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<header class="main-header">
<!-- ********** Begin HEADER ********** -->
<?php include'inc/userHeader.php'; ?>
<!-- ********** Finish HEADER ********** -->
<!--header end-->
</header>
<!--sidebar start-->
<aside class="main-sidebar">
<!-- ********** Begin Aside ********** -->


 <?php 
 //include'inc/userAside.php'; 
include 'chkmenu.php';
 ?>
<!-- ********** Finish Aside ********** -->
</aside>
<!--sidebar end-->
<!--- ####################################################### Begin Content ##################################################################### -->


<!--main content start-->
<section id="main-content">
<?php
  $_SESSION["user"]["code"] = "true";

/* ********************************************************************************************************************************************************
Begin Main Content
*********************************************************************************************************************************************************** */
/*
  After this will be adquire all document info and its tables
 */
include 'sql/DocumentSQL.php';

include '../controller/DocumentController.php';

include '../helps/date.php';


?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      SIOP
      <small>Sistema de Información de Oficialia de Partes</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="/secoduvi/siop/user/"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Consultas</a></li>
      <li class="active"><i class="fa fa-edit"></i> Documentos</li>
    </ol>
  </section>

<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <i class="ion ion-aperture"></i> Registro de Documentos sin seguimiento 
        <small class="pull-right"><?php echo date("d-m-Y") ?></small>
      </h2>
    </div>
    <!-- /.col -->
  </div>


  <div class="row">

  </div>

  <div class="row invoice-info">

<!-- ************************   Begin List Documents   **************************** -->

<div class="col-lg-12">
<?php
  
  if (isset($_GET["m"])){ 

    $m = $_GET["m"];

    $date = date($m);

  }else{

    $date = date("m");

    $m = $date;

  }  


$year = date('Y');
  

?>
<form name="SECODUVI">

    <h1 class="page-header">Listado de documento registrados durante el mes de  <?= month($date) . ' de ' . date('Y') ?> | 


      <span style="color:gray;">Otro Mes</span>
      <select name="theMonth"  class="input-sm"  style="width:200px;" size="1" onChange="go()" id="month">
      <option selected>Seleccione...</option>
      <option value="op.php?a=14&m=1">Enero</option>
      <option value="op.php?a=14&m=2">Febrero</option>
      <option value="op.php?a=14&m=3">Marzo</option>
      <option value="op.php?a=14&m=4">Abril</option>
      <option value="op.php?a=14&m=5">Mayo</option>
      <option value="op.php?a=14&m=6">Junio</option>
      <option value="op.php?a=14&m=7">Julio</option>
      <option value="op.php?a=14&m=8">Agosto</option>
      <option value="op.php?a=14&m=9">Septiembre</option>
      <option value="op.php?a=14&m=10">Octubre</option>
      <option value="op.php?a=14&m=11">Noviembre</option>
      <option value="op.php?a=14&m=12">Diciembre</option>
      </select>

      <script type="text/javascript">
      <!--
      function go(){
      location=
      document.SECODUVI.theMonth.
      options[document.SECODUVI.theMonth.selectedIndex].value
      }
      //-->
      </script>

</h1>

</form>

</div>

      <!-- INICIO CONTENIDO -->
<?php
  
//$query_count = "SELECT COUNT(*) FROM documents where status ='1'";


$query = "SELECT A.id_doc as ID, A.folio_doc as Folio, A.origin_doc as Origen, A.subject_doc as Asunto
  FROM documents A 
WHERE A.status = '1' AND MONTH(date_doc) = '$m' AND YEAR(date_doc) = '$year' ORDER BY folio_doc";


//$params = "";

//echo $query;
echo '<h4> Mes: ' . month($date) . '</h4>';

//echo $query;
DocumentSQL::tableDocReg($query);


//echo "Mes: "  . $m;

?>
<!-- ************************   End List Documents   **************************** -->


  <?php

  //modal
  //include 'modalGeneral.php';

  ?>




  </div>


  	</section>
<!-- /.content -->

<div class="pad margin no-print"></div>
<div class="clearfix"></div>
</div>
  <!-- /.content-wrapper -->
<?php
/* **********************************************************************************************************************************************************
Begin Main Content
*********************************************************************************************************************************************************** */
?>
</section><!-- /MAIN CONTENT -->

<!--- ####################################################### Finish Content ##################################################################### -->
<!--main content end-->

<!--footer start-->
<?php include'inc/userFooter.php'; ?>
<!--footer ends-->

</body>
</html>
<?php
ob_end_flush();
?>

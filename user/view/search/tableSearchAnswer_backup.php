<?php
ob_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>

</head>
<!-- ********** Begins Meta and Links ********** -->
<?php include'inc/userHeadDashboard.php'; ?>
<!-- ********** Finish Meta and Links ********** -->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<header class="main-header">
<!-- ********** Begin HEADER ********** -->
<?php include'inc/userHeader.php'; ?>
<!-- ********** Finish HEADER ********** -->
<!--header end-->
</header>
<!--sidebar start-->
<aside class="main-sidebar">
<!-- ********** Begin Aside ********** -->
 <?php include 'chkmenu.php'; ?>
<!-- ********** Finish Aside ********** -->
</aside>
<!--sidebar end-->
<!--- ####################################################### Begin Content ##################################################################### -->


<!--main content start-->
<section id="main-content">
<?php
  $_SESSION["user"]["code"] = "true";

/* ********************************************************************************************************************************************************
Begin Main Content
*********************************************************************************************************************************************************** */
/*
  After this will be adquire all document info and its tables
 */

include '../sql/Combo.php';
include '../helps/helps.php';
include 'sql/AnswerSQL.php';

include ("../assets/calendario/calendario.php");


echo '<script language="JavaScript" src="../assets/calendario/javascripts.js"></script>
    <link rel="STYLESHEET" type="text/css" href="../assets/calendario/estilo.css">';


 


?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      SIOP
      <small>Sistema de Información de Oficialia de Partes</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="/secoduvi/siop/user/"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Consultas</a></li>
      <li class="active"><i class="fa fa-edit"></i> Seguimiento</li>
    </ol>
  </section>

<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <i class="ion ion-aperture"></i> Documentos con Respuesta
        <small class="pull-right"><?php echo date("d-m-Y") ?></small>
      </h2>
    </div>
    <!-- /.col -->
  </div>


  <div class="row">

  </div>

  <div class="row invoice-info">

<!-- ************************   Begin List Documents   **************************** -->

<div class="col-lg-12">
    
</div>
<!-- INICIO CONTENIDO -->


<?php
if (isset($_POST["create"])){ //If the user wants find a doc by folio

  if($_POST["firstDate"] != '' and $_POST["secondDate"]!= ''){




  //echo "Generando Hoja...";
  
  $first           = validate_field($_POST["firstDate"]);
  $first           = format_date($first);
  $firstInverse    = inverse_date($first);

  $second          = validate_field($_POST["secondDate"]);
  $second          = format_date($second);
  $secondInverse   = inverse_date($second);

  $dir = $_POST["direction"];

  $classif = '2';
  



  echo '
  <div class="col-lg-12">
    <div class="page-header">Periodo de documentos registrados como solicitudes entre el periodo de ' . $firstInverse . ' y ' . $secondInverse . '<br/><strong>' . AnswerSQL::selectDir($dir) . '</strong> |  <a href="op.php?a=66" class="btn btn-success"><i class="fa fa-search"></i> Nuevo filtro</a></div>
</div>
  ';


  $query_count = "SELECT COUNT(*) FROM documents_turned where (date_turned BETWEEN '$first' AND '$second') and id_dir = '$dir' and id_classif = '$classif' ";
  
  //$folio_doc = $_POST["search"];
  /* 
  $query = "SELECT A.id_turn as ID, A.folio_turned as Folio, A.id_dir as Dirección, B.dir_name as Name, A.id_classif as Tipo, A.doc_origin as Origen FROM documents_turned A, directions B   
    WHERE (A.date_turned BETWEEN '$first' and '$second') and A.id_dir = '$dir' and A.id_dir = B.id_dir ORDER BY id_turn";
  */
 
 $query = "SELECT A.id_turn as ID, A.folio_turned as Folio, B.subject_doc as Asunto, B.reference as Referencia, A.doc_origin as Origen, A.answer as Respuesta, A.observation as Observacion FROM documents_turned A, documents B 
    WHERE (A.date_turned BETWEEN '$first' and '$second') and A.id_dir = '$dir' and A.id_doc = B.id_doc and A.id_classif = '$classif' ORDER BY id_turn";
 
  $params = "";
  
  //echo $query;
  

  AnswerSQL::getTableAnswerQuery_pag($query_count, $query, $params);

  //include 'modalSearch.php';
  
  include '../data/FormNew.php';

  $form = new FormNew('genAnswer','POST','op.php?a=73', 'form', '', 'blank');
  
  echo '<div class="col-md-12 ">';

    $form -> addField(4, array(
                           "field_name"    =>  "first",
                           "value"   =>  $firstInverse
                           ));

    $form -> addField(4, array(
                           "field_name"    =>  "second",
                           "value"   =>  $secondInverse
                           ));
   

    $form -> addField(4, array(
                           "field_name"    =>  "query",
                           "value"   =>  $query
                           ));

     $form -> addField(3, array(
      "name"          =>  "generate",
      "type_button"   =>  "btn btn-danger pull-right",
      "icon"          =>  "fa fa-file",
      "disabled"      =>  "",
      "legend"        =>  "Generar Documento PDF",
      

      ));




    echo '</div>';


   $form->closeForm(); 

  }//if(isset($_POST["firstDate"]) and isset($_POST["secondDate"])){
  else{
    echo '
    <center>
  <div class="col-lg-4 col-offset-2">
    
  </div>
  
  <div class="col-lg-6">
  </div>
    <h3 class="page-header"> <a href="op.php?a=64" class="btn btn-success"><i class="fa fa-search"></i> Nueva busqueda por Periodo</a></h3>
</div>
</center>
  ';
    echo 'No ha seleccionado un periodo de fechas correctamente';
  }



}else{
?>
<div class=" ">
  <section class="content-header">
          <h3><i class="fa fa-search text-blue"></i> Filtro para generar hoja de respuestas.</h3>

          <p>
            Para generar la hoja de respuestas seleccione los parametros correspondientes.
          </p>
  </section>
<?php

include '../data/Form.php';
$form = new Form('searchAnswer','POST','', 'form', '', 'search-form');
//echo '<form class="search-form">';
?>
<center>

<table border="0" width="50%">
          <tr>
            <td width="40%" align="center">
            <?php

            $firstDate = date("d/m/Y");
            escribe_formulario_fecha_vacio(
              "firstDate",
              $firstDate,
              "Fecha Inicial",
              "searchAnswer"
              ); 

            ?>
            </td><td>&nbsp;</td><td width="50%" align="center">
            <?php

            $secondDate = date("d/m/Y");
            escribe_formulario_fecha_vacio(
              "secondDate",
              $secondDate,
              "Fecha Final",
              "searchAnswer"
              );
                    
            ?>
          </td>
          </tr>
          <tr>
            <td colspan="3">
            <?php

            $query = "SELECT id_dir, dir_name from directions order by id_dir";
            $combo = new combo($query,"direction","SelectDir", "", "Dirección","","","","1");
            ?>

            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <td></td>
          <td width="30%" align="center">
          <?php
          $form -> addField(3, array(
          "name"          =>  "create",
          "type_button"   =>  "btn btn-primary pull-right",
          "icon"          =>  "fa fa-file",
          "disabled"      =>  "",
          "legend"        =>  "Generar Hoja de respuesta",
          "tooltip"       =>  "",

          ));
          ?>
        
          </td>
          <td></td>
        
      </table>


</center>
<?php
$form->closeForm(); 
//echo '</form>';
}//else of if(isset(_POST['track']));
?>            

</div>
<!-- /.error-content -->





<div class="col-lg-12">
  <div class="space">&nbsp;</div>
  <div class="space">&nbsp;</div>
  <div class="space">&nbsp;</div>
</div>

      
<!-- ************************   End List Documents   **************************** -->


  <?php

  
  
  ?>




  </div>


    </section>
<!-- /.content -->

<div class="pad margin no-print"></div>
<div class="clearfix"></div>
</div>
  <!-- /.content-wrapper -->
<?php
/* **********************************************************************************************************************************************************
Begin Main Content
*********************************************************************************************************************************************************** */
?>
</section><!-- /MAIN CONTENT -->

<!--- ####################################################### Finish Content ##################################################################### -->
<!--main content end-->

<!--footer start-->
<?php include'inc/userFooter.php'; ?>
<!--footer ends-->



</body>
</html>
<?php
ob_end_flush();
?>

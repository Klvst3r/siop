<?php
ob_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>

<!-- ********** Begins Meta and Links ********** -->
<?php include'inc/userHeadDashboard.php'; ?>
<!-- ********** Finish Meta and Links ********** -->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<header class="main-header">
<!-- ********** Begin HEADER ********** -->
<?php include'inc/userHeader.php'; ?>
<!-- ********** Finish HEADER ********** -->
<!--header end-->
</header>
<!--sidebar start-->
<aside class="main-sidebar">
<!-- ********** Begin Aside ********** -->
 <?php include 'chkmenu.php'; ?>
<!-- ********** Finish Aside ********** -->
</aside>
<!--sidebar end-->
<!--- ############################ Begin Content ####################################### -->


<!--main content start-->
<section id="main-content">
<?php
  $_SESSION["user"]["code"] = "true";

/* ***********************************************************************************************************************
Begin Main Content
************************************************************************************************************************** */
/*
  After this will be adquire all document info and its tables
 */

include '../data/Form.php';
include '../helps/helps.php';
include 'sql/DocumentSQL.php';
/*include '../sql/Combo.php';
*/

include ("../assets/calendario/calendario.php");

echo '<script language="JavaScript" src="../assets/calendario/javascripts.js"></script>
    <link rel="STYLESHEET" type="text/css" href="../assets/calendario/estilo.css">';


?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      SIOP
      <small>Sistema de Información de Oficialia de Partes</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="/secoduvi/siop/user/"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Consultas</a></li>
      <li class="active"><i class="fa fa-edit"></i> Documentos</li>
    </ol>
  </section>

<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <i class="ion ion-aperture"></i> Busqueda de Documento por Periodo
        <small class="pull-right"><?php echo date("d-m-Y") ?></small>
      </h2>
    </div>
    <!-- /.col -->
  </div>


  <div class="row">

  </div>

  <div class="row invoice-info">

<!-- ************************   Begin List Documents   **************************** -->



<!-- INICIO CONTENIDO -->


<?php



//if(isset($_POST["searchPeriod"])){ //If the user wants find a doc by folio
if(isset($_POST["search"])){


      $search = $_POST["search"];

  //if($_POST["firstDate"] != '' and $_POST["secondDate"]!= ''){
      


      $fisrtDate    = $_POST["firstDate"];

      $secondDate   = $_POST["secondDate"];




  ///$doc = DocMuniSQL::getMunicipality($id_muni);
  
  echo "Periodo: ".$_POST["search"];

  echo '
  <div class="col-lg-12">
    <h3 class="page-header">Periodo de recepción de documentos: Del ' . $fisrtDate . ' al ' . $secondDate . '  |  <a href="op.php?a=64" class="btn btn-success"><i class="fa fa-search"></i> Nueva busqueda por Periodo</a></h3>
</div>
  ';

  $first    = reinverse_date($fisrtDate);
  $second   = reinverse_date($secondDate); 

  

  $query_count = "SELECT COUNT(*) FROM documents where (date_recep BETWEEN '$first' AND '$second') and status ='1'";

  //echo $query_count;
  
  /*$query = "SELECT A.id_doc as ID, A.folio_doc as Folio, B.desc_destiny as Destinatario, B.destiny_position as Cargo, A.subject_doc as Asunto,
    A.send_doc as Remitente, A.origin_doc as Origen, A.date_doc as Elaborado, A.date_recep as Recibido,
    D.desc_type_doc as Tipo, A.id_destiny as Destinatario, A.id_muni as Muni, C.desc_muni as Municipio, A.desc_send_position as Send_position,
    A.dir_sender as Domicilio, A.tel_doc as Telefono, A.ext_doc as Extension, A.movil_doc as Movil, A.email_doc as Email,
    A.reference as Referencia, E.desc_status_docs as Status_doc
    FROM documents A, destiny_docs B, municipalities C, type_docs D, status_docs E
    WHERE A.id_muni = C.id_muni and A.id_type_doc = D.id_type_doc and A.id_destiny = B.id_destiny and A.id_status_doc = E.id_status_doc 
    and A.status = '1' and (date_recep BETWEEN '$first' AND '$second')  ORDER BY id_doc";*/

  $query = "SELECT A.id_doc as ID, A.folio_doc as Folio, B.desc_destiny as Destinatario, B.destiny_position as Cargo, A.subject_doc as Asunto,
    A.send_doc as Remitente, A.origin_doc as Origen, A.date_doc as Elaborado, A.date_recep as Recibido,
    D.desc_type_doc as Tipo, A.id_destiny as Destinatario, A.id_muni as Muni, C.desc_muni as Municipio, A.desc_send_position as Send_position,
    A.dir_sender as Domicilio, A.tel_doc as Telefono, A.ext_doc as Extension, A.movil_doc as Movil, A.email_doc as Email,
    A.reference as Referencia, E.desc_status_docs as Status_doc
    FROM documents A, destiny_docs B, municipalities C, type_docs D, status_docs E
    WHERE A.id_muni = C.id_muni and A.id_type_doc = D.id_type_doc and A.id_destiny = B.id_destiny and A.id_status_doc = E.id_status_doc 
    and (date_recep BETWEEN '$first' AND '$second')  ORDER BY id_doc";

  //echo $query;  
 
  $params = $search;
  

  DocumentSQL::getTableDocPeriod_Pag($query_count, $query, $params);

  include 'modalSearch.php';

  //}//if(isset($_POST["firstDate"]) and isset($_POST["secondDate"])){
  /*else{
    echo '
    <center>
  <div class="col-lg-4 col-offset-2">
    
  </div>
  
  <div class="col-lg-6">
  </div>
    <h3 class="page-header"> <a href="op.php?a=64" class="btn btn-success"><i class="fa fa-search"></i> Nueva busqueda por Periodo</a></h3>
</div>
</center>
  ';
    echo 'No ha seleccionado un periodo de fechas correctamente';
  }*/


}else{




?>
<div class="">
  <section class="content-header">
          <h3><i class="fa fa-search text-blue"></i> Seleccione el periodo.</h3>

          <p>
            Para poder localizar el documento y visualizar, solicitamos seleccione un periodo de fechas.            
          </p>
  </section>
<?php



//$form = new Form('searchDoc','POST','op.php?a=68&pos=1&b=search_2018-1-1_2018-7-19', 'form', '', 'search-form');
$form = new Form('searchDoc','POST','op.php?a=68', 'form', '', 'search-form');
//echo '<form class="search-form">';
?>
<center>
<div class="input-group" style="width:500px;">
<?php

echo '<div class="col-sm-6">';
escribe_formulario_fecha_vacio(
      "firstDate",
      "",
      "Fecha Incial",
      "searchDoc"
      );
echo '</div>';

echo '<div class="col-sm-6">';
escribe_formulario_fecha_vacio(
      "secondDate",
      "",
      "Fecha Final",
      "searchDoc"
      );
echo '</div>';



?>
<div class="col-md-5 col-md-offset-9">
     <div class="pad margin no-print"></div>
<div class="clearfix"></div> 

</div>
<?php



echo '<div class="col-md-6 ">';

     $form -> addField(4, array(
                           "field_name"    =>  "search",
                           "value"   =>  "search"
                           ));

      

     $form -> addField(3, array(
      "name"          =>  "searchPeriod",
      "type_button"   =>  "btn btn-primary pull-right",
      "icon"          =>  "fa fa-search",
      "disabled"      =>  "",
      "legend"        =>  "Buscar Documento",
      "tooltip"       =>  "",

      ));
?>  


</div>
  <!-- /.input-group -->
</center>

<?php
$form->closeForm(); 
//echo '</form>';
}//else of if(isset(_POST['searchPeriod']));
?>            

</div>
<!-- /.error-content -->

<div class="col-lg-12">
  <div class="space">&nbsp;</div>
  <div class="space">&nbsp;</div>
  <div class="space">&nbsp;</div>
</div>

      
<!-- ************************   End List Documents   **************************** -->


  <?php

  
  
  ?>




  </div>


    </section>
<!-- /.content -->

<div class="pad margin no-print"></div>
<div class="clearfix"></div>
</div>
  <!-- /.content-wrapper -->
<?php
/* **********************************************************************************************************
Begin Main Content
************************************************************************************************************* */
?>
</section><!-- /MAIN CONTENT -->

<!--- ################################### Finish Content ######################################################## -->
<!--main content end-->

<!--footer start-->
<?php include'inc/userFooter.php'; ?>
<!--footer ends-->



</body>
</html>
<?php
ob_end_flush();
?>

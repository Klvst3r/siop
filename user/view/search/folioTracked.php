<?php
ob_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>

</head>
<!-- ********** Begins Meta and Links ********** -->
<?php include'inc/userHeadDashboard.php'; ?>
<!-- ********** Finish Meta and Links ********** -->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<header class="main-header">
<!-- ********** Begin HEADER ********** -->
<?php include'inc/userHeader.php'; ?>
<!-- ********** Finish HEADER ********** -->
<!--header end-->
</header>
<!--sidebar start-->
<aside class="main-sidebar">
<!-- ********** Begin Aside ********** -->
 <?php include'chkmenu.php'; ?>
<!-- ********** Finish Aside ********** -->
</aside>
<!--sidebar end-->
<!--- ####################################################### Begin Content ##################################################################### -->


<!--main content start-->
<section id="main-content">
<?php
  $_SESSION["user"]["code"] = "true";

/* ********************************************************************************************************************************************************
Begin Main Content
*********************************************************************************************************************************************************** */
/*
  After this will be adquire all document info and its tables
 */
include '../data/Form.php';
include 'sql/DocumentSQL.php';
include '../controller/DocumentController.php';

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      SIOP
      <small>Sistema de Información de Oficialia de Partes</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="/secoduvi/siop/user/"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Consultas</a></li>
      <li class="active"><i class="fa fa-edit"></i> Documentos con Seguimiento</li>
    </ol>
  </section>

<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <i class="ion ion-aperture"></i> Busqueda de Documento con Seguimiento por Folio
        <small class="pull-right"><?php echo date("d-m-Y") ?></small>
      </h2>
    </div>
    <!-- /.col -->
  </div>


  <div class="row">

  </div>

  <div class="row invoice-info">

<!-- ************************   Begin List Documents   **************************** -->

<div class="col-lg-12">
    <!-- <h1 class="page-header">Documento a Localizar | <a href="op.php?a=1" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo usuario</a>
    </h1> -->
</div>
<!-- INICIO CONTENIDO -->


<?php
if (isset($_POST["submit"])){ //If the user wants find a doc by folio

$form = new Form('updateDoc','POST','', 'form', '', '');

?>

  <div class="col-sm-6">
    <?php
  /* ------------------------------------    Begin First Column  ----------------------------------*/

   $id = $_POST['search'];

    $form -> addField(4, array(
                           "field_name"    =>  "id",
                           "value"   =>  $id
                           ));

    $doc = DocumentController::getDoc($id);

    $form -> addField(1, array(
      "field_name"    =>  "id_doc",
      "class_label"   =>  "",
      "label_field"   =>  "ID",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "disabled",
      "value"         =>  $id,
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "ID del documento",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));


    /*if(isset($_POST['folio_doc'])){
      $folio_doc = $_POST["folio_doc"];
    }else
    {
      $folio_doc = $doc->getFolio_doc();
    }*/

    $form -> addField(1, array(
      "field_name"    =>  "folio_doc",
      "class_label"   =>  "",
      "label_field"   =>  "Folio",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  "folio_doc",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Folio del documento",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));



     


     /* if(isset($_POST['subject'])){
        $subject = $_POST["subject"];
      }else
      {
        $subject = $doc->getSubject_doc();  
      }*/
      $form -> addField(9, array(
        "field_name"    =>  "subject",
        "label_field"   =>  "Asunto",
        "readonly"      =>  "",
        "disabled"      =>  "",
        "value"         =>  "subject",
        "rows"          =>  "7",
        "cols"          =>  "",
        "style"         =>  "",
        "js"            =>  "",
        "placeholder"   =>  "Breve descripción del Asunto...",
        "content"       =>  "subject",
        "required"      =>  ""

      ));


       $field = $form -> addField(1, array(
      "field_name"    =>  "destiny_position",
      "class_label"   =>  "",
      "label_field"   =>  "Origen",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  "origen",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Origen",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));

   /* if(isset($_POST['send_doc'])){
        $send_doc = $_POST["send_doc"];
    }else{
        $send_doc = $doc->getSend_doc();
    }*/
    $form -> addField(1, array(
      "field_name"    =>  "send_doc",
      "class_label"   =>  "",
      "label_field"   =>  "Respuesta",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  "respuesta",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Respuesta",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));

    /*if(isset($_POST['desc_send_position'])){
        $desc_send_position = $_POST["desc_send_position"];
    }else{
        $desc_send_position = $doc->getDesc_send_position();
    }*/
    $form -> addField(1, array(
      "field_name"    =>  "observation",
      "class_label"   =>  "",
      "label_field"   =>  "Observacion",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  "observacion",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Observacion",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));

    /*if(isset($_POST['origin_doc'])){
        $origin_doc = $_POST["origin_doc"];
    }else{
        $origin_doc = $doc->getOrigin_doc();
    }*/
    $form -> addField(1, array(
      "field_name"    =>  "destinatario",
      "class_label"   =>  "",
      "label_field"   =>  "Destinatario",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  "destinatario",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Destinatario",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));


    
   /*  if(isset($_POST['dir_sender'])){
        $dir_sender = $_POST["dir_sender"];
     }else{
        $dir_sender = $doc->getDir_sender();
     }*/
      $form -> addField(9, array(
        "field_name"    =>  "cargo",
        "label_field"   =>  "Cargo",
        "readonly"      =>  "",
        "disabled"      =>  "",
        "value"         =>  "cargo",
        "rows"          =>  "3",
        "cols"          =>  "",
        "style"         =>  "",
        "js"            =>  "",
        "placeholder"   =>  "Cargo",
        "content"       =>  "Cargo",
        "required"      =>  ""

      ));

      $form -> addField(7, array(
                "field_name"    =>  "remitente",
                "label_field"   =>  "Remitente",
                "class_label"   =>  "",
                "div_field"     =>  "",
                "input_class"   =>  "col-md-12",
                "readonly"      =>  "",
                "disabled"      =>  "",
                "value"         =>  "remitente",
                "maxlength"     =>  "10",
                "size"          =>  "10",
                "style"         =>  "",
                "js"            =>  "",
                "placeholder"   =>  "Remitente",
                "required"      =>  "required",
                "autofocus"     =>  ""
                ));

    /*if(isset($_POST['ext_doc'])){
        $ext_doc = $_POST["ext_doc"];
     }else{
        $ext_doc = $doc->getExt_doc();
     }*/
     $form -> addField(7, array(
                  "field_name"    =>  "elaborado",
                  "label_field"   =>  "Elaborado",
                  "class_label"   =>  "",
                  "div_field"     =>  "",
                  "input_class"   =>  "col-md-12",
                  "readonly"      =>  "",
                  "disabled"      =>  "",
                  "value"         =>  "elaborado",
                  "maxlength"     =>  "",
                  "size"          =>  "",
                  "style"         =>  "",
                  "js"            =>  "",
                  "placeholder"   =>  "Elaborado",
                  "required"      =>  "required",
                  "autofocus"     =>  ""
                ));


          

    /* ------------------------------------    End First Column  ----------------------------------*/
    ?>
    </div>
    <!-- /.col -->


    <div class="col-sm-6 invoice-col">
    <?php
    /* ------------------------------------    Second Column  ----------------------------------*/

    $form -> addField(7, array(
                  "field_name"    =>  "recibido",
                  "label_field"   =>  "Recibido",
                  "class_label"   =>  "",
                  "div_field"     =>  "",
                  "input_class"   =>  "col-md-12",
                  "readonly"      =>  "",
                  "disabled"      =>  "",
                  "value"         =>  "recibido",
                  "maxlength"     =>  "",
                  "size"          =>  "",
                  "style"         =>  "",
                  "js"            =>  "",
                  "placeholder"   =>  "Recibido",
                  "required"      =>  "required",
                  "autofocus"     =>  ""
                ));

    

    /* if(isset($_POST["municipality"])){
        //$desc_muni = $_POST["desc_muni"];
        $desc_muni = $id_muni;
     }else{
        $desc_mni = $doc->getDesc_muni();
     }
     $form -> addField(1, array(
      "field_name"    =>  "municipio",
      "class_label"   =>  "",
      "label_field"   =>  "Municipio",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  $desc_muni,
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Municipio",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));*/
       

  /*    if(isset($_POST['folio_doc'])){
      $folio_doc = $_POST["folio_doc"];
    }else
    {
      $folio_doc = $doc->getFolio_doc();
    }*/


   /* if(isset($_POST['tel_doc'])){
        $tel_doc = $_POST["tel_doc"];
     }else{
        $tel_doc = $doc->getTel_doc();
     }*/
    

   /*  if(isset($_POST['movil_doc'])){
        $movil_doc = $_POST["movil_doc"];
     }else{
        $movil_doc = $doc->getMovil_doc();
     }*/
   

     /*   if(isset($_POST['email_doc'])){
          $email_doc = $_POST["email_doc"];
        }else{
           $email_doc = $doc->getEmail_doc();
        }*/
        $form -> addField(1, array(
            "field_name"    =>  "municipio",
            "class_label"   =>  "",
            "label_field"   =>  "Municipio",
            "div_field"     =>  "",
            "input_class"   =>  "col-md-12",
            "readonly"      =>  "",
            "disabled"      =>  "",
            "value"         =>  "municipio",
            "maxlength"     =>  "",
            "size"          =>  "",
            "style"         =>  "",
            "js"            =>  "",
            "placeholder"   =>  "Municipio",
            "required"      =>  "",
            "autofocus"     =>  ""
            ));

        
        

    //echo '<div class="space">&nbsp;</div>';


    

   /* if(isset($_POST["date_doc"])){
      $date_doc = $_POST["date_doc"];
    }else{
      $date_doc = inverse_date($doc->getDate_doc());
    }     
    escribe_formulario_fecha_vacio(
      "date_doc",
      $date_doc,
      "Fecha de Documento",
      "updateDoc"
      );*/


   /* if(isset($_POST["date_recep"])){
      $date_recep = $_POST["date_recep"];
    }else{
      $date_recep = inverse_date($doc->getDate_recep());
    }
    escribe_formulario_fecha_vacio(
      "date_recep",
      $date_recep,
      "Fecha de Recepción",
      "updateDoc"
      );*/

   /*  if(isset($_POST['reference'])){
          $reference = $_POST["reference"];
      }else{
          $reference = $doc->getReference();
      }*/
     $form -> addField(9, array(
        "field_name"    =>  "puesto destinatario",
        "label_field"   =>  "Puesto Destinatario",
        "readonly"      =>  "",
        "disabled"      =>  "",
        "value"         =>  "puesto dstinatario",
        "rows"          =>  "3",
        "cols"          =>  "",
        "style"         =>  "",
        "js"            =>  "",
        "placeholder"   =>  "Puesto destinatario",
        "content"       =>  "Puesto destinatario",
        "required"      =>  ""

      ));

    $form -> addField(1, array(
      "field_name"    =>  "domicilio_destinatario",
      "class_label"   =>  "",
      "label_field"   =>  "Domicilio Destinatario",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  "Domicilio destinatario",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "Domicilio Destinatario",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));

    $form -> addField(1, array(
      "field_name"    =>  "tel_destitanario",
      "class_label"   =>  "",
      "label_field"   =>  "tel Destinatario",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  "tel destinatario",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "tel Destinatario",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));

     $form -> addField(1, array(
      "field_name"    =>  "ext_destitanario",
      "class_label"   =>  "",
      "label_field"   =>  "ext Destinatario",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  "ext destinatario",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "ext Destinatario",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));

      $form -> addField(1, array(
      "field_name"    =>  "movil_destitanario",
      "class_label"   =>  "",
      "label_field"   =>  "movil Destinatario",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  "movil destinatario",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "movil Destinatario",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));

       $form -> addField(1, array(
      "field_name"    =>  "email_destitanario",
      "class_label"   =>  "",
      "label_field"   =>  "email Destinatario",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  "email destinatario",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "email Destinatario",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));

       $form -> addField(1, array(
      "field_name"    =>  "referencia",
      "class_label"   =>  "",
      "label_field"   =>  "Referencia",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  "Referencia",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "referencia",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));

     $form -> addField(1, array(
      "field_name"    =>  "tipo",
      "class_label"   =>  "",
      "label_field"   =>  "tipo",
      "div_field"     =>  "",
      "input_class"   =>  "col-md-12",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  "tipo",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "",
      "js"            =>  "",
      "placeholder"   =>  "tipo",
      "required"      =>  "required",
      "autofocus"     =>  ""
      ));

    /*$id_type_doc = $doc->getId_type_doc();
    $desc_type_doc = $doc->getDesc_type_doc();
    $val_type_doc = $id_type_doc . " - " . $desc_type_doc;

    $query = "SELECT id_type_doc, desc_type_doc from type_docs EXCEPT where id_type_doc <> '$id_type_doc' order by id_type_doc";
    $combo = new combo($query,"desc_type_doc","SelectTypeDoc", $val_type_doc, "Tipo de Documento","","","","1");

    
    $id_status_doc = $doc->getId_status_doc();
    $desc_status_docs = $doc->getDesc_status_doc();
    $val_status_doc = $id_status_doc . " - " . $desc_status_docs;

    $query = "SELECT id_status_doc, desc_status_docs from status_docs EXCEPT where id_status_doc <> '$id_status_doc' order by id_status_doc";
    $combo = new combo($query,"desc_status_doc","SelectStatusDoc", $val_status_doc, "Estatus Documento","","","","1");
*/

    ?>

    </div>
    <!-- /.col -->

    <div class="space">&nbsp;</div>
    <div class="ln_solid"></div>

    <?php

    echo '<div class="space">&nbsp;</div>
    <div class="ln_solid"></div>



    <div class="space">&nbsp;</div>
    ';

    echo '<div class="col-md-6 ">';

     $form -> addField(3, array(
      "name"          =>  "update",
      "type_button"   =>  "btn btn-primary pull-right",
      "icon"          =>  "fa fa-save",
      "disabled"      =>  "",
      "legend"        =>  "Actualizar Información",
      "tooltip"       =>  "",

      ));

    echo '</div>';


    ?>

    <div class="col-md-5 col-md-offset-9">


    </div>


<?php

  $form->closeForm();
}//else of if(isset(_POST['update']));
else{
?>
<div class=" ">
  <section class="content-header">
          <h3><i class="fa fa-search text-blue"></i> Busque su Documento.</h3>

          <p>
            Para poder localizar el documento y visualizar, solicitamos escriba su folio del documento.            
          </p>
  </section>
<?php
$form = new Form('searchDoc','POST','', 'form', '', 'search-form');
//echo '<form class="search-form">';
?>
<center>
<div class="input-group" style="width:500px;">
<?php
$form -> addField(1, array(
      "field_name"    =>  "search",
      "class_label"   =>  "",
      "label_field"   =>  "",
      "div_field"     =>  "",
      "input_class"   =>  "form-control",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  "",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "width:450px;",
      "js"            =>  "",
      "placeholder"   =>  "Buscar",
      "required"      =>  "",
      "autofocus"     =>  ""
      ));
//echo '<input type="text" name="search" class="form-control" placeholder="Buscar">';
?>
<div class="input-group-btn">
<?php
$form -> addField(3, array(
      "name"          =>  "submit",
      "type_button"   =>  "btn btn-success btn-flat",
      "icon"          =>  "fa fa-search",
      "disabled"      =>  "",
      "legend"        =>  "",
      "tooltip"       =>  "Buscar",

      ));
//echo ' <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i></button>';
?>  

</div>
</div>
  <!-- /.input-group -->
</center>
<?php
$form->closeForm(); 
//echo '</form>';
}//else of if(isset(_POST['submit']));
?>            

</div>
<!-- /.error-content -->

<div class="col-lg-12">
  <div class="space">&nbsp;</div>
  <div class="space">&nbsp;</div>
  <div class="space">&nbsp;</div>
</div>

      
<!-- ************************   End List Documents   **************************** -->


  <?php

  
  
  ?>




  </div>


    </section>
<!-- /.content -->

<div class="pad margin no-print"></div>
<div class="clearfix"></div>
</div>
  <!-- /.content-wrapper -->
<?php
/* **********************************************************************************************************************************************************
Begin Main Content
*********************************************************************************************************************************************************** */
?>
</section><!-- /MAIN CONTENT -->

<!--- ####################################################### Finish Content ##################################################################### -->
<!--main content end-->

<!--footer start-->
<?php include'inc/userFooter.php'; ?>
<!--footer ends-->



</body>
</html>
<?php
ob_end_flush();
?>

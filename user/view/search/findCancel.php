<?php
ob_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>

</head>
<!-- ********** Begins Meta and Links ********** -->
<?php include'inc/userHeadDashboard.php'; ?>
<!-- ********** Finish Meta and Links ********** -->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<header class="main-header">
<!-- ********** Begin HEADER ********** -->
<?php include'inc/userHeader.php'; ?>
<!-- ********** Finish HEADER ********** -->
<!--header end-->
</header>
<!--sidebar start-->
<aside class="main-sidebar">
<!-- ********** Begin Aside ********** -->
 <?php include'chkmenu.php'; ?>
<!-- ********** Finish Aside ********** -->
</aside>
<!--sidebar end-->
<!--- ####################################################### Begin Content ##################################################################### -->


<!--main content start-->
<section id="main-content">
<?php
  $_SESSION["user"]["code"] = "true";

/* ********************************************************************************************************************************************************
Begin Main Content
*********************************************************************************************************************************************************** */
/*
  After this will be adquire all document info and its tables
 */
include '../data/Form.php';
include 'sql/DocumentSQL.php';

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      SIOP
      <small>Sistema de Información de Oficialia de Partes</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="/secoduvi/siop/user/"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Consultas</a></li>
      <li class="active"><i class="fa fa-edit"></i> Documentos</li>
    </ol>
  </section>

<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <i class="ion ion-aperture"></i> Busqueda de Documento a eliminar
        <small class="pull-right"><?php echo date("d-m-Y") ?></small>
      </h2>
    </div>
    <!-- /.col -->
  </div>


  <div class="row">

  </div>

  <div class="row invoice-info">

<!-- ************************   Begin List Documents   **************************** -->

<div class="col-lg-12">
    <!-- <h1 class="page-header">Documento a Localizar | <a href="op.php?a=1" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo usuario</a>
    </h1> -->
</div>
<!-- INICIO CONTENIDO -->


<?php
if (isset($_POST["submit"])){ //If the user wants find a doc by folio

  //echo "Buscando...";
  
  $folio_doc = $_POST["search"];
  /*$query = "SELECT A.id_doc as ID, A.folio_doc as Folio, B.desc_destiny as Destinatario, B.destiny_position as Cargo, A.subject_doc as Asunto,
    A.send_doc as Remitente, A.origin_doc as Origen, A.date_doc as Elaborado, A.date_recep as Recibido,
    D.desc_type_doc as Tipo, A.id_destiny as Destinatario, A.id_muni as Muni, C.desc_muni as Municipio, A.desc_send_position as Send_position,
    A.dir_sender as Domicilio, A.tel_doc as Telefono, A.ext_doc as Extension, A.movil_doc as Movil, A.email_doc as Email,
    A.reference as Referencia, E.desc_status_docs as Status_doc
    FROM documents A, destiny_docs B, municipalities C, type_docs D, status_docs E
    WHERE A.id_muni = C.id_muni and A.id_type_doc = D.id_type_doc and A.id_destiny = B.id_destiny and A.id_status_doc = E.id_status_doc 
    and A.status = '1' and A.id_doc = '$id_doc' ORDER BY id_doc";*/

    

  $query = "SELECT A.id_doc as ID, A.folio_doc as Folio, B.desc_destiny as Destinatario, B.destiny_position as Cargo, A.subject_doc as Asunto,
    A.send_doc as Remitente, A.origin_doc as Origen, A.date_doc as Elaborado, A.date_recep as Recibido,
    D.desc_type_doc as Tipo, A.id_destiny as Destinatario, A.id_muni as Muni, C.desc_muni as Municipio, A.desc_send_position as Send_position,
    A.dir_sender as Domicilio, A.tel_doc as Telefono, A.ext_doc as Extension, A.movil_doc as Movil, A.email_doc as Email,
    A.reference as Referencia, E.desc_status_docs as Status_doc
    FROM documents A, destiny_docs B, municipalities C, type_docs D, status_docs E
    WHERE A.id_muni = C.id_muni and A.id_type_doc = D.id_type_doc and A.id_destiny = B.id_destiny and A.id_status_doc = E.id_status_doc 
    and A.folio_doc = '$folio_doc' and A.status = '1' ORDER BY id_doc";

  
  /*$query = "SELECT A.id_doc as ID, A.folio_doc as Folio, B.desc_destiny as Destinatario, A.subject_doc as Asunto,
    A.send_doc as Remitente, A.origin_doc as Origen     
    FROM documents A, destiny_docs B, municipalities C, type_docs D, status_docs E
    WHERE A.id_destiny = B.id_destiny  and A.id_doc = '$id_doc' 
    and A.status = '1' ORDER BY id_doc";*/

  /*$query = "SELECT A.id_doc as ID, A.folio_doc as Folio, B.desc_destiny as Destinatario, A.subject_doc as Asunto,
    A.send_doc as Remitente, A.origin_doc as Origen     
    FROM documents A, destiny_docs B  WHERE A.id_destiny = B.id_destiny  and A.id_doc = '$id_doc' 
    and A.status = '1' ORDER BY id_doc";*/

  
  //echo $query;
  

  DocumentSQL::getTableDoc($query);

  include 'modalSearch.php';

}else{
?>
<div class=" ">
  <section class="content-header">
          <h3><i class="fa fa-search text-blue"></i> Busque su Documento.</h3>

          <p>
            Para poder localizar el documengto a cancelar, solicitamos escriba el folio del documento.            
          </p>
  </section>
<?php
$form = new Form('searchDoc','POST','', 'form', '', 'search-form');
//echo '<form class="search-form">';
?>
<center>
<div class="input-group" style="width:500px;">
<?php
$form -> addField(1, array(
      "field_name"    =>  "search",
      "class_label"   =>  "",
      "label_field"   =>  "",
      "div_field"     =>  "",
      "input_class"   =>  "form-control",
      "readonly"      =>  "",
      "disabled"      =>  "",
      "value"         =>  "",
      "maxlength"     =>  "",
      "size"          =>  "",
      "style"         =>  "width:450px;",
      "js"            =>  "",
      "placeholder"   =>  "Buscar",
      "required"      =>  "",
      "autofocus"     =>  ""
      ));
//echo '<input type="text" name="search" class="form-control" placeholder="Buscar">';
?>
<div class="input-group-btn">
<?php
$form -> addField(3, array(
      "name"          =>  "submit",
      "type_button"   =>  "btn btn-warning btn-flat",
      "icon"          =>  "fa fa-search",
      "disabled"      =>  "",
      "legend"        =>  "",
      "tooltip"       =>  "Buscar",

      ));
//echo ' <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i></button>';
?>  

</div>
</div>
  <!-- /.input-group -->
</center>
<?php
$form->closeForm(); 
//echo '</form>';
}//else of if(isset(_POST['submit']));
?>            

</div>
<!-- /.error-content -->

<div class="col-lg-12">
  <div class="space">&nbsp;</div>
  <div class="space">&nbsp;</div>
  <div class="space">&nbsp;</div>
</div>

      
<!-- ************************   End List Documents   **************************** -->


  <?php

  
  
  ?>




  </div>


    </section>
<!-- /.content -->

<div class="pad margin no-print"></div>
<div class="clearfix"></div>
</div>
  <!-- /.content-wrapper -->
<?php
/* **********************************************************************************************************************************************************
Begin Main Content
*********************************************************************************************************************************************************** */
?>
</section><!-- /MAIN CONTENT -->

<!--- ####################################################### Finish Content ##################################################################### -->
<!--main content end-->

<!--footer start-->
<?php include'inc/userFooter.php'; ?>
<!--footer ends-->



</body>
</html>
<?php
ob_end_flush();
?>

<?php
ob_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>

</head>
<!-- ********** Begins Meta and Links ********** -->
<?php include'inc/userHeadDashboard.php'; ?>
<!-- ********** Finish Meta and Links ********** -->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<header class="main-header">
<!-- ********** Begin HEADER ********** -->
<?php include'inc/userHeader.php'; ?>
<!-- ********** Finish HEADER ********** -->
<!--header end-->
</header>
<!--sidebar start-->
<aside class="main-sidebar">
<!-- ********** Begin Aside ********** -->
 <?php include 'chkmenu.php'; ?>
<!-- ********** Finish Aside ********** -->
</aside>
<!--sidebar end-->
<!--- #################################### Begin Content ####################################################### -->


<!--main content start-->
<section id="main-content">
<?php
  $_SESSION["user"]["code"] = "true";

/* *********************************************************************************************************************
Begin Main Content
************************************************************************************************************************ */
/*
  After this will be adquire all document info and its tables
 */
include '../controller/DocumentController.php';

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      SIOP
      <small>Sistema de Información de Oficialia de Partes</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="/secoduvi/siop/user/"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Consultas</a></li>
      <li class="active"><i class="fa fa-edit"></i> Documentos</li>
    </ol>
  </section>

<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <i class="ion ion-aperture"></i> Eliminar Documento
        <small class="pull-right"><?php echo date("d-m-Y") ?></small>
      </h2>
    </div>
    <!-- /.col -->
  </div>


  <div class="row">

  </div>

  <div class="row invoice-info">

<!-- ************************   Begin List Documents   **************************** -->

<div class="col-lg-12">
    <!-- <h1 class="page-header">Documento a Localizar | <a href="op.php?a=1" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo usuario</a>
    </h1> -->
</div>
<!-- INICIO CONTENIDO -->


<?php

  if($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["b"])){
  $id = $_GET["b"];   
  
  DocumentController::delbyFolio($id);
  
  //echo $id;
  
  header("location:index.php");

 }else{
  //In case fail register the user send again to the list of Privileges
  //last 13 -> 14
  header("location:index.php");
  //echo"<meta HTTP-EQUIV='Refresh' CONTENT='2; URL=action.php?a=13'<head/>";
  }

?>            

</div>
<!-- /.error-content -->

<div class="col-lg-12">
  <div class="space">&nbsp;</div>
  <div class="space">&nbsp;</div>
  <div class="space">&nbsp;</div>
</div>

      
<!-- ************************   End List Documents   **************************** -->


  <?php

  
  
  ?>




  </div>


    </section>
<!-- /.content -->

<div class="pad margin no-print"></div>
<div class="clearfix"></div>
</div>
  <!-- /.content-wrapper -->
<?php
/* *******************************************************************************************************************************
Begin Main Content
********************************************************************************************************************************** */
?>
</section><!-- /MAIN CONTENT -->

<!--- ##################################### Finish Content ################################################# -->
<!--main content end-->

<!--footer start-->
<?php include'inc/userFooter.php'; ?>
<!--footer ends-->



</body>
</html>
<?php
ob_end_flush();
?>

 <?php
/*include '../controller/UserController.php';
include '../controller/OtherPrivController.php';
include '../controller/OtherHistoryController.php';*/

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      SIOP 
      <small>Sistema de Información de Oficialia de Partes</small>
    </h1>
    <ol class="breadcrumb">
      <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
      <!-- <li><a href="#">Usuarios</a></li> -->
      <li class="active"><i class="fa fa-dashboard"></i> Home</li>
    </ol>
  </section>


<?php
/*$live  = UserController::getUserActive(1);

$down = UserController::getUserDeleted(0);

$priv = OtherPrivController::getNumPriv();

$history = OtherHistoryController::getNumAccess();*/

include '../sql/Docs.php';
include '../sql/Porcent.php';
include '../sql/Track.php';

?> 

<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <i class="ion ion-aperture"></i> Dashboard
        <small class="pull-right"><?php echo date("d-m-Y") ?></small>
      </h2>
    </div>
    <!-- /.col -->
  </div>
  

  <div class="row">
    
  </div>

  

      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <?php

              $query = "SELECT COUNT(A.id_doc) FROM documents A where status = '1' ";
              

              ?>
              <h3><?php $dataQuery = new Docs($query);

                ?></h3>

              <p>Documentos Recibidos </p>
            </div>
            <div class="icon">
              <i class="ion ion-compose"></i>
            </div>
            <a href="op.php?a=14" class="small-box-footer">Listado Registrados <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <?php

              //$query = "SELECT COUNT(*) FROM documents_turned";
              $query = " SELECT COUNT(A.id_doc) FROM documents_turned A, documents B WHERE B.status = '1' AND B.id_doc = A.id_doc ";

              ?>

              <h3><?php $dataQuery = new Docs($query);  ?></h3>
              

              <p>Seguimientos</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-checkbox-outline"></i>
            </div>
            <a href="op.php?a=15" class="small-box-footer">Listar turnados <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
               <?php

              /*$queryA = "SELECT COUNT(*) FROM documents where status ='1'";
              $queryB = "SELECT COUNT(*) FROM documents_turned";*/

              $queryA = " SELECT COUNT(A.id_doc) FROM documents A where status = '1' ";
              $queryB = " SELECT COUNT(A.id_doc) FROM documents_turned A, documents B WHERE B.status = '1' AND B.id_doc = A.id_doc ";

              ?>
              <h3><?php $dataQuery = new Porcent($queryA, $queryB);  ?><sup style="font-size: 20px"> %</sup></h3>

              <p>Avance</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">Más Información <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <?php

              $query = "SELECT id_doc FROM documents WHERE NOT id_doc IN (SELECT id_doc FROM documents_turned) AND status ='1'";
              

              ?>
              <h3><?php $dataQuery = new Track($query);  ?></h3>

              <p>Documentos sin Turnar</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="op.php?a=17" class="small-box-footer">Detalles <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
       <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
               <?php

              //$query = "SELECT * FROM `documents_turned` WHERE answer <> '' ";
              $query = "SELECT B.id_doc FROM `documents` A, `documents_turned` B WHERE answer <> '' and A.id_doc = B.id_doc and A.status ='1' ";

              ?>
              <h3><?php $dataQuery = new Track($query);  ?></h3>

              <p>Turnados con respuesta</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-desktop"></i>
            </div>
            
            <a href="op.php?a=18" class="small-box-footer">Más Información <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <?php

              //$query = "SELECT * FROM `documents_turned` WHERE answer = '' ";
              
              $query = "SELECT B.id_doc FROM `documents` A, `documents_turned` B WHERE B.answer = '' and A.id_doc = B.id_doc and A.status ='1'";
              

              ?>
              <h3><?php $dataQuery = new Track($query);  ?></h3>

              <p>Turnados sin respuesta</p>
            </div>
            <div class="icon">
              <i class="ion 

              ion-android-folder

"></i>
            </div>
            <a href="op.php?a=19" class="small-box-footer">Detalles <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
 
</section>
<!-- /.content -->
  
<div class="pad margin no-print">

  
</div>



<div class="clearfix"></div>
</div>
  <!-- /.content-wrapper -->
 <?php

$path = $_SESSION["user"]["url"];

 ?>

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
        	<?php
        	/*<img src="../assets/img/users/<?php echo $sideUser->getUser_photo(); ?>" class="img-circle" width="60"/>*/
        	?>
          <img src="../assets/img/users/<?php echo $user->getUser_photo() ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
        <?php
        /*<h5 class="centered"><?php echo $sideUser->getName(); ?></h5>*/
       	?>
          <p><?php echo $user->getName() ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> <?= $user->getDesc_priv() ?></a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <div>
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"> - SIOP | MAIN NAVIGATION - </li>
        <li>
        <?php
        /* <a href="localhost/secoduvi/siop/admin/" > */
        ?>
          <?php 
          
          echo '<a href="' . $path . '/user/">'; 
          ?>
            <i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
            <span class="pull-right-container">
              
            </span>
          </a>
       
        </li>
      </ul>
      <div>
      <!-- search form -->
      <form action="op.php?a=71" method="post" class="sidebar-form" target='blank'>
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Buscar Folio...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <ul class="sidebar-menu" data-widget="tree">
        
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Consultas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="op.php?a=11"><i class="fa fa-desktop"></i> General sin seguimiento</a></li>
            <li><a href="op.php?a=62"><i class="fa fa-clipboard"></i> Buscar Folio</a></li>
            <li><a href="op.php?a=63"><i class="fa fa-building"></i> Buscar Municipio</a></li>
            <li><a href="op.php?a=64"><i class="fa fa-calendar"></i> Buscar Periodo</a></li>
            <li><a href="op.php?a=67"><i class="fa fa-search"></i> Buscar Asunto</a></li>
          </ul>
        </li>
        


        <!-- <li><a href="#"><i class="fa fa-book"></i> <span>Documentation</span></a></li> -->
        <li class="header">Etiquetas</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Importante</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Advertencia</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Información</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
<?php
include '../data/OtherConnect.php';
//include '../model/Document.php';

class AnswerSQL extends OtherConnect {

	protected static $cnx;

	private static function getConection(){

		self::$cnx = Reconnect::connection();
		
	}//getConection

	private static function disconnect(){
		//This close the conection in PDO
		self::$cnx = null;
	}// disconnect

	

public static function getTableAnswer($sql){
		$query = $sql;
		self::getConection();
		$result = self::$cnx->prepare($query);
		$result->execute();
		$rows = $result->rowCount();
		$cols = $result->columnCount();
		if($rows > 0){
			echo '<table class="table table-striped table-hover">';
			echo '<thead>
			<tr>';
			foreach(range(0, $result->columnCount() - 1) as $column_index){
				$meta[] = $result->getColumnMeta($column_index);
			}
			for ($i=0; $i < $cols; $i++){
				//echo '<th>' . $meta[$i]["name"] . '</td>';
				if($i > 8){
					//echo '<th style="visibility: hidden">' . $meta[$i]["name"] . '</td>';	
					
					//echo '<th style="display:none">' . $meta[$i]["name"] . '</td>';	
				}else{
					echo '<th>' . $meta[$i]["name"]  . '</td>';	
				}
			}
			echo '<th> &nbsp; </th>';
			echo '<th><center>  Acción</center></th>';

			echo '</tr>
			</thead>
			<tbody>';

			for($i = 0; $i < $rows; $i++){
				$data = $result->fetch();
				echo '<tr>';
				for($j = 0; $j < $cols; $j++){
					//echo '<td>' . $data[$j] .'</td>';
					
					if($j > 8){
							
							//echo '<td style="visibility: hidden"><span id="'. $meta[$j]["name"] . $i .  '">' . $data[$j] . '</span></td>';
							echo '<td style="display:none"><span id="'. $meta[$j]["name"] . $i .  '">' . $data[$j] . '</span></td>';
						}else{
							echo '<td><span id="'. $meta[$j]["name"] . $i .  '">' . $data[$j] . '</span></td>';
						}
				}//for $j
				//echo '<td>' . $action . '</td>';
				//$id = $data[$i];
				$id = $data["ID"];
				$folio = $data["Folio"];
				$detail = '<td><button type="button" class="btn btn-default detail" value="' . $i . '"><span class="glyphicon glyphicon-search"></span> Detalle</button>';
				//$delete = '<a class="btn btn-danger delete" href="op.php?a=21&b='. $id .'"><span class="glyphicon glyphicon-edit"></span> Eliminar</a></td>';

				//$delete = '<button class="btn btn-danger delete" onclick="confirmar(\'op.php?a=91&b=' . $id . '\')">Eliminar</button>';

				$track = '<a class="btn btn-success" href="op.php?a=42&b='. $id .'&c=' . $folio . '"><span class="glyphicon glyphicon-edit"></span> Turnar</a>';
            	//$delete = '<button class="btn btn-danger btn-sm" onclick="confirmar(action.php?a=6)">Eliminar</button>';

            	//$delete = '<button class="btn btn-danger btn-sm" onclick="confirmar(\'action.php?a=25&b=' . $id . '\')">Eliminar</button>';
				echo '<td>' . $detail . ' ' . $track . '</td>';
				echo '</tr>';
			}
			echo "</tbody></table><br/>";
		}else{

			echo '
			<section class="content-header">
			<center>
				<p>No hay Documento en la Base de Datos con los parametros seleccionados. </p>
				<p> Verifique los parametros del filtro. </p>
			</center>
			</section>				
			';

			echo"<meta HTTP-EQUIV='Refresh' CONTENT='3; URL=index.php'<head/>";
		}
		//free memory
		self::disconnect();
}//function getTableDocTrack


public static function getTableAnswer_pag($query_count, $sql, $action){

		/**
		 * Vars to build pagination
		 */
		$order="id_user ASC";
		$id_url = $_GET["a"];
		$url = basename($_SERVER ["PHP_SELF"]) . '?a=' . $id_url;
		$limit_end = 5;
		//safe the value of the actual position
		if(!isset($_GET["pos"])){
			$ini = 1;
		}else{
			$ini = $_GET["pos"];

		}
		$init = ($ini-1) * $limit_end;

		/*-------------------------*/

		$query = $sql;
		$query .= " LIMIT $init, $limit_end";//add limit init and limit end to the query
		


		self::getConection();

		/**
		 * -Execute a new query only to count the fields in the table of BD
		 */
		$count = self::$cnx->prepare($query_count);

		$count->execute();

		$rows_count = $count->fetch();

		$counted = $rows_count[0];
		/*---------------------------------------------------------*/


		//Query of data 
		$result = self::$cnx->prepare($query);

		$result->execute();

		$rows = $result->rowCount();
		$cols = $result->columnCount();

		/**
		 * Calculate pages
		 */
		$total = ceil($counted/$limit_end);

  		/*-------------------------------------*/

		if($rows > 0){

			echo '<table class="table table-striped table-hover">';
			echo '<thead>
			<tr>';
			foreach(range(0, $result->columnCount() - 1) as $column_index){
				$meta[] = $result->getColumnMeta($column_index);
			}

			for ($i=0; $i < $cols; $i++){

				if($i > 7){
					//echo '<th style="visibility: hidden">' . $meta[$i]["name"] . '</td>';	
					
					//echo '<th style="display:none">' . $meta[$i]["name"] . '</td>';	
				}else{
					echo '<th>' . $meta[$i]["name"]  . '</td>';	
				}
			}       		
				echo '<th> &nbsp; </th>';
				echo '<th><center>  Acción</center></th>';
			
			/*if( $i <8 ){
			}else {
				echo '<th style="visibility: hidden">Acción ' . $i . '</th>';	
			}*/


			echo '</tr>
			</thead>
			<tbody>';
		

			for($i = 0; $i < $rows; $i++){
				$data = $result->fetch();
				echo '<tr>';

				for($j = 0; $j < $cols; $j++){

					//echo '<td>' . $data[$j] .'</td>';
					// <span id="firstname1">Klvst3r</span>
					
						if($j > 7){
							
							//echo '<td style="visibility: hidden"><span id="'. $meta[$j]["name"] . $i .  '">' . $data[$j] . '</span></td>';
							echo '<td style="display:none"><span id="'. $meta[$j]["name"] . $i .  '">' . $data[$j] . '</span></td>';
						}else{
							echo '<td><span id="'. $meta[$j]["name"] . $i .  '">' . $data[$j] . '</span></td>';
						}
					

				}
				//echo '<td>' . $action . '</td>';
				//$id = $data[$i];
				$id = $data["ID"];
				$folio = $data["Folio"];

				//$detail = '<td><button type="button" class="btn btn-success detail" value="' . $i . '">Detalle <span class="glyphicon glyphicon-triangle-right"></span></button>';
				$track = '<td><a class="btn btn-success" href="op.php?a=42&b='. $id .'&c=' . $folio . '"><span class="glyphicon glyphicon-edit"></span> Turnar</a></td>';

				echo '<td>' . $track . '</td>';
				echo '</tr>';


			}
			echo "</tbody></table>";

			  /*
			   * numeration of records [important]
			   */ 
				  //echo "<div class='pagination'>";
				  echo '<ul class="pagination">';
				  /****************************************/
				  if(($ini - 1) == 0)
				  {
				    echo '<li class="disabled"><a href="#">&laquo;</a></li>';
				  }
				  else
				  {
				    echo '<li><a href="'.$url.'&pos='.($ini-1).'"><b>&laquo;</b></a></li>';
				  }
				  /****************************************/
				  for($k=1; $k <= $total; $k++)
				  {
				    if($ini == $k)
				    {
				      echo '<li class="active"><a href="#""><b>'.$k.'</b></a></li>';
				      
				    }
				    else
				    {
				    	
				      echo "<li><a href='$url&pos=$k'>".$k."</a></li>";
				    }
				  }
				  /****************************************/
				  if($ini == $total)
				  {
				    echo '<li class="disabled"><a href="#">&raquo;</a></li>';
				  }
				  else
				  {
				  	echo '<li><a href="'.$url.'&pos='.($ini+1).'"><b>&raquo;</b></a></li>';

				  }
				  echo "</ul>";
				  //echo "</div>";
				  /*******************END*******************/
				 /*
			   * End numeration of records [important]
			   */ 


		}else{
			echo "No hay Documento en la Base de Datos con el folio introducido. ";
		}

		//free memory
		self::disconnect();


}//function getTableTracking_Pag

public static function getTableAnswerQuery_pag($query_count, $sql, $action){

		/**
		 * Vars to build pagination
		 */
		$order="id_user ASC";
		$id_url = $_GET["a"];
		$url = basename($_SERVER ["PHP_SELF"]) . '?a=' . $id_url;
		$limit_end = 10;
		//safe the value of the actual position
		if(!isset($_GET["pos"])){
			$ini = 1;
		}else{
			$ini = $_GET["pos"];

		}
		$init = ($ini-1) * $limit_end;

		/*-------------------------*/

		$query = $sql;
		$query .= " LIMIT $init, $limit_end";//add limit init and limit end to the query
		


		self::getConection();

		/**
		 * -Execute a new query only to count the fields in the table of BD
		 */
		$count = self::$cnx->prepare($query_count);

		$count->execute();

		$rows_count = $count->fetch();

		$counted = $rows_count[0];
		/*---------------------------------------------------------*/


		//Query of data 
		$result = self::$cnx->prepare($query);

		$result->execute();

		$rows = $result->rowCount();
		$cols = $result->columnCount();

		/**
		 * Calculate pages
		 */
		$total = ceil($counted/$limit_end);

  		/*-------------------------------------*/

		if($rows > 0){

			echo '<table id ="employee_grid" class="table table-bordered table-hover">';
			echo '<thead>
			<tr>';
			foreach(range(0, $result->columnCount() - 1) as $column_index){
				$meta[] = $result->getColumnMeta($column_index);
			}

			for ($i=0; $i < $cols; $i++){

				if($i > 7){
					//echo '<th style="visibility: hidden">' . $meta[$i]["name"] . '</td>';	
					
					//echo '<th style="display:none">' . $meta[$i]["name"] . '</td>';	
				}else{
					echo '<th>' . $meta[$i]["name"]  . '</td>';	
				}
			}       		
				//echo '<th> &nbsp; </th>';
				/*echo '<th><center>  Respuesta</center></th>';
				echo '<th><center>  Observación</center></th>';*/
			
			/*if( $i <8 ){
			}else {
				echo '<th style="visibility: hidden">Acción ' . $i . '</th>';	
			}*/


			echo '</tr>
			</thead>
			<tbody>';
		

			for($i = 0; $i < $rows; $i++){
				$data = $result->fetch();
				echo '<tr>';

				for($j = 0; $j < $cols; $j++){

					//echo '<td>' . $data[$j] .'</td>';
					// <span id="firstname1">Klvst3r</span>
					
						if($j > 7){
							
							//echo '<td style="visibility: hidden"><span id="'. $meta[$j]["name"] . $i .  '">' . $data[$j] . '</span></td>';
							echo '<td style="display:none"><span id="'. $meta[$j]["name"] . $i .  '">' . $data[$j] . '</span></td>';
						}else{
							echo '<td><span id="'. $meta[$j]["name"] . $i .  '">' . $data[$j] . '</span></td>';
						}
					

				}
				//echo '<td>' . $action . '</td>';
				//$id = $data[$i];
				$id = $data["ID"];
				$folio = $data["Folio"];

				//$detail = '<td><button type="button" class="btn btn-success detail" value="' . $i . '">Detalle <span class="glyphicon glyphicon-triangle-right"></span></button>';
				//$track = '<td><a class="btn btn-success" href="op.php?a=42&b='. $id .'&c=' . $folio . '"><span class="glyphicon glyphicon-edit"></span> Turnar</a></td>';

				//echo '<td>' . $track . '</td>';
				
				//$track = '<td><span></span> </td> <td><span></span> </td>';
				//$track = ' ';

				//echo '<td>' . $track . '</td>';
				
				echo '</tr>';


			}
			echo "</tbody></table>";

			  /*
			   * numeration of records [important]
			   */ 
				  //echo "<div class='pagination'>";
				  echo '<ul class="pagination">';
				  /****************************************/
				  if(($ini - 1) == 0)
				  {
				    echo '<li class="disabled"><a href="#">&laquo;</a></li>';
				  }
				  else
				  {
				    echo '<li><a href="'.$url.'&pos='.($ini-1).'"><b>&laquo;</b></a></li>';
				  }
				  /****************************************/
				  for($k=1; $k <= $total; $k++)
				  {
				    if($ini == $k)
				    {
				      echo '<li class="active"><a href="#""><b>'.$k.'</b></a></li>';
				      
				    }
				    else
				    {
				    	
				      echo "<li><a href='$url&pos=$k'>".$k."</a></li>";
				    }
				  }
				  /****************************************/
				  if($ini == $total)
				  {
				    echo '<li class="disabled"><a href="#">&raquo;</a></li>';
				  }
				  else
				  {
				  	echo '<li><a href="'.$url.'&pos='.($ini+1).'"><b>&raquo;</b></a></li>';

				  }
				  echo "</ul>";
				  //echo "</div>";
				  /*******************END*******************/
				 /*
			   * End numeration of records [important]
			   */ 


		}else{
			echo "No hay Documento en la Base de Datos con el filtro introducido. ";
		}

		//free memory
		self::disconnect();


}//function getTableAnswerQuery_pag

public static function getTableAnswerQuery_pag_date($query_count, $sql, $action, $date1, $date2, $dir, $reg){

		/**
		 * Vars to build pagination
		 */
		$order="id_user ASC";
		$id_url = $_GET["a"];
		$url = basename($_SERVER ["PHP_SELF"]) . '?a=' . $id_url;
		$first = $date1;
		$second = $date2;
		$dir = $dir;
		//$limit_end = 10;
		$limit_end = $reg;
		//safe the value of the actual position
		if(!isset($_GET["pos"])){
			$ini = 1;
		}else{
			$ini = $_GET["pos"];

		}
		$init = ($ini-1) * $limit_end;

		/*-------------------------*/

		$query = $sql;
		$query .= " LIMIT $init, $limit_end";//add limit init and limit end to the query
		


		self::getConection();

		/**
		 * -Execute a new query only to count the fields in the table of BD
		 */
		$count = self::$cnx->prepare($query_count);

		$count->execute();

		$rows_count = $count->fetch();

		$counted = $rows_count[0];
		/*---------------------------------------------------------*/


		//Query of data 
		$result = self::$cnx->prepare($query);

		$result->execute();

		$rows = $result->rowCount();
		$cols = $result->columnCount();

		/**
		 * Calculate pages
		 */
		$total = ceil($counted/$limit_end);

  		/*-------------------------------------*/

		if($rows > 0){

			echo '<table id ="employee_grid" class="table table-bordered table-hover">';
			echo '<thead>
			<tr>';
			
			foreach(range(0, $result->columnCount() - 1) as $column_index){
				$meta[] = $result->getColumnMeta($column_index);
			}

			for ($i=0; $i < $cols; $i++){

				if($i > 7){
					//echo '<th style="visibility: hidden">' . $meta[$i]["name"] . '</td>';	
					
					//echo '<th style="display:none">' . $meta[$i]["name"] . '</td>';	
				}else{

					echo '<th>' . $meta[$i]["name"]  . '</td>';	
				}
			}       		
				//echo '<th> &nbsp; </th>';
				/*echo '<th><center>  Respuesta</center></th>';
				echo '<th><center>  Observación</center></th>';*/
			
			/*if( $i <8 ){
			}else {
				echo '<th style="visibility: hidden">Acción ' . $i . '</th>';	
			}*/


			echo '</tr>
			</thead>
			<tbody>';
		

			for($i = 0; $i < $rows; $i++){
				$data = $result->fetch();
				echo '<tr>';

				for($j = 0; $j < $cols; $j++){

					//echo '<td>' . $data[$j] .'</td>';
					// <span id="firstname1">Klvst3r</span>
					
						if($j > 7){
							
							//echo '<td style="visibility: hidden"><span id="'. $meta[$j]["name"] . $i .  '">' . $data[$j] . '</span></td>';
							
							echo '<td style="display:none"><span id="'. $meta[$j]["name"] . $i .  '">' . $data[$j] . '</span></td>';
						}else{
							echo '<td><span id="'. $meta[$j]["name"] . $i .  '">' . $data[$j] . '</span></td>';
						}
					

				}
				//echo '<td>' . $action . '</td>';
				//$id = $data[$i];
				$id = $data["ID"];
				$folio = $data["Folio"];

				//$detail = '<td><button type="button" class="btn btn-success detail" value="' . $i . '">Detalle <span class="glyphicon glyphicon-triangle-right"></span></button>';
				//$track = '<td><a class="btn btn-success" href="op.php?a=42&b='. $id .'&c=' . $folio . '"><span class="glyphicon glyphicon-edit"></span> Turnar</a></td>';

				//echo '<td>' . $track . '</td>';
				
				//$track = '<td><span></span> </td> <td><span></span> </td>';
				//$track = ' ';

				//echo '<td>' . $track . '</td>';
				
				echo '</tr>';


			}
			echo "</tbody></table>";

			  /*
			   * numeration of records [important]
			   */ 
				  //echo "<div class='pagination'>";
				  echo '<ul class="pagination">';
				  /****************************************/
				  if(($ini - 1) == 0)
				  {
				    echo '<li class="disabled"><a href="#">&laquo;</a></li>';
				  }
				  else
				  {
				    echo '<li><a href="'.$url.'&pos='.($ini-1).'"><b>&laquo;</b></a></li>';
				  }
				  /****************************************/
				  for($k=1; $k <= $total; $k++)
				  {
				    if($ini == $k)
				    {
				      echo '<li class="active"><a href="#""><b>'.$k.'</b></a></li>';
				      
				    }
				    else
				    {
				    	
				      echo "<li><a href='$url&pos=$k&f=$first&s=$second&dir=$dir&reg=$reg'>".$k."</a></li>";
				    }
				  }
				  /****************************************/
				  if($ini == $total)
				  {
				    echo '<li class="disabled"><a href="#">&raquo;</a></li>';
				  }
				  else
				  {
				  	echo '<li><a href="'.$url.'&pos='.($ini+1).'"><b>&raquo;</b></a></li>';

				  }
				  echo "</ul>";
				  //echo "</div>";
				  /*******************END*******************/
				 /*
			   * End numeration of records [important]
			   */ 


		}else{
			echo "No hay Documento en la Base de Datos con el filtro introducido. ";
		}

		//free memory
		self::disconnect();


}//function getTableAnswerQuery_pag_date

public static function selectDir($id){

	$query = "SELECT dir_name FROM directions WHERE id_dir = :id_dir ";

	self::getConection();
	
	$result = self::$cnx->prepare($query);

	$id_dir = $id;
	$result->bindParam(":id_dir", $id_dir);

	$result->execute();

	$data = $result->fetch();	

	$dir_name = $data["dir_name"];

	self::disconnect();

	//echo $dir_name;
	return $dir_name;

}	// selectDir method




} //Class UserSQL	

?>
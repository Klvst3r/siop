<?php
include '../data/OtherConnect.php';
include '../model/Document.php';

class DocMuniSQL extends OtherConnect {

	protected static $cnx;

	private static function getConection(){

		self::$cnx = Reconnect::connection();
		
	}//getConection

	private static function disconnect(){
		//This close the conection in PDO
		self::$cnx = null;
	}// disconnect


public static function getTableDocuments_Pag($query_count, $sql, $action){

		/**
		 * Vars to build pagination
		 */
		$order="id_user ASC";
		$id_url = $_GET["a"];
		$url = basename($_SERVER ["PHP_SELF"]) . '?a=' . $id_url;
		$limit_end = 10;
		//safe the value of the actual position
		if(!isset($_GET["pos"])){
			$ini = 1;
		}else{
			$ini = $_GET["pos"];

		}
		$init = ($ini-1) * $limit_end;

		/*-------------------------*/

		$query = $sql;
		$query .= " LIMIT $init, $limit_end";//add limit init and limit end to the query
		


		self::getConection();

		/**
		 * -Execute a new query only to count the fields in the table of BD
		 */
		$count = self::$cnx->prepare($query_count);

		$count->execute();

		$rows_count = $count->fetch();

		$counted = $rows_count[0];
		/*---------------------------------------------------------*/


		//Query of data 
		$result = self::$cnx->prepare($query);

		$result->execute();

		$rows = $result->rowCount();
		$cols = $result->columnCount();

		/**
		 * Calculate pages
		 */
		$total = ceil($counted/$limit_end);

  		/*-------------------------------------*/

		if($rows > 0){

			echo '<table class="table table-striped table-hover">';
			echo '<thead>
			<tr>';
			echo '<th>No. </th>'; 
			foreach(range(0, $result->columnCount() - 1) as $column_index){
				$meta[] = $result->getColumnMeta($column_index);
			}

			for ($i=0; $i < $cols; $i++){

				if($i > 7){
					//echo '<th style="visibility: hidden">' . $meta[$i]["name"] . '</td>';	
					
					//echo '<th style="display:none">' . $meta[$i]["name"] . '</td>';	
				}else{
					echo '<th>' . $meta[$i]["name"]  . '</td>';	
				}
			}       		
				echo '<th> &nbsp; </th>';
				echo '<th><center>  Acción</center></th>';
			
			/*if( $i <8 ){
			}else {
				echo '<th style="visibility: hidden">Acción ' . $i . '</th>';	
			}*/


			echo '</tr>
			</thead>
			<tbody>';
		

			for($i = 0; $i < $rows; $i++){
				$data = $result->fetch();
				echo '<tr>';

				$n = $i + 1;
				echo '<td>' . $n . '</td>';

				for($j = 0; $j < $cols; $j++){

					//echo '<td>' . $data[$j] .'</td>';
					// <span id="firstname1">Klvst3r</span>
					
						if($j > 7){
							
							//echo '<td style="visibility: hidden"><span id="'. $meta[$j]["name"] . $i .  '">' . $data[$j] . '</span></td>';
							echo '<td style="display:none"><span id="'. $meta[$j]["name"] . $i .  '">' . $data[$j] . '</span></td>';
						}else{
							echo '<td><span id="'. $meta[$j]["name"] . $i .  '">' . $data[$j] . '</span></td>';
						}
					

				}
				//echo '<td>' . $action . '</td>';
				//$id = $data[$i];
				$id = $data["ID"];
				
				//$detail = '<a class="btn btn-success btn-sm" href="action.php?a=10&b='. $id .'">Detalles</a>';
				//$detail = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default">Detalles</button>';
				
				//$detail =  '<a onclick="accion(' . $id . ');" href="#" class="btn btn-success" data-toggle="modal" data-target="#myModalmostrar" role="button">Detalles</a>';
				
				//$detail = '<td><button type="button" class="btn btn-success edit" value="' . $id . '"><span class="glyphicon glyphicon-edit"></span> Edit</button></td>';
				$detail = '<td align="center"><button type="button" class="btn btn-primary detail" value="' . $i . '" data-toggle="tooltip" data-placement="top" title="Detalle"><span class="glyphicon glyphicon-screenshot"></span> </button>';
				//$delete = '<a class="btn btn-danger delete" href="op.php?a=21&b='. $id .'"><span class="glyphicon glyphicon-edit"></span> Eliminar</a></td>';

				//$delete = '<button class="btn btn-danger delete" onclick="confirmar(\'op.php?a=91&b=' . $id . '\')">Eliminar</button>';

				//$edit = '<a class="btn btn-primary btn-sm" href="action.php?a=23&b='. $id .'">Editar</a>';
            	//$delete = '<button class="btn btn-danger btn-sm" onclick="confirmar(action.php?a=6)">Eliminar</button>';

            	//$delete = '<button class="btn btn-danger btn-sm" onclick="confirmar(\'action.php?a=25&b=' . $id . '\')">Eliminar</button>';
				//echo '<td>' . $detail . ' ' . $delete . '</td>';
				echo '</td>';
				echo '<td>' . $detail . ' </td>';
				echo '</tr>';


			}
			echo "</tbody></table>";

			  /*
			   * numeration of records [important]
			   */ 
				  //echo "<div class='pagination'>";
				  echo '<ul class="pagination">';
				  /****************************************/
				  if(($ini - 1) == 0)
				  {
				    echo '<li class="disabled"><a href="#">&laquo;</a></li>';
				  }
				  else
				  {
				    echo '<li><a href="'.$url.'&pos='.($ini-1).'"><b>&laquo;</b></a></li>';
				  }
				  /****************************************/
				  for($k=1; $k <= $total; $k++)
				  {
				    if($ini == $k)
				    {
				      echo '<li class="active"><a href="#""><b>'.$k.'</b></a></li>';
				      
				    }
				    else
				    {
				    	
				      echo "<li><a href='$url&pos=$k'>".$k."</a></li>";
				    }
				  }
				  /****************************************/
				  if($ini == $total)
				  {
				    echo '<li class="disabled"><a href="#">&raquo;</a></li>';
				  }
				  else
				  {
				  	echo '<li><a href="'.$url.'&pos='.($ini+1).'"><b>&raquo;</b></a></li>';

				  }
				  echo "</ul>";
				  //echo "</div>";
				  /*******************END*******************/
				 /*
			   * End numeration of records [important]
			   */ 


		}else{
			echo "No existen documentos en la base de datos con el municipio seleccionado. ";
		}

		//free memory
		self::disconnect();


}//function getTableUsers
	

public static function getTableDoc($sql){
		$query = $sql;
		self::getConection();
		$result = self::$cnx->prepare($query);
		$result->execute();
		$rows = $result->rowCount();
		$cols = $result->columnCount();
		if($rows > 0){
			echo '<table class="table table-striped table-hover">';
			echo '<thead>
			<tr>';
			echo '<th>No. </th>'; 
			foreach(range(0, $result->columnCount() - 1) as $column_index){
				$meta[] = $result->getColumnMeta($column_index);
			}
			for ($i=0; $i < $cols; $i++){
				//echo '<th>' . $meta[$i]["name"] . '</td>';
				


				if($i > 8){
					//echo '<th style="visibility: hidden">' . $meta[$i]["name"] . '</td>';	
					
					//echo '<th style="display:none">' . $meta[$i]["name"] . '</td>';	
				}else{
					echo '<th>' . $meta[$i]["name"]  . '</td>';	
				}
			}
			echo '<th> &nbsp; </th>';
			echo '<th><center>  Acción</center></th>';

			echo '</tr>
			</thead>
			<tbody>';

			for($i = 0; $i < $rows; $i++){
				$data = $result->fetch();
				echo '<tr>';
				$n = $i + 1;
				echo '<td>' . $n . '</td>';
				for($j = 0; $j < $cols; $j++){
					//echo '<td>' . $data[$j] .'</td>';
					
					
					if($j > 8){
							
							//echo '<td style="visibility: hidden"><span id="'. $meta[$j]["name"] . $i .  '">' . $data[$j] . '</span></td>';
							echo '<td style="display:none"><span id="'. $meta[$j]["name"] . $i .  '">' . $data[$j] . '</span></td>';
						}else{
							echo '<td><span id="'. $meta[$j]["name"] . $i .  '">' . $data[$j] . '</span></td>';
						}
				}//for $j
				//echo '<td>' . $action . '</td>';
				//$id = $data[$i];
				$id = $data["ID"];
				$detail = '<td><button type="button" class="btn btn-warning detail" value="' . $i . '"><span class="glyphicon glyphicon-search"></span> Detalle</button>';
				//$delete = '<a class="btn btn-danger delete" href="op.php?a=21&b='. $id .'"><span class="glyphicon glyphicon-edit"></span> Eliminar</a></td>';

				$delete = '<button class="btn btn-danger delete" onclick="confirmar(\'op.php?a=91&b=' . $id . '\')">Eliminar</button>';

				//$edit = '<a class="btn btn-primary btn-sm" href="action.php?a=23&b='. $id .'">Editar</a>';
            	//$delete = '<button class="btn btn-danger btn-sm" onclick="confirmar(action.php?a=6)">Eliminar</button>';

            	//$delete = '<button class="btn btn-danger btn-sm" onclick="confirmar(\'action.php?a=25&b=' . $id . '\')">Eliminar</button>';
				echo '<td>' . $detail . ' ' . $delete . '</td>';
				echo '</tr>';
			}
			echo "</tbody></table><br/>";
		}else{

			echo '
			<section class="content-header">
			<center>
				No hay Documento en la Base de Datos con el municipio seleccionado. 
			</center>
			</section>				
			';

			echo"<meta HTTP-EQUIV='Refresh' CONTENT='3; URL=index.php'<head/>";
		}
		//free memory
		self::disconnect();
}//function getTableUsers

public static function getMunicipality($id_muni){




	$query = "SELECT desc_muni from municipalities WHERE id_muni = :id_muni";

	self::getConection();
	
	$result = self::$cnx->prepare($query);

	$result->bindParam(":id_muni", $id_muni);

	$result->execute();

	if($result->rowCount() > 0){

		$rows = $result->fetch();

		$doc = new Document();

		//echo $rows["desc_muni"];
		
		$doc->setDesc_muni($rows["desc_muni"]);

		self::disconnect();

		
		return $doc;



	}




}//method getMunicipality

public static function getTableDocUpdate_Pag($query_count, $sql, $action, $muni, $reg){

		/**
		 * Vars to build pagination
		 */
		$order="id_user ASC";
		$id_url = $_GET["a"];
		$url = basename($_SERVER ["PHP_SELF"]) . '?a=' . $id_url;

		$muni = $muni;

		//$limit_end = 10;
		$limit_end = $reg;
		//safe the value of the actual position
		if(!isset($_GET["pos"])){
			$ini = 1;
		}else{
			$ini = $_GET["pos"];

		}
		$init = ($ini-1) * $limit_end;

		/*-------------------------*/

		$query = $sql;
		$query .= " LIMIT $init, $limit_end";//add limit init and limit end to the query
		


		self::getConection();

		/**
		 * -Execute a new query only to count the fields in the table of BD
		 */
		$count = self::$cnx->prepare($query_count);

		$count->execute();

		$rows_count = $count->fetch();

		$counted = $rows_count[0];
		/*---------------------------------------------------------*/


		//Query of data 
		$result = self::$cnx->prepare($query);

		$result->execute();

		$rows = $result->rowCount();
		$cols = $result->columnCount();

		/**
		 * Calculate pages
		 */
		$total = ceil($counted/$limit_end);

  		/*-------------------------------------*/

		if($rows > 0){

			echo '<table class="table table-striped table-hover">';
			echo '<thead>
			<tr>';
			foreach(range(0, $result->columnCount() - 1) as $column_index){
				$meta[] = $result->getColumnMeta($column_index);
			}

			for ($i=0; $i < $cols; $i++){

				if($i > 7){
					//echo '<th style="visibility: hidden">' . $meta[$i]["name"] . '</td>';	
					
					//echo '<th style="display:none">' . $meta[$i]["name"] . '</td>';	
				}else{
					echo '<th>' . $meta[$i]["name"]  . '</td>';	
				}
			}       		
				echo '<th> &nbsp; </th>';
				echo '<th><center>  Acción</center></th>';
			
			/*if( $i <8 ){
			}else {
				echo '<th style="visibility: hidden">Acción ' . $i . '</th>';	
			}*/


			echo '</tr>
			</thead>
			<tbody>';
		

			for($i = 0; $i < $rows; $i++){
				$data = $result->fetch();
				echo '<tr>';

				for($j = 0; $j < $cols; $j++){

					//echo '<td>' . $data[$j] .'</td>';
					// <span id="firstname1">Klvst3r</span>
					
						if($j > 7){
							
							//echo '<td style="visibility: hidden"><span id="'. $meta[$j]["name"] . $i .  '">' . $data[$j] . '</span></td>';
							echo '<td style="display:none"><span id="'. $meta[$j]["name"] . $i .  '">' . $data[$j] . '</span></td>';
						}else{
							echo '<td><span id="'. $meta[$j]["name"] . $i .  '">' . $data[$j] . '</span></td>';
						}
					

				}
				//echo '<td>' . $action . '</td>';
				//$id = $data[$i];
				$id = $data["ID"];
				
				//$detail = '<a class="btn btn-success btn-sm" href="action.php?a=10&b='. $id .'">Detalles</a>';
				//$detail = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default">Detalles</button>';
				
				//$detail =  '<a onclick="accion(' . $id . ');" href="#" class="btn btn-success" data-toggle="modal" data-target="#myModalmostrar" role="button">Detalles</a>';
				
				//$detail = '<td><button type="button" class="btn btn-success edit" value="' . $id . '"><span class="glyphicon glyphicon-edit"></span> Edit</button></td>';
				$detail = '<td style="width:120px"><button type="button" class="btn btn-primary detail" value="' . $i . '" data-toggle="tooltip" data-placement="top" title="Detalle"><span class="glyphicon glyphicon-bookmark"></span> </button>';
				//$edit = '<td><button type="button" class="btn btn-success edit" value="' . $i . '"><span class="glyphicon glyphicon-edit"></span> Detalle</button></td>';
				$edit = '<a class="btn btn-warning edit" href="op.php?a=21&b='. $id .'" data-toggle="tooltip" data-placement="top" title="Editar"><span class="glyphicon glyphicon-edit"></span> </a></td>';
				

				//$edit = '<a class="btn btn-primary btn-sm" href="action.php?a=11&b='. $id .'">Editar</a>';
				//$edit = '';
            	//$delete = '<button class="btn btn-danger btn-sm" onclick="confirmar(action.php?a=6)">Eliminar</button>';
            	
            	//$delete = '<button class="btn btn-danger btn-sm" onclick="confirmar(\'action.php?a=21&b=' . $id . '\')">Eliminar</button>';
            	$delete = '';
				echo '<td>' . $detail . ' ' . $edit . ' ' . $delete . '</td>';
				echo '</tr>';


			}
			echo "</tbody></table>";

			  /*
			   * numeration of records [important]
			   */ 
				  //echo "<div class='pagination'>";
				  echo '<ul class="pagination">';
				  /****************************************/
				  if(($ini - 1) == 0)
				  {
				    echo '<li class="disabled"><a href="#">&laquo;</a></li>';
				  }
				  else
				  {
				    echo '<li><a href="'.$url.'&pos='.($ini-1).'"><b>&laquo;</b></a></li>';
				  }
				  /****************************************/
				  for($k=1; $k <= $total; $k++)
				  {
				    if($ini == $k)
				    {
				      echo '<li class="active"><a href="#""><b>'.$k.'</b></a></li>';
				      
				    }
				    else
				    {
				    	
				      echo "<li><a href='$url&pos=$k&muni=$muni&reg=$reg'>".$k."</a></li>";
				    }
				  }
				  /****************************************/
				  if($ini == $total)
				  {
				    echo '<li class="disabled"><a href="#">&raquo;</a></li>';
				  }
				  else
				  {
				  	echo '<li><a href="'.$url.'&pos='.($ini+1).'"><b>&raquo;</b></a></li>';

				  }
				  echo "</ul>";
				  //echo "</div>";
				  /*******************END*******************/
				 /*
			   * End numeration of records [important]
			   */ 


		}else{
			echo "No hay Documento en la Base de Datos con el folio introducido. ";
		}

		//free memory
		self::disconnect();


}//function getTableDocUpdate_Pag



} //Class UserSQL	

?>
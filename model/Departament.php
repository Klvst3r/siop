<?php
	//variables names accord to the cols in the table, definded as properties from our class

	/*
	 * Setters y Getters Created by http://mikeangstadt.name/projects/getter-setter-gen/
	 */
	//Private Propieties

class Departament {

	private $id_dep;
	private $departament;
	private $status;

	public function getId_dep(){
		return $this->id_dep;
	}

	public function setId_dep($id_dep){
		$this->id_dep = $id_dep;
	}

	public function getDepartament(){
		return $this->departament;
	}

	public function setDepartament($departament){
		$this->departament = $departament;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setStatus($status){
		$this->status = $status;
	}


}// class Departament

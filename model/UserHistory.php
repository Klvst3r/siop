<?php

	/*
	Php getter and setters generator a falta de plugin pegando la clase en:
	http://mikeangstadt.name/projects/getter-setter-gen/
	To Generate y establish
	*/

class UserHistory {
	//variables names accord to the cols in the table, definded as properties from our class
	private $id_user;
	private $id_priv;
	private $desc_priv;
    private $create_priv;
    private $select_priv;
    private $insert_priv;
    private $update_priv;
    private $delete_priv;
    private $backup_priv;
    private $report_priv;
	private $id_user_status;
	private $id_dep;
	private $id_history;
	private $name;
	private $user_name;
	private $user_pass;
	private $new_pass;
	private $rewrite_pass;
	private $user_movil;
	private $user_email;
	private $user_photo;
	private $user_position;
	private $user_tel;
	private $user_ext;
	private $active;
	private $user_online;
	private $date_reg;
	private $about;

	public function getId_user(){
		return $this->id_user;
	}

	public function setId_user($id_user){
		$this->id_user = $id_user;
	}

	public function getId_priv(){
		return $this->id_priv;
	}

	public function setId_priv($id_priv){
		$this->id_priv = $id_priv;
	}
    public function getDesc_priv(){
		return $this->desc_priv;
	}

	public function setDesc_priv($desc_priv){
		$this->desc_priv = $desc_priv;
	}
    
    public function getCreate_priv(){
		return $this->create_priv;
	}

	public function setCreate_priv($create_priv){
		$this->create_priv = $create_priv;
	}

	public function getSelect_priv(){
		return $this->select_priv;
	}

	public function setSelect_priv($select_priv){
		$this->select_priv = $select_priv;
	}

	public function getInsert_priv(){
		return $this->insert_priv;
	}

	public function setInsert_priv($insert_priv){
		$this->insert_priv = $insert_priv;
	}

	public function getUpdate_priv(){
		return $this->update_priv;
	}

	public function setUpdate_priv($update_priv){
		$this->update_priv = $update_priv;
	}

	public function getDelete_priv(){
		return $this->delete_priv;
	}

	public function setDelete_priv($delete_priv){
		$this->delete_priv = $delete_priv;
	}

	public function getBackup_priv(){
		return $this->backup_priv;
	}

	public function setBackup_priv($backup_priv){
		$this->backup_priv = $backup_priv;
	}

	public function getReport_priv(){
		return $this->report_priv;
	}

	public function setReport_priv($report_priv){
		$this->report_priv = $report_priv;
	}

	public function getId_user_status(){
		return $this->id_user_status;
	}

	public function setId_user_status($id_user_status){
		$this->id_user_status = $id_user_status;
	}

	public function getDirection(){
		return $this->direction;
	}

	public function setDirection($direction){
		$this->direction = $direction;
	}

	public function getDepartament(){
		return $this->departament;
	}

	public function setDepartament($departament){
		$this->departament = $departament;
	}

	public function getId_dep(){
		return $this->id_dep;
	}

	public function setId_dep($id_dep){
		$this->id_dep = $id_dep;
	}

	public function getId_history(){
		return $this->id_history;
	}

	public function setId_history($id_history){
		$this->id_history = $id_history;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getUser_name(){
		return $this->user_name;
	}

	public function setUser_name($user_name){
		$this->user_name = $user_name;
	}

	public function getUser_pass(){
		return $this->user_pass;
	}

	public function setUser_pass($user_pass){
		$this->user_pass = md5($user_pass);
	}

	public function getNew_pass(){
		return $this->new_pass;
	}

	public function setNew_pass($new_pass){
		$this->new_pass = $new_pass;
	}

	public function getRewrite_pass(){
		return $this->rewrite_pass;
	}

	public function setRewrite_pass($rewrite_pass){
		$this->rewrite_pass = $rewrite_pass;
	}

	public function getUser_movil(){
		return $this->user_movil;
	}

	public function setUser_movil($user_movil){
		$this->user_movil = $user_movil;
	}

	public function getUser_email(){
		return $this->user_email;
	}

	public function setUser_email($user_email){
		$this->user_email = $user_email;
	}

	public function getUser_photo(){
		return $this->user_photo;
	}

	public function setUser_photo($user_photo){
		$this->user_photo = $user_photo;
	}

	public function getUser_position(){
		return $this->user_position;
	}

	public function setUser_position($user_position){
		$this->user_position = $user_position;
	}

	public function getUser_tel(){
		return $this->user_tel;
	}

	public function setUser_tel($user_tel){
		$this->user_tel = $user_tel;
	}

	public function getUser_ext(){
		return $this->user_ext;
	}

	public function setUser_ext($user_ext){
		$this->user_ext = $user_ext;
	}

	public function getActive(){
		return $this->active;
	}

	public function setActive($active){
		$this->active = $active;
	}

	public function getUser_online(){
		return $this->user_online;
	}

	public function setUser_online($user_online){
		$this->user_online = $user_online;
	}

	public function getDate_reg(){
		return $this->date_reg;
	}

	public function setDate_reg($date_reg){
		$this->date_reg = $date_reg;
	}

	public function getAbout(){
		return $this->about;
	}

	public function setAbout($about){
		$this->about = $about;
	}

}//Class User

?>
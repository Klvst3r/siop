<?php

Class HeadUser{

	private $id_user;
	private $user_photo;
	private $name;
	private $user_name;
	private $user_resume;
	private $desc_priv;

	public function getId_user(){
		return $this->id_user;
	}

	public function setId_user($id_user){
		$this->id_user = $id_user;
	}

	public function getUser_photo(){
		return $this->user_photo;
	}

	public function setUser_photo($user_photo){
		$this->user_photo = $user_photo;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getUser_name(){
		return $this->user_name;
	}

	public function setUser_name($user_name){
		$this->user_name = $user_name;
	}

	public function getUser_resume(){
		return $this->user_resume;
	}

	public function setUser_resume($user_resume){
		$this->user_resume = $user_resume;
	}

	public function getDesc_priv(){
		return $this->desc_priv;
	}

	public function setDesc_priv($desc_priv){
		$this->desc_priv = $desc_priv;
	}



}//Class HeadUser

?>

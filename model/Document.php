<?php
	/*
	http://mikeangstadt.name/projects/getter-setter-gen/
	*/

class Document {

	private $id_doc;
	private $id_destiny;

	private $desc_destiny;
	private $destiny_position;

	private $id_muni;
	private $desc_muni;

	private $id_type_doc;
	private $desc_type_doc;

	private $id_status_doc;
	private $desc_status_doc;

	private $id_classif;
	private $desc_classif;

	private $subject_doc;
	private $desc_send_position;
	private $folio_doc;
	private $send_doc;
	private $origin_doc;
	private $dir_sender;
	private $movil_doc;
	private $tel_doc;
	private $ext_doc;
	private $email_doc;
	private $date_doc;
	private $date_recep;
	private $reference;

	private $status;

	private $answer;
	private $observation;

	public function getId_doc(){
		return $this->id_doc;
	}

	public function setId_doc($id_doc){
		$this->id_doc = $id_doc;
	}

	public function getId_destiny(){
		return $this->id_destiny;
	}

	public function setId_destiny($id_destiny){
		$this->id_destiny = $id_destiny;
	}

	public function getDesc_destiny(){
		return $this->desc_destiny;
	}

	public function setDesc_destiny($desc_destiny){
		$this->desc_destiny = $desc_destiny;
	}

	public function getDestiny_position(){
		return $this->destiny_position;
	}

	public function setDestiny_position($destiny_position){
		$this->destiny_position = $destiny_position;
	}

	public function getId_muni(){
		return $this->id_muni;
	}

	public function setId_muni($id_muni){
		$this->id_muni = $id_muni;
	}

	public function getDesc_muni(){
		return $this->desc_muni;
	}

	public function setDesc_muni($desc_muni){
		$this->desc_muni = $desc_muni;
	}

	public function getId_type_doc(){
		return $this->id_type_doc;
	}

	public function setId_type_doc($id_type_doc){
		$this->id_type_doc = $id_type_doc;
	}

	public function getDesc_type_doc(){
		return $this->desc_type_doc;
	}

	public function setDesc_type_doc($desc_type_doc){
		$this->desc_type_doc = $desc_type_doc;
	}

	public function getId_status_doc(){
		return $this->id_status_doc;
	}

	public function setId_status_doc($id_status_doc){
		$this->id_status_doc = $id_status_doc;
	}

	public function getDesc_status_doc(){
		return $this->desc_status_doc;
	}

	public function setDesc_status_doc($desc_status_doc){
		$this->desc_status_doc = $desc_status_doc;
	}

	public function getId_classif(){
		return $this->id_classif;
	}

	public function setId_classif($id_classif){
		$this->id_classif = $id_classif;
	}

	public function getDesc_classif(){
		return $this->desc_classif;
	}

	public function setDesc_classif($desc_classif){
		$this->desc_classif = $desc_classif;
	}

	public function getSubject_doc(){
		return $this->subject_doc;
	}

	public function setSubject_doc($subject_doc){
		$this->subject_doc = $subject_doc;
	}

	public function getDesc_send_position(){
		return $this->desc_send_position;
	}

	public function setDesc_send_position($desc_send_position){
		$this->desc_send_position = $desc_send_position;
	}

	public function getFolio_doc(){
		return $this->folio_doc;
	}

	public function setFolio_doc($folio_doc){
		$this->folio_doc = $folio_doc;
	}

	public function getSend_doc(){
		return $this->send_doc;
	}

	public function setSend_doc($send_doc){
		$this->send_doc = $send_doc;
	}

	public function getOrigin_doc(){
		return $this->origin_doc;
	}

	public function setOrigin_doc($origin_doc){
		$this->origin_doc = $origin_doc;
	}

	public function getDir_sender(){
		return $this->dir_sender;
	}

	public function setDir_sender($dir_sender){
		$this->dir_sender = $dir_sender;
	}

	public function getMovil_doc(){
		return $this->movil_doc;
	}

	public function setMovil_doc($movil_doc){
		$this->movil_doc = $movil_doc;
	}

	public function getTel_doc(){
		return $this->tel_doc;
	}

	public function setTel_doc($tel_doc){
		$this->tel_doc = $tel_doc;
	}

	public function getExt_doc(){
		return $this->ext_doc;
	}

	public function setExt_doc($ext_doc){
		$this->ext_doc = $ext_doc;
	}

	public function getEmail_doc(){
		return $this->email_doc;
	}

	public function setEmail_doc($email_doc){
		$this->email_doc = $email_doc;
	}

	public function getDate_doc(){
		return $this->date_doc;
	}

	public function setDate_doc($date_doc){
		$this->date_doc = $date_doc;
	}

	public function getDate_recep(){
		return $this->date_recep;
	}

	public function setDate_recep($date_recep){
		$this->date_recep = $date_recep;
	}

	public function getReference(){
		return $this->reference;
	}

	public function setReference($reference){
		$this->reference = $reference;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setStatus($status){
		$this->status = $status;
	}

	public function getAnswer(){
		return $this->answer;
	}

	public function setAnswer($answer){
		$this->answer = $answer;
	}

	public function getObservation(){
		return $this->observation;
	}

	public function setObservation($observation){
		$this->observation = $observation;
	}

}

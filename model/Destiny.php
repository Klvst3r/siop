<?php
/*
	 * Setters y Getters Created by http://mikeangstadt.name/projects/getter-setter-gen/
	 */

class Destiny {	
	private $id_destiny;
	private $desc_destiny;
	private $destiny_position;

	public function getId_destiny(){
		return $this->id_destiny;
	}

	public function setId_destiny($id_destiny){
		$this->id_destiny = $id_destiny;
	}

	public function getDesc_destiny(){
		return $this->desc_destiny;
	}

	public function setDesc_destiny($desc_destiny){
		$this->desc_destiny = $desc_destiny;
	}

	public function getDestiny_position(){
		return $this->destiny_position;
	}

	public function setDestiny_position($destiny_position){
		$this->destiny_position = $destiny_position;
	}


}// Model Destiny

?>
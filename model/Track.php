<?php
	/*
	http://mikeangstadt.name/projects/getter-setter-gen/
	*/

class Track {
	private $id_turn;
	private $id_doc;
	private $id_dir;
	private $id_status_doc;
	private $id_dep;
	private $id_char;
	private $id_classif;
	private $id_desc;
	private $folio_turned;
	private $doc_origin;
	private $doc_cpp;
	private $date_turned;
	private $instructions;

	private $folio_doc;

	private $id_destiny;
	private $desc_destiny;
	private $destiny_position;

	private $subject_doc;
	private $send_doc;
	private $origin_doc;
	private $date_doc;
	private $date_recep;

	private $dir_name;

	private $departament;

	private $answer;
	private $observation;




	public function getId_turn(){
		return $this->id_turn;
	}

	public function setId_turn($id_turn){
		$this->id_turn = $id_turn;
	}

	public function getId_doc(){
		return $this->id_doc;
	}

	public function setId_doc($id_doc){
		$this->id_doc = $id_doc;
	}

	public function getId_dir(){
		return $this->id_dir;
	}

	public function setId_dir($id_dir){
		$this->id_dir = $id_dir;
	}

	public function getId_status_doc(){
		return $this->id_status_doc;
	}

	public function setId_status_doc($id_status_doc){
		$this->id_status_doc = $id_status_doc;
	}

	public function getId_dep(){
		return $this->id_dep;
	}

	public function setId_dep($id_dep){
		$this->id_dep = $id_dep;
	}

	public function getId_char(){
		return $this->id_char;
	}

	public function setId_char($id_char){
		$this->id_char = $id_char;
	}

	public function getId_classif(){
		return $this->id_classif;
	}

	public function setId_classif($id_classif){
		$this->id_classif = $id_classif;
	}

	public function getId_desc(){
		return $this->id_desc;
	}

	public function setId_desc($id_desc){
		$this->id_desc = $id_desc;
	}

	public function getFolio_turned(){
		return $this->folio_turned;
	}

	public function setFolio_turned($folio_turned){
		$this->folio_turned = $folio_turned;
	}

	public function getDoc_origin(){
		return $this->doc_origin;
	}

	public function setDoc_origin($doc_origin){
		$this->doc_origin = $doc_origin;
	}

	public function getDoc_cpp(){
		return $this->doc_cpp;
	}

	public function setDoc_cpp($doc_cpp){
		$this->doc_cpp = $doc_cpp;
	}

	public function getDate_turned(){
		return $this->date_turned;
	}

	public function setDate_turned($date_turned){
		$this->date_turned = $date_turned;
	}

	public function getInstructions(){
		return $this->instructions;
	}

	public function setInstructions($instructions){
		$this->instructions = $instructions;
	}





	public function getFolio_doc(){
		return $this->folio_doc;
	}

	public function setFolio_doc($folio_doc){
		$this->folio_doc = $folio_doc;
	}

	public function getId_destiny(){
		return $this->id_destiny;
	}

	public function setId_destiny($id_destiny){
		$this->id_destiny = $id_destiny;
	}

	public function getDesc_destiny(){
		return $this->desc_destiny;
	}

	public function setDesc_destiny($desc_destiny){
		$this->desc_destiny = $desc_destiny;
	}

	public function getDestiny_position(){
		return $this->destiny_position;
	}

	public function setDestiny_position($destiny_position){
		$this->destiny_position = $destiny_position;
	}

	public function getSubject_doc(){
		return $this->subject_doc;
	}

	public function setSubject_doc($subject_doc){
		$this->subject_doc = $subject_doc;
	}

	public function getSend_doc(){
		return $this->send_doc;
	}

	public function setSend_doc($send_doc){
		$this->send_doc = $send_doc;
	}

	public function getOrigin_doc(){
		return $this->origin_doc;
	}

	public function setOrigin_doc($origin_doc){
		$this->origin_doc = $origin_doc;
	}

	public function getDate_doc(){
		return $this->date_doc;
	}

	public function setDate_doc($date_doc){
		$this->date_doc = $date_doc;
	}

	public function getDate_recep(){
		return $this->date_recep;
	}

	public function setDate_recep($date_recep){
		$this->date_recep = $date_recep;
	}

	public function getDir_name(){
		return $this->dir_name;
	}

	public function setDir_name($dir_name){
		$this->dir_name = $dir_name;
	}

	public function getDepartament(){
		return $this->departament;
	}

	public function setDepartament($departament){
		$this->departament = $departament;
	}

	public function getAnswer(){
		return $this->answer;
	}

	public function setAnswer($answer){
		$this->answer = $answer;
	}

	public function getObservation(){
		return $this->observation;
	}

	public function setObservation($observation){
		$this->observation = $observation;
	}



}

?>
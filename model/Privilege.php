<?php
	//variables names accord to the cols in the table, definded as properties from our class

	/*
	 * Setters y Getters Created by http://mikeangstadt.name/projects/getter-setter-gen/
	 */
	//Private Propieties

class Privilege {

	private $id_priv;
	private $desc_priv;
	private $create_priv;
	private $select_priv;
	private $insert_priv;
	private $update_priv;
	private $delete_priv;
	private $backup_priv;
	private $report_priv;
	private $records;

	public function getId_priv(){
		return $this->id_priv;
	}

	public function setId_priv($id_priv){
		$this->id_priv = $id_priv;
	}

	public function getDesc_priv(){
		return $this->desc_priv;
	}

	public function setDesc_priv($desc_priv){
		$this->desc_priv = $desc_priv;
	}

	public function getCreate_priv(){
		return $this->create_priv;
	}

	public function setCreate_priv($create_priv){
		$this->create_priv = $create_priv;
	}

	public function getSelect_priv(){
		return $this->select_priv;
	}

	public function setSelect_priv($select_priv){
		$this->select_priv = $select_priv;
	}

	public function getInsert_priv(){
		return $this->insert_priv;
	}

	public function setInsert_priv($insert_priv){
		$this->insert_priv = $insert_priv;
	}

	public function getUpdate_priv(){
		return $this->update_priv;
	}

	public function setUpdate_priv($update_priv){
		$this->update_priv = $update_priv;
	}

	public function getDelete_priv(){
		return $this->delete_priv;
	}

	public function setDelete_priv($delete_priv){
		$this->delete_priv = $delete_priv;
	}

	public function getBackup_priv(){
		return $this->backup_priv;
	}

	public function setBackup_priv($backup_priv){
		$this->backup_priv = $backup_priv;
	}

	public function getReport_priv(){
		return $this->report_priv;
	}

	public function setReport_priv($report_priv){
		$this->report_priv = $report_priv;
	}

	public function getRecords(){
		return $this->records;
	}

	public function setRecords($records){
		$this->records = $records;
	}
}

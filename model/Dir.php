<?php
	//variables names accord to the cols in the table, definded as properties from our class

	/*
	 * Setters y Getters Created by http://mikeangstadt.name/projects/getter-setter-gen/
	 */
	//Private Propieties

class Dir {

	private $id_dir;
	private $dir_name;
	private $status;

	public function getId_dir(){
		return $this->id_dir;
	}

	public function setId_dir($id_dir){
		$this->id_dir = $id_dir;
	}

	public function getDir_name(){
		return $this->dir_name;
	}

	public function setDir_name($dir_name){
		$this->dir_name = $dir_name;
	}
	public function getStatus(){
		return $this->status;
	}

	public function setStatus($status){
		$this->status = $status;
	}

}// class Dir

<?php

/*
	 * Setters y Getters Created by http://mikeangstadt.name/projects/getter-setter-gen/
	 */
	//Private Propieties
class Organization {

	private $id_org;
	private $org_nick;
	private $org_name;
	private $org_dir;
	private $org_pob;
	private $org_cp;
	private $org_tel;
	private $org_ext;
	private $org_email;
	private $id_user;

	public function getId_org(){
		return $this->id_org;
	}

	public function setId_org($id_org){
		$this->id_org = $id_org;
	}

	public function getOrg_nick(){
		return $this->org_nick;
	}

	public function setOrg_nick($org_nick){
		$this->org_nick = $org_nick;
	}

	public function getOrg_name(){
		return $this->org_name;
	}

	public function setOrg_name($org_name){
		$this->org_name = $org_name;
	}

	public function getOrg_dir(){
		return $this->org_dir;
	}

	public function setOrg_dir($org_dir){
		$this->org_dir = $org_dir;
	}

	public function getOrg_pob(){
		return $this->org_pob;
	}

	public function setOrg_pob($org_pob){
		$this->org_pob = $org_pob;
	}

	public function getOrg_cp(){
		return $this->org_cp;
	}

	public function setOrg_cp($org_cp){
		$this->org_cp = $org_cp;
	}

	public function getOrg_tel(){
		return $this->org_tel;
	}

	public function setOrg_tel($org_tel){
		$this->org_tel = $org_tel;
	}

	public function getOrg_ext(){
		return $this->org_ext;
	}

	public function setOrg_ext($org_ext){
		$this->org_ext = $org_ext;
	}

	public function getOrg_email(){
		return $this->org_email;
	}

	public function setOrg_email($org_email){
		$this->org_email = $org_email;
	}

	public function getId_user(){
		return $this->id_user;
	}

	public function setId_user($id_user){
		$this->id_user = $id_user;
	}

}
	

?>